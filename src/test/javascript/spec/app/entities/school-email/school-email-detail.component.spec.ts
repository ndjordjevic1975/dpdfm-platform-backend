/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BusRidePlatformBackendTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { SchoolEmailDetailComponent } from '../../../../../../main/webapp/app/entities/school-email/school-email-detail.component';
import { SchoolEmailService } from '../../../../../../main/webapp/app/entities/school-email/school-email.service';
import { SchoolEmail } from '../../../../../../main/webapp/app/entities/school-email/school-email.model';

describe('Component Tests', () => {

    describe('SchoolEmail Management Detail Component', () => {
        let comp: SchoolEmailDetailComponent;
        let fixture: ComponentFixture<SchoolEmailDetailComponent>;
        let service: SchoolEmailService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BusRidePlatformBackendTestModule],
                declarations: [SchoolEmailDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    SchoolEmailService,
                    JhiEventManager
                ]
            }).overrideTemplate(SchoolEmailDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SchoolEmailDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SchoolEmailService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new SchoolEmail(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.schoolEmail).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
