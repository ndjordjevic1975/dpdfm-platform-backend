/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BusRidePlatformBackendTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { WhiteLabelDetailComponent } from '../../../../../../main/webapp/app/entities/white-label/white-label-detail.component';
import { WhiteLabelService } from '../../../../../../main/webapp/app/entities/white-label/white-label.service';
import { WhiteLabel } from '../../../../../../main/webapp/app/entities/white-label/white-label.model';

describe('Component Tests', () => {

    describe('WhiteLabel Management Detail Component', () => {
        let comp: WhiteLabelDetailComponent;
        let fixture: ComponentFixture<WhiteLabelDetailComponent>;
        let service: WhiteLabelService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BusRidePlatformBackendTestModule],
                declarations: [WhiteLabelDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    WhiteLabelService,
                    JhiEventManager
                ]
            }).overrideTemplate(WhiteLabelDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(WhiteLabelDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(WhiteLabelService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new WhiteLabel(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.whiteLabel).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
