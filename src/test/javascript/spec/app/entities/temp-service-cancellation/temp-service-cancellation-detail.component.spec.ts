/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BusRidePlatformBackendTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TempServiceCancellationDetailComponent } from '../../../../../../main/webapp/app/entities/temp-service-cancellation/temp-service-cancellation-detail.component';
import { TempServiceCancellationService } from '../../../../../../main/webapp/app/entities/temp-service-cancellation/temp-service-cancellation.service';
import { TempServiceCancellation } from '../../../../../../main/webapp/app/entities/temp-service-cancellation/temp-service-cancellation.model';

describe('Component Tests', () => {

    describe('TempServiceCancellation Management Detail Component', () => {
        let comp: TempServiceCancellationDetailComponent;
        let fixture: ComponentFixture<TempServiceCancellationDetailComponent>;
        let service: TempServiceCancellationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BusRidePlatformBackendTestModule],
                declarations: [TempServiceCancellationDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TempServiceCancellationService,
                    JhiEventManager
                ]
            }).overrideTemplate(TempServiceCancellationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TempServiceCancellationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TempServiceCancellationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TempServiceCancellation(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.tempServiceCancellation).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
