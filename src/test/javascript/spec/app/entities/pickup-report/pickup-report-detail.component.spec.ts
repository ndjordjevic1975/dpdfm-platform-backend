/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BusRidePlatformBackendTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PickupReportDetailComponent } from '../../../../../../main/webapp/app/entities/pickup-report/pickup-report-detail.component';
import { PickupReportService } from '../../../../../../main/webapp/app/entities/pickup-report/pickup-report.service';
import { PickupReport } from '../../../../../../main/webapp/app/entities/pickup-report/pickup-report.model';

describe('Component Tests', () => {

    describe('PickupReport Management Detail Component', () => {
        let comp: PickupReportDetailComponent;
        let fixture: ComponentFixture<PickupReportDetailComponent>;
        let service: PickupReportService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BusRidePlatformBackendTestModule],
                declarations: [PickupReportDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PickupReportService,
                    JhiEventManager
                ]
            }).overrideTemplate(PickupReportDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PickupReportDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PickupReportService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PickupReport(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.pickupReport).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
