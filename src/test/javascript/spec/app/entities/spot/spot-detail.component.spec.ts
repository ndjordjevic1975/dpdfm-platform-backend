/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BusRidePlatformBackendTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { SpotDetailComponent } from '../../../../../../main/webapp/app/entities/spot/spot-detail.component';
import { SpotService } from '../../../../../../main/webapp/app/entities/spot/spot.service';
import { Spot } from '../../../../../../main/webapp/app/entities/spot/spot.model';

describe('Component Tests', () => {

    describe('Spot Management Detail Component', () => {
        let comp: SpotDetailComponent;
        let fixture: ComponentFixture<SpotDetailComponent>;
        let service: SpotService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BusRidePlatformBackendTestModule],
                declarations: [SpotDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    SpotService,
                    JhiEventManager
                ]
            }).overrideTemplate(SpotDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SpotDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SpotService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Spot(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.spot).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
