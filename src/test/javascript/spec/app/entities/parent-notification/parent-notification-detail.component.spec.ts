/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BusRidePlatformBackendTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ParentNotificationDetailComponent } from '../../../../../../main/webapp/app/entities/parent-notification/parent-notification-detail.component';
import { ParentNotificationService } from '../../../../../../main/webapp/app/entities/parent-notification/parent-notification.service';
import { ParentNotification } from '../../../../../../main/webapp/app/entities/parent-notification/parent-notification.model';

describe('Component Tests', () => {

    describe('ParentNotification Management Detail Component', () => {
        let comp: ParentNotificationDetailComponent;
        let fixture: ComponentFixture<ParentNotificationDetailComponent>;
        let service: ParentNotificationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BusRidePlatformBackendTestModule],
                declarations: [ParentNotificationDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ParentNotificationService,
                    JhiEventManager
                ]
            }).overrideTemplate(ParentNotificationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ParentNotificationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ParentNotificationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ParentNotification(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.parentNotification).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
