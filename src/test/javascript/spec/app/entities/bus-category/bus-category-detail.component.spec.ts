/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BusRidePlatformBackendTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { BusCategoryDetailComponent } from '../../../../../../main/webapp/app/entities/bus-category/bus-category-detail.component';
import { BusCategoryService } from '../../../../../../main/webapp/app/entities/bus-category/bus-category.service';
import { BusCategory } from '../../../../../../main/webapp/app/entities/bus-category/bus-category.model';

describe('Component Tests', () => {

    describe('BusCategory Management Detail Component', () => {
        let comp: BusCategoryDetailComponent;
        let fixture: ComponentFixture<BusCategoryDetailComponent>;
        let service: BusCategoryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BusRidePlatformBackendTestModule],
                declarations: [BusCategoryDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    BusCategoryService,
                    JhiEventManager
                ]
            }).overrideTemplate(BusCategoryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BusCategoryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BusCategoryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new BusCategory(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.busCategory).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
