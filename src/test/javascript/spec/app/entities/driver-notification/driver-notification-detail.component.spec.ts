/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BusRidePlatformBackendTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DriverNotificationDetailComponent } from '../../../../../../main/webapp/app/entities/driver-notification/driver-notification-detail.component';
import { DriverNotificationService } from '../../../../../../main/webapp/app/entities/driver-notification/driver-notification.service';
import { DriverNotification } from '../../../../../../main/webapp/app/entities/driver-notification/driver-notification.model';

describe('Component Tests', () => {

    describe('DriverNotification Management Detail Component', () => {
        let comp: DriverNotificationDetailComponent;
        let fixture: ComponentFixture<DriverNotificationDetailComponent>;
        let service: DriverNotificationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BusRidePlatformBackendTestModule],
                declarations: [DriverNotificationDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DriverNotificationService,
                    JhiEventManager
                ]
            }).overrideTemplate(DriverNotificationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DriverNotificationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DriverNotificationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new DriverNotification(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.driverNotification).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
