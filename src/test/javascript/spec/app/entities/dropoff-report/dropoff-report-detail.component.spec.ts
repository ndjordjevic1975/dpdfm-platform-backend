/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BusRidePlatformBackendTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DropoffReportDetailComponent } from '../../../../../../main/webapp/app/entities/dropoff-report/dropoff-report-detail.component';
import { DropoffReportService } from '../../../../../../main/webapp/app/entities/dropoff-report/dropoff-report.service';
import { DropoffReport } from '../../../../../../main/webapp/app/entities/dropoff-report/dropoff-report.model';

describe('Component Tests', () => {

    describe('DropoffReport Management Detail Component', () => {
        let comp: DropoffReportDetailComponent;
        let fixture: ComponentFixture<DropoffReportDetailComponent>;
        let service: DropoffReportService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BusRidePlatformBackendTestModule],
                declarations: [DropoffReportDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DropoffReportService,
                    JhiEventManager
                ]
            }).overrideTemplate(DropoffReportDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DropoffReportDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DropoffReportService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new DropoffReport(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.dropoffReport).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
