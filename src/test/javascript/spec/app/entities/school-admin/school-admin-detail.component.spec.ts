/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BusRidePlatformBackendTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { SchoolAdminDetailComponent } from '../../../../../../main/webapp/app/entities/school-admin/school-admin-detail.component';
import { SchoolAdminService } from '../../../../../../main/webapp/app/entities/school-admin/school-admin.service';
import { SchoolAdmin } from '../../../../../../main/webapp/app/entities/school-admin/school-admin.model';

describe('Component Tests', () => {

    describe('SchoolAdmin Management Detail Component', () => {
        let comp: SchoolAdminDetailComponent;
        let fixture: ComponentFixture<SchoolAdminDetailComponent>;
        let service: SchoolAdminService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BusRidePlatformBackendTestModule],
                declarations: [SchoolAdminDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    SchoolAdminService,
                    JhiEventManager
                ]
            }).overrideTemplate(SchoolAdminDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SchoolAdminDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SchoolAdminService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new SchoolAdmin(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.schoolAdmin).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
