import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';


describe('SchoolAdmin e2e test', () => {

    let navBarPage: NavBarPage;
    let schoolAdminDialogPage: SchoolAdminDialogPage;
    let schoolAdminComponentsPage: SchoolAdminComponentsPage;


    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load SchoolAdmins', () => {
        navBarPage.goToEntity('school-admin');
        schoolAdminComponentsPage = new SchoolAdminComponentsPage();
        expect(schoolAdminComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.schoolAdmin.home.title/);

    });

    it('should load create SchoolAdmin dialog', () => {
        schoolAdminComponentsPage.clickOnCreateButton();
        schoolAdminDialogPage = new SchoolAdminDialogPage();
        expect(schoolAdminDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.schoolAdmin.home.createOrEditLabel/);
        schoolAdminDialogPage.close();
    });

    it('should create and save SchoolAdmins', () => {
        schoolAdminComponentsPage.clickOnCreateButton();
        schoolAdminDialogPage.userSelectLastOption();
        schoolAdminDialogPage.schoolSelectLastOption();
        schoolAdminDialogPage.save();
        expect(schoolAdminDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SchoolAdminComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-school-admin div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class SchoolAdminDialogPage {
    modalTitle = element(by.css('h4#mySchoolAdminLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    userSelect = element(by.css('select#field_user'));
    schoolSelect = element(by.css('select#field_school'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    userSelectLastOption = function () {
        this.userSelect.all(by.tagName('option')).last().click();
    }

    userSelectOption = function (option) {
        this.userSelect.sendKeys(option);
    }

    getUserSelect = function () {
        return this.userSelect;
    }

    getUserSelectedOption = function () {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    schoolSelectLastOption = function () {
        this.schoolSelect.all(by.tagName('option')).last().click();
    }

    schoolSelectOption = function (option) {
        this.schoolSelect.sendKeys(option);
    }

    getSchoolSelect = function () {
        return this.schoolSelect;
    }

    getSchoolSelectedOption = function () {
        return this.schoolSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
