import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('SchoolEmail e2e test', () => {

    let navBarPage: NavBarPage;
    let schoolEmailDialogPage: SchoolEmailDialogPage;
    let schoolEmailComponentsPage: SchoolEmailComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load SchoolEmails', () => {
        navBarPage.goToEntity('school-email');
        schoolEmailComponentsPage = new SchoolEmailComponentsPage();
        expect(schoolEmailComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.schoolEmail.home.title/);

    });

    it('should load create SchoolEmail dialog', () => {
        schoolEmailComponentsPage.clickOnCreateButton();
        schoolEmailDialogPage = new SchoolEmailDialogPage();
        expect(schoolEmailDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.schoolEmail.home.createOrEditLabel/);
        schoolEmailDialogPage.close();
    });

    it('should create and save SchoolEmails', () => {
        schoolEmailComponentsPage.clickOnCreateButton();
        schoolEmailDialogPage.setSubjectInput('subject');
        expect(schoolEmailDialogPage.getSubjectInput()).toMatch('subject');
        schoolEmailDialogPage.setContentInput('content');
        expect(schoolEmailDialogPage.getContentInput()).toMatch('content');
        schoolEmailDialogPage.setTimestampInput(12310020012301);
        expect(schoolEmailDialogPage.getTimestampInput()).toMatch('2001-12-31T02:30');
        schoolEmailDialogPage.schoolSelectLastOption();
        schoolEmailDialogPage.save();
        expect(schoolEmailDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SchoolEmailComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-school-email div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class SchoolEmailDialogPage {
    modalTitle = element(by.css('h4#mySchoolEmailLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    subjectInput = element(by.css('input#field_subject'));
    contentInput = element(by.css('input#field_content'));
    timestampInput = element(by.css('input#field_timestamp'));
    schoolSelect = element(by.css('select#field_school'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setSubjectInput = function (subject) {
        this.subjectInput.sendKeys(subject);
    }

    getSubjectInput = function () {
        return this.subjectInput.getAttribute('value');
    }

    setContentInput = function (content) {
        this.contentInput.sendKeys(content);
    }

    getContentInput = function () {
        return this.contentInput.getAttribute('value');
    }

    setTimestampInput = function (timestamp) {
        this.timestampInput.sendKeys(timestamp);
    }

    getTimestampInput = function () {
        return this.timestampInput.getAttribute('value');
    }

    schoolSelectLastOption = function () {
        this.schoolSelect.all(by.tagName('option')).last().click();
    }

    schoolSelectOption = function (option) {
        this.schoolSelect.sendKeys(option);
    }

    getSchoolSelect = function () {
        return this.schoolSelect;
    }

    getSchoolSelectedOption = function () {
        return this.schoolSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
