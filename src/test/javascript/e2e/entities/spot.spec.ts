import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Spot e2e test', () => {

    let navBarPage: NavBarPage;
    let spotDialogPage: SpotDialogPage;
    let spotComponentsPage: SpotComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Spots', () => {
        navBarPage.goToEntity('spot');
        spotComponentsPage = new SpotComponentsPage();
        expect(spotComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.spot.home.title/);

    });

    it('should load create Spot dialog', () => {
        spotComponentsPage.clickOnCreateButton();
        spotDialogPage = new SpotDialogPage();
        expect(spotDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.spot.home.createOrEditLabel/);
        spotDialogPage.close();
    });

    it('should create and save Spots', () => {
        spotComponentsPage.clickOnCreateButton();
        spotDialogPage.setGpsLatitudeInput('5');
        expect(spotDialogPage.getGpsLatitudeInput()).toMatch('5');
        spotDialogPage.setGpsLongitudeInput('5');
        expect(spotDialogPage.getGpsLongitudeInput()).toMatch('5');
        spotDialogPage.typeSelectLastOption();
        spotDialogPage.save();
        expect(spotDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SpotComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-spot div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class SpotDialogPage {
    modalTitle = element(by.css('h4#mySpotLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    gpsLatitudeInput = element(by.css('input#field_gpsLatitude'));
    gpsLongitudeInput = element(by.css('input#field_gpsLongitude'));
    typeSelect = element(by.css('select#field_type'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setGpsLatitudeInput = function (gpsLatitude) {
        this.gpsLatitudeInput.sendKeys(gpsLatitude);
    }

    getGpsLatitudeInput = function () {
        return this.gpsLatitudeInput.getAttribute('value');
    }

    setGpsLongitudeInput = function (gpsLongitude) {
        this.gpsLongitudeInput.sendKeys(gpsLongitude);
    }

    getGpsLongitudeInput = function () {
        return this.gpsLongitudeInput.getAttribute('value');
    }

    setTypeSelect = function (type) {
        this.typeSelect.sendKeys(type);
    }

    getTypeSelect = function () {
        return this.typeSelect.element(by.css('option:checked')).getText();
    }

    typeSelectLastOption = function () {
        this.typeSelect.all(by.tagName('option')).last().click();
    }
    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
