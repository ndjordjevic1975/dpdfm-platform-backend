import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Parent e2e test', () => {

    let navBarPage: NavBarPage;
    let parentDialogPage: ParentDialogPage;
    let parentComponentsPage: ParentComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Parents', () => {
        navBarPage.goToEntity('parent');
        parentComponentsPage = new ParentComponentsPage();
        expect(parentComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.parent.home.title/);

    });

    it('should load create Parent dialog', () => {
        parentComponentsPage.clickOnCreateButton();
        parentDialogPage = new ParentDialogPage();
        expect(parentDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.parent.home.createOrEditLabel/);
        parentDialogPage.close();
    });

    it('should create and save Parents', () => {
        parentComponentsPage.clickOnCreateButton();
        parentDialogPage.setAddressInput('address');
        expect(parentDialogPage.getAddressInput()).toMatch('address');
        parentDialogPage.setHouseNumberInput('houseNumber');
        expect(parentDialogPage.getHouseNumberInput()).toMatch('houseNumber');
        parentDialogPage.setCityInput('city');
        expect(parentDialogPage.getCityInput()).toMatch('city');
        parentDialogPage.setZipCodeInput('zipCode');
        expect(parentDialogPage.getZipCodeInput()).toMatch('zipCode');
        parentDialogPage.setStateInput('state');
        expect(parentDialogPage.getStateInput()).toMatch('state');
        parentDialogPage.setCountryInput('country');
        expect(parentDialogPage.getCountryInput()).toMatch('country');
        parentDialogPage.setPhoneInput('phone');
        expect(parentDialogPage.getPhoneInput()).toMatch('phone');
        parentDialogPage.setLandPhoneInput('landPhone');
        expect(parentDialogPage.getLandPhoneInput()).toMatch('landPhone');
        parentDialogPage.setNoOfStudentsInput('5');
        expect(parentDialogPage.getNoOfStudentsInput()).toMatch('5');
        parentDialogPage.setDeviceIdentifierInput('deviceIdentifier');
        expect(parentDialogPage.getDeviceIdentifierInput()).toMatch('deviceIdentifier');
        parentDialogPage.deviceTypeSelectLastOption();
        parentDialogPage.setGcmSessionIdInput('gcmSessionId');
        expect(parentDialogPage.getGcmSessionIdInput()).toMatch('gcmSessionId');
        parentDialogPage.setEndpointARNInput('endpointARN');
        expect(parentDialogPage.getEndpointARNInput()).toMatch('endpointARN');
        parentDialogPage.setNationalIdInput('nationalId');
        expect(parentDialogPage.getNationalIdInput()).toMatch('nationalId');
        parentDialogPage.userSelectLastOption();
        parentDialogPage.spotSelectLastOption();
        parentDialogPage.linkToPrimarySelectLastOption();
        parentDialogPage.save();
        expect(parentDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ParentComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-parent div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ParentDialogPage {
    modalTitle = element(by.css('h4#myParentLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    addressInput = element(by.css('input#field_address'));
    houseNumberInput = element(by.css('input#field_houseNumber'));
    cityInput = element(by.css('input#field_city'));
    zipCodeInput = element(by.css('input#field_zipCode'));
    stateInput = element(by.css('input#field_state'));
    countryInput = element(by.css('input#field_country'));
    phoneInput = element(by.css('input#field_phone'));
    landPhoneInput = element(by.css('input#field_landPhone'));
    noOfStudentsInput = element(by.css('input#field_noOfStudents'));
    deviceIdentifierInput = element(by.css('input#field_deviceIdentifier'));
    deviceTypeSelect = element(by.css('select#field_deviceType'));
    gcmSessionIdInput = element(by.css('input#field_gcmSessionId'));
    endpointARNInput = element(by.css('input#field_endpointARN'));
    nationalIdInput = element(by.css('input#field_nationalId'));
    userSelect = element(by.css('select#field_user'));
    spotSelect = element(by.css('select#field_spot'));
    linkToPrimarySelect = element(by.css('select#field_linkToPrimary'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setAddressInput = function (address) {
        this.addressInput.sendKeys(address);
    }

    getAddressInput = function () {
        return this.addressInput.getAttribute('value');
    }

    setHouseNumberInput = function (houseNumber) {
        this.houseNumberInput.sendKeys(houseNumber);
    }

    getHouseNumberInput = function () {
        return this.houseNumberInput.getAttribute('value');
    }

    setCityInput = function (city) {
        this.cityInput.sendKeys(city);
    }

    getCityInput = function () {
        return this.cityInput.getAttribute('value');
    }

    setZipCodeInput = function (zipCode) {
        this.zipCodeInput.sendKeys(zipCode);
    }

    getZipCodeInput = function () {
        return this.zipCodeInput.getAttribute('value');
    }

    setStateInput = function (state) {
        this.stateInput.sendKeys(state);
    }

    getStateInput = function () {
        return this.stateInput.getAttribute('value');
    }

    setCountryInput = function (country) {
        this.countryInput.sendKeys(country);
    }

    getCountryInput = function () {
        return this.countryInput.getAttribute('value');
    }

    setPhoneInput = function (phone) {
        this.phoneInput.sendKeys(phone);
    }

    getPhoneInput = function () {
        return this.phoneInput.getAttribute('value');
    }

    setLandPhoneInput = function (landPhone) {
        this.landPhoneInput.sendKeys(landPhone);
    }

    getLandPhoneInput = function () {
        return this.landPhoneInput.getAttribute('value');
    }

    setNoOfStudentsInput = function (noOfStudents) {
        this.noOfStudentsInput.sendKeys(noOfStudents);
    }

    getNoOfStudentsInput = function () {
        return this.noOfStudentsInput.getAttribute('value');
    }

    setDeviceIdentifierInput = function (deviceIdentifier) {
        this.deviceIdentifierInput.sendKeys(deviceIdentifier);
    }

    getDeviceIdentifierInput = function () {
        return this.deviceIdentifierInput.getAttribute('value');
    }

    setDeviceTypeSelect = function (deviceType) {
        this.deviceTypeSelect.sendKeys(deviceType);
    }

    getDeviceTypeSelect = function () {
        return this.deviceTypeSelect.element(by.css('option:checked')).getText();
    }

    deviceTypeSelectLastOption = function () {
        this.deviceTypeSelect.all(by.tagName('option')).last().click();
    }
    setGcmSessionIdInput = function (gcmSessionId) {
        this.gcmSessionIdInput.sendKeys(gcmSessionId);
    }

    getGcmSessionIdInput = function () {
        return this.gcmSessionIdInput.getAttribute('value');
    }

    setEndpointARNInput = function (endpointARN) {
        this.endpointARNInput.sendKeys(endpointARN);
    }

    getEndpointARNInput = function () {
        return this.endpointARNInput.getAttribute('value');
    }

    setNationalIdInput = function (nationalId) {
        this.nationalIdInput.sendKeys(nationalId);
    }

    getNationalIdInput = function () {
        return this.nationalIdInput.getAttribute('value');
    }

    userSelectLastOption = function () {
        this.userSelect.all(by.tagName('option')).last().click();
    }

    userSelectOption = function (option) {
        this.userSelect.sendKeys(option);
    }

    getUserSelect = function () {
        return this.userSelect;
    }

    getUserSelectedOption = function () {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    spotSelectLastOption = function () {
        this.spotSelect.all(by.tagName('option')).last().click();
    }

    spotSelectOption = function (option) {
        this.spotSelect.sendKeys(option);
    }

    getSpotSelect = function () {
        return this.spotSelect;
    }

    getSpotSelectedOption = function () {
        return this.spotSelect.element(by.css('option:checked')).getText();
    }

    linkToPrimarySelectLastOption = function () {
        this.linkToPrimarySelect.all(by.tagName('option')).last().click();
    }

    linkToPrimarySelectOption = function (option) {
        this.linkToPrimarySelect.sendKeys(option);
    }

    getLinkToPrimarySelect = function () {
        return this.linkToPrimarySelect;
    }

    getLinkToPrimarySelectedOption = function () {
        return this.linkToPrimarySelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
