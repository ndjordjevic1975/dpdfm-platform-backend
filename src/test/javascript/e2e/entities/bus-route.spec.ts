import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('BusRoute e2e test', () => {

    let navBarPage: NavBarPage;
    let busRouteDialogPage: BusRouteDialogPage;
    let busRouteComponentsPage: BusRouteComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load BusRoutes', () => {
        navBarPage.goToEntity('bus-route');
        busRouteComponentsPage = new BusRouteComponentsPage();
        expect(busRouteComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.busRoute.home.title/);

    });

    it('should load create BusRoute dialog', () => {
        busRouteComponentsPage.clickOnCreateButton();
        busRouteDialogPage = new BusRouteDialogPage();
        expect(busRouteDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.busRoute.home.createOrEditLabel/);
        busRouteDialogPage.close();
    });

    it('should create and save BusRoutes', () => {
        busRouteComponentsPage.clickOnCreateButton();
        busRouteDialogPage.setDetailsInput('details');
        expect(busRouteDialogPage.getDetailsInput()).toMatch('details');
        busRouteDialogPage.typeSelectLastOption();
        busRouteDialogPage.setCapacityInput('5');
        expect(busRouteDialogPage.getCapacityInput()).toMatch('5');
        busRouteDialogPage.setCreatedOnInput(12310020012301);
        expect(busRouteDialogPage.getCreatedOnInput()).toMatch('2001-12-31T02:30');
        busRouteDialogPage.setInactiveOnInput(12310020012301);
        expect(busRouteDialogPage.getInactiveOnInput()).toMatch('2001-12-31T02:30');
        busRouteDialogPage.schoolSelectLastOption();
        busRouteDialogPage.driverSelectLastOption();
        // busRouteDialogPage.spotSelectLastOption();
        busRouteDialogPage.save();
        expect(busRouteDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class BusRouteComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-bus-route div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class BusRouteDialogPage {
    modalTitle = element(by.css('h4#myBusRouteLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    detailsInput = element(by.css('textarea#field_details'));
    typeSelect = element(by.css('select#field_type'));
    capacityInput = element(by.css('input#field_capacity'));
    createdOnInput = element(by.css('input#field_createdOn'));
    inactiveOnInput = element(by.css('input#field_inactiveOn'));
    schoolSelect = element(by.css('select#field_school'));
    driverSelect = element(by.css('select#field_driver'));
    spotSelect = element(by.css('select#field_spot'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setDetailsInput = function (details) {
        this.detailsInput.sendKeys(details);
    }

    getDetailsInput = function () {
        return this.detailsInput.getAttribute('value');
    }

    setTypeSelect = function (type) {
        this.typeSelect.sendKeys(type);
    }

    getTypeSelect = function () {
        return this.typeSelect.element(by.css('option:checked')).getText();
    }

    typeSelectLastOption = function () {
        this.typeSelect.all(by.tagName('option')).last().click();
    }
    setCapacityInput = function (capacity) {
        this.capacityInput.sendKeys(capacity);
    }

    getCapacityInput = function () {
        return this.capacityInput.getAttribute('value');
    }

    setCreatedOnInput = function (createdOn) {
        this.createdOnInput.sendKeys(createdOn);
    }

    getCreatedOnInput = function () {
        return this.createdOnInput.getAttribute('value');
    }

    setInactiveOnInput = function (inactiveOn) {
        this.inactiveOnInput.sendKeys(inactiveOn);
    }

    getInactiveOnInput = function () {
        return this.inactiveOnInput.getAttribute('value');
    }

    schoolSelectLastOption = function () {
        this.schoolSelect.all(by.tagName('option')).last().click();
    }

    schoolSelectOption = function (option) {
        this.schoolSelect.sendKeys(option);
    }

    getSchoolSelect = function () {
        return this.schoolSelect;
    }

    getSchoolSelectedOption = function () {
        return this.schoolSelect.element(by.css('option:checked')).getText();
    }

    driverSelectLastOption = function () {
        this.driverSelect.all(by.tagName('option')).last().click();
    }

    driverSelectOption = function (option) {
        this.driverSelect.sendKeys(option);
    }

    getDriverSelect = function () {
        return this.driverSelect;
    }

    getDriverSelectedOption = function () {
        return this.driverSelect.element(by.css('option:checked')).getText();
    }

    spotSelectLastOption = function () {
        this.spotSelect.all(by.tagName('option')).last().click();
    }

    spotSelectOption = function (option) {
        this.spotSelect.sendKeys(option);
    }

    getSpotSelect = function () {
        return this.spotSelect;
    }

    getSpotSelectedOption = function () {
        return this.spotSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
