import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('PickupReport e2e test', () => {

    let navBarPage: NavBarPage;
    let pickupReportDialogPage: PickupReportDialogPage;
    let pickupReportComponentsPage: PickupReportComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load PickupReports', () => {
        navBarPage.goToEntity('pickup-report');
        pickupReportComponentsPage = new PickupReportComponentsPage();
        expect(pickupReportComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.pickupReport.home.title/);

    });

    it('should load create PickupReport dialog', () => {
        pickupReportComponentsPage.clickOnCreateButton();
        pickupReportDialogPage = new PickupReportDialogPage();
        expect(pickupReportDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.pickupReport.home.createOrEditLabel/);
        pickupReportDialogPage.close();
    });

    it('should create and save PickupReports', () => {
        pickupReportComponentsPage.clickOnCreateButton();
        pickupReportDialogPage.statusSelectLastOption();
        pickupReportDialogPage.setDelayInput('5');
        expect(pickupReportDialogPage.getDelayInput()).toMatch('5');
        pickupReportDialogPage.setPlannedInput('5');
        expect(pickupReportDialogPage.getPlannedInput()).toMatch('5');
        pickupReportDialogPage.setAccomplishedInput('5');
        expect(pickupReportDialogPage.getAccomplishedInput()).toMatch('5');
        pickupReportDialogPage.busRouteSelectLastOption();
        pickupReportDialogPage.spotSelectLastOption();
        pickupReportDialogPage.save();
        expect(pickupReportDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class PickupReportComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-pickup-report div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PickupReportDialogPage {
    modalTitle = element(by.css('h4#myPickupReportLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    statusSelect = element(by.css('select#field_status'));
    delayInput = element(by.css('input#field_delay'));
    plannedInput = element(by.css('input#field_planned'));
    accomplishedInput = element(by.css('input#field_accomplished'));
    busRouteSelect = element(by.css('select#field_busRoute'));
    spotSelect = element(by.css('select#field_spot'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setStatusSelect = function (status) {
        this.statusSelect.sendKeys(status);
    }

    getStatusSelect = function () {
        return this.statusSelect.element(by.css('option:checked')).getText();
    }

    statusSelectLastOption = function () {
        this.statusSelect.all(by.tagName('option')).last().click();
    }
    setDelayInput = function (delay) {
        this.delayInput.sendKeys(delay);
    }

    getDelayInput = function () {
        return this.delayInput.getAttribute('value');
    }

    setPlannedInput = function (planned) {
        this.plannedInput.sendKeys(planned);
    }

    getPlannedInput = function () {
        return this.plannedInput.getAttribute('value');
    }

    setAccomplishedInput = function (accomplished) {
        this.accomplishedInput.sendKeys(accomplished);
    }

    getAccomplishedInput = function () {
        return this.accomplishedInput.getAttribute('value');
    }

    busRouteSelectLastOption = function () {
        this.busRouteSelect.all(by.tagName('option')).last().click();
    }

    busRouteSelectOption = function (option) {
        this.busRouteSelect.sendKeys(option);
    }

    getBusRouteSelect = function () {
        return this.busRouteSelect;
    }

    getBusRouteSelectedOption = function () {
        return this.busRouteSelect.element(by.css('option:checked')).getText();
    }

    spotSelectLastOption = function () {
        this.spotSelect.all(by.tagName('option')).last().click();
    }

    spotSelectOption = function (option) {
        this.spotSelect.sendKeys(option);
    }

    getSpotSelect = function () {
        return this.spotSelect;
    }

    getSpotSelectedOption = function () {
        return this.spotSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
