import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('School e2e test', () => {

    let navBarPage: NavBarPage;
    let schoolDialogPage: SchoolDialogPage;
    let schoolComponentsPage: SchoolComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Schools', () => {
        navBarPage.goToEntity('school');
        schoolComponentsPage = new SchoolComponentsPage();
        expect(schoolComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.school.home.title/);

    });

    it('should load create School dialog', () => {
        schoolComponentsPage.clickOnCreateButton();
        schoolDialogPage = new SchoolDialogPage();
        expect(schoolDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.school.home.createOrEditLabel/);
        schoolDialogPage.close();
    });

    it('should create and save Schools', () => {
        schoolComponentsPage.clickOnCreateButton();
        schoolDialogPage.setNameInput('name');
        expect(schoolDialogPage.getNameInput()).toMatch('name');
        schoolDialogPage.setAddressInput('address');
        expect(schoolDialogPage.getAddressInput()).toMatch('address');
        schoolDialogPage.setHouseNumberInput('houseNumber');
        expect(schoolDialogPage.getHouseNumberInput()).toMatch('houseNumber');
        schoolDialogPage.setPhoneInput('phone');
        expect(schoolDialogPage.getPhoneInput()).toMatch('phone');
        schoolDialogPage.setCityInput('city');
        expect(schoolDialogPage.getCityInput()).toMatch('city');
        schoolDialogPage.setZipcodeInput('zipcode');
        expect(schoolDialogPage.getZipcodeInput()).toMatch('zipcode');
        schoolDialogPage.setStateInput('state');
        expect(schoolDialogPage.getStateInput()).toMatch('state');
        schoolDialogPage.setCountryInput('country');
        expect(schoolDialogPage.getCountryInput()).toMatch('country');
        schoolDialogPage.setLogoInput(absolutePath);
        schoolDialogPage.spotSelectLastOption();
        // schoolDialogPage.parentSelectLastOption();
        schoolDialogPage.save();
        expect(schoolDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SchoolComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-school div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class SchoolDialogPage {
    modalTitle = element(by.css('h4#mySchoolLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    addressInput = element(by.css('input#field_address'));
    houseNumberInput = element(by.css('input#field_houseNumber'));
    phoneInput = element(by.css('input#field_phone'));
    cityInput = element(by.css('input#field_city'));
    zipcodeInput = element(by.css('input#field_zipcode'));
    stateInput = element(by.css('input#field_state'));
    countryInput = element(by.css('input#field_country'));
    logoInput = element(by.css('input#file_logo'));
    spotSelect = element(by.css('select#field_spot'));
    parentSelect = element(by.css('select#field_parent'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function (name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function () {
        return this.nameInput.getAttribute('value');
    }

    setAddressInput = function (address) {
        this.addressInput.sendKeys(address);
    }

    getAddressInput = function () {
        return this.addressInput.getAttribute('value');
    }

    setHouseNumberInput = function (houseNumber) {
        this.houseNumberInput.sendKeys(houseNumber);
    }

    getHouseNumberInput = function () {
        return this.houseNumberInput.getAttribute('value');
    }

    setPhoneInput = function (phone) {
        this.phoneInput.sendKeys(phone);
    }

    getPhoneInput = function () {
        return this.phoneInput.getAttribute('value');
    }

    setCityInput = function (city) {
        this.cityInput.sendKeys(city);
    }

    getCityInput = function () {
        return this.cityInput.getAttribute('value');
    }

    setZipcodeInput = function (zipcode) {
        this.zipcodeInput.sendKeys(zipcode);
    }

    getZipcodeInput = function () {
        return this.zipcodeInput.getAttribute('value');
    }

    setStateInput = function (state) {
        this.stateInput.sendKeys(state);
    }

    getStateInput = function () {
        return this.stateInput.getAttribute('value');
    }

    setCountryInput = function (country) {
        this.countryInput.sendKeys(country);
    }

    getCountryInput = function () {
        return this.countryInput.getAttribute('value');
    }

    setLogoInput = function (logo) {
        this.logoInput.sendKeys(logo);
    }

    getLogoInput = function () {
        return this.logoInput.getAttribute('value');
    }

    spotSelectLastOption = function () {
        this.spotSelect.all(by.tagName('option')).last().click();
    }

    spotSelectOption = function (option) {
        this.spotSelect.sendKeys(option);
    }

    getSpotSelect = function () {
        return this.spotSelect;
    }

    getSpotSelectedOption = function () {
        return this.spotSelect.element(by.css('option:checked')).getText();
    }

    parentSelectLastOption = function () {
        this.parentSelect.all(by.tagName('option')).last().click();
    }

    parentSelectOption = function (option) {
        this.parentSelect.sendKeys(option);
    }

    getParentSelect = function () {
        return this.parentSelect;
    }

    getParentSelectedOption = function () {
        return this.parentSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
