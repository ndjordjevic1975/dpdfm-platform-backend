import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Student e2e test', () => {

    let navBarPage: NavBarPage;
    let studentDialogPage: StudentDialogPage;
    let studentComponentsPage: StudentComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Students', () => {
        navBarPage.goToEntity('student');
        studentComponentsPage = new StudentComponentsPage();
        expect(studentComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.student.home.title/);

    });

    it('should load create Student dialog', () => {
        studentComponentsPage.clickOnCreateButton();
        studentDialogPage = new StudentDialogPage();
        expect(studentDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.student.home.createOrEditLabel/);
        studentDialogPage.close();
    });

    it('should create and save Students', () => {
        studentComponentsPage.clickOnCreateButton();
        studentDialogPage.setNameInput('name');
        expect(studentDialogPage.getNameInput()).toMatch('name');
        studentDialogPage.setImageInput(absolutePath);
        studentDialogPage.typeOfServiceSelectLastOption();
        studentDialogPage.schoolSelectLastOption();
        studentDialogPage.parentSelectLastOption();
        studentDialogPage.save();
        expect(studentDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class StudentComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-student div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class StudentDialogPage {
    modalTitle = element(by.css('h4#myStudentLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    imageInput = element(by.css('input#file_image'));
    typeOfServiceSelect = element(by.css('select#field_typeOfService'));
    schoolSelect = element(by.css('select#field_school'));
    parentSelect = element(by.css('select#field_parent'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function (name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function () {
        return this.nameInput.getAttribute('value');
    }

    setImageInput = function (image) {
        this.imageInput.sendKeys(image);
    }

    getImageInput = function () {
        return this.imageInput.getAttribute('value');
    }

    setTypeOfServiceSelect = function (typeOfService) {
        this.typeOfServiceSelect.sendKeys(typeOfService);
    }

    getTypeOfServiceSelect = function () {
        return this.typeOfServiceSelect.element(by.css('option:checked')).getText();
    }

    typeOfServiceSelectLastOption = function () {
        this.typeOfServiceSelect.all(by.tagName('option')).last().click();
    }
    schoolSelectLastOption = function () {
        this.schoolSelect.all(by.tagName('option')).last().click();
    }

    schoolSelectOption = function (option) {
        this.schoolSelect.sendKeys(option);
    }

    getSchoolSelect = function () {
        return this.schoolSelect;
    }

    getSchoolSelectedOption = function () {
        return this.schoolSelect.element(by.css('option:checked')).getText();
    }

    parentSelectLastOption = function () {
        this.parentSelect.all(by.tagName('option')).last().click();
    }

    parentSelectOption = function (option) {
        this.parentSelect.sendKeys(option);
    }

    getParentSelect = function () {
        return this.parentSelect;
    }

    getParentSelectedOption = function () {
        return this.parentSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
