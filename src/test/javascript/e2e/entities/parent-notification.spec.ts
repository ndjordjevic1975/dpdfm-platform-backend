import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('ParentNotification e2e test', () => {

    let navBarPage: NavBarPage;
    let parentNotificationDialogPage: ParentNotificationDialogPage;
    let parentNotificationComponentsPage: ParentNotificationComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load ParentNotifications', () => {
        navBarPage.goToEntity('parent-notification');
        parentNotificationComponentsPage = new ParentNotificationComponentsPage();
        expect(parentNotificationComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.parentNotification.home.title/);

    });

    it('should load create ParentNotification dialog', () => {
        parentNotificationComponentsPage.clickOnCreateButton();
        parentNotificationDialogPage = new ParentNotificationDialogPage();
        expect(parentNotificationDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.parentNotification.home.createOrEditLabel/);
        parentNotificationDialogPage.close();
    });

    it('should create and save ParentNotifications', () => {
        parentNotificationComponentsPage.clickOnCreateButton();
        parentNotificationDialogPage.setNotificationInput('notification');
        expect(parentNotificationDialogPage.getNotificationInput()).toMatch('notification');
        parentNotificationDialogPage.setTimestampInput(12310020012301);
        expect(parentNotificationDialogPage.getTimestampInput()).toMatch('2001-12-31T02:30');
        parentNotificationDialogPage.typeSelectLastOption();
        parentNotificationDialogPage.parentSelectLastOption();
        parentNotificationDialogPage.save();
        expect(parentNotificationDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ParentNotificationComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-parent-notification div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ParentNotificationDialogPage {
    modalTitle = element(by.css('h4#myParentNotificationLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    notificationInput = element(by.css('input#field_notification'));
    timestampInput = element(by.css('input#field_timestamp'));
    typeSelect = element(by.css('select#field_type'));
    parentSelect = element(by.css('select#field_parent'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNotificationInput = function (notification) {
        this.notificationInput.sendKeys(notification);
    }

    getNotificationInput = function () {
        return this.notificationInput.getAttribute('value');
    }

    setTimestampInput = function (timestamp) {
        this.timestampInput.sendKeys(timestamp);
    }

    getTimestampInput = function () {
        return this.timestampInput.getAttribute('value');
    }

    setTypeSelect = function (type) {
        this.typeSelect.sendKeys(type);
    }

    getTypeSelect = function () {
        return this.typeSelect.element(by.css('option:checked')).getText();
    }

    typeSelectLastOption = function () {
        this.typeSelect.all(by.tagName('option')).last().click();
    }
    parentSelectLastOption = function () {
        this.parentSelect.all(by.tagName('option')).last().click();
    }

    parentSelectOption = function (option) {
        this.parentSelect.sendKeys(option);
    }

    getParentSelect = function () {
        return this.parentSelect;
    }

    getParentSelectedOption = function () {
        return this.parentSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
