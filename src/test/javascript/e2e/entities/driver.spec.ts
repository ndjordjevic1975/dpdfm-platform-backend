import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Driver e2e test', () => {

    let navBarPage: NavBarPage;
    let driverDialogPage: DriverDialogPage;
    let driverComponentsPage: DriverComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Drivers', () => {
        navBarPage.goToEntity('driver');
        driverComponentsPage = new DriverComponentsPage();
        expect(driverComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.driver.home.title/);

    });

    it('should load create Driver dialog', () => {
        driverComponentsPage.clickOnCreateButton();
        driverDialogPage = new DriverDialogPage();
        expect(driverDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.driver.home.createOrEditLabel/);
        driverDialogPage.close();
    });

    it('should create and save Drivers', () => {
        driverComponentsPage.clickOnCreateButton();
        driverDialogPage.setCapacityInput('5');
        expect(driverDialogPage.getCapacityInput()).toMatch('5');
        driverDialogPage.setDeviceIdentifierInput('deviceIdentifier');
        expect(driverDialogPage.getDeviceIdentifierInput()).toMatch('deviceIdentifier');
        driverDialogPage.setGcmSessionIdInput('gcmSessionId');
        expect(driverDialogPage.getGcmSessionIdInput()).toMatch('gcmSessionId');
        driverDialogPage.setEndpointARNInput('endpointARN');
        expect(driverDialogPage.getEndpointARNInput()).toMatch('endpointARN');
        driverDialogPage.setTabletNumberInput('tabletNumber');
        expect(driverDialogPage.getTabletNumberInput()).toMatch('tabletNumber');
        driverDialogPage.userSelectLastOption();
        driverDialogPage.activeBusRouteSelectLastOption();
        driverDialogPage.schoolSelectLastOption();
        driverDialogPage.save();
        expect(driverDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class DriverComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-driver div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class DriverDialogPage {
    modalTitle = element(by.css('h4#myDriverLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    capacityInput = element(by.css('input#field_capacity'));
    deviceIdentifierInput = element(by.css('input#field_deviceIdentifier'));
    gcmSessionIdInput = element(by.css('input#field_gcmSessionId'));
    endpointARNInput = element(by.css('input#field_endpointARN'));
    tabletNumberInput = element(by.css('input#field_tabletNumber'));
    userSelect = element(by.css('select#field_user'));
    activeBusRouteSelect = element(by.css('select#field_activeBusRoute'));
    schoolSelect = element(by.css('select#field_school'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCapacityInput = function (capacity) {
        this.capacityInput.sendKeys(capacity);
    }

    getCapacityInput = function () {
        return this.capacityInput.getAttribute('value');
    }

    setDeviceIdentifierInput = function (deviceIdentifier) {
        this.deviceIdentifierInput.sendKeys(deviceIdentifier);
    }

    getDeviceIdentifierInput = function () {
        return this.deviceIdentifierInput.getAttribute('value');
    }

    setGcmSessionIdInput = function (gcmSessionId) {
        this.gcmSessionIdInput.sendKeys(gcmSessionId);
    }

    getGcmSessionIdInput = function () {
        return this.gcmSessionIdInput.getAttribute('value');
    }

    setEndpointARNInput = function (endpointARN) {
        this.endpointARNInput.sendKeys(endpointARN);
    }

    getEndpointARNInput = function () {
        return this.endpointARNInput.getAttribute('value');
    }

    setTabletNumberInput = function (tabletNumber) {
        this.tabletNumberInput.sendKeys(tabletNumber);
    }

    getTabletNumberInput = function () {
        return this.tabletNumberInput.getAttribute('value');
    }

    userSelectLastOption = function () {
        this.userSelect.all(by.tagName('option')).last().click();
    }

    userSelectOption = function (option) {
        this.userSelect.sendKeys(option);
    }

    getUserSelect = function () {
        return this.userSelect;
    }

    getUserSelectedOption = function () {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    activeBusRouteSelectLastOption = function () {
        this.activeBusRouteSelect.all(by.tagName('option')).last().click();
    }

    activeBusRouteSelectOption = function (option) {
        this.activeBusRouteSelect.sendKeys(option);
    }

    getActiveBusRouteSelect = function () {
        return this.activeBusRouteSelect;
    }

    getActiveBusRouteSelectedOption = function () {
        return this.activeBusRouteSelect.element(by.css('option:checked')).getText();
    }

    schoolSelectLastOption = function () {
        this.schoolSelect.all(by.tagName('option')).last().click();
    }

    schoolSelectOption = function (option) {
        this.schoolSelect.sendKeys(option);
    }

    getSchoolSelect = function () {
        return this.schoolSelect;
    }

    getSchoolSelectedOption = function () {
        return this.schoolSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
