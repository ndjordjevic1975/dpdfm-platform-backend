import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('BusCategory e2e test', () => {

    let navBarPage: NavBarPage;
    let busCategoryDialogPage: BusCategoryDialogPage;
    let busCategoryComponentsPage: BusCategoryComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load BusCategories', () => {
        navBarPage.goToEntity('bus-category');
        busCategoryComponentsPage = new BusCategoryComponentsPage();
        expect(busCategoryComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.busCategory.home.title/);

    });

    it('should load create BusCategory dialog', () => {
        busCategoryComponentsPage.clickOnCreateButton();
        busCategoryDialogPage = new BusCategoryDialogPage();
        expect(busCategoryDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.busCategory.home.createOrEditLabel/);
        busCategoryDialogPage.close();
    });

    it('should create and save BusCategories', () => {
        busCategoryComponentsPage.clickOnCreateButton();
        busCategoryDialogPage.setNoOfBusesInput('5');
        expect(busCategoryDialogPage.getNoOfBusesInput()).toMatch('5');
        busCategoryDialogPage.setCapacityInput('5');
        expect(busCategoryDialogPage.getCapacityInput()).toMatch('5');
        busCategoryDialogPage.schoolSelectLastOption();
        busCategoryDialogPage.save();
        expect(busCategoryDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class BusCategoryComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-bus-category div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class BusCategoryDialogPage {
    modalTitle = element(by.css('h4#myBusCategoryLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    noOfBusesInput = element(by.css('input#field_noOfBuses'));
    capacityInput = element(by.css('input#field_capacity'));
    schoolSelect = element(by.css('select#field_school'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNoOfBusesInput = function (noOfBuses) {
        this.noOfBusesInput.sendKeys(noOfBuses);
    }

    getNoOfBusesInput = function () {
        return this.noOfBusesInput.getAttribute('value');
    }

    setCapacityInput = function (capacity) {
        this.capacityInput.sendKeys(capacity);
    }

    getCapacityInput = function () {
        return this.capacityInput.getAttribute('value');
    }

    schoolSelectLastOption = function () {
        this.schoolSelect.all(by.tagName('option')).last().click();
    }

    schoolSelectOption = function (option) {
        this.schoolSelect.sendKeys(option);
    }

    getSchoolSelect = function () {
        return this.schoolSelect;
    }

    getSchoolSelectedOption = function () {
        return this.schoolSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
