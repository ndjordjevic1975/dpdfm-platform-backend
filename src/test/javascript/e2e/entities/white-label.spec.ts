import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('WhiteLabel e2e test', () => {

    let navBarPage: NavBarPage;
    let whiteLabelDialogPage: WhiteLabelDialogPage;
    let whiteLabelComponentsPage: WhiteLabelComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load WhiteLabels', () => {
        navBarPage.goToEntity('white-label');
        whiteLabelComponentsPage = new WhiteLabelComponentsPage();
        expect(whiteLabelComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.whiteLabel.home.title/);

    });

    it('should load create WhiteLabel dialog', () => {
        whiteLabelComponentsPage.clickOnCreateButton();
        whiteLabelDialogPage = new WhiteLabelDialogPage();
        expect(whiteLabelDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.whiteLabel.home.createOrEditLabel/);
        whiteLabelDialogPage.close();
    });

    it('should create and save WhiteLabels', () => {
        whiteLabelComponentsPage.clickOnCreateButton();
        whiteLabelDialogPage.setParentWhiteLabelInput(absolutePath);
        whiteLabelDialogPage.setDriverWhiteLabelInput(absolutePath);
        whiteLabelDialogPage.setParentWhiteLabelVersionInput('5');
        expect(whiteLabelDialogPage.getParentWhiteLabelVersionInput()).toMatch('5');
        whiteLabelDialogPage.setDriverWhiteLabelVersionInput('5');
        expect(whiteLabelDialogPage.getDriverWhiteLabelVersionInput()).toMatch('5');
        whiteLabelDialogPage.setSplashScreenInput(absolutePath);
        whiteLabelDialogPage.setIcnBusInput(absolutePath);
        whiteLabelDialogPage.setLogoBgInput(absolutePath);
        whiteLabelDialogPage.setLogoTxtInput(absolutePath);
        whiteLabelDialogPage.schoolSelectLastOption();
        whiteLabelDialogPage.save();
        expect(whiteLabelDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class WhiteLabelComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-white-label div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class WhiteLabelDialogPage {
    modalTitle = element(by.css('h4#myWhiteLabelLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    parentWhiteLabelInput = element(by.css('input#file_parentWhiteLabel'));
    driverWhiteLabelInput = element(by.css('input#file_driverWhiteLabel'));
    parentWhiteLabelVersionInput = element(by.css('input#field_parentWhiteLabelVersion'));
    driverWhiteLabelVersionInput = element(by.css('input#field_driverWhiteLabelVersion'));
    splashScreenInput = element(by.css('input#file_splashScreen'));
    icnBusInput = element(by.css('input#file_icnBus'));
    logoBgInput = element(by.css('input#file_logoBg'));
    logoTxtInput = element(by.css('input#file_logoTxt'));
    schoolSelect = element(by.css('select#field_school'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setParentWhiteLabelInput = function (parentWhiteLabel) {
        this.parentWhiteLabelInput.sendKeys(parentWhiteLabel);
    }

    getParentWhiteLabelInput = function () {
        return this.parentWhiteLabelInput.getAttribute('value');
    }

    setDriverWhiteLabelInput = function (driverWhiteLabel) {
        this.driverWhiteLabelInput.sendKeys(driverWhiteLabel);
    }

    getDriverWhiteLabelInput = function () {
        return this.driverWhiteLabelInput.getAttribute('value');
    }

    setParentWhiteLabelVersionInput = function (parentWhiteLabelVersion) {
        this.parentWhiteLabelVersionInput.sendKeys(parentWhiteLabelVersion);
    }

    getParentWhiteLabelVersionInput = function () {
        return this.parentWhiteLabelVersionInput.getAttribute('value');
    }

    setDriverWhiteLabelVersionInput = function (driverWhiteLabelVersion) {
        this.driverWhiteLabelVersionInput.sendKeys(driverWhiteLabelVersion);
    }

    getDriverWhiteLabelVersionInput = function () {
        return this.driverWhiteLabelVersionInput.getAttribute('value');
    }

    setSplashScreenInput = function (splashScreen) {
        this.splashScreenInput.sendKeys(splashScreen);
    }

    getSplashScreenInput = function () {
        return this.splashScreenInput.getAttribute('value');
    }

    setIcnBusInput = function (icnBus) {
        this.icnBusInput.sendKeys(icnBus);
    }

    getIcnBusInput = function () {
        return this.icnBusInput.getAttribute('value');
    }

    setLogoBgInput = function (logoBg) {
        this.logoBgInput.sendKeys(logoBg);
    }

    getLogoBgInput = function () {
        return this.logoBgInput.getAttribute('value');
    }

    setLogoTxtInput = function (logoTxt) {
        this.logoTxtInput.sendKeys(logoTxt);
    }

    getLogoTxtInput = function () {
        return this.logoTxtInput.getAttribute('value');
    }

    schoolSelectLastOption = function () {
        this.schoolSelect.all(by.tagName('option')).last().click();
    }

    schoolSelectOption = function (option) {
        this.schoolSelect.sendKeys(option);
    }

    getSchoolSelect = function () {
        return this.schoolSelect;
    }

    getSchoolSelectedOption = function () {
        return this.schoolSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
