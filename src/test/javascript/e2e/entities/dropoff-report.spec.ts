import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('DropoffReport e2e test', () => {

    let navBarPage: NavBarPage;
    let dropoffReportDialogPage: DropoffReportDialogPage;
    let dropoffReportComponentsPage: DropoffReportComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load DropoffReports', () => {
        navBarPage.goToEntity('dropoff-report');
        dropoffReportComponentsPage = new DropoffReportComponentsPage();
        expect(dropoffReportComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.dropoffReport.home.title/);

    });

    it('should load create DropoffReport dialog', () => {
        dropoffReportComponentsPage.clickOnCreateButton();
        dropoffReportDialogPage = new DropoffReportDialogPage();
        expect(dropoffReportDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.dropoffReport.home.createOrEditLabel/);
        dropoffReportDialogPage.close();
    });

    it('should create and save DropoffReports', () => {
        dropoffReportComponentsPage.clickOnCreateButton();
        dropoffReportDialogPage.statusSelectLastOption();
        dropoffReportDialogPage.setPlannedInput('5');
        expect(dropoffReportDialogPage.getPlannedInput()).toMatch('5');
        dropoffReportDialogPage.setAccomplishedInput('5');
        expect(dropoffReportDialogPage.getAccomplishedInput()).toMatch('5');
        dropoffReportDialogPage.busRouteSelectLastOption();
        dropoffReportDialogPage.spotSelectLastOption();
        dropoffReportDialogPage.save();
        expect(dropoffReportDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class DropoffReportComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-dropoff-report div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class DropoffReportDialogPage {
    modalTitle = element(by.css('h4#myDropoffReportLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    statusSelect = element(by.css('select#field_status'));
    plannedInput = element(by.css('input#field_planned'));
    accomplishedInput = element(by.css('input#field_accomplished'));
    busRouteSelect = element(by.css('select#field_busRoute'));
    spotSelect = element(by.css('select#field_spot'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setStatusSelect = function (status) {
        this.statusSelect.sendKeys(status);
    }

    getStatusSelect = function () {
        return this.statusSelect.element(by.css('option:checked')).getText();
    }

    statusSelectLastOption = function () {
        this.statusSelect.all(by.tagName('option')).last().click();
    }
    setPlannedInput = function (planned) {
        this.plannedInput.sendKeys(planned);
    }

    getPlannedInput = function () {
        return this.plannedInput.getAttribute('value');
    }

    setAccomplishedInput = function (accomplished) {
        this.accomplishedInput.sendKeys(accomplished);
    }

    getAccomplishedInput = function () {
        return this.accomplishedInput.getAttribute('value');
    }

    busRouteSelectLastOption = function () {
        this.busRouteSelect.all(by.tagName('option')).last().click();
    }

    busRouteSelectOption = function (option) {
        this.busRouteSelect.sendKeys(option);
    }

    getBusRouteSelect = function () {
        return this.busRouteSelect;
    }

    getBusRouteSelectedOption = function () {
        return this.busRouteSelect.element(by.css('option:checked')).getText();
    }

    spotSelectLastOption = function () {
        this.spotSelect.all(by.tagName('option')).last().click();
    }

    spotSelectOption = function (option) {
        this.spotSelect.sendKeys(option);
    }

    getSpotSelect = function () {
        return this.spotSelect;
    }

    getSpotSelectedOption = function () {
        return this.spotSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
