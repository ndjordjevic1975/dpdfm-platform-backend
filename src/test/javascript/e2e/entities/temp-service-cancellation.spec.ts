import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('TempServiceCancellation e2e test', () => {

    let navBarPage: NavBarPage;
    let tempServiceCancellationDialogPage: TempServiceCancellationDialogPage;
    let tempServiceCancellationComponentsPage: TempServiceCancellationComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TempServiceCancellations', () => {
        navBarPage.goToEntity('temp-service-cancellation');
        tempServiceCancellationComponentsPage = new TempServiceCancellationComponentsPage();
        expect(tempServiceCancellationComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.tempServiceCancellation.home.title/);

    });

    it('should load create TempServiceCancellation dialog', () => {
        tempServiceCancellationComponentsPage.clickOnCreateButton();
        tempServiceCancellationDialogPage = new TempServiceCancellationDialogPage();
        expect(tempServiceCancellationDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.tempServiceCancellation.home.createOrEditLabel/);
        tempServiceCancellationDialogPage.close();
    });

    it('should create and save TempServiceCancellations', () => {
        tempServiceCancellationComponentsPage.clickOnCreateButton();
        tempServiceCancellationDialogPage.cancellationTypeSelectLastOption();
        tempServiceCancellationDialogPage.setReasonInput('reason');
        expect(tempServiceCancellationDialogPage.getReasonInput()).toMatch('reason');
        tempServiceCancellationDialogPage.studentSelectLastOption();
        tempServiceCancellationDialogPage.spotSelectLastOption();
        tempServiceCancellationDialogPage.save();
        expect(tempServiceCancellationDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TempServiceCancellationComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-temp-service-cancellation div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TempServiceCancellationDialogPage {
    modalTitle = element(by.css('h4#myTempServiceCancellationLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    cancellationTypeSelect = element(by.css('select#field_cancellationType'));
    reasonInput = element(by.css('input#field_reason'));
    studentSelect = element(by.css('select#field_student'));
    spotSelect = element(by.css('select#field_spot'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCancellationTypeSelect = function (cancellationType) {
        this.cancellationTypeSelect.sendKeys(cancellationType);
    }

    getCancellationTypeSelect = function () {
        return this.cancellationTypeSelect.element(by.css('option:checked')).getText();
    }

    cancellationTypeSelectLastOption = function () {
        this.cancellationTypeSelect.all(by.tagName('option')).last().click();
    }
    setReasonInput = function (reason) {
        this.reasonInput.sendKeys(reason);
    }

    getReasonInput = function () {
        return this.reasonInput.getAttribute('value');
    }

    studentSelectLastOption = function () {
        this.studentSelect.all(by.tagName('option')).last().click();
    }

    studentSelectOption = function (option) {
        this.studentSelect.sendKeys(option);
    }

    getStudentSelect = function () {
        return this.studentSelect;
    }

    getStudentSelectedOption = function () {
        return this.studentSelect.element(by.css('option:checked')).getText();
    }

    spotSelectLastOption = function () {
        this.spotSelect.all(by.tagName('option')).last().click();
    }

    spotSelectOption = function (option) {
        this.spotSelect.sendKeys(option);
    }

    getSpotSelect = function () {
        return this.spotSelect;
    }

    getSpotSelectedOption = function () {
        return this.spotSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
