import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('DriverNotification e2e test', () => {

    let navBarPage: NavBarPage;
    let driverNotificationDialogPage: DriverNotificationDialogPage;
    let driverNotificationComponentsPage: DriverNotificationComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load DriverNotifications', () => {
        navBarPage.goToEntity('driver-notification');
        driverNotificationComponentsPage = new DriverNotificationComponentsPage();
        expect(driverNotificationComponentsPage.getTitle()).toMatch(/busRidePlatformBackendApp.driverNotification.home.title/);

    });

    it('should load create DriverNotification dialog', () => {
        driverNotificationComponentsPage.clickOnCreateButton();
        driverNotificationDialogPage = new DriverNotificationDialogPage();
        expect(driverNotificationDialogPage.getModalTitle()).toMatch(/busRidePlatformBackendApp.driverNotification.home.createOrEditLabel/);
        driverNotificationDialogPage.close();
    });

    it('should create and save DriverNotifications', () => {
        driverNotificationComponentsPage.clickOnCreateButton();
        driverNotificationDialogPage.setNotificationInput('notification');
        expect(driverNotificationDialogPage.getNotificationInput()).toMatch('notification');
        driverNotificationDialogPage.setCreatedOnDeviceInput(12310020012301);
        expect(driverNotificationDialogPage.getCreatedOnDeviceInput()).toMatch('2001-12-31T02:30');
        driverNotificationDialogPage.typeSelectLastOption();
        driverNotificationDialogPage.driverSelectLastOption();
        driverNotificationDialogPage.spotSelectLastOption();
        driverNotificationDialogPage.save();
        expect(driverNotificationDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class DriverNotificationComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-driver-notification div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class DriverNotificationDialogPage {
    modalTitle = element(by.css('h4#myDriverNotificationLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    notificationInput = element(by.css('input#field_notification'));
    createdOnDeviceInput = element(by.css('input#field_createdOnDevice'));
    typeSelect = element(by.css('select#field_type'));
    driverSelect = element(by.css('select#field_driver'));
    spotSelect = element(by.css('select#field_spot'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNotificationInput = function (notification) {
        this.notificationInput.sendKeys(notification);
    }

    getNotificationInput = function () {
        return this.notificationInput.getAttribute('value');
    }

    setCreatedOnDeviceInput = function (createdOnDevice) {
        this.createdOnDeviceInput.sendKeys(createdOnDevice);
    }

    getCreatedOnDeviceInput = function () {
        return this.createdOnDeviceInput.getAttribute('value');
    }

    setTypeSelect = function (type) {
        this.typeSelect.sendKeys(type);
    }

    getTypeSelect = function () {
        return this.typeSelect.element(by.css('option:checked')).getText();
    }

    typeSelectLastOption = function () {
        this.typeSelect.all(by.tagName('option')).last().click();
    }
    driverSelectLastOption = function () {
        this.driverSelect.all(by.tagName('option')).last().click();
    }

    driverSelectOption = function (option) {
        this.driverSelect.sendKeys(option);
    }

    getDriverSelect = function () {
        return this.driverSelect;
    }

    getDriverSelectedOption = function () {
        return this.driverSelect.element(by.css('option:checked')).getText();
    }

    spotSelectLastOption = function () {
        this.spotSelect.all(by.tagName('option')).last().click();
    }

    spotSelectOption = function (option) {
        this.spotSelect.sendKeys(option);
    }

    getSpotSelect = function () {
        return this.spotSelect;
    }

    getSpotSelectedOption = function () {
        return this.spotSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
