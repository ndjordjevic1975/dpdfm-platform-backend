package rs.co.tntech.busride.web.rest;

import rs.co.tntech.busride.BusRidePlatformBackendApp;

import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.repository.ParentRepository;
import rs.co.tntech.busride.service.ParentService;
import rs.co.tntech.busride.service.dto.ParentDTO;
import rs.co.tntech.busride.service.dto.ParentExtendedDTO;
import rs.co.tntech.busride.service.mapper.ParentExtendedMapper;
import rs.co.tntech.busride.service.mapper.ParentMapper;
import rs.co.tntech.busride.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import rs.co.tntech.busride.domain.enumeration.DeviceType;
import rs.co.tntech.busride.domain.enumeration.SpotType;
/**
 * Test class for the ParentResource REST controller.
 *
 * @see ParentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BusRidePlatformBackendApp.class)
public class ParentResourceIntTest {

    public static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    public static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_HOUSE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_HOUSE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_ZIP_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ZIP_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_LAND_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_LAND_PHONE = "BBBBBBBBBB";

    private static final Integer DEFAULT_NO_OF_STUDENTS = 1;
    private static final Integer UPDATED_NO_OF_STUDENTS = 2;

    private static final String DEFAULT_DEVICE_IDENTIFIER = "AAAAAAAAAA";
    private static final String UPDATED_DEVICE_IDENTIFIER = "BBBBBBBBBB";

    private static final DeviceType DEFAULT_DEVICE_TYPE = DeviceType.ANDROID;
    private static final DeviceType UPDATED_DEVICE_TYPE = DeviceType.IOS;

    private static final String DEFAULT_GCM_SESSION_ID = "AAAAAAAAAA";
    private static final String UPDATED_GCM_SESSION_ID = "BBBBBBBBBB";

    private static final String DEFAULT_ENDPOINT_ARN = "AAAAAAAAAA";
    private static final String UPDATED_ENDPOINT_ARN = "BBBBBBBBBB";
    
    private static final String THIRD_EMAIL = "jhipster_2@localhost";

    @Autowired
    private ParentRepository parentRepository;
    @Autowired
    private ParentMapper parentMapper;
    @Autowired
    private ParentExtendedMapper parentExtendedMapper;
    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
    @Autowired
    private ExceptionTranslator exceptionTranslator;
    @Autowired
    private EntityManager em;
    @Autowired
    private TestSecurityUtil testSecurityUtil;
    @Autowired
    private TestDataUtil testDataUtil;
    @Autowired
    private ParentResource parentResource;

    private MockMvc restParentMockMvc;

    private Parent parent;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.restParentMockMvc = MockMvcBuilders.standaloneSetup(parentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Parent createEntity(EntityManager em) {
        Parent parent = new Parent()
            .address(DEFAULT_ADDRESS)
            .houseNumber(DEFAULT_HOUSE_NUMBER)
            .city(DEFAULT_CITY)
            .zipCode(DEFAULT_ZIP_CODE)
            .state(DEFAULT_STATE)
            .country(DEFAULT_COUNTRY)
            .phone(DEFAULT_PHONE)
            .landPhone(DEFAULT_LAND_PHONE)
            .noOfStudents(DEFAULT_NO_OF_STUDENTS)
            .deviceIdentifier(DEFAULT_DEVICE_IDENTIFIER)
            .deviceType(DEFAULT_DEVICE_TYPE)
            .gcmSessionId(DEFAULT_GCM_SESSION_ID)
            .endpointARN(DEFAULT_ENDPOINT_ARN);
        return parent;
    }

    @Before
    public void initTest() {
        parent = createEntity(em);
    }

    @Test
    @Transactional
    public void createParent() throws Exception {
        int databaseSizeBeforeCreate = parentRepository.findAll().size();

        // Create the Parent
        ParentDTO parentDTO = parentMapper.toDto(parent);
        restParentMockMvc.perform(post("/api/parents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parentDTO)))
            .andExpect(status().isCreated());

        // Validate the Parent in the database
        List<Parent> parentList = parentRepository.findAll();
        assertThat(parentList).hasSize(databaseSizeBeforeCreate + 1);
        Parent testParent = parentList.get(parentList.size() - 1);
        assertThat(testParent.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testParent.getHouseNumber()).isEqualTo(DEFAULT_HOUSE_NUMBER);
        assertThat(testParent.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testParent.getZipCode()).isEqualTo(DEFAULT_ZIP_CODE);
        assertThat(testParent.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testParent.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testParent.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testParent.getLandPhone()).isEqualTo(DEFAULT_LAND_PHONE);
        assertThat(testParent.getNoOfStudents()).isEqualTo(DEFAULT_NO_OF_STUDENTS);
        assertThat(testParent.getDeviceIdentifier()).isEqualTo(DEFAULT_DEVICE_IDENTIFIER);
        assertThat(testParent.getDeviceType()).isEqualTo(DEFAULT_DEVICE_TYPE);
        assertThat(testParent.getGcmSessionId()).isEqualTo(DEFAULT_GCM_SESSION_ID);
        assertThat(testParent.getEndpointARN()).isEqualTo(DEFAULT_ENDPOINT_ARN);
    }

    @Test
    @Transactional
    public void createParentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = parentRepository.findAll().size();

        // Create the Parent with an existing ID
        parent.setId(1L);
        ParentDTO parentDTO = parentMapper.toDto(parent);

        // An entity with an existing ID cannot be created, so this API call must fail
        restParentMockMvc.perform(post("/api/parents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Parent> parentList = parentRepository.findAll();
        assertThat(parentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllParents() throws Exception {
        // Initialize the database
        parentRepository.saveAndFlush(parent);

        // Get all the parentList
        restParentMockMvc.perform(get("/api/parents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(parent.getId().intValue())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].houseNumber").value(hasItem(DEFAULT_HOUSE_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].zipCode").value(hasItem(DEFAULT_ZIP_CODE.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].landPhone").value(hasItem(DEFAULT_LAND_PHONE.toString())))
            .andExpect(jsonPath("$.[*].noOfStudents").value(hasItem(DEFAULT_NO_OF_STUDENTS)))
            .andExpect(jsonPath("$.[*].deviceIdentifier").value(hasItem(DEFAULT_DEVICE_IDENTIFIER.toString())))
            .andExpect(jsonPath("$.[*].deviceType").value(hasItem(DEFAULT_DEVICE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].gcmSessionId").value(hasItem(DEFAULT_GCM_SESSION_ID.toString())))
            .andExpect(jsonPath("$.[*].endpointARN").value(hasItem(DEFAULT_ENDPOINT_ARN.toString())));
    }

    @Test
    @Transactional
    public void getParent() throws Exception {
        // Initialize the database
        parentRepository.saveAndFlush(parent);

        // Get the parent
        restParentMockMvc.perform(get("/api/parents/{id}", parent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(parent.getId().intValue()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.houseNumber").value(DEFAULT_HOUSE_NUMBER.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.zipCode").value(DEFAULT_ZIP_CODE.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.landPhone").value(DEFAULT_LAND_PHONE.toString()))
            .andExpect(jsonPath("$.noOfStudents").value(DEFAULT_NO_OF_STUDENTS))
            .andExpect(jsonPath("$.deviceIdentifier").value(DEFAULT_DEVICE_IDENTIFIER.toString()))
            .andExpect(jsonPath("$.deviceType").value(DEFAULT_DEVICE_TYPE.toString()))
            .andExpect(jsonPath("$.gcmSessionId").value(DEFAULT_GCM_SESSION_ID.toString()))
            .andExpect(jsonPath("$.endpointARN").value(DEFAULT_ENDPOINT_ARN.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingParent() throws Exception {
        // Get the parent
        restParentMockMvc.perform(get("/api/parents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateParent() throws Exception {
        // Initialize the database
        parentRepository.saveAndFlush(parent);
        int databaseSizeBeforeUpdate = parentRepository.findAll().size();

        // Update the parent
        Parent updatedParent = parentRepository.findOne(parent.getId());
        updatedParent
            .address(UPDATED_ADDRESS)
            .houseNumber(UPDATED_HOUSE_NUMBER)
            .city(UPDATED_CITY)
            .zipCode(UPDATED_ZIP_CODE)
            .state(UPDATED_STATE)
            .country(UPDATED_COUNTRY)
            .phone(UPDATED_PHONE)
            .landPhone(UPDATED_LAND_PHONE)
            .noOfStudents(UPDATED_NO_OF_STUDENTS)
            .deviceIdentifier(UPDATED_DEVICE_IDENTIFIER)
            .deviceType(UPDATED_DEVICE_TYPE)
            .gcmSessionId(UPDATED_GCM_SESSION_ID)
            .endpointARN(UPDATED_ENDPOINT_ARN);
        ParentDTO parentDTO = parentMapper.toDto(updatedParent);

        restParentMockMvc.perform(put("/api/parents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parentDTO)))
            .andExpect(status().isOk());

        // Validate the Parent in the database
        List<Parent> parentList = parentRepository.findAll();
        assertThat(parentList).hasSize(databaseSizeBeforeUpdate);
        Parent testParent = parentList.get(parentList.size() - 1);
        assertThat(testParent.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testParent.getHouseNumber()).isEqualTo(UPDATED_HOUSE_NUMBER);
        assertThat(testParent.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testParent.getZipCode()).isEqualTo(UPDATED_ZIP_CODE);
        assertThat(testParent.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testParent.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testParent.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testParent.getLandPhone()).isEqualTo(UPDATED_LAND_PHONE);
        assertThat(testParent.getNoOfStudents()).isEqualTo(UPDATED_NO_OF_STUDENTS);
        assertThat(testParent.getDeviceIdentifier()).isEqualTo(UPDATED_DEVICE_IDENTIFIER);
        assertThat(testParent.getDeviceType()).isEqualTo(UPDATED_DEVICE_TYPE);
        assertThat(testParent.getGcmSessionId()).isEqualTo(UPDATED_GCM_SESSION_ID);
        assertThat(testParent.getEndpointARN()).isEqualTo(UPDATED_ENDPOINT_ARN);
    }

    @Test
    @Transactional
    public void updateNonExistingParent() throws Exception {
        int databaseSizeBeforeUpdate = parentRepository.findAll().size();

        // Create the Parent
        ParentDTO parentDTO = parentMapper.toDto(parent);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restParentMockMvc.perform(put("/api/parents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parentDTO)))
            .andExpect(status().isCreated());

        // Validate the Parent in the database
        List<Parent> parentList = parentRepository.findAll();
        assertThat(parentList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteParent() throws Exception {
        // Initialize the database
        parentRepository.saveAndFlush(parent);
        int databaseSizeBeforeDelete = parentRepository.findAll().size();

        // Get the parent
        restParentMockMvc.perform(delete("/api/parents/{id}", parent.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Parent> parentList = parentRepository.findAll();
        assertThat(parentList).hasSize(databaseSizeBeforeDelete - 1);
    }
    
    @Test
    @Transactional
    public void createParentsList() throws Exception {
        // Initialize the database
        int databaseSizeBeforeCreate = parentRepository.findAll().size();

        testSecurityUtil.login(TestSecurityUtil.USER_SCHOOL_ADMIN);
        
        List<ParentExtendedDTO> parents = new ArrayList<>();
        // primary parent
        ParentExtendedDTO parentDTO = parentExtendedMapper.toDto(createEntity(em));
        parentDTO.setFirstName(UserResourceIntTest.DEFAULT_FIRSTNAME);
        parentDTO.setLastName(UserResourceIntTest.DEFAULT_LASTNAME);
        parentDTO.setEmail(UserResourceIntTest.DEFAULT_EMAIL);
        parentDTO.setLongitude(SpotResourceIntTest.DEFAULT_GPS_LONGITUDE);
        parentDTO.setLatitude(SpotResourceIntTest.DEFAULT_GPS_LATITUDE);
        parents.add(parentDTO);
        // 2. parent
        parentDTO = parentExtendedMapper.toDto(createEntity(em));
        parentDTO.setFirstName(UserResourceIntTest.UPDATED_FIRSTNAME);
        parentDTO.setLastName(UserResourceIntTest.UPDATED_LASTNAME);
        parentDTO.setEmail(UserResourceIntTest.UPDATED_EMAIL);
		parents.add(parentDTO);
		// 3. parent
		parentDTO = parentExtendedMapper.toDto(createEntity(em));
        parentDTO.setFirstName(UserResourceIntTest.UPDATED_FIRSTNAME);
        parentDTO.setLastName(UserResourceIntTest.UPDATED_LASTNAME);
        parentDTO.setEmail(THIRD_EMAIL);
		parents.add(parentDTO);
        
        restParentMockMvc.perform(post("/api/parents/list")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parents)))
            .andExpect(status().isCreated());
        
//        ParentExtendedDTO parentDTO = parentExtendedMapper.toDto(updatedParent);
//        parentDTO.setFirstName(UserResourceIntTest.UPDATED_FIRSTNAME);
//        parentDTO.setLastName(UserResourceIntTest.UPDATED_LASTNAME);
//        parentDTO.setEmail(UserResourceIntTest.UPDATED_EMAIL);
//        parentDTO.setLongitude(SpotResourceIntTest.UPDATED_GPS_LONGITUDE);
//        parentDTO.setLatitude(SpotResourceIntTest.UPDATED_GPS_LATITUDE);


        // Validate the Parent in the database
        List<Parent> parentList = parentRepository.findAll();
		assertThat(parentList).hasSize(databaseSizeBeforeCreate + 3);
        
        Parent testPrimaryParent = parentList.get(parentList.size() - 3);
        validatePrimaryParent(testPrimaryParent);
        validateNonPrimaryParent(parentList.get(parentList.size() - 2), testPrimaryParent.getId(), UserResourceIntTest.UPDATED_EMAIL);
        validateNonPrimaryParent(parentList.get(parentList.size() - 1), testPrimaryParent.getId(), THIRD_EMAIL);
    }
    
    private void validatePrimaryParent(Parent testPrimaryParent) {
    	assertThat(testPrimaryParent.getUser().getFirstName()).isEqualTo(UserResourceIntTest.DEFAULT_FIRSTNAME);
		assertThat(testPrimaryParent.getUser().getLastName()).isEqualTo(UserResourceIntTest.DEFAULT_LASTNAME);
		assertThat(testPrimaryParent.getUser().getEmail()).isEqualTo(UserResourceIntTest.DEFAULT_EMAIL);
        assertThat(testPrimaryParent.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testPrimaryParent.getHouseNumber()).isEqualTo(DEFAULT_HOUSE_NUMBER);
        assertThat(testPrimaryParent.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testPrimaryParent.getZipCode()).isEqualTo(DEFAULT_ZIP_CODE);
        assertThat(testPrimaryParent.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testPrimaryParent.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testPrimaryParent.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testPrimaryParent.getLandPhone()).isEqualTo(DEFAULT_LAND_PHONE);
        assertThat(testPrimaryParent.getNoOfStudents()).isEqualTo(DEFAULT_NO_OF_STUDENTS);
        assertThat(testPrimaryParent.getDeviceIdentifier()).isEqualTo(DEFAULT_DEVICE_IDENTIFIER);
        assertThat(testPrimaryParent.getDeviceType()).isEqualTo(DEFAULT_DEVICE_TYPE);
        assertThat(testPrimaryParent.getGcmSessionId()).isEqualTo(DEFAULT_GCM_SESSION_ID);
        assertThat(testPrimaryParent.getSpot().getGpsLatitude()).isEqualTo(SpotResourceIntTest.DEFAULT_GPS_LATITUDE);
        assertThat(testPrimaryParent.getSpot().getGpsLongitude()).isEqualTo(SpotResourceIntTest.DEFAULT_GPS_LONGITUDE);
        assertThat(testPrimaryParent.getSpot().getType()).isEqualTo(SpotType.PICKUP_DROPOFF);
        assertThat(testPrimaryParent.getLinkToPrimary()).isNull();
    }
    
    private void validateNonPrimaryParent(Parent testParent, Long primaryParentId, String email) {
    	assertThat(testParent.getUser().getFirstName()).isEqualTo(UserResourceIntTest.UPDATED_FIRSTNAME);
		assertThat(testParent.getUser().getLastName()).isEqualTo(UserResourceIntTest.UPDATED_LASTNAME);
		assertThat(testParent.getUser().getEmail()).isEqualTo(email);
        assertThat(testParent.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testParent.getHouseNumber()).isEqualTo(DEFAULT_HOUSE_NUMBER);
        assertThat(testParent.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testParent.getZipCode()).isEqualTo(DEFAULT_ZIP_CODE);
        assertThat(testParent.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testParent.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testParent.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testParent.getLandPhone()).isEqualTo(DEFAULT_LAND_PHONE);
        assertThat(testParent.getNoOfStudents()).isNull();
        assertThat(testParent.getDeviceIdentifier()).isEqualTo(DEFAULT_DEVICE_IDENTIFIER);
        assertThat(testParent.getDeviceType()).isEqualTo(DEFAULT_DEVICE_TYPE);
        assertThat(testParent.getGcmSessionId()).isEqualTo(DEFAULT_GCM_SESSION_ID);
        assertThat(testParent.getSpot()).isNull();
        assertThat(testParent.getLinkToPrimary()).isNotNull();
        assertThat(testParent.getLinkToPrimary().getId()).isEqualByComparingTo(primaryParentId);
    }

    @Test
    @Transactional
    public void updateCurrentParent() throws Exception {
        // Initialize the database
        Parent tParent = testDataUtil.createParent();
        int databaseSizeBeforeUpdate = parentRepository.findAll().size();

        testSecurityUtil.login(tParent.getUser().getLogin());
        
        // Update the parent
        Parent updatedParent = parentRepository.findOne(tParent.getId());
        updatedParent
            .address(UPDATED_ADDRESS)
            .houseNumber(UPDATED_HOUSE_NUMBER)
            .city(UPDATED_CITY)
            .zipCode(UPDATED_ZIP_CODE)
            .state(UPDATED_STATE)
            .country(UPDATED_COUNTRY)
            .phone(UPDATED_PHONE)
            .landPhone(UPDATED_LAND_PHONE)
            .noOfStudents(UPDATED_NO_OF_STUDENTS)
            .deviceIdentifier(UPDATED_DEVICE_IDENTIFIER)
            .deviceType(UPDATED_DEVICE_TYPE)
            .gcmSessionId(UPDATED_GCM_SESSION_ID)
            .endpointARN(UPDATED_ENDPOINT_ARN);
        ParentExtendedDTO parentDTO = parentExtendedMapper.toDto(updatedParent);
        parentDTO.setFirstName(UserResourceIntTest.UPDATED_FIRSTNAME);
        parentDTO.setLastName(UserResourceIntTest.UPDATED_LASTNAME);
        parentDTO.setEmail(UserResourceIntTest.UPDATED_EMAIL);
        parentDTO.setLongitude(SpotResourceIntTest.UPDATED_GPS_LONGITUDE);
        parentDTO.setLatitude(SpotResourceIntTest.UPDATED_GPS_LATITUDE);

        restParentMockMvc.perform(put("/api/parents/current-parent")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parentDTO)))
            .andExpect(status().isOk());

        // Validate the Parent in the database
        List<Parent> parentList = parentRepository.findAll();
        assertThat(parentList).hasSize(databaseSizeBeforeUpdate);
        Parent testParent = parentList.get(parentList.size() - 1);
        assertThat(testParent.getId()).isEqualTo(tParent.getId());
		// assertThat(testParent.getUser().getFirstName()).isEqualTo(UserResourceIntTest.UPDATED_FIRSTNAME);
		// assertThat(testParent.getUser().getLastName()).isEqualTo(UserResourceIntTest.UPDATED_LASTNAME);
		// assertThat(testParent.getUser().getEmail()).isEqualTo(UserResourceIntTest.UPDATED_EMAIL);
        assertThat(testParent.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testParent.getHouseNumber()).isEqualTo(UPDATED_HOUSE_NUMBER);
        assertThat(testParent.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testParent.getZipCode()).isEqualTo(UPDATED_ZIP_CODE);
        assertThat(testParent.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testParent.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testParent.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testParent.getLandPhone()).isEqualTo(UPDATED_LAND_PHONE);
        assertThat(testParent.getNoOfStudents()).isEqualTo(UPDATED_NO_OF_STUDENTS);
        assertThat(testParent.getDeviceIdentifier()).isEqualTo(UPDATED_DEVICE_IDENTIFIER);
        assertThat(testParent.getDeviceType()).isEqualTo(UPDATED_DEVICE_TYPE);
        assertThat(testParent.getGcmSessionId()).isEqualTo(UPDATED_GCM_SESSION_ID);
        assertThat(testParent.getEndpointARN()).isEqualTo(UPDATED_ENDPOINT_ARN);
        assertThat(testParent.getSpot().getGpsLatitude()).isEqualTo(SpotResourceIntTest.UPDATED_GPS_LATITUDE);
        assertThat(testParent.getSpot().getGpsLongitude()).isEqualTo(SpotResourceIntTest.UPDATED_GPS_LONGITUDE);
        assertThat(testParent.getSpot().getType()).isEqualTo(SpotType.PICKUP_DROPOFF);
    }
    
    @Test
    @Transactional
    public void getCurrentParent() throws Exception {
        // Initialize the database
        Parent tParent = testDataUtil.createParent();
        
        testSecurityUtil.login(tParent.getUser().getLogin());

        // Get the parent
        restParentMockMvc.perform(get("/api/parents/current-parent"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tParent.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(UserResourceIntTest.DEFAULT_FIRSTNAME))
            .andExpect(jsonPath("$.lastName").value(UserResourceIntTest.DEFAULT_LASTNAME))
            .andExpect(jsonPath("$.email").value(UserResourceIntTest.DEFAULT_EMAIL))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.houseNumber").value(DEFAULT_HOUSE_NUMBER.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.zipCode").value(DEFAULT_ZIP_CODE.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.landPhone").value(DEFAULT_LAND_PHONE.toString()))
            .andExpect(jsonPath("$.noOfStudents").value(DEFAULT_NO_OF_STUDENTS))
            .andExpect(jsonPath("$.deviceIdentifier").value(DEFAULT_DEVICE_IDENTIFIER.toString()))
            .andExpect(jsonPath("$.deviceType").value(DEFAULT_DEVICE_TYPE.toString()))
            .andExpect(jsonPath("$.gcmSessionId").value(DEFAULT_GCM_SESSION_ID.toString()))
            .andExpect(jsonPath("$.latitude").value(SpotResourceIntTest.DEFAULT_GPS_LATITUDE))
            .andExpect(jsonPath("$.longitude").value(SpotResourceIntTest.DEFAULT_GPS_LONGITUDE));
    }

    
    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Parent.class);
        Parent parent1 = new Parent();
        parent1.setId(1L);
        Parent parent2 = new Parent();
        parent2.setId(parent1.getId());
        assertThat(parent1).isEqualTo(parent2);
        parent2.setId(2L);
        assertThat(parent1).isNotEqualTo(parent2);
        parent1.setId(null);
        assertThat(parent1).isNotEqualTo(parent2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ParentDTO.class);
        ParentDTO parentDTO1 = new ParentDTO();
        parentDTO1.setId(1L);
        ParentDTO parentDTO2 = new ParentDTO();
        assertThat(parentDTO1).isNotEqualTo(parentDTO2);
        parentDTO2.setId(parentDTO1.getId());
        assertThat(parentDTO1).isEqualTo(parentDTO2);
        parentDTO2.setId(2L);
        assertThat(parentDTO1).isNotEqualTo(parentDTO2);
        parentDTO1.setId(null);
        assertThat(parentDTO1).isNotEqualTo(parentDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(parentMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(parentMapper.fromId(null)).isNull();
    }
}
