package rs.co.tntech.busride.web.rest;

import rs.co.tntech.busride.BusRidePlatformBackendApp;

import rs.co.tntech.busride.domain.ParentNotification;
import rs.co.tntech.busride.repository.ParentNotificationRepository;
import rs.co.tntech.busride.service.ParentNotificationService;
import rs.co.tntech.busride.service.dto.ParentNotificationDTO;
import rs.co.tntech.busride.service.mapper.ParentNotificationMapper;
import rs.co.tntech.busride.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import rs.co.tntech.busride.domain.enumeration.ParentNotificationType;
/**
 * Test class for the ParentNotificationResource REST controller.
 *
 * @see ParentNotificationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BusRidePlatformBackendApp.class)
public class ParentNotificationResourceIntTest {

    private static final String DEFAULT_NOTIFICATION = "AAAAAAAAAA";
    private static final String UPDATED_NOTIFICATION = "BBBBBBBBBB";

    private static final Instant DEFAULT_TIMESTAMP = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TIMESTAMP = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final ParentNotificationType DEFAULT_TYPE = ParentNotificationType.ANNOUNCEMENT;
    private static final ParentNotificationType UPDATED_TYPE = ParentNotificationType.POLICY_CHANGE_ANNOUNCEMENT;

    @Autowired
    private ParentNotificationRepository parentNotificationRepository;

    @Autowired
    private ParentNotificationMapper parentNotificationMapper;

    @Autowired
    private ParentNotificationService parentNotificationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restParentNotificationMockMvc;

    private ParentNotification parentNotification;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ParentNotificationResource parentNotificationResource = new ParentNotificationResource(parentNotificationService);
        this.restParentNotificationMockMvc = MockMvcBuilders.standaloneSetup(parentNotificationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ParentNotification createEntity(EntityManager em) {
        ParentNotification parentNotification = new ParentNotification()
            .notification(DEFAULT_NOTIFICATION)
            .timestamp(DEFAULT_TIMESTAMP)
            .type(DEFAULT_TYPE);
        return parentNotification;
    }

    @Before
    public void initTest() {
        parentNotification = createEntity(em);
    }

    @Test
    @Transactional
    public void createParentNotification() throws Exception {
        int databaseSizeBeforeCreate = parentNotificationRepository.findAll().size();

        // Create the ParentNotification
        ParentNotificationDTO parentNotificationDTO = parentNotificationMapper.toDto(parentNotification);
        restParentNotificationMockMvc.perform(post("/api/parent-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parentNotificationDTO)))
            .andExpect(status().isCreated());

        // Validate the ParentNotification in the database
        List<ParentNotification> parentNotificationList = parentNotificationRepository.findAll();
        assertThat(parentNotificationList).hasSize(databaseSizeBeforeCreate + 1);
        ParentNotification testParentNotification = parentNotificationList.get(parentNotificationList.size() - 1);
        assertThat(testParentNotification.getNotification()).isEqualTo(DEFAULT_NOTIFICATION);
        assertThat(testParentNotification.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testParentNotification.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createParentNotificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = parentNotificationRepository.findAll().size();

        // Create the ParentNotification with an existing ID
        parentNotification.setId(1L);
        ParentNotificationDTO parentNotificationDTO = parentNotificationMapper.toDto(parentNotification);

        // An entity with an existing ID cannot be created, so this API call must fail
        restParentNotificationMockMvc.perform(post("/api/parent-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parentNotificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ParentNotification> parentNotificationList = parentNotificationRepository.findAll();
        assertThat(parentNotificationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllParentNotifications() throws Exception {
        // Initialize the database
        parentNotificationRepository.saveAndFlush(parentNotification);

        // Get all the parentNotificationList
        restParentNotificationMockMvc.perform(get("/api/parent-notifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(parentNotification.getId().intValue())))
            .andExpect(jsonPath("$.[*].notification").value(hasItem(DEFAULT_NOTIFICATION.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getParentNotification() throws Exception {
        // Initialize the database
        parentNotificationRepository.saveAndFlush(parentNotification);

        // Get the parentNotification
        restParentNotificationMockMvc.perform(get("/api/parent-notifications/{id}", parentNotification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(parentNotification.getId().intValue()))
            .andExpect(jsonPath("$.notification").value(DEFAULT_NOTIFICATION.toString()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingParentNotification() throws Exception {
        // Get the parentNotification
        restParentNotificationMockMvc.perform(get("/api/parent-notifications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateParentNotification() throws Exception {
        // Initialize the database
        parentNotificationRepository.saveAndFlush(parentNotification);
        int databaseSizeBeforeUpdate = parentNotificationRepository.findAll().size();

        // Update the parentNotification
        ParentNotification updatedParentNotification = parentNotificationRepository.findOne(parentNotification.getId());
        updatedParentNotification
            .notification(UPDATED_NOTIFICATION)
            .timestamp(UPDATED_TIMESTAMP)
            .type(UPDATED_TYPE);
        ParentNotificationDTO parentNotificationDTO = parentNotificationMapper.toDto(updatedParentNotification);

        restParentNotificationMockMvc.perform(put("/api/parent-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parentNotificationDTO)))
            .andExpect(status().isOk());

        // Validate the ParentNotification in the database
        List<ParentNotification> parentNotificationList = parentNotificationRepository.findAll();
        assertThat(parentNotificationList).hasSize(databaseSizeBeforeUpdate);
        ParentNotification testParentNotification = parentNotificationList.get(parentNotificationList.size() - 1);
        assertThat(testParentNotification.getNotification()).isEqualTo(UPDATED_NOTIFICATION);
        assertThat(testParentNotification.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testParentNotification.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingParentNotification() throws Exception {
        int databaseSizeBeforeUpdate = parentNotificationRepository.findAll().size();

        // Create the ParentNotification
        ParentNotificationDTO parentNotificationDTO = parentNotificationMapper.toDto(parentNotification);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restParentNotificationMockMvc.perform(put("/api/parent-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parentNotificationDTO)))
            .andExpect(status().isCreated());

        // Validate the ParentNotification in the database
        List<ParentNotification> parentNotificationList = parentNotificationRepository.findAll();
        assertThat(parentNotificationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteParentNotification() throws Exception {
        // Initialize the database
        parentNotificationRepository.saveAndFlush(parentNotification);
        int databaseSizeBeforeDelete = parentNotificationRepository.findAll().size();

        // Get the parentNotification
        restParentNotificationMockMvc.perform(delete("/api/parent-notifications/{id}", parentNotification.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ParentNotification> parentNotificationList = parentNotificationRepository.findAll();
        assertThat(parentNotificationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ParentNotification.class);
        ParentNotification parentNotification1 = new ParentNotification();
        parentNotification1.setId(1L);
        ParentNotification parentNotification2 = new ParentNotification();
        parentNotification2.setId(parentNotification1.getId());
        assertThat(parentNotification1).isEqualTo(parentNotification2);
        parentNotification2.setId(2L);
        assertThat(parentNotification1).isNotEqualTo(parentNotification2);
        parentNotification1.setId(null);
        assertThat(parentNotification1).isNotEqualTo(parentNotification2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ParentNotificationDTO.class);
        ParentNotificationDTO parentNotificationDTO1 = new ParentNotificationDTO();
        parentNotificationDTO1.setId(1L);
        ParentNotificationDTO parentNotificationDTO2 = new ParentNotificationDTO();
        assertThat(parentNotificationDTO1).isNotEqualTo(parentNotificationDTO2);
        parentNotificationDTO2.setId(parentNotificationDTO1.getId());
        assertThat(parentNotificationDTO1).isEqualTo(parentNotificationDTO2);
        parentNotificationDTO2.setId(2L);
        assertThat(parentNotificationDTO1).isNotEqualTo(parentNotificationDTO2);
        parentNotificationDTO1.setId(null);
        assertThat(parentNotificationDTO1).isNotEqualTo(parentNotificationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(parentNotificationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(parentNotificationMapper.fromId(null)).isNull();
    }
}
