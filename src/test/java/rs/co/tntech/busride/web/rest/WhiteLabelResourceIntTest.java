package rs.co.tntech.busride.web.rest;

import rs.co.tntech.busride.BusRidePlatformBackendApp;
import rs.co.tntech.busride.domain.WhiteLabel;
import rs.co.tntech.busride.repository.WhiteLabelRepository;
import rs.co.tntech.busride.service.dto.WhiteLabelDTO;
import rs.co.tntech.busride.service.mapper.WhiteLabelMapper;
import rs.co.tntech.busride.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the WhiteLabelResource REST controller.
 *
 * @see WhiteLabelResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BusRidePlatformBackendApp.class)
public class WhiteLabelResourceIntTest {

    private static final byte[] DEFAULT_PARENT_WHITE_LABEL = TestUtil.createByteArray(2, "00");
    private static final byte[] UPDATED_PARENT_WHITE_LABEL = TestUtil.createByteArray(2, "01");
    private static final String DEFAULT_PARENT_WHITE_LABEL_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PARENT_WHITE_LABEL_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_DRIVER_WHITE_LABEL = TestUtil.createByteArray(2, "10");
    private static final byte[] UPDATED_DRIVER_WHITE_LABEL = TestUtil.createByteArray(2, "11");
    private static final String DEFAULT_DRIVER_WHITE_LABEL_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_DRIVER_WHITE_LABEL_CONTENT_TYPE = "image/png";

    @Autowired
    private WhiteLabelRepository whiteLabelRepository;
    @Autowired
    private WhiteLabelMapper whiteLabelMapper;
    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
    @Autowired
    private ExceptionTranslator exceptionTranslator;
    @Autowired
    private EntityManager em;
    @Autowired
    private TestSecurityUtil testSecurityUtil;
    @Autowired
    private WhiteLabelResource whiteLabelResource ;
    @Autowired
    private TestDataUtil testDataUtil;

    private MockMvc restWhiteLabelMockMvc;

    private WhiteLabel whiteLabel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        
        ResourceHttpMessageConverter resourceMessageConverter = new ResourceHttpMessageConverter();
        
        this.restWhiteLabelMockMvc = MockMvcBuilders.standaloneSetup(whiteLabelResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(resourceMessageConverter, jacksonMessageConverter).build();
    }
    

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WhiteLabel createEntity(EntityManager em) {
        WhiteLabel whiteLabel = new WhiteLabel()
            .parentWhiteLabel(DEFAULT_PARENT_WHITE_LABEL)
            .parentWhiteLabelContentType(DEFAULT_PARENT_WHITE_LABEL_CONTENT_TYPE)
            .driverWhiteLabel(DEFAULT_DRIVER_WHITE_LABEL)
            .driverWhiteLabelContentType(DEFAULT_DRIVER_WHITE_LABEL_CONTENT_TYPE);
        return whiteLabel;
    }

    @Before
    public void initTest() {
        whiteLabel = createEntity(em);
    }

    @Test
    @Transactional
    public void createWhiteLabel() throws Exception {
        int databaseSizeBeforeCreate = whiteLabelRepository.findAll().size();

        // Create the WhiteLabel
        WhiteLabelDTO whiteLabelDTO = whiteLabelMapper.toDto(whiteLabel);
        restWhiteLabelMockMvc.perform(post("/api/white-labels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(whiteLabelDTO)))
            .andExpect(status().isCreated());

        // Validate the WhiteLabel in the database
        List<WhiteLabel> whiteLabelList = whiteLabelRepository.findAll();
        assertThat(whiteLabelList).hasSize(databaseSizeBeforeCreate + 1);
        WhiteLabel testWhiteLabel = whiteLabelList.get(whiteLabelList.size() - 1);
        assertThat(testWhiteLabel.getParentWhiteLabel()).isEqualTo(DEFAULT_PARENT_WHITE_LABEL);
        assertThat(testWhiteLabel.getParentWhiteLabelContentType()).isEqualTo(DEFAULT_PARENT_WHITE_LABEL_CONTENT_TYPE);
        assertThat(testWhiteLabel.getDriverWhiteLabel()).isEqualTo(DEFAULT_DRIVER_WHITE_LABEL);
        assertThat(testWhiteLabel.getDriverWhiteLabelContentType()).isEqualTo(DEFAULT_DRIVER_WHITE_LABEL_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createWhiteLabelWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = whiteLabelRepository.findAll().size();

        // Create the WhiteLabel with an existing ID
        whiteLabel.setId(1L);
        WhiteLabelDTO whiteLabelDTO = whiteLabelMapper.toDto(whiteLabel);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWhiteLabelMockMvc.perform(post("/api/white-labels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(whiteLabelDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<WhiteLabel> whiteLabelList = whiteLabelRepository.findAll();
        assertThat(whiteLabelList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllWhiteLabels() throws Exception {
        // Initialize the database
        whiteLabelRepository.saveAndFlush(whiteLabel);

        // Get all the whiteLabelList
        restWhiteLabelMockMvc.perform(get("/api/white-labels?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(whiteLabel.getId().intValue())))
            .andExpect(jsonPath("$.[*].parentWhiteLabelContentType").value(hasItem(DEFAULT_PARENT_WHITE_LABEL_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].parentWhiteLabel").value(hasItem(Base64Utils.encodeToString(DEFAULT_PARENT_WHITE_LABEL))))
            .andExpect(jsonPath("$.[*].driverWhiteLabelContentType").value(hasItem(DEFAULT_DRIVER_WHITE_LABEL_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].driverWhiteLabel").value(hasItem(Base64Utils.encodeToString(DEFAULT_DRIVER_WHITE_LABEL))));
    }

    @Test
    @Transactional
    public void getWhiteLabel() throws Exception {
        // Initialize the database
        whiteLabelRepository.saveAndFlush(whiteLabel);

        // Get the whiteLabel
        restWhiteLabelMockMvc.perform(get("/api/white-labels/{id}", whiteLabel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(whiteLabel.getId().intValue()))
            .andExpect(jsonPath("$.parentWhiteLabelContentType").value(DEFAULT_PARENT_WHITE_LABEL_CONTENT_TYPE))
            .andExpect(jsonPath("$.parentWhiteLabel").value(Base64Utils.encodeToString(DEFAULT_PARENT_WHITE_LABEL)))
            .andExpect(jsonPath("$.driverWhiteLabelContentType").value(DEFAULT_DRIVER_WHITE_LABEL_CONTENT_TYPE))
            .andExpect(jsonPath("$.driverWhiteLabel").value(Base64Utils.encodeToString(DEFAULT_DRIVER_WHITE_LABEL)));
    }

    @Test
    @Transactional
    public void getNonExistingWhiteLabel() throws Exception {
        // Get the whiteLabel
        restWhiteLabelMockMvc.perform(get("/api/white-labels/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWhiteLabel() throws Exception {
        // Initialize the database
        whiteLabelRepository.saveAndFlush(whiteLabel);
        int databaseSizeBeforeUpdate = whiteLabelRepository.findAll().size();

        // Update the whiteLabel
        WhiteLabel updatedWhiteLabel = whiteLabelRepository.findOne(whiteLabel.getId());
        updatedWhiteLabel
            .parentWhiteLabel(UPDATED_PARENT_WHITE_LABEL)
            .parentWhiteLabelContentType(UPDATED_PARENT_WHITE_LABEL_CONTENT_TYPE)
            .driverWhiteLabel(UPDATED_DRIVER_WHITE_LABEL)
            .driverWhiteLabelContentType(UPDATED_DRIVER_WHITE_LABEL_CONTENT_TYPE);
        WhiteLabelDTO whiteLabelDTO = whiteLabelMapper.toDto(updatedWhiteLabel);

        restWhiteLabelMockMvc.perform(put("/api/white-labels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(whiteLabelDTO)))
            .andExpect(status().isOk());

        // Validate the WhiteLabel in the database
        List<WhiteLabel> whiteLabelList = whiteLabelRepository.findAll();
        assertThat(whiteLabelList).hasSize(databaseSizeBeforeUpdate);
        WhiteLabel testWhiteLabel = whiteLabelList.get(whiteLabelList.size() - 1);
        assertThat(testWhiteLabel.getParentWhiteLabel()).isEqualTo(UPDATED_PARENT_WHITE_LABEL);
        assertThat(testWhiteLabel.getParentWhiteLabelContentType()).isEqualTo(UPDATED_PARENT_WHITE_LABEL_CONTENT_TYPE);
        assertThat(testWhiteLabel.getDriverWhiteLabel()).isEqualTo(UPDATED_DRIVER_WHITE_LABEL);
        assertThat(testWhiteLabel.getDriverWhiteLabelContentType()).isEqualTo(UPDATED_DRIVER_WHITE_LABEL_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingWhiteLabel() throws Exception {
        int databaseSizeBeforeUpdate = whiteLabelRepository.findAll().size();

        // Create the WhiteLabel
        WhiteLabelDTO whiteLabelDTO = whiteLabelMapper.toDto(whiteLabel);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restWhiteLabelMockMvc.perform(put("/api/white-labels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(whiteLabelDTO)))
            .andExpect(status().isCreated());

        // Validate the WhiteLabel in the database
        List<WhiteLabel> whiteLabelList = whiteLabelRepository.findAll();
        assertThat(whiteLabelList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteWhiteLabel() throws Exception {
        // Initialize the database
        whiteLabelRepository.saveAndFlush(whiteLabel);
        int databaseSizeBeforeDelete = whiteLabelRepository.findAll().size();

        // Get the whiteLabel
        restWhiteLabelMockMvc.perform(delete("/api/white-labels/{id}", whiteLabel.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<WhiteLabel> whiteLabelList = whiteLabelRepository.findAll();
        assertThat(whiteLabelList).hasSize(databaseSizeBeforeDelete - 1);
    }
    
    @Test
	@Transactional
	public void getWhiteLabelBySchoolIdForParent() throws Exception {
		WhiteLabel tWhiteLabel = testDataUtil.createWhiteLabel();

		testSecurityUtil.login(TestSecurityUtil.USER_PARENT504);

		// Get the whiteLabel
		restWhiteLabelMockMvc.perform(get("/api/white-labels/by-school-id/{schoolId}", tWhiteLabel.getSchool().getId())
			.accept(MediaType.IMAGE_JPEG))
			.andExpect(status().isOk())
			.andExpect(content().contentType(DEFAULT_PARENT_WHITE_LABEL_CONTENT_TYPE))
			.andExpect(content().bytes(DEFAULT_PARENT_WHITE_LABEL));
	}
    
	@Test
	@Transactional
	public void getWhiteLabelBySchoolIdForDriver() throws Exception {
		WhiteLabel tWhiteLabel = testDataUtil.createWhiteLabel();

		testSecurityUtil.login(TestSecurityUtil.USER_DRIVER1);

		// Get the whiteLabel
		restWhiteLabelMockMvc.perform(get("/api/white-labels/by-school-id/{schoolId}", tWhiteLabel.getSchool().getId())
			.accept(MediaType.IMAGE_JPEG))
			.andExpect(status().isOk())
			.andExpect(content().contentType(DEFAULT_DRIVER_WHITE_LABEL_CONTENT_TYPE))
			.andExpect(content().bytes(DEFAULT_DRIVER_WHITE_LABEL));
	}
    
	@Test
	@Transactional
	public void getWhiteLabelForInvalidUserRole() throws Exception {
		WhiteLabel tWhiteLabel = testDataUtil.createWhiteLabel();

		testSecurityUtil.login(TestSecurityUtil.USER_ADMIN);

		// Get the whiteLabel
		restWhiteLabelMockMvc.perform(get("/api/white-labels/by-school-id/{schoolId}", tWhiteLabel.getSchool().getId()))
			.andExpect(status().isNoContent());
	}

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WhiteLabel.class);
        WhiteLabel whiteLabel1 = new WhiteLabel();
        whiteLabel1.setId(1L);
        WhiteLabel whiteLabel2 = new WhiteLabel();
        whiteLabel2.setId(whiteLabel1.getId());
        assertThat(whiteLabel1).isEqualTo(whiteLabel2);
        whiteLabel2.setId(2L);
        assertThat(whiteLabel1).isNotEqualTo(whiteLabel2);
        whiteLabel1.setId(null);
        assertThat(whiteLabel1).isNotEqualTo(whiteLabel2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(WhiteLabelDTO.class);
        WhiteLabelDTO whiteLabelDTO1 = new WhiteLabelDTO();
        whiteLabelDTO1.setId(1L);
        WhiteLabelDTO whiteLabelDTO2 = new WhiteLabelDTO();
        assertThat(whiteLabelDTO1).isNotEqualTo(whiteLabelDTO2);
        whiteLabelDTO2.setId(whiteLabelDTO1.getId());
        assertThat(whiteLabelDTO1).isEqualTo(whiteLabelDTO2);
        whiteLabelDTO2.setId(2L);
        assertThat(whiteLabelDTO1).isNotEqualTo(whiteLabelDTO2);
        whiteLabelDTO1.setId(null);
        assertThat(whiteLabelDTO1).isNotEqualTo(whiteLabelDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(whiteLabelMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(whiteLabelMapper.fromId(null)).isNull();
    }
}
