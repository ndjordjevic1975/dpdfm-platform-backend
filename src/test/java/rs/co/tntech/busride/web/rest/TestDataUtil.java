/**
 *
 */
package rs.co.tntech.busride.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.domain.enumeration.SpotType;
import rs.co.tntech.busride.repository.*;
import rs.co.tntech.busride.security.AuthoritiesConstants;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Helper class to create test data.
 *
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
@Component
public class TestDataUtil {

	@Autowired
	private EntityManager em;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ParentRepository parentRepository;
	@Autowired
	private StudentRepository studentRepository;
	@Autowired
	private SchoolRepository schoolRepository;
	@Autowired
	private SchoolAdminRepository schoolAdminRepository;
	@Autowired
	private SchoolEmailRepository schoolEmailRepository;
	@Autowired
	private DriverRepository driverRepository;
	@Autowired
	private SpotRepository spotRepository;
	@Autowired
	private BusRouteRepository busRouteRepository;
	@Autowired
	private WhiteLabelRepository whiteLabelRepository;
	@Autowired
	private TempServiceCancellationRepository tempServiceCancellationRepository;

	/**
	 * Creates default user object
	 *
	 * @return
	 */
	public User createUser() {
		User user = UserResourceIntTest.createEntity(em);
		return userRepository.saveAndFlush(user);
	}

	/**
	 * Creates default user object
	 *
	 * @return
	 */
	public User createCustomUser(String... authorities) {
		try {
			Thread.sleep(1l);
		} catch (InterruptedException e) {
		}
		User user = UserResourceIntTest.createEntity(em);
		String nanoTime = String.valueOf(System.nanoTime());
		user.setLogin(nanoTime);
		user.setEmail(nanoTime + "@mail");
		if (authorities != null) {
			Set<Authority> hashSet = new HashSet<>();
			for (String authority : authorities) {
				Authority a = new Authority();
				a.setName(authority);
				hashSet.add(a);
			}
			user.setAuthorities(hashSet);
		}
		return userRepository.saveAndFlush(user);
	}

	/**
	 * Creates default parent object
	 *
	 * @return
	 */
	public Parent createParent() {
		Parent parent = ParentResourceIntTest.createEntity(em);
		parent.setUser(createUser());
		parent.setSpot(createParentSpot());
		parent.addSchool(createSchool());
		return parentRepository.saveAndFlush(parent);
	}

	/**
	 * Creates default parent object
	 *
	 * @return
	 */
	public Parent createCustomParent() {
		Parent parent = ParentResourceIntTest.createEntity(em);
		parent.setUser(createCustomUser());
		parent.setSpot(createParentSpot());
		parent.addSchool(createSchool());
		return parentRepository.saveAndFlush(parent);
	}

	/**
	 * Creates default student object (also creates default parent and school objects)
	 *
	 * @return
	 */
	public Student createStudent(Parent... parent) {
		Student student = StudentResourceIntTest.createEntity(em);
		if (parent == null || parent.length == 0) {
			student.setParent(createParent());
		} else {
			student.setParent(parent[0]);
		}
		student.setSchool(createSchool());
        return studentRepository.saveAndFlush(student);
	}

	/**
	 * Create default student object for given parameters
	 *
	 * @param parent
	 * @param school
	 * @return
	 */
	public Student createStudent(Parent parent, School school) {
		Student student = StudentResourceIntTest.createEntity(em);
		student.setParent(parent);
		student.setSchool(school);
		return studentRepository.saveAndFlush(student);
	}

	/**
	 * Creates temp service cancellation for given student
	 *
	 * @param student
	 * @return
	 */
	public TempServiceCancellation createTempServiceCancellation(Student student) {
		TempServiceCancellation tempServiceCancellation = new TempServiceCancellation();
		tempServiceCancellation.setStudent(student);
		tempServiceCancellation.setSpot(student.getParent().getSpot());
		return tempServiceCancellationRepository.saveAndFlush(tempServiceCancellation);
	}

	/**
	 * Creates default school object
	 *
	 * @return
	 */
	public School createSchool() {
		School school = SchoolResourceIntTest.createEntity(em);
		return schoolRepository.saveAndFlush(school);
	}

	/**
	 * Creates default spot object for school
	 *
	 * @return
	 */
	public Spot createSchoolSpot() {
		Spot spot = SpotResourceIntTest.createEntity(em);
		spot.setType(SpotType.SCHOOL);
		return spotRepository.saveAndFlush(spot);
	}

	/**
	 * Creates default spot object for parent
	 *
	 * @return
	 */
	public Spot createParentSpot() {
		Spot spot = SpotResourceIntTest.createEntity(em);
		spot.setType(SpotType.PICKUP_DROPOFF);
		return spotRepository.saveAndFlush(spot);
	}

	/**
	 * Creates default white label object (also creates default school object)
	 *
	 * @return
	 */
	public WhiteLabel createWhiteLabel() {
		School school = SchoolResourceIntTest.createEntity(em);
		schoolRepository.save(school);
		WhiteLabel whiteLabel = WhiteLabelResourceIntTest.createEntity(em);
		whiteLabel.setSchool(createSchool());
		return whiteLabelRepository.saveAndFlush(whiteLabel);
	}

	/**
	 * Creates default driver object (also creates default user object and school if it is not given)
	 *
	 * @param school
	 * @return
	 */
	public Driver createDriver(School... school) {
		Driver driver = DriverResourceIntTest.createEntity(em);
		if (school != null && school.length > 0) {
			driver.setSchool(school[0]);
		} else {
			driver.setSchool(createSchool());
		}
		driver.setUser(createCustomUser(AuthoritiesConstants.DRIVER));
		return driverRepository.saveAndFlush(driver);
	}

	/**
	 * Creates default active bus route
	 *
	 * @param school
	 * @param spots
	 * @return
	 */
	public BusRoute createActiveBusRoute(School school, Collection<Spot> spots) {
		BusRoute busRoute = BusRouteResourceIntTest.createEntity(em);
		if (school == null) {
			school = createSchool();
		}
		busRoute.setSchool(school);
		busRoute.setDriver(createDriver(school));
		if (spots != null) {
			busRoute.setSpots(new HashSet<>(spots));
		}
		BusRoute busRouteDB = busRouteRepository.saveAndFlush(busRoute);
		busRouteDB.getDriver().setActiveBusRoute(busRouteDB);
		driverRepository.saveAndFlush(busRouteDB.getDriver());
		return busRouteDB;
	}

	/**
	 * Creates default school admin
	 *
	 * @return
	 */
	public SchoolAdmin createSchoolAdmin() {
		SchoolAdmin schoolAdmin = SchoolAdminResourceIntTest.createEntity(em);
		schoolAdmin.setUser(createCustomUser(AuthoritiesConstants.SCHOOL_ADMIN));
		schoolAdmin.setSchool(createSchool());
		return schoolAdminRepository.saveAndFlush(schoolAdmin);
	}

	/**
	 * Creates school email object
	 *
	 * @param school
	 * @return
	 */
	public SchoolEmail createSchoolEmail(School school) {
		String uniqueValue = getUniqueValue();
		SchoolEmail schoolEmail = new SchoolEmail();
		schoolEmail.setContent("content "+uniqueValue);
		schoolEmail.setSubject("subject "+uniqueValue);
		schoolEmail.setSchool(school);
		schoolEmail.setTimestamp(Instant.now());
		return schoolEmailRepository.saveAndFlush(schoolEmail);
	}

	private String getUniqueValue() {
		try {
			Thread.sleep(1l);
		} catch (InterruptedException e) {
		}
		return String.valueOf(System.nanoTime());
	}

}
