package rs.co.tntech.busride.web.rest;

import rs.co.tntech.busride.BusRidePlatformBackendApp;

import rs.co.tntech.busride.domain.Spot;
import rs.co.tntech.busride.repository.SpotRepository;
import rs.co.tntech.busride.service.SpotService;
import rs.co.tntech.busride.service.dto.SpotDTO;
import rs.co.tntech.busride.service.mapper.SpotMapper;
import rs.co.tntech.busride.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import rs.co.tntech.busride.domain.enumeration.SpotType;
/**
 * Test class for the SpotResource REST controller.
 *
 * @see SpotResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BusRidePlatformBackendApp.class)
public class SpotResourceIntTest {

    public static final Double DEFAULT_GPS_LATITUDE = 1D;
    public static final Double UPDATED_GPS_LATITUDE = 2D;

    public static final Double DEFAULT_GPS_LONGITUDE = 1D;
    public static final Double UPDATED_GPS_LONGITUDE = 2D;

    public static final SpotType DEFAULT_TYPE = SpotType.SCHOOL;
    public static final SpotType UPDATED_TYPE = SpotType.PICKUP_DROPOFF;

    @Autowired
    private SpotRepository spotRepository;

    @Autowired
    private SpotMapper spotMapper;

    @Autowired
    private SpotService spotService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSpotMockMvc;

    private Spot spot;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SpotResource spotResource = new SpotResource(spotService);
        this.restSpotMockMvc = MockMvcBuilders.standaloneSetup(spotResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Spot createEntity(EntityManager em) {
        Spot spot = new Spot()
            .gpsLatitude(DEFAULT_GPS_LATITUDE)
            .gpsLongitude(DEFAULT_GPS_LONGITUDE)
            .type(DEFAULT_TYPE);
        return spot;
    }

    @Before
    public void initTest() {
        spot = createEntity(em);
    }

    @Test
    @Transactional
    public void createSpot() throws Exception {
        int databaseSizeBeforeCreate = spotRepository.findAll().size();

        // Create the Spot
        SpotDTO spotDTO = spotMapper.toDto(spot);
        restSpotMockMvc.perform(post("/api/spots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spotDTO)))
            .andExpect(status().isCreated());

        // Validate the Spot in the database
        List<Spot> spotList = spotRepository.findAll();
        assertThat(spotList).hasSize(databaseSizeBeforeCreate + 1);
        Spot testSpot = spotList.get(spotList.size() - 1);
        assertThat(testSpot.getGpsLatitude()).isEqualTo(DEFAULT_GPS_LATITUDE);
        assertThat(testSpot.getGpsLongitude()).isEqualTo(DEFAULT_GPS_LONGITUDE);
        assertThat(testSpot.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createSpotWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = spotRepository.findAll().size();

        // Create the Spot with an existing ID
        spot.setId(1L);
        SpotDTO spotDTO = spotMapper.toDto(spot);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpotMockMvc.perform(post("/api/spots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spotDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Spot> spotList = spotRepository.findAll();
        assertThat(spotList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSpots() throws Exception {
        // Initialize the database
        spotRepository.saveAndFlush(spot);

        // Get all the spotList
        restSpotMockMvc.perform(get("/api/spots?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spot.getId().intValue())))
            .andExpect(jsonPath("$.[*].gpsLatitude").value(hasItem(DEFAULT_GPS_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].gpsLongitude").value(hasItem(DEFAULT_GPS_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getSpot() throws Exception {
        // Initialize the database
        spotRepository.saveAndFlush(spot);

        // Get the spot
        restSpotMockMvc.perform(get("/api/spots/{id}", spot.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(spot.getId().intValue()))
            .andExpect(jsonPath("$.gpsLatitude").value(DEFAULT_GPS_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.gpsLongitude").value(DEFAULT_GPS_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSpot() throws Exception {
        // Get the spot
        restSpotMockMvc.perform(get("/api/spots/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpot() throws Exception {
        // Initialize the database
        spotRepository.saveAndFlush(spot);
        int databaseSizeBeforeUpdate = spotRepository.findAll().size();

        // Update the spot
        Spot updatedSpot = spotRepository.findOne(spot.getId());
        updatedSpot
            .gpsLatitude(UPDATED_GPS_LATITUDE)
            .gpsLongitude(UPDATED_GPS_LONGITUDE)
            .type(UPDATED_TYPE);
        SpotDTO spotDTO = spotMapper.toDto(updatedSpot);

        restSpotMockMvc.perform(put("/api/spots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spotDTO)))
            .andExpect(status().isOk());

        // Validate the Spot in the database
        List<Spot> spotList = spotRepository.findAll();
        assertThat(spotList).hasSize(databaseSizeBeforeUpdate);
        Spot testSpot = spotList.get(spotList.size() - 1);
        assertThat(testSpot.getGpsLatitude()).isEqualTo(UPDATED_GPS_LATITUDE);
        assertThat(testSpot.getGpsLongitude()).isEqualTo(UPDATED_GPS_LONGITUDE);
        assertThat(testSpot.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingSpot() throws Exception {
        int databaseSizeBeforeUpdate = spotRepository.findAll().size();

        // Create the Spot
        SpotDTO spotDTO = spotMapper.toDto(spot);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSpotMockMvc.perform(put("/api/spots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spotDTO)))
            .andExpect(status().isCreated());

        // Validate the Spot in the database
        List<Spot> spotList = spotRepository.findAll();
        assertThat(spotList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSpot() throws Exception {
        // Initialize the database
        spotRepository.saveAndFlush(spot);
        int databaseSizeBeforeDelete = spotRepository.findAll().size();

        // Get the spot
        restSpotMockMvc.perform(delete("/api/spots/{id}", spot.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Spot> spotList = spotRepository.findAll();
        assertThat(spotList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Spot.class);
        Spot spot1 = new Spot();
        spot1.setId(1L);
        Spot spot2 = new Spot();
        spot2.setId(spot1.getId());
        assertThat(spot1).isEqualTo(spot2);
        spot2.setId(2L);
        assertThat(spot1).isNotEqualTo(spot2);
        spot1.setId(null);
        assertThat(spot1).isNotEqualTo(spot2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpotDTO.class);
        SpotDTO spotDTO1 = new SpotDTO();
        spotDTO1.setId(1L);
        SpotDTO spotDTO2 = new SpotDTO();
        assertThat(spotDTO1).isNotEqualTo(spotDTO2);
        spotDTO2.setId(spotDTO1.getId());
        assertThat(spotDTO1).isEqualTo(spotDTO2);
        spotDTO2.setId(2L);
        assertThat(spotDTO1).isNotEqualTo(spotDTO2);
        spotDTO1.setId(null);
        assertThat(spotDTO1).isNotEqualTo(spotDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(spotMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(spotMapper.fromId(null)).isNull();
    }
}
