package rs.co.tntech.busride.web.rest;

import rs.co.tntech.busride.BusRidePlatformBackendApp;
import rs.co.tntech.busride.domain.Driver;
import rs.co.tntech.busride.domain.DriverNotification;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.repository.DriverNotificationRepository;
import rs.co.tntech.busride.service.dto.DriverNotificationDTO;
import rs.co.tntech.busride.service.mapper.DriverNotificationMapper;
import rs.co.tntech.busride.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonObject;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import rs.co.tntech.busride.domain.enumeration.DriverNotificationType;
/**
 * Test class for the DriverNotificationResource REST controller.
 *
 * @see DriverNotificationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BusRidePlatformBackendApp.class)
public class DriverNotificationResourceIntTest {

    private static final String DEFAULT_NOTIFICATION = "AAAAAAAAAA";
    private static final String UPDATED_NOTIFICATION = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_ON_DEVICE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_ON_DEVICE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final DriverNotificationType DEFAULT_TYPE = DriverNotificationType.PROXIMITY;
    private static final DriverNotificationType UPDATED_TYPE = DriverNotificationType.ARRIVED;

    @Autowired
    private DriverNotificationRepository driverNotificationRepository;
    @Autowired
    private DriverNotificationMapper driverNotificationMapper;
    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
    @Autowired
    private ExceptionTranslator exceptionTranslator;
    @Autowired
    private EntityManager em;
    @Autowired
    private DriverNotificationResource driverNotificationResource;
    @Autowired
    private TestDataUtil testDataUtil;
    @Autowired
    private TestSecurityUtil testSecurityUtil;

    private MockMvc restDriverNotificationMockMvc;

    private DriverNotification driverNotification;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.restDriverNotificationMockMvc = MockMvcBuilders.standaloneSetup(driverNotificationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DriverNotification createEntity(EntityManager em) {
        DriverNotification driverNotification = new DriverNotification()
            .notification(DEFAULT_NOTIFICATION)
            .createdOnDevice(DEFAULT_CREATED_ON_DEVICE)
            .type(DEFAULT_TYPE);
        return driverNotification;
    }

    @Before
    public void initTest() {
        driverNotification = createEntity(em);
    }

    @Test
    @Transactional
    public void createDriverNotification() throws Exception {
        int databaseSizeBeforeCreate = driverNotificationRepository.findAll().size();

        // Create the DriverNotification
        DriverNotificationDTO driverNotificationDTO = driverNotificationMapper.toDto(driverNotification);
        restDriverNotificationMockMvc.perform(post("/api/driver-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(driverNotificationDTO)))
            .andExpect(status().isCreated());

        // Validate the DriverNotification in the database
        List<DriverNotification> driverNotificationList = driverNotificationRepository.findAll();
        assertThat(driverNotificationList).hasSize(databaseSizeBeforeCreate + 1);
        DriverNotification testDriverNotification = driverNotificationList.get(driverNotificationList.size() - 1);
        assertThat(testDriverNotification.getNotification()).isEqualTo(DEFAULT_NOTIFICATION);
        assertThat(testDriverNotification.getCreatedOnDevice()).isEqualTo(DEFAULT_CREATED_ON_DEVICE);
        assertThat(testDriverNotification.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createDriverNotificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = driverNotificationRepository.findAll().size();

        // Create the DriverNotification with an existing ID
        driverNotification.setId(1L);
        DriverNotificationDTO driverNotificationDTO = driverNotificationMapper.toDto(driverNotification);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDriverNotificationMockMvc.perform(post("/api/driver-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(driverNotificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DriverNotification> driverNotificationList = driverNotificationRepository.findAll();
        assertThat(driverNotificationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDriverNotifications() throws Exception {
        // Initialize the database
        driverNotificationRepository.saveAndFlush(driverNotification);

        // Get all the driverNotificationList
        restDriverNotificationMockMvc.perform(get("/api/driver-notifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(driverNotification.getId().intValue())))
            .andExpect(jsonPath("$.[*].notification").value(hasItem(DEFAULT_NOTIFICATION.toString())))
            .andExpect(jsonPath("$.[*].createdOnDevice").value(hasItem(DEFAULT_CREATED_ON_DEVICE.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getDriverNotification() throws Exception {
        // Initialize the database
        driverNotificationRepository.saveAndFlush(driverNotification);

        // Get the driverNotification
        restDriverNotificationMockMvc.perform(get("/api/driver-notifications/{id}", driverNotification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(driverNotification.getId().intValue()))
            .andExpect(jsonPath("$.notification").value(DEFAULT_NOTIFICATION.toString()))
            .andExpect(jsonPath("$.createdOnDevice").value(DEFAULT_CREATED_ON_DEVICE.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDriverNotification() throws Exception {
        // Get the driverNotification
        restDriverNotificationMockMvc.perform(get("/api/driver-notifications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDriverNotification() throws Exception {
        // Initialize the database
        driverNotificationRepository.saveAndFlush(driverNotification);
        int databaseSizeBeforeUpdate = driverNotificationRepository.findAll().size();

        // Update the driverNotification
        DriverNotification updatedDriverNotification = driverNotificationRepository.findOne(driverNotification.getId());
        updatedDriverNotification
            .notification(UPDATED_NOTIFICATION)
            .createdOnDevice(UPDATED_CREATED_ON_DEVICE)
            .type(UPDATED_TYPE);
        DriverNotificationDTO driverNotificationDTO = driverNotificationMapper.toDto(updatedDriverNotification);

        restDriverNotificationMockMvc.perform(put("/api/driver-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(driverNotificationDTO)))
            .andExpect(status().isOk());

        // Validate the DriverNotification in the database
        List<DriverNotification> driverNotificationList = driverNotificationRepository.findAll();
        assertThat(driverNotificationList).hasSize(databaseSizeBeforeUpdate);
        DriverNotification testDriverNotification = driverNotificationList.get(driverNotificationList.size() - 1);
        assertThat(testDriverNotification.getNotification()).isEqualTo(UPDATED_NOTIFICATION);
        assertThat(testDriverNotification.getCreatedOnDevice()).isEqualTo(UPDATED_CREATED_ON_DEVICE);
        assertThat(testDriverNotification.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingDriverNotification() throws Exception {
        int databaseSizeBeforeUpdate = driverNotificationRepository.findAll().size();

        // Create the DriverNotification
        DriverNotificationDTO driverNotificationDTO = driverNotificationMapper.toDto(driverNotification);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDriverNotificationMockMvc.perform(put("/api/driver-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(driverNotificationDTO)))
            .andExpect(status().isCreated());

        // Validate the DriverNotification in the database
        List<DriverNotification> driverNotificationList = driverNotificationRepository.findAll();
        assertThat(driverNotificationList).hasSize(databaseSizeBeforeUpdate + 1);
    }
    
    @Test
    @Transactional
    public void createDriverNotificationForCurrentDriver() throws Exception {
    	Parent parent = testDataUtil.createParent();
    	Driver driver = testDataUtil.createDriver();
        int databaseSizeBeforeCreate = driverNotificationRepository.findAll().size();
        testSecurityUtil.login(driver.getUser().getLogin());

        // Create the DriverNotification
        JsonObject payload = new JsonObject();
		payload.addProperty("createdOnDevice", new Date().getTime());
		payload.addProperty("spotId", parent.getSpot().getId());
		payload.addProperty("type", DriverNotificationType.PROXIMITY.toString());
        restDriverNotificationMockMvc.perform(post("/api/driver-notifications/current-driver")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(payload.toString()))
            .andExpect(status().isCreated());

        // Validate the DriverNotification in the database
        List<DriverNotification> driverNotificationList = driverNotificationRepository.findAll();
        assertThat(driverNotificationList).hasSize(databaseSizeBeforeCreate + 1);
        DriverNotification testDriverNotification = driverNotificationList.get(driverNotificationList.size() - 1);
		assertThat(testDriverNotification.getNotification()).isEqualTo("The bus is about to arrive at " + ParentResourceIntTest.DEFAULT_ADDRESS + ".");
        assertThat(testDriverNotification.getCreatedOnDevice()).isLessThan(Instant.now());
        assertThat(testDriverNotification.getType()).isEqualTo(DriverNotificationType.PROXIMITY);
        assertThat(testDriverNotification.getDriver().getId()).isEqualTo(driver.getId());
        assertThat(testDriverNotification.getSpot().getId()).isEqualTo(parent.getSpot().getId());
    }

    @Test
    @Transactional
    public void deleteDriverNotification() throws Exception {
        // Initialize the database
        driverNotificationRepository.saveAndFlush(driverNotification);
        int databaseSizeBeforeDelete = driverNotificationRepository.findAll().size();

        // Get the driverNotification
        restDriverNotificationMockMvc.perform(delete("/api/driver-notifications/{id}", driverNotification.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DriverNotification> driverNotificationList = driverNotificationRepository.findAll();
        assertThat(driverNotificationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DriverNotification.class);
        DriverNotification driverNotification1 = new DriverNotification();
        driverNotification1.setId(1L);
        DriverNotification driverNotification2 = new DriverNotification();
        driverNotification2.setId(driverNotification1.getId());
        assertThat(driverNotification1).isEqualTo(driverNotification2);
        driverNotification2.setId(2L);
        assertThat(driverNotification1).isNotEqualTo(driverNotification2);
        driverNotification1.setId(null);
        assertThat(driverNotification1).isNotEqualTo(driverNotification2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DriverNotificationDTO.class);
        DriverNotificationDTO driverNotificationDTO1 = new DriverNotificationDTO();
        driverNotificationDTO1.setId(1L);
        DriverNotificationDTO driverNotificationDTO2 = new DriverNotificationDTO();
        assertThat(driverNotificationDTO1).isNotEqualTo(driverNotificationDTO2);
        driverNotificationDTO2.setId(driverNotificationDTO1.getId());
        assertThat(driverNotificationDTO1).isEqualTo(driverNotificationDTO2);
        driverNotificationDTO2.setId(2L);
        assertThat(driverNotificationDTO1).isNotEqualTo(driverNotificationDTO2);
        driverNotificationDTO1.setId(null);
        assertThat(driverNotificationDTO1).isNotEqualTo(driverNotificationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(driverNotificationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(driverNotificationMapper.fromId(null)).isNull();
    }
}
