package rs.co.tntech.busride.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

/**
 * Helper class for user authentication
 * 
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
@Component
public class TestSecurityUtil {

	public static final String USER_ADMIN = "admin";
	public static final String USER_SCHOOL_ADMIN = "schoola1";
	public static final String USER_DRIVER1 = "schoola1_tablet_1";
	public static final String USER_PARENT504 = "Parent504";

	@Autowired
	private UserDetailsService userDetailsService;

	/**
	 * Initializes security context for given user
	 * 
	 * @param username
	 */
	public void login(String username) {
		UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		securityContext
				.setAuthentication(new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities()));
		SecurityContextHolder.setContext(securityContext);
	}

}
