package rs.co.tntech.busride.web.rest;

import rs.co.tntech.busride.BusRidePlatformBackendApp;
import rs.co.tntech.busride.domain.Driver;
import rs.co.tntech.busride.domain.School;
import rs.co.tntech.busride.domain.SchoolAdmin;
import rs.co.tntech.busride.domain.SchoolEmail;
import rs.co.tntech.busride.repository.SchoolEmailRepository;
import rs.co.tntech.busride.service.dto.SchoolEmailDTO;
import rs.co.tntech.busride.service.mapper.SchoolEmailMapper;
import rs.co.tntech.busride.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SchoolEmailResource REST controller.
 *
 * @see SchoolEmailResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BusRidePlatformBackendApp.class)
public class SchoolEmailResourceIntTest {

    private static final String DEFAULT_SUBJECT = "AAAAAAAAAA";
    private static final String UPDATED_SUBJECT = "BBBBBBBBBB";

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    private static final Instant DEFAULT_TIMESTAMP = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TIMESTAMP = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private SchoolEmailRepository schoolEmailRepository;
    @Autowired
    private SchoolEmailMapper schoolEmailMapper;
    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
    @Autowired
    private ExceptionTranslator exceptionTranslator;
    @Autowired
    private EntityManager em;
    @Autowired
    private SchoolEmailResource schoolEmailResource;
    @Autowired
    private TestDataUtil testDataUtil;
    @Autowired
    private TestSecurityUtil testSecurityUtil;

    private MockMvc restSchoolEmailMockMvc;

    private SchoolEmail schoolEmail;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.restSchoolEmailMockMvc = MockMvcBuilders.standaloneSetup(schoolEmailResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SchoolEmail createEntity(EntityManager em) {
        SchoolEmail schoolEmail = new SchoolEmail()
            .subject(DEFAULT_SUBJECT)
            .content(DEFAULT_CONTENT)
            .timestamp(DEFAULT_TIMESTAMP);
        return schoolEmail;
    }

    @Before
    public void initTest() {
        schoolEmail = createEntity(em);
    }

    @Test
    @Transactional
    public void createSchoolEmail() throws Exception {
        int databaseSizeBeforeCreate = schoolEmailRepository.findAll().size();

        // Create the SchoolEmail
        SchoolEmailDTO schoolEmailDTO = schoolEmailMapper.toDto(schoolEmail);
        restSchoolEmailMockMvc.perform(post("/api/school-emails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(schoolEmailDTO)))
            .andExpect(status().isCreated());

        // Validate the SchoolEmail in the database
        List<SchoolEmail> schoolEmailList = schoolEmailRepository.findAll();
        assertThat(schoolEmailList).hasSize(databaseSizeBeforeCreate + 1);
        SchoolEmail testSchoolEmail = schoolEmailList.get(schoolEmailList.size() - 1);
        assertThat(testSchoolEmail.getSubject()).isEqualTo(DEFAULT_SUBJECT);
        assertThat(testSchoolEmail.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testSchoolEmail.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
    }

    @Test
    @Transactional
    public void createSchoolEmailWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = schoolEmailRepository.findAll().size();

        // Create the SchoolEmail with an existing ID
        schoolEmail.setId(1L);
        SchoolEmailDTO schoolEmailDTO = schoolEmailMapper.toDto(schoolEmail);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSchoolEmailMockMvc.perform(post("/api/school-emails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(schoolEmailDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<SchoolEmail> schoolEmailList = schoolEmailRepository.findAll();
        assertThat(schoolEmailList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSchoolEmails() throws Exception {
        // Initialize the database
        schoolEmailRepository.saveAndFlush(schoolEmail);

        // Get all the schoolEmailList
        restSchoolEmailMockMvc.perform(get("/api/school-emails?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(schoolEmail.getId().intValue())))
            .andExpect(jsonPath("$.[*].subject").value(hasItem(DEFAULT_SUBJECT.toString())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.toString())));
    }

    @Test
    @Transactional
    public void getSchoolEmail() throws Exception {
        // Initialize the database
        schoolEmailRepository.saveAndFlush(schoolEmail);

        // Get the schoolEmail
        restSchoolEmailMockMvc.perform(get("/api/school-emails/{id}", schoolEmail.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(schoolEmail.getId().intValue()))
            .andExpect(jsonPath("$.subject").value(DEFAULT_SUBJECT.toString()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSchoolEmail() throws Exception {
        // Get the schoolEmail
        restSchoolEmailMockMvc.perform(get("/api/school-emails/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSchoolEmail() throws Exception {
        // Initialize the database
        schoolEmailRepository.saveAndFlush(schoolEmail);
        int databaseSizeBeforeUpdate = schoolEmailRepository.findAll().size();

        // Update the schoolEmail
        SchoolEmail updatedSchoolEmail = schoolEmailRepository.findOne(schoolEmail.getId());
        updatedSchoolEmail
            .subject(UPDATED_SUBJECT)
            .content(UPDATED_CONTENT)
            .timestamp(UPDATED_TIMESTAMP);
        SchoolEmailDTO schoolEmailDTO = schoolEmailMapper.toDto(updatedSchoolEmail);

        restSchoolEmailMockMvc.perform(put("/api/school-emails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(schoolEmailDTO)))
            .andExpect(status().isOk());

        // Validate the SchoolEmail in the database
        List<SchoolEmail> schoolEmailList = schoolEmailRepository.findAll();
        assertThat(schoolEmailList).hasSize(databaseSizeBeforeUpdate);
        SchoolEmail testSchoolEmail = schoolEmailList.get(schoolEmailList.size() - 1);
        assertThat(testSchoolEmail.getSubject()).isEqualTo(UPDATED_SUBJECT);
        assertThat(testSchoolEmail.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testSchoolEmail.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void updateNonExistingSchoolEmail() throws Exception {
        int databaseSizeBeforeUpdate = schoolEmailRepository.findAll().size();

        // Create the SchoolEmail
        SchoolEmailDTO schoolEmailDTO = schoolEmailMapper.toDto(schoolEmail);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSchoolEmailMockMvc.perform(put("/api/school-emails")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(schoolEmailDTO)))
            .andExpect(status().isCreated());

        // Validate the SchoolEmail in the database
        List<SchoolEmail> schoolEmailList = schoolEmailRepository.findAll();
        assertThat(schoolEmailList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSchoolEmail() throws Exception {
        // Initialize the database
        schoolEmailRepository.saveAndFlush(schoolEmail);
        int databaseSizeBeforeDelete = schoolEmailRepository.findAll().size();

        // Get the schoolEmail
        restSchoolEmailMockMvc.perform(delete("/api/school-emails/{id}", schoolEmail.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SchoolEmail> schoolEmailList = schoolEmailRepository.findAll();
        assertThat(schoolEmailList).hasSize(databaseSizeBeforeDelete - 1);
    }
    
    @Test
    @Transactional
    public void createSchoolEmailForCurrentSchoolAdmin() throws Exception {
        int databaseSizeBeforeCreate = schoolEmailRepository.findAll().size();
        
        SchoolAdmin schoolAdmin = testDataUtil.createSchoolAdmin();
        testSecurityUtil.login(schoolAdmin.getUser().getLogin());

        // Create the SchoolEmail
        SchoolEmailDTO schoolEmailDTO = schoolEmailMapper.toDto(schoolEmail);
        restSchoolEmailMockMvc.perform(post("/api/school-emails/current-school-admin")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(schoolEmailDTO)))
            .andExpect(status().isCreated());

        // Validate the SchoolEmail in the database
        List<SchoolEmail> schoolEmailList = schoolEmailRepository.findAll();
        assertThat(schoolEmailList).hasSize(databaseSizeBeforeCreate + 1);
        SchoolEmail testSchoolEmail = schoolEmailList.get(schoolEmailList.size() - 1);
        assertThat(testSchoolEmail.getSchool().getId()).isEqualTo(schoolAdmin.getSchool().getId());
        assertThat(testSchoolEmail.getSubject()).isEqualTo(DEFAULT_SUBJECT);
        assertThat(testSchoolEmail.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testSchoolEmail.getTimestamp()).isLessThan(Instant.now());
    }
    
    @Test
    @Transactional
    public void createSchoolEmailForCurrentNonSchoolAdmin() throws Exception {
        Driver driver = testDataUtil.createDriver();
        testSecurityUtil.login(driver.getUser().getLogin());

        // Create the SchoolEmail
        SchoolEmailDTO schoolEmailDTO = schoolEmailMapper.toDto(schoolEmail);
        restSchoolEmailMockMvc.perform(post("/api/school-emails/current-school-admin")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(schoolEmailDTO)))
            .andExpect(status().isForbidden());
    }
    
    @Test
    @Transactional
    public void getAllSchoolEmailsForCurrentSchoolAdmin() throws Exception {
        // Initialize the database
        SchoolAdmin schoolAdmin = testDataUtil.createSchoolAdmin();
        SchoolEmail schoolEmail1 = testDataUtil.createSchoolEmail(schoolAdmin.getSchool());
        SchoolEmail schoolEmail2 = testDataUtil.createSchoolEmail(schoolAdmin.getSchool());
        
        School school2 = testDataUtil.createSchool();
        SchoolEmail schoolEmail3 = testDataUtil.createSchoolEmail(school2);
        testSecurityUtil.login(schoolAdmin.getUser().getLogin());

        // Get all the schoolEmailList
        restSchoolEmailMockMvc.perform(get("/api/school-emails/current-school-admin"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(schoolEmail1.getId().intValue())))
            .andExpect(jsonPath("$.[*].subject").value(hasItem(schoolEmail1.getSubject())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(schoolEmail1.getContent())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(schoolEmail1.getTimestamp().toString())))
            .andExpect(jsonPath("$.[*].id").value(hasItem(schoolEmail2.getId().intValue())))
            .andExpect(jsonPath("$.[*].subject").value(hasItem(schoolEmail2.getSubject())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(schoolEmail2.getContent())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(schoolEmail2.getTimestamp().toString())))
            .andExpect(jsonPath("$.[*].id").value(not(hasItem(schoolEmail3.getId().intValue()))));;
    }

    @Test
    @Transactional
    public void getAllSchoolEmailsForCurrentNonSchoolAdmin() throws Exception {
        Driver driver = testDataUtil.createDriver();
        testSecurityUtil.login(driver.getUser().getLogin());

		// Get all the schoolEmailList
        restSchoolEmailMockMvc.perform(get("/api/school-emails/current-school-admin"))
            .andExpect(status().isForbidden());
    }
    
    @Test
    @Transactional
    public void deleteAllSchoolEmailsForCurrentSchoolAdmin() throws Exception {
        // Initialize the database
        SchoolAdmin schoolAdmin = testDataUtil.createSchoolAdmin();
        testDataUtil.createSchoolEmail(schoolAdmin.getSchool());
        testDataUtil.createSchoolEmail(schoolAdmin.getSchool());
        
        School school2 = testDataUtil.createSchool();
        testDataUtil.createSchoolEmail(school2);
        
        int databaseSizeBeforeDelete = schoolEmailRepository.findAll().size();
        
        testSecurityUtil.login(schoolAdmin.getUser().getLogin());

        // Delete all the schoolEmailList
        restSchoolEmailMockMvc.perform(delete("/api/school-emails/current-school-admin", schoolEmail.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
        
		// Validate the database is empty
		List<SchoolEmail> schoolEmailList = schoolEmailRepository.findAll();
		assertThat(schoolEmailList).hasSize(databaseSizeBeforeDelete - 2);
    }
    
    @Test
    @Transactional
    public void deleteAllSchoolEmailsForCurrentNonSchoolAdmin() throws Exception {
        // Initialize the database
    	Driver driver = testDataUtil.createDriver();
        testSecurityUtil.login(driver.getUser().getLogin());

        // Delete all the schoolEmailList
        restSchoolEmailMockMvc.perform(delete("/api/school-emails/current-school-admin", schoolEmail.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden());
    }
    
	@Test
	@Transactional
	public void deleteSchoolEmailForCurrentSchoolAdmin() throws Exception {
		// Initialize the database
		SchoolAdmin schoolAdmin = testDataUtil.createSchoolAdmin();
		SchoolEmail schoolEmail = testDataUtil.createSchoolEmail(schoolAdmin.getSchool());
		int databaseSizeBeforeDelete = schoolEmailRepository.findAll().size();

		testSecurityUtil.login(schoolAdmin.getUser().getLogin());

		// Get the schoolEmail
		restSchoolEmailMockMvc.perform(delete("/api/school-emails/{id}", schoolEmail.getId())
				.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<SchoolEmail> schoolEmailList = schoolEmailRepository.findAll();
		assertThat(schoolEmailList).hasSize(databaseSizeBeforeDelete - 1);
	}

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SchoolEmail.class);
        SchoolEmail schoolEmail1 = new SchoolEmail();
        schoolEmail1.setId(1L);
        SchoolEmail schoolEmail2 = new SchoolEmail();
        schoolEmail2.setId(schoolEmail1.getId());
        assertThat(schoolEmail1).isEqualTo(schoolEmail2);
        schoolEmail2.setId(2L);
        assertThat(schoolEmail1).isNotEqualTo(schoolEmail2);
        schoolEmail1.setId(null);
        assertThat(schoolEmail1).isNotEqualTo(schoolEmail2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SchoolEmailDTO.class);
        SchoolEmailDTO schoolEmailDTO1 = new SchoolEmailDTO();
        schoolEmailDTO1.setId(1L);
        SchoolEmailDTO schoolEmailDTO2 = new SchoolEmailDTO();
        assertThat(schoolEmailDTO1).isNotEqualTo(schoolEmailDTO2);
        schoolEmailDTO2.setId(schoolEmailDTO1.getId());
        assertThat(schoolEmailDTO1).isEqualTo(schoolEmailDTO2);
        schoolEmailDTO2.setId(2L);
        assertThat(schoolEmailDTO1).isNotEqualTo(schoolEmailDTO2);
        schoolEmailDTO1.setId(null);
        assertThat(schoolEmailDTO1).isNotEqualTo(schoolEmailDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(schoolEmailMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(schoolEmailMapper.fromId(null)).isNull();
    }
}
