package rs.co.tntech.busride.web.rest;

import rs.co.tntech.busride.BusRidePlatformBackendApp;

import rs.co.tntech.busride.domain.BusCategory;
import rs.co.tntech.busride.repository.BusCategoryRepository;
import rs.co.tntech.busride.service.BusCategoryService;
import rs.co.tntech.busride.service.dto.BusCategoryDTO;
import rs.co.tntech.busride.service.mapper.BusCategoryMapper;
import rs.co.tntech.busride.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BusCategoryResource REST controller.
 *
 * @see BusCategoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BusRidePlatformBackendApp.class)
public class BusCategoryResourceIntTest {

    private static final Integer DEFAULT_NO_OF_BUSES = 1;
    private static final Integer UPDATED_NO_OF_BUSES = 2;

    private static final Integer DEFAULT_CAPACITY = 1;
    private static final Integer UPDATED_CAPACITY = 2;

    @Autowired
    private BusCategoryRepository busCategoryRepository;

    @Autowired
    private BusCategoryMapper busCategoryMapper;

    @Autowired
    private BusCategoryService busCategoryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restBusCategoryMockMvc;

    private BusCategory busCategory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BusCategoryResource busCategoryResource = new BusCategoryResource(busCategoryService);
        this.restBusCategoryMockMvc = MockMvcBuilders.standaloneSetup(busCategoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BusCategory createEntity(EntityManager em) {
        BusCategory busCategory = new BusCategory()
            .noOfBuses(DEFAULT_NO_OF_BUSES)
            .capacity(DEFAULT_CAPACITY);
        return busCategory;
    }

    @Before
    public void initTest() {
        busCategory = createEntity(em);
    }

    @Test
    @Transactional
    public void createBusCategory() throws Exception {
        int databaseSizeBeforeCreate = busCategoryRepository.findAll().size();

        // Create the BusCategory
        BusCategoryDTO busCategoryDTO = busCategoryMapper.toDto(busCategory);
        restBusCategoryMockMvc.perform(post("/api/bus-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(busCategoryDTO)))
            .andExpect(status().isCreated());

        // Validate the BusCategory in the database
        List<BusCategory> busCategoryList = busCategoryRepository.findAll();
        assertThat(busCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        BusCategory testBusCategory = busCategoryList.get(busCategoryList.size() - 1);
        assertThat(testBusCategory.getNoOfBuses()).isEqualTo(DEFAULT_NO_OF_BUSES);
        assertThat(testBusCategory.getCapacity()).isEqualTo(DEFAULT_CAPACITY);
    }

    @Test
    @Transactional
    public void createBusCategoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = busCategoryRepository.findAll().size();

        // Create the BusCategory with an existing ID
        busCategory.setId(1L);
        BusCategoryDTO busCategoryDTO = busCategoryMapper.toDto(busCategory);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBusCategoryMockMvc.perform(post("/api/bus-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(busCategoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<BusCategory> busCategoryList = busCategoryRepository.findAll();
        assertThat(busCategoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllBusCategories() throws Exception {
        // Initialize the database
        busCategoryRepository.saveAndFlush(busCategory);

        // Get all the busCategoryList
        restBusCategoryMockMvc.perform(get("/api/bus-categories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(busCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].noOfBuses").value(hasItem(DEFAULT_NO_OF_BUSES)))
            .andExpect(jsonPath("$.[*].capacity").value(hasItem(DEFAULT_CAPACITY)));
    }

    @Test
    @Transactional
    public void getBusCategory() throws Exception {
        // Initialize the database
        busCategoryRepository.saveAndFlush(busCategory);

        // Get the busCategory
        restBusCategoryMockMvc.perform(get("/api/bus-categories/{id}", busCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(busCategory.getId().intValue()))
            .andExpect(jsonPath("$.noOfBuses").value(DEFAULT_NO_OF_BUSES))
            .andExpect(jsonPath("$.capacity").value(DEFAULT_CAPACITY));
    }

    @Test
    @Transactional
    public void getNonExistingBusCategory() throws Exception {
        // Get the busCategory
        restBusCategoryMockMvc.perform(get("/api/bus-categories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBusCategory() throws Exception {
        // Initialize the database
        busCategoryRepository.saveAndFlush(busCategory);
        int databaseSizeBeforeUpdate = busCategoryRepository.findAll().size();

        // Update the busCategory
        BusCategory updatedBusCategory = busCategoryRepository.findOne(busCategory.getId());
        updatedBusCategory
            .noOfBuses(UPDATED_NO_OF_BUSES)
            .capacity(UPDATED_CAPACITY);
        BusCategoryDTO busCategoryDTO = busCategoryMapper.toDto(updatedBusCategory);

        restBusCategoryMockMvc.perform(put("/api/bus-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(busCategoryDTO)))
            .andExpect(status().isOk());

        // Validate the BusCategory in the database
        List<BusCategory> busCategoryList = busCategoryRepository.findAll();
        assertThat(busCategoryList).hasSize(databaseSizeBeforeUpdate);
        BusCategory testBusCategory = busCategoryList.get(busCategoryList.size() - 1);
        assertThat(testBusCategory.getNoOfBuses()).isEqualTo(UPDATED_NO_OF_BUSES);
        assertThat(testBusCategory.getCapacity()).isEqualTo(UPDATED_CAPACITY);
    }

    @Test
    @Transactional
    public void updateNonExistingBusCategory() throws Exception {
        int databaseSizeBeforeUpdate = busCategoryRepository.findAll().size();

        // Create the BusCategory
        BusCategoryDTO busCategoryDTO = busCategoryMapper.toDto(busCategory);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restBusCategoryMockMvc.perform(put("/api/bus-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(busCategoryDTO)))
            .andExpect(status().isCreated());

        // Validate the BusCategory in the database
        List<BusCategory> busCategoryList = busCategoryRepository.findAll();
        assertThat(busCategoryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteBusCategory() throws Exception {
        // Initialize the database
        busCategoryRepository.saveAndFlush(busCategory);
        int databaseSizeBeforeDelete = busCategoryRepository.findAll().size();

        // Get the busCategory
        restBusCategoryMockMvc.perform(delete("/api/bus-categories/{id}", busCategory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<BusCategory> busCategoryList = busCategoryRepository.findAll();
        assertThat(busCategoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BusCategory.class);
        BusCategory busCategory1 = new BusCategory();
        busCategory1.setId(1L);
        BusCategory busCategory2 = new BusCategory();
        busCategory2.setId(busCategory1.getId());
        assertThat(busCategory1).isEqualTo(busCategory2);
        busCategory2.setId(2L);
        assertThat(busCategory1).isNotEqualTo(busCategory2);
        busCategory1.setId(null);
        assertThat(busCategory1).isNotEqualTo(busCategory2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BusCategoryDTO.class);
        BusCategoryDTO busCategoryDTO1 = new BusCategoryDTO();
        busCategoryDTO1.setId(1L);
        BusCategoryDTO busCategoryDTO2 = new BusCategoryDTO();
        assertThat(busCategoryDTO1).isNotEqualTo(busCategoryDTO2);
        busCategoryDTO2.setId(busCategoryDTO1.getId());
        assertThat(busCategoryDTO1).isEqualTo(busCategoryDTO2);
        busCategoryDTO2.setId(2L);
        assertThat(busCategoryDTO1).isNotEqualTo(busCategoryDTO2);
        busCategoryDTO1.setId(null);
        assertThat(busCategoryDTO1).isNotEqualTo(busCategoryDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(busCategoryMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(busCategoryMapper.fromId(null)).isNull();
    }
}
