package rs.co.tntech.busride.web.rest;

import rs.co.tntech.busride.BusRidePlatformBackendApp;

import rs.co.tntech.busride.domain.SchoolAdmin;
import rs.co.tntech.busride.repository.SchoolAdminRepository;
import rs.co.tntech.busride.service.SchoolAdminService;
import rs.co.tntech.busride.service.dto.SchoolAdminDTO;
import rs.co.tntech.busride.service.mapper.SchoolAdminMapper;
import rs.co.tntech.busride.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SchoolAdminResource REST controller.
 *
 * @see SchoolAdminResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BusRidePlatformBackendApp.class)
public class SchoolAdminResourceIntTest {

    @Autowired
    private SchoolAdminRepository schoolAdminRepository;

    @Autowired
    private SchoolAdminMapper schoolAdminMapper;

    @Autowired
    private SchoolAdminService schoolAdminService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSchoolAdminMockMvc;

    private SchoolAdmin schoolAdmin;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SchoolAdminResource schoolAdminResource = new SchoolAdminResource(schoolAdminService);
        this.restSchoolAdminMockMvc = MockMvcBuilders.standaloneSetup(schoolAdminResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SchoolAdmin createEntity(EntityManager em) {
        SchoolAdmin schoolAdmin = new SchoolAdmin();
        return schoolAdmin;
    }

    @Before
    public void initTest() {
        schoolAdmin = createEntity(em);
    }

    @Test
    @Transactional
    public void createSchoolAdmin() throws Exception {
        int databaseSizeBeforeCreate = schoolAdminRepository.findAll().size();

        // Create the SchoolAdmin
        SchoolAdminDTO schoolAdminDTO = schoolAdminMapper.toDto(schoolAdmin);
        restSchoolAdminMockMvc.perform(post("/api/school-admins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(schoolAdminDTO)))
            .andExpect(status().isCreated());

        // Validate the SchoolAdmin in the database
        List<SchoolAdmin> schoolAdminList = schoolAdminRepository.findAll();
        assertThat(schoolAdminList).hasSize(databaseSizeBeforeCreate + 1);
        SchoolAdmin testSchoolAdmin = schoolAdminList.get(schoolAdminList.size() - 1);
    }

    @Test
    @Transactional
    public void createSchoolAdminWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = schoolAdminRepository.findAll().size();

        // Create the SchoolAdmin with an existing ID
        schoolAdmin.setId(1L);
        SchoolAdminDTO schoolAdminDTO = schoolAdminMapper.toDto(schoolAdmin);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSchoolAdminMockMvc.perform(post("/api/school-admins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(schoolAdminDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<SchoolAdmin> schoolAdminList = schoolAdminRepository.findAll();
        assertThat(schoolAdminList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSchoolAdmins() throws Exception {
        // Initialize the database
        schoolAdminRepository.saveAndFlush(schoolAdmin);

        // Get all the schoolAdminList
        restSchoolAdminMockMvc.perform(get("/api/school-admins?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(schoolAdmin.getId().intValue())));
    }

    @Test
    @Transactional
    public void getSchoolAdmin() throws Exception {
        // Initialize the database
        schoolAdminRepository.saveAndFlush(schoolAdmin);

        // Get the schoolAdmin
        restSchoolAdminMockMvc.perform(get("/api/school-admins/{id}", schoolAdmin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(schoolAdmin.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSchoolAdmin() throws Exception {
        // Get the schoolAdmin
        restSchoolAdminMockMvc.perform(get("/api/school-admins/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSchoolAdmin() throws Exception {
        // Initialize the database
        schoolAdminRepository.saveAndFlush(schoolAdmin);
        int databaseSizeBeforeUpdate = schoolAdminRepository.findAll().size();

        // Update the schoolAdmin
        SchoolAdmin updatedSchoolAdmin = schoolAdminRepository.findOne(schoolAdmin.getId());
        SchoolAdminDTO schoolAdminDTO = schoolAdminMapper.toDto(updatedSchoolAdmin);

        restSchoolAdminMockMvc.perform(put("/api/school-admins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(schoolAdminDTO)))
            .andExpect(status().isOk());

        // Validate the SchoolAdmin in the database
        List<SchoolAdmin> schoolAdminList = schoolAdminRepository.findAll();
        assertThat(schoolAdminList).hasSize(databaseSizeBeforeUpdate);
        SchoolAdmin testSchoolAdmin = schoolAdminList.get(schoolAdminList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingSchoolAdmin() throws Exception {
        int databaseSizeBeforeUpdate = schoolAdminRepository.findAll().size();

        // Create the SchoolAdmin
        SchoolAdminDTO schoolAdminDTO = schoolAdminMapper.toDto(schoolAdmin);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSchoolAdminMockMvc.perform(put("/api/school-admins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(schoolAdminDTO)))
            .andExpect(status().isCreated());

        // Validate the SchoolAdmin in the database
        List<SchoolAdmin> schoolAdminList = schoolAdminRepository.findAll();
        assertThat(schoolAdminList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSchoolAdmin() throws Exception {
        // Initialize the database
        schoolAdminRepository.saveAndFlush(schoolAdmin);
        int databaseSizeBeforeDelete = schoolAdminRepository.findAll().size();

        // Get the schoolAdmin
        restSchoolAdminMockMvc.perform(delete("/api/school-admins/{id}", schoolAdmin.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SchoolAdmin> schoolAdminList = schoolAdminRepository.findAll();
        assertThat(schoolAdminList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SchoolAdmin.class);
        SchoolAdmin schoolAdmin1 = new SchoolAdmin();
        schoolAdmin1.setId(1L);
        SchoolAdmin schoolAdmin2 = new SchoolAdmin();
        schoolAdmin2.setId(schoolAdmin1.getId());
        assertThat(schoolAdmin1).isEqualTo(schoolAdmin2);
        schoolAdmin2.setId(2L);
        assertThat(schoolAdmin1).isNotEqualTo(schoolAdmin2);
        schoolAdmin1.setId(null);
        assertThat(schoolAdmin1).isNotEqualTo(schoolAdmin2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SchoolAdminDTO.class);
        SchoolAdminDTO schoolAdminDTO1 = new SchoolAdminDTO();
        schoolAdminDTO1.setId(1L);
        SchoolAdminDTO schoolAdminDTO2 = new SchoolAdminDTO();
        assertThat(schoolAdminDTO1).isNotEqualTo(schoolAdminDTO2);
        schoolAdminDTO2.setId(schoolAdminDTO1.getId());
        assertThat(schoolAdminDTO1).isEqualTo(schoolAdminDTO2);
        schoolAdminDTO2.setId(2L);
        assertThat(schoolAdminDTO1).isNotEqualTo(schoolAdminDTO2);
        schoolAdminDTO1.setId(null);
        assertThat(schoolAdminDTO1).isNotEqualTo(schoolAdminDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(schoolAdminMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(schoolAdminMapper.fromId(null)).isNull();
    }
}
