package rs.co.tntech.busride.web.rest;

import rs.co.tntech.busride.BusRidePlatformBackendApp;

import rs.co.tntech.busride.domain.PickupReport;
import rs.co.tntech.busride.repository.PickupReportRepository;
import rs.co.tntech.busride.service.PickupReportService;
import rs.co.tntech.busride.service.dto.PickupReportDTO;
import rs.co.tntech.busride.service.mapper.PickupReportMapper;
import rs.co.tntech.busride.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import rs.co.tntech.busride.domain.enumeration.PickupDropoffType;
/**
 * Test class for the PickupReportResource REST controller.
 *
 * @see PickupReportResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BusRidePlatformBackendApp.class)
public class PickupReportResourceIntTest {

    private static final PickupDropoffType DEFAULT_STATUS = PickupDropoffType.SUCCESSFUL;
    private static final PickupDropoffType UPDATED_STATUS = PickupDropoffType.INCOMPLETE;

    private static final Double DEFAULT_DELAY = 1D;
    private static final Double UPDATED_DELAY = 2D;

    private static final Integer DEFAULT_PLANNED = 1;
    private static final Integer UPDATED_PLANNED = 2;

    private static final Integer DEFAULT_ACCOMPLISHED = 1;
    private static final Integer UPDATED_ACCOMPLISHED = 2;

    @Autowired
    private PickupReportRepository pickupReportRepository;

    @Autowired
    private PickupReportMapper pickupReportMapper;

    @Autowired
    private PickupReportService pickupReportService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPickupReportMockMvc;

    private PickupReport pickupReport;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PickupReportResource pickupReportResource = new PickupReportResource(pickupReportService);
        this.restPickupReportMockMvc = MockMvcBuilders.standaloneSetup(pickupReportResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PickupReport createEntity(EntityManager em) {
        PickupReport pickupReport = new PickupReport()
            .status(DEFAULT_STATUS)
            .delay(DEFAULT_DELAY)
            .planned(DEFAULT_PLANNED)
            .accomplished(DEFAULT_ACCOMPLISHED);
        return pickupReport;
    }

    @Before
    public void initTest() {
        pickupReport = createEntity(em);
    }

    @Test
    @Transactional
    public void createPickupReport() throws Exception {
        int databaseSizeBeforeCreate = pickupReportRepository.findAll().size();

        // Create the PickupReport
        PickupReportDTO pickupReportDTO = pickupReportMapper.toDto(pickupReport);
        restPickupReportMockMvc.perform(post("/api/pickup-reports")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pickupReportDTO)))
            .andExpect(status().isCreated());

        // Validate the PickupReport in the database
        List<PickupReport> pickupReportList = pickupReportRepository.findAll();
        assertThat(pickupReportList).hasSize(databaseSizeBeforeCreate + 1);
        PickupReport testPickupReport = pickupReportList.get(pickupReportList.size() - 1);
        assertThat(testPickupReport.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testPickupReport.getDelay()).isEqualTo(DEFAULT_DELAY);
        assertThat(testPickupReport.getPlanned()).isEqualTo(DEFAULT_PLANNED);
        assertThat(testPickupReport.getAccomplished()).isEqualTo(DEFAULT_ACCOMPLISHED);
    }

    @Test
    @Transactional
    public void createPickupReportWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pickupReportRepository.findAll().size();

        // Create the PickupReport with an existing ID
        pickupReport.setId(1L);
        PickupReportDTO pickupReportDTO = pickupReportMapper.toDto(pickupReport);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPickupReportMockMvc.perform(post("/api/pickup-reports")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pickupReportDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PickupReport> pickupReportList = pickupReportRepository.findAll();
        assertThat(pickupReportList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPickupReports() throws Exception {
        // Initialize the database
        pickupReportRepository.saveAndFlush(pickupReport);

        // Get all the pickupReportList
        restPickupReportMockMvc.perform(get("/api/pickup-reports?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pickupReport.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].delay").value(hasItem(DEFAULT_DELAY.doubleValue())))
            .andExpect(jsonPath("$.[*].planned").value(hasItem(DEFAULT_PLANNED)))
            .andExpect(jsonPath("$.[*].accomplished").value(hasItem(DEFAULT_ACCOMPLISHED)));
    }

    @Test
    @Transactional
    public void getPickupReport() throws Exception {
        // Initialize the database
        pickupReportRepository.saveAndFlush(pickupReport);

        // Get the pickupReport
        restPickupReportMockMvc.perform(get("/api/pickup-reports/{id}", pickupReport.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pickupReport.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.delay").value(DEFAULT_DELAY.doubleValue()))
            .andExpect(jsonPath("$.planned").value(DEFAULT_PLANNED))
            .andExpect(jsonPath("$.accomplished").value(DEFAULT_ACCOMPLISHED));
    }

    @Test
    @Transactional
    public void getNonExistingPickupReport() throws Exception {
        // Get the pickupReport
        restPickupReportMockMvc.perform(get("/api/pickup-reports/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePickupReport() throws Exception {
        // Initialize the database
        pickupReportRepository.saveAndFlush(pickupReport);
        int databaseSizeBeforeUpdate = pickupReportRepository.findAll().size();

        // Update the pickupReport
        PickupReport updatedPickupReport = pickupReportRepository.findOne(pickupReport.getId());
        updatedPickupReport
            .status(UPDATED_STATUS)
            .delay(UPDATED_DELAY)
            .planned(UPDATED_PLANNED)
            .accomplished(UPDATED_ACCOMPLISHED);
        PickupReportDTO pickupReportDTO = pickupReportMapper.toDto(updatedPickupReport);

        restPickupReportMockMvc.perform(put("/api/pickup-reports")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pickupReportDTO)))
            .andExpect(status().isOk());

        // Validate the PickupReport in the database
        List<PickupReport> pickupReportList = pickupReportRepository.findAll();
        assertThat(pickupReportList).hasSize(databaseSizeBeforeUpdate);
        PickupReport testPickupReport = pickupReportList.get(pickupReportList.size() - 1);
        assertThat(testPickupReport.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testPickupReport.getDelay()).isEqualTo(UPDATED_DELAY);
        assertThat(testPickupReport.getPlanned()).isEqualTo(UPDATED_PLANNED);
        assertThat(testPickupReport.getAccomplished()).isEqualTo(UPDATED_ACCOMPLISHED);
    }

    @Test
    @Transactional
    public void updateNonExistingPickupReport() throws Exception {
        int databaseSizeBeforeUpdate = pickupReportRepository.findAll().size();

        // Create the PickupReport
        PickupReportDTO pickupReportDTO = pickupReportMapper.toDto(pickupReport);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPickupReportMockMvc.perform(put("/api/pickup-reports")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pickupReportDTO)))
            .andExpect(status().isCreated());

        // Validate the PickupReport in the database
        List<PickupReport> pickupReportList = pickupReportRepository.findAll();
        assertThat(pickupReportList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePickupReport() throws Exception {
        // Initialize the database
        pickupReportRepository.saveAndFlush(pickupReport);
        int databaseSizeBeforeDelete = pickupReportRepository.findAll().size();

        // Get the pickupReport
        restPickupReportMockMvc.perform(delete("/api/pickup-reports/{id}", pickupReport.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PickupReport> pickupReportList = pickupReportRepository.findAll();
        assertThat(pickupReportList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PickupReport.class);
        PickupReport pickupReport1 = new PickupReport();
        pickupReport1.setId(1L);
        PickupReport pickupReport2 = new PickupReport();
        pickupReport2.setId(pickupReport1.getId());
        assertThat(pickupReport1).isEqualTo(pickupReport2);
        pickupReport2.setId(2L);
        assertThat(pickupReport1).isNotEqualTo(pickupReport2);
        pickupReport1.setId(null);
        assertThat(pickupReport1).isNotEqualTo(pickupReport2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PickupReportDTO.class);
        PickupReportDTO pickupReportDTO1 = new PickupReportDTO();
        pickupReportDTO1.setId(1L);
        PickupReportDTO pickupReportDTO2 = new PickupReportDTO();
        assertThat(pickupReportDTO1).isNotEqualTo(pickupReportDTO2);
        pickupReportDTO2.setId(pickupReportDTO1.getId());
        assertThat(pickupReportDTO1).isEqualTo(pickupReportDTO2);
        pickupReportDTO2.setId(2L);
        assertThat(pickupReportDTO1).isNotEqualTo(pickupReportDTO2);
        pickupReportDTO1.setId(null);
        assertThat(pickupReportDTO1).isNotEqualTo(pickupReportDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pickupReportMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pickupReportMapper.fromId(null)).isNull();
    }
}
