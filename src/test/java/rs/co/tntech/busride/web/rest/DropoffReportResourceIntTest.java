package rs.co.tntech.busride.web.rest;

import rs.co.tntech.busride.BusRidePlatformBackendApp;

import rs.co.tntech.busride.domain.DropoffReport;
import rs.co.tntech.busride.repository.DropoffReportRepository;
import rs.co.tntech.busride.service.DropoffReportService;
import rs.co.tntech.busride.service.dto.DropoffReportDTO;
import rs.co.tntech.busride.service.mapper.DropoffReportMapper;
import rs.co.tntech.busride.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import rs.co.tntech.busride.domain.enumeration.PickupDropoffType;
/**
 * Test class for the DropoffReportResource REST controller.
 *
 * @see DropoffReportResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BusRidePlatformBackendApp.class)
public class DropoffReportResourceIntTest {

    private static final PickupDropoffType DEFAULT_STATUS = PickupDropoffType.SUCCESSFUL;
    private static final PickupDropoffType UPDATED_STATUS = PickupDropoffType.INCOMPLETE;

    private static final Integer DEFAULT_PLANNED = 1;
    private static final Integer UPDATED_PLANNED = 2;

    private static final Integer DEFAULT_ACCOMPLISHED = 1;
    private static final Integer UPDATED_ACCOMPLISHED = 2;

    @Autowired
    private DropoffReportRepository dropoffReportRepository;

    @Autowired
    private DropoffReportMapper dropoffReportMapper;

    @Autowired
    private DropoffReportService dropoffReportService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDropoffReportMockMvc;

    private DropoffReport dropoffReport;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DropoffReportResource dropoffReportResource = new DropoffReportResource(dropoffReportService);
        this.restDropoffReportMockMvc = MockMvcBuilders.standaloneSetup(dropoffReportResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DropoffReport createEntity(EntityManager em) {
        DropoffReport dropoffReport = new DropoffReport()
            .status(DEFAULT_STATUS)
            .planned(DEFAULT_PLANNED)
            .accomplished(DEFAULT_ACCOMPLISHED);
        return dropoffReport;
    }

    @Before
    public void initTest() {
        dropoffReport = createEntity(em);
    }

    @Test
    @Transactional
    public void createDropoffReport() throws Exception {
        int databaseSizeBeforeCreate = dropoffReportRepository.findAll().size();

        // Create the DropoffReport
        DropoffReportDTO dropoffReportDTO = dropoffReportMapper.toDto(dropoffReport);
        restDropoffReportMockMvc.perform(post("/api/dropoff-reports")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dropoffReportDTO)))
            .andExpect(status().isCreated());

        // Validate the DropoffReport in the database
        List<DropoffReport> dropoffReportList = dropoffReportRepository.findAll();
        assertThat(dropoffReportList).hasSize(databaseSizeBeforeCreate + 1);
        DropoffReport testDropoffReport = dropoffReportList.get(dropoffReportList.size() - 1);
        assertThat(testDropoffReport.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testDropoffReport.getPlanned()).isEqualTo(DEFAULT_PLANNED);
        assertThat(testDropoffReport.getAccomplished()).isEqualTo(DEFAULT_ACCOMPLISHED);
    }

    @Test
    @Transactional
    public void createDropoffReportWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dropoffReportRepository.findAll().size();

        // Create the DropoffReport with an existing ID
        dropoffReport.setId(1L);
        DropoffReportDTO dropoffReportDTO = dropoffReportMapper.toDto(dropoffReport);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDropoffReportMockMvc.perform(post("/api/dropoff-reports")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dropoffReportDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DropoffReport> dropoffReportList = dropoffReportRepository.findAll();
        assertThat(dropoffReportList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDropoffReports() throws Exception {
        // Initialize the database
        dropoffReportRepository.saveAndFlush(dropoffReport);

        // Get all the dropoffReportList
        restDropoffReportMockMvc.perform(get("/api/dropoff-reports?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dropoffReport.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].planned").value(hasItem(DEFAULT_PLANNED)))
            .andExpect(jsonPath("$.[*].accomplished").value(hasItem(DEFAULT_ACCOMPLISHED)));
    }

    @Test
    @Transactional
    public void getDropoffReport() throws Exception {
        // Initialize the database
        dropoffReportRepository.saveAndFlush(dropoffReport);

        // Get the dropoffReport
        restDropoffReportMockMvc.perform(get("/api/dropoff-reports/{id}", dropoffReport.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dropoffReport.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.planned").value(DEFAULT_PLANNED))
            .andExpect(jsonPath("$.accomplished").value(DEFAULT_ACCOMPLISHED));
    }

    @Test
    @Transactional
    public void getNonExistingDropoffReport() throws Exception {
        // Get the dropoffReport
        restDropoffReportMockMvc.perform(get("/api/dropoff-reports/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDropoffReport() throws Exception {
        // Initialize the database
        dropoffReportRepository.saveAndFlush(dropoffReport);
        int databaseSizeBeforeUpdate = dropoffReportRepository.findAll().size();

        // Update the dropoffReport
        DropoffReport updatedDropoffReport = dropoffReportRepository.findOne(dropoffReport.getId());
        updatedDropoffReport
            .status(UPDATED_STATUS)
            .planned(UPDATED_PLANNED)
            .accomplished(UPDATED_ACCOMPLISHED);
        DropoffReportDTO dropoffReportDTO = dropoffReportMapper.toDto(updatedDropoffReport);

        restDropoffReportMockMvc.perform(put("/api/dropoff-reports")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dropoffReportDTO)))
            .andExpect(status().isOk());

        // Validate the DropoffReport in the database
        List<DropoffReport> dropoffReportList = dropoffReportRepository.findAll();
        assertThat(dropoffReportList).hasSize(databaseSizeBeforeUpdate);
        DropoffReport testDropoffReport = dropoffReportList.get(dropoffReportList.size() - 1);
        assertThat(testDropoffReport.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDropoffReport.getPlanned()).isEqualTo(UPDATED_PLANNED);
        assertThat(testDropoffReport.getAccomplished()).isEqualTo(UPDATED_ACCOMPLISHED);
    }

    @Test
    @Transactional
    public void updateNonExistingDropoffReport() throws Exception {
        int databaseSizeBeforeUpdate = dropoffReportRepository.findAll().size();

        // Create the DropoffReport
        DropoffReportDTO dropoffReportDTO = dropoffReportMapper.toDto(dropoffReport);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDropoffReportMockMvc.perform(put("/api/dropoff-reports")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dropoffReportDTO)))
            .andExpect(status().isCreated());

        // Validate the DropoffReport in the database
        List<DropoffReport> dropoffReportList = dropoffReportRepository.findAll();
        assertThat(dropoffReportList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDropoffReport() throws Exception {
        // Initialize the database
        dropoffReportRepository.saveAndFlush(dropoffReport);
        int databaseSizeBeforeDelete = dropoffReportRepository.findAll().size();

        // Get the dropoffReport
        restDropoffReportMockMvc.perform(delete("/api/dropoff-reports/{id}", dropoffReport.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DropoffReport> dropoffReportList = dropoffReportRepository.findAll();
        assertThat(dropoffReportList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DropoffReport.class);
        DropoffReport dropoffReport1 = new DropoffReport();
        dropoffReport1.setId(1L);
        DropoffReport dropoffReport2 = new DropoffReport();
        dropoffReport2.setId(dropoffReport1.getId());
        assertThat(dropoffReport1).isEqualTo(dropoffReport2);
        dropoffReport2.setId(2L);
        assertThat(dropoffReport1).isNotEqualTo(dropoffReport2);
        dropoffReport1.setId(null);
        assertThat(dropoffReport1).isNotEqualTo(dropoffReport2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DropoffReportDTO.class);
        DropoffReportDTO dropoffReportDTO1 = new DropoffReportDTO();
        dropoffReportDTO1.setId(1L);
        DropoffReportDTO dropoffReportDTO2 = new DropoffReportDTO();
        assertThat(dropoffReportDTO1).isNotEqualTo(dropoffReportDTO2);
        dropoffReportDTO2.setId(dropoffReportDTO1.getId());
        assertThat(dropoffReportDTO1).isEqualTo(dropoffReportDTO2);
        dropoffReportDTO2.setId(2L);
        assertThat(dropoffReportDTO1).isNotEqualTo(dropoffReportDTO2);
        dropoffReportDTO1.setId(null);
        assertThat(dropoffReportDTO1).isNotEqualTo(dropoffReportDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(dropoffReportMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(dropoffReportMapper.fromId(null)).isNull();
    }
}
