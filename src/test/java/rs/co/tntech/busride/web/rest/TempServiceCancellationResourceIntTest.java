package rs.co.tntech.busride.web.rest;

import rs.co.tntech.busride.BusRidePlatformBackendApp;
import rs.co.tntech.busride.domain.BusRoute;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.School;
import rs.co.tntech.busride.domain.Student;
import rs.co.tntech.busride.domain.TempServiceCancellation;
import rs.co.tntech.busride.repository.TempServiceCancellationRepository;
import rs.co.tntech.busride.service.dto.TempServiceCancellationDTO;
import rs.co.tntech.busride.service.mapper.TempServiceCancellationMapper;
import rs.co.tntech.busride.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonObject;

import javax.persistence.EntityManager;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TempServiceCancellationResource REST controller.
 *
 * @see TempServiceCancellationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BusRidePlatformBackendApp.class)
public class TempServiceCancellationResourceIntTest {

    @Autowired
    private TempServiceCancellationRepository tempServiceCancellationRepository;
    @Autowired
    private TempServiceCancellationMapper tempServiceCancellationMapper;
    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
    @Autowired
    private ExceptionTranslator exceptionTranslator;
    @Autowired
    private EntityManager em;
    @Autowired
    private TempServiceCancellationResource tempServiceCancellationResource;
    @Autowired
    private TestDataUtil testDataUtil;
    @Autowired
    private TestSecurityUtil testSecurityUtil;
    
    private MockMvc restTempServiceCancellationMockMvc;

    private TempServiceCancellation tempServiceCancellation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.restTempServiceCancellationMockMvc = MockMvcBuilders.standaloneSetup(tempServiceCancellationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TempServiceCancellation createEntity(EntityManager em) {
        TempServiceCancellation tempServiceCancellation = new TempServiceCancellation();
        return tempServiceCancellation;
    }

    @Before
    public void initTest() {
        tempServiceCancellation = createEntity(em);
    }

    @Test
    @Transactional
    public void createTempServiceCancellation() throws Exception {
        int databaseSizeBeforeCreate = tempServiceCancellationRepository.findAll().size();

        // Create the TempServiceCancellation
        TempServiceCancellationDTO tempServiceCancellationDTO = tempServiceCancellationMapper.toDto(tempServiceCancellation);
        restTempServiceCancellationMockMvc.perform(post("/api/temp-service-cancellations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tempServiceCancellationDTO)))
            .andExpect(status().isCreated());

        // Validate the TempServiceCancellation in the database
        List<TempServiceCancellation> tempServiceCancellationList = tempServiceCancellationRepository.findAll();
        assertThat(tempServiceCancellationList).hasSize(databaseSizeBeforeCreate + 1);
        TempServiceCancellation testTempServiceCancellation = tempServiceCancellationList.get(tempServiceCancellationList.size() - 1);
    }

    @Test
    @Transactional
    public void createTempServiceCancellationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tempServiceCancellationRepository.findAll().size();

        // Create the TempServiceCancellation with an existing ID
        tempServiceCancellation.setId(1L);
        TempServiceCancellationDTO tempServiceCancellationDTO = tempServiceCancellationMapper.toDto(tempServiceCancellation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTempServiceCancellationMockMvc.perform(post("/api/temp-service-cancellations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tempServiceCancellationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<TempServiceCancellation> tempServiceCancellationList = tempServiceCancellationRepository.findAll();
        assertThat(tempServiceCancellationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTempServiceCancellations() throws Exception {
        // Initialize the database
        tempServiceCancellationRepository.saveAndFlush(tempServiceCancellation);

        // Get all the tempServiceCancellationList
        restTempServiceCancellationMockMvc.perform(get("/api/temp-service-cancellations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tempServiceCancellation.getId().intValue())));
    }

    @Test
    @Transactional
    public void getTempServiceCancellation() throws Exception {
        // Initialize the database
        tempServiceCancellationRepository.saveAndFlush(tempServiceCancellation);

        // Get the tempServiceCancellation
        restTempServiceCancellationMockMvc.perform(get("/api/temp-service-cancellations/{id}", tempServiceCancellation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tempServiceCancellation.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTempServiceCancellation() throws Exception {
        // Get the tempServiceCancellation
        restTempServiceCancellationMockMvc.perform(get("/api/temp-service-cancellations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTempServiceCancellation() throws Exception {
        // Initialize the database
        tempServiceCancellationRepository.saveAndFlush(tempServiceCancellation);
        int databaseSizeBeforeUpdate = tempServiceCancellationRepository.findAll().size();

        // Update the tempServiceCancellation
        TempServiceCancellation updatedTempServiceCancellation = tempServiceCancellationRepository.findOne(tempServiceCancellation.getId());
        TempServiceCancellationDTO tempServiceCancellationDTO = tempServiceCancellationMapper.toDto(updatedTempServiceCancellation);

        restTempServiceCancellationMockMvc.perform(put("/api/temp-service-cancellations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tempServiceCancellationDTO)))
            .andExpect(status().isOk());

        // Validate the TempServiceCancellation in the database
        List<TempServiceCancellation> tempServiceCancellationList = tempServiceCancellationRepository.findAll();
        assertThat(tempServiceCancellationList).hasSize(databaseSizeBeforeUpdate);
        TempServiceCancellation testTempServiceCancellation = tempServiceCancellationList.get(tempServiceCancellationList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingTempServiceCancellation() throws Exception {
        int databaseSizeBeforeUpdate = tempServiceCancellationRepository.findAll().size();

        // Create the TempServiceCancellation
        TempServiceCancellationDTO tempServiceCancellationDTO = tempServiceCancellationMapper.toDto(tempServiceCancellation);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTempServiceCancellationMockMvc.perform(put("/api/temp-service-cancellations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tempServiceCancellationDTO)))
            .andExpect(status().isCreated());

        // Validate the TempServiceCancellation in the database
        List<TempServiceCancellation> tempServiceCancellationList = tempServiceCancellationRepository.findAll();
        assertThat(tempServiceCancellationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTempServiceCancellation() throws Exception {
        // Initialize the database
        tempServiceCancellationRepository.saveAndFlush(tempServiceCancellation);
        int databaseSizeBeforeDelete = tempServiceCancellationRepository.findAll().size();

        // Get the tempServiceCancellation
        restTempServiceCancellationMockMvc.perform(delete("/api/temp-service-cancellations/{id}", tempServiceCancellation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TempServiceCancellation> tempServiceCancellationList = tempServiceCancellationRepository.findAll();
        assertThat(tempServiceCancellationList).hasSize(databaseSizeBeforeDelete - 1);
    }
    
    @Test
    @Transactional
    public void createTempServiceCancellationForCurrentParentAndNonExistingUser() throws Exception {
		int databaseSizeBeforeCreate = tempServiceCancellationRepository.findAll().size();

		testSecurityUtil.login(TestSecurityUtil.USER_PARENT504);

		// Create the TempServiceCancellation for student 1
		JsonObject payload = new JsonObject();
		payload.addProperty("studentId", 123456789l);
		payload.addProperty("cancellation", "ON");
        
        restTempServiceCancellationMockMvc.perform(post("/api/temp-service-cancellations/current-parent")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(payload.toString()))
            .andExpect(status().isNotFound());
        
        List<TempServiceCancellation> tempServiceCancellationList = tempServiceCancellationRepository.findAll();
		assertThat(tempServiceCancellationList).hasSize(databaseSizeBeforeCreate);
    }
    
    @Test
    @Transactional
    public void createTempServiceCancellationForCurrentParent() throws Exception {
		Parent parent = testDataUtil.createParent();
		Student student1 = testDataUtil.createStudent(parent);
		Student student2 = testDataUtil.createStudent(parent);
		testDataUtil.createStudent(parent);
		int databaseSizeBeforeCreate = tempServiceCancellationRepository.findAll().size();

		testSecurityUtil.login(parent.getUser().getLogin());

		// Create the TempServiceCancellation for student 1
		JsonObject payload = new JsonObject();
		payload.addProperty("studentId", student1.getId());
		payload.addProperty("cancellation", "ON");
        
        restTempServiceCancellationMockMvc.perform(post("/api/temp-service-cancellations/current-parent")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(payload.toString()))
            .andExpect(status().isOk());

		// Validate the TempServiceCancellation in the database
		List<TempServiceCancellation> tempServiceCancellationList = tempServiceCancellationRepository.findAll();
		assertThat(tempServiceCancellationList).hasSize(databaseSizeBeforeCreate + 1);
		TempServiceCancellation testTempServiceCancellation = tempServiceCancellationList.get(tempServiceCancellationList.size() - 1);
		assertThat(testTempServiceCancellation.getStudent().getId()).isEqualTo(student1.getId());
		assertThat(testTempServiceCancellation.getSpot().getId()).isEqualTo(parent.getSpot().getId());
        
		// Create the TempServiceCancellation for student 2
		payload = new JsonObject();
		payload.addProperty("studentId", student2.getId());
		payload.addProperty("cancellation", "ON");
        
        restTempServiceCancellationMockMvc.perform(post("/api/temp-service-cancellations/current-parent")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(payload.toString()))
            .andExpect(status().isOk());

		// Validate the TempServiceCancellation in the database
		tempServiceCancellationList = tempServiceCancellationRepository.findAll();
		assertThat(tempServiceCancellationList).hasSize(databaseSizeBeforeCreate + 2);
		testTempServiceCancellation = tempServiceCancellationList.get(tempServiceCancellationList.size() - 1);
		assertThat(testTempServiceCancellation.getStudent().getId()).isEqualTo(student2.getId());
		assertThat(testTempServiceCancellation.getSpot().getId()).isEqualTo(parent.getSpot().getId());
		
		// Remove the TempServiceCancellation for student 1
		payload = new JsonObject();
		payload.addProperty("studentId", student1.getId());
		payload.addProperty("cancellation", "OFF");
        
        restTempServiceCancellationMockMvc.perform(post("/api/temp-service-cancellations/current-parent")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(payload.toString()))
            .andExpect(status().isOk());

		// Validate the TempServiceCancellation in the database
		tempServiceCancellationList = tempServiceCancellationRepository.findAll();
		assertThat(tempServiceCancellationList).hasSize(databaseSizeBeforeCreate + 1);
		testTempServiceCancellation = tempServiceCancellationList.get(tempServiceCancellationList.size() - 1);
		assertThat(testTempServiceCancellation.getStudent().getId()).isEqualTo(student2.getId());
		assertThat(testTempServiceCancellation.getSpot().getId()).isEqualTo(parent.getSpot().getId());

		// Remove the TempServiceCancellation for student 2
		payload = new JsonObject();
		payload.addProperty("studentId", student2.getId());
		payload.addProperty("cancellation", "OFF");
        
        restTempServiceCancellationMockMvc.perform(post("/api/temp-service-cancellations/current-parent")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(payload.toString()))
            .andExpect(status().isOk());

		// Validate the TempServiceCancellation in the database
		tempServiceCancellationList = tempServiceCancellationRepository.findAll();
		assertThat(tempServiceCancellationList).hasSize(databaseSizeBeforeCreate);
    }
    
    @Test
    @Transactional
    public void getTempServiceCancellationsForCurrentParent() throws Exception {
		Parent parent = testDataUtil.createCustomParent();
		Student student1 = testDataUtil.createStudent(parent);
		Student student2 = testDataUtil.createStudent(parent);
		testDataUtil.createStudent(parent);
		testDataUtil.createTempServiceCancellation(student1);
		testDataUtil.createTempServiceCancellation(student2);

		testSecurityUtil.login(parent.getUser().getLogin());

		// Create the TempServiceCancellation for student 1
        restTempServiceCancellationMockMvc.perform(get("/api/temp-service-cancellations/current-parent"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].studentId").value(hasItem(student1.getId().intValue())))
            .andExpect(jsonPath("$.[*].studentId").value(hasItem(student2.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getTempServiceCancellationsForCurrentDriver() throws Exception {
		Parent parent = testDataUtil.createCustomParent();
    	School school = parent.getSchools().iterator().next();
		Student student1 = testDataUtil.createStudent(parent, school);
		Student student2 = testDataUtil.createStudent(parent, school);
		Student student3 = testDataUtil.createStudent(parent);;
		TempServiceCancellation tmpSerCanc1 = testDataUtil.createTempServiceCancellation(student1);
		TempServiceCancellation tmpSerCanc2 = testDataUtil.createTempServiceCancellation(student2);
		TempServiceCancellation tmpSerCanc3 = testDataUtil.createTempServiceCancellation(student3);
		BusRoute busRoute = testDataUtil.createActiveBusRoute(school, Arrays.asList(parent.getSpot()));
		
		testSecurityUtil.login(busRoute.getDriver().getUser().getLogin());

		// Create the TempServiceCancellation for student 1
        restTempServiceCancellationMockMvc.perform(get("/api/temp-service-cancellations/current-driver"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].id").value(hasItem(tmpSerCanc1.getId().intValue())))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tmpSerCanc2.getId().intValue())));
            // TODO add this validation
            //.andExpect(jsonPath("$.[*].id").value(not(hasItem(tmpSerCanc2.getId().intValue()))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TempServiceCancellation.class);
        TempServiceCancellation tempServiceCancellation1 = new TempServiceCancellation();
        tempServiceCancellation1.setId(1L);
        TempServiceCancellation tempServiceCancellation2 = new TempServiceCancellation();
        tempServiceCancellation2.setId(tempServiceCancellation1.getId());
        assertThat(tempServiceCancellation1).isEqualTo(tempServiceCancellation2);
        tempServiceCancellation2.setId(2L);
        assertThat(tempServiceCancellation1).isNotEqualTo(tempServiceCancellation2);
        tempServiceCancellation1.setId(null);
        assertThat(tempServiceCancellation1).isNotEqualTo(tempServiceCancellation2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TempServiceCancellationDTO.class);
        TempServiceCancellationDTO tempServiceCancellationDTO1 = new TempServiceCancellationDTO();
        tempServiceCancellationDTO1.setId(1L);
        TempServiceCancellationDTO tempServiceCancellationDTO2 = new TempServiceCancellationDTO();
        assertThat(tempServiceCancellationDTO1).isNotEqualTo(tempServiceCancellationDTO2);
        tempServiceCancellationDTO2.setId(tempServiceCancellationDTO1.getId());
        assertThat(tempServiceCancellationDTO1).isEqualTo(tempServiceCancellationDTO2);
        tempServiceCancellationDTO2.setId(2L);
        assertThat(tempServiceCancellationDTO1).isNotEqualTo(tempServiceCancellationDTO2);
        tempServiceCancellationDTO1.setId(null);
        assertThat(tempServiceCancellationDTO1).isNotEqualTo(tempServiceCancellationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(tempServiceCancellationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(tempServiceCancellationMapper.fromId(null)).isNull();
    }
}
