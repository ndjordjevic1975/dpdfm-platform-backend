package rs.co.tntech.busride.cucumber.stepdefs;

import rs.co.tntech.busride.BusRidePlatformBackendApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = BusRidePlatformBackendApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
