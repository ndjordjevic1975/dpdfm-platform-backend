package rs.co.tntech.busride.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import rs.co.tntech.busride.domain.enumeration.SpotType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Spot.
 */
@Entity
@Table(name = "spot")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Spot implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "gps_latitude")
    private Double gpsLatitude;

    @Column(name = "gps_longitude")
    private Double gpsLongitude;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type")
    private SpotType type;

    @ManyToMany(mappedBy = "spots")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<BusRoute> busRoutes = new HashSet<>();

    public Spot() {
    }

    public Spot(Double gpsLatitude, Double gpsLongitude) {
        this.gpsLatitude = gpsLatitude;
        this.gpsLongitude = gpsLongitude;
    }

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getGpsLatitude() {
        return gpsLatitude;
    }

    public Spot gpsLatitude(Double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
        return this;
    }

    public void setGpsLatitude(Double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public Double getGpsLongitude() {
        return gpsLongitude;
    }

    public Spot gpsLongitude(Double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
        return this;
    }

    public void setGpsLongitude(Double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public SpotType getType() {
        return type;
    }

    public Spot type(SpotType type) {
        this.type = type;
        return this;
    }

    public void setType(SpotType type) {
        this.type = type;
    }

    public Set<BusRoute> getBusRoutes() {
        return busRoutes;
    }

    public Spot busRoutes(Set<BusRoute> busRoutes) {
        this.busRoutes = busRoutes;
        return this;
    }

    public Spot addBusRoute(BusRoute busRoute) {
        this.busRoutes.add(busRoute);
        busRoute.getSpots().add(this);
        return this;
    }

    public Spot removeBusRoute(BusRoute busRoute) {
        this.busRoutes.remove(busRoute);
        busRoute.getSpots().remove(this);
        return this;
    }

    public void setBusRoutes(Set<BusRoute> busRoutes) {
        this.busRoutes = busRoutes;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Spot spot = (Spot) o;
        if (spot.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), spot.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Spot{" +
            "id=" + getId() +
            ", gpsLatitude='" + getGpsLatitude() + "'" +
            ", gpsLongitude='" + getGpsLongitude() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
