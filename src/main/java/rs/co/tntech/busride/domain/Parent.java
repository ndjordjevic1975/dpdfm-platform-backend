package rs.co.tntech.busride.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import rs.co.tntech.busride.domain.enumeration.DeviceType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Parent.
 */
@Entity
@Table(name = "parent")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Parent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "address")
    private String address;

    @Column(name = "house_number")
    private String houseNumber;

    @Column(name = "city")
    private String city;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "state")
    private String state;

    @Column(name = "country")
    private String country;

    @Column(name = "phone")
    private String phone;

    @Column(name = "land_phone")
    private String landPhone;

    @Column(name = "no_of_students")
    private Integer noOfStudents;

    @Column(name = "device_identifier")
    private String deviceIdentifier;

    @Enumerated(EnumType.STRING)
    @Column(name = "device_type")
    private DeviceType deviceType;

    @Column(name = "gcm_session_id")
    private String gcmSessionId;

    @Column(name = "endpoint_arn")
    private String endpointARN;

    @Column(name = "national_id")
    private String nationalId;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToOne
    @JoinColumn(unique = true)
    private Spot spot;

    @ManyToOne
    private Parent linkToPrimary;

    @ManyToMany(mappedBy = "parents")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<School> schools = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public Parent address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public Parent houseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
        return this;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getCity() {
        return city;
    }

    public Parent city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public Parent zipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getState() {
        return state;
    }

    public Parent state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public Parent country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public Parent phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLandPhone() {
        return landPhone;
    }

    public Parent landPhone(String landPhone) {
        this.landPhone = landPhone;
        return this;
    }

    public void setLandPhone(String landPhone) {
        this.landPhone = landPhone;
    }

    public Integer getNoOfStudents() {
        return noOfStudents;
    }

    public Parent noOfStudents(Integer noOfStudents) {
        this.noOfStudents = noOfStudents;
        return this;
    }

    public void setNoOfStudents(Integer noOfStudents) {
        this.noOfStudents = noOfStudents;
    }

    public String getDeviceIdentifier() {
        return deviceIdentifier;
    }

    public Parent deviceIdentifier(String deviceIdentifier) {
        this.deviceIdentifier = deviceIdentifier;
        return this;
    }

    public void setDeviceIdentifier(String deviceIdentifier) {
        this.deviceIdentifier = deviceIdentifier;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public Parent deviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
        return this;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public String getGcmSessionId() {
        return gcmSessionId;
    }

    public Parent gcmSessionId(String gcmSessionId) {
        this.gcmSessionId = gcmSessionId;
        return this;
    }

    public void setGcmSessionId(String gcmSessionId) {
        this.gcmSessionId = gcmSessionId;
    }

    public String getEndpointARN() {
        return endpointARN;
    }

    public Parent endpointARN(String endpointARN) {
        this.endpointARN = endpointARN;
        return this;
    }

    public void setEndpointARN(String endpointARN) {
        this.endpointARN = endpointARN;
    }

    public String getNationalId() {
        return nationalId;
    }

    public Parent nationalId(String nationalId) {
        this.nationalId = nationalId;
        return this;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public User getUser() {
        return user;
    }

    public Parent user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Spot getSpot() {
        return spot;
    }

    public Parent spot(Spot spot) {
        this.spot = spot;
        return this;
    }

    public void setSpot(Spot spot) {
        this.spot = spot;
    }

    public Parent getLinkToPrimary() {
        return linkToPrimary;
    }

    public Parent linkToPrimary(Parent parent) {
        this.linkToPrimary = parent;
        return this;
    }

    public void setLinkToPrimary(Parent parent) {
        this.linkToPrimary = parent;
    }

    public Set<School> getSchools() {
        return schools;
    }

    public Parent schools(Set<School> schools) {
        this.schools = schools;
        return this;
    }

    public Parent addSchool(School school) {
        this.schools.add(school);
        school.getParents().add(this);
        return this;
    }

    public Parent removeSchool(School school) {
        this.schools.remove(school);
        school.getParents().remove(this);
        return this;
    }

    public void setSchools(Set<School> schools) {
        this.schools = schools;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Parent parent = (Parent) o;
        if (parent.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), parent.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Parent{" +
            "id=" + getId() +
            ", address='" + getAddress() + "'" +
            ", houseNumber='" + getHouseNumber() + "'" +
            ", city='" + getCity() + "'" +
            ", zipCode='" + getZipCode() + "'" +
            ", state='" + getState() + "'" +
            ", country='" + getCountry() + "'" +
            ", phone='" + getPhone() + "'" +
            ", landPhone='" + getLandPhone() + "'" +
            ", noOfStudents='" + getNoOfStudents() + "'" +
            ", deviceIdentifier='" + getDeviceIdentifier() + "'" +
            ", deviceType='" + getDeviceType() + "'" +
            ", gcmSessionId='" + getGcmSessionId() + "'" +
            ", endpointARN='" + getEndpointARN() + "'" +
            ", nationalId='" + getNationalId() + "'" +
            "}";
    }
}
