package rs.co.tntech.busride.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import rs.co.tntech.busride.domain.enumeration.PickupDropoffType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DropoffReport.
 */
@Entity
@Table(name = "dropoff_report")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DropoffReport implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private PickupDropoffType status;

    @Column(name = "planned")
    private Integer planned;

    @Column(name = "accomplished")
    private Integer accomplished;

    @ManyToOne
    private BusRoute busRoute;

    @ManyToOne
    private Spot spot;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PickupDropoffType getStatus() {
        return status;
    }

    public DropoffReport status(PickupDropoffType status) {
        this.status = status;
        return this;
    }

    public void setStatus(PickupDropoffType status) {
        this.status = status;
    }

    public Integer getPlanned() {
        return planned;
    }

    public DropoffReport planned(Integer planned) {
        this.planned = planned;
        return this;
    }

    public void setPlanned(Integer planned) {
        this.planned = planned;
    }

    public Integer getAccomplished() {
        return accomplished;
    }

    public DropoffReport accomplished(Integer accomplished) {
        this.accomplished = accomplished;
        return this;
    }

    public void setAccomplished(Integer accomplished) {
        this.accomplished = accomplished;
    }

    public BusRoute getBusRoute() {
        return busRoute;
    }

    public DropoffReport busRoute(BusRoute busRoute) {
        this.busRoute = busRoute;
        return this;
    }

    public void setBusRoute(BusRoute busRoute) {
        this.busRoute = busRoute;
    }

    public Spot getSpot() {
        return spot;
    }

    public DropoffReport spot(Spot spot) {
        this.spot = spot;
        return this;
    }

    public void setSpot(Spot spot) {
        this.spot = spot;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DropoffReport dropoffReport = (DropoffReport) o;
        if (dropoffReport.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dropoffReport.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DropoffReport{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", planned='" + getPlanned() + "'" +
            ", accomplished='" + getAccomplished() + "'" +
            "}";
    }
}
