package rs.co.tntech.busride.domain.enumeration;

/**
 * The DeviceType enumeration.
 */
public enum DeviceType {
    ANDROID, IOS
}
