package rs.co.tntech.busride.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import rs.co.tntech.busride.domain.enumeration.ParentNotificationType;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A ParentNotification.
 */
@Entity
@Table(name = "parent_notification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ParentNotification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "notification")
    private String notification;

    @Column(name = "jhi_timestamp")
    private Instant timestamp;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type")
    private ParentNotificationType type;

    @ManyToOne
    private Parent parent;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNotification() {
        return notification;
    }

    public ParentNotification notification(String notification) {
        this.notification = notification;
        return this;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public ParentNotification timestamp(Instant timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public ParentNotificationType getType() {
        return type;
    }

    public ParentNotification type(ParentNotificationType type) {
        this.type = type;
        return this;
    }

    public void setType(ParentNotificationType type) {
        this.type = type;
    }

    public Parent getParent() {
        return parent;
    }

    public ParentNotification parent(Parent parent) {
        this.parent = parent;
        return this;
    }

    public void setParent(Parent parent) {
        this.parent = parent;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ParentNotification parentNotification = (ParentNotification) o;
        if (parentNotification.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), parentNotification.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ParentNotification{" +
            "id=" + getId() +
            ", notification='" + getNotification() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
