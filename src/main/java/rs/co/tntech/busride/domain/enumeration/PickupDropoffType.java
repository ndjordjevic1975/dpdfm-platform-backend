package rs.co.tntech.busride.domain.enumeration;

/**
 * The PickupDropoffType enumeration.
 */
public enum PickupDropoffType {
    SUCCESSFUL, INCOMPLETE, UNSUCCESSFUL
}
