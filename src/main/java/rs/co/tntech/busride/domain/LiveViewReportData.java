package rs.co.tntech.busride.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

public class LiveViewReportData implements Serializable {

    private Double gpsLatitude;
    private Double gpsLongitude;
    private LocalDateTime updatedOn;

    protected LiveViewReportData() {
    }

    public LiveViewReportData(Double gpsLatitude, Double gpsLongitude, LocalDateTime updatedOn) {
        this.gpsLatitude = gpsLatitude;
        this.gpsLongitude = gpsLongitude;
        this.updatedOn = updatedOn;
    }

    public Double getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(Double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public Double getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(Double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateTime updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "LiveViewReportData{" +
            "gpsLatitude=" + gpsLatitude +
            ", gpsLongitude=" + gpsLongitude +
            ", updatedOn=" + updatedOn +
            '}';
    }
}
