package rs.co.tntech.busride.domain.enumeration;

/**
 * The DriverNotificationType enumeration.
 */
public enum DriverNotificationType {
    PROXIMITY, ARRIVED, SCHOOL_START, SCHOOL_END, SKIP_SPOT
}
