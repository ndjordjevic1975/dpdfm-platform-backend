package rs.co.tntech.busride.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Driver.
 */
@Entity
@Table(name = "driver")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Driver implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "capacity")
    private Integer capacity;

    @Column(name = "device_identifier")
    private String deviceIdentifier;

    @Column(name = "gcm_session_id")
    private String gcmSessionId;

    @Column(name = "endpoint_arn")
    private String endpointARN;

    @Column(name = "tablet_number")
    private String tabletNumber;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToOne
    @JoinColumn(unique = true)
    private BusRoute activeBusRoute;

    @ManyToOne
    private School school;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public Driver capacity(Integer capacity) {
        this.capacity = capacity;
        return this;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getDeviceIdentifier() {
        return deviceIdentifier;
    }

    public Driver deviceIdentifier(String deviceIdentifier) {
        this.deviceIdentifier = deviceIdentifier;
        return this;
    }

    public void setDeviceIdentifier(String deviceIdentifier) {
        this.deviceIdentifier = deviceIdentifier;
    }

    public String getGcmSessionId() {
        return gcmSessionId;
    }

    public Driver gcmSessionId(String gcmSessionId) {
        this.gcmSessionId = gcmSessionId;
        return this;
    }

    public void setGcmSessionId(String gcmSessionId) {
        this.gcmSessionId = gcmSessionId;
    }

    public String getEndpointARN() {
        return endpointARN;
    }

    public Driver endpointARN(String endpointARN) {
        this.endpointARN = endpointARN;
        return this;
    }

    public void setEndpointARN(String endpointARN) {
        this.endpointARN = endpointARN;
    }

    public String getTabletNumber() {
        return tabletNumber;
    }

    public Driver tabletNumber(String tabletNumber) {
        this.tabletNumber = tabletNumber;
        return this;
    }

    public void setTabletNumber(String tabletNumber) {
        this.tabletNumber = tabletNumber;
    }

    public User getUser() {
        return user;
    }

    public Driver user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BusRoute getActiveBusRoute() {
        return activeBusRoute;
    }

    public Driver activeBusRoute(BusRoute busRoute) {
        this.activeBusRoute = busRoute;
        return this;
    }

    public void setActiveBusRoute(BusRoute busRoute) {
        this.activeBusRoute = busRoute;
    }

    public School getSchool() {
        return school;
    }

    public Driver school(School school) {
        this.school = school;
        return this;
    }

    public void setSchool(School school) {
        this.school = school;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Driver driver = (Driver) o;
        if (driver.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), driver.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Driver{" +
            "id=" + getId() +
            ", capacity='" + getCapacity() + "'" +
            ", deviceIdentifier='" + getDeviceIdentifier() + "'" +
            ", gcmSessionId='" + getGcmSessionId() + "'" +
            ", endpointARN='" + getEndpointARN() + "'" +
            ", tabletNumber='" + getTabletNumber() + "'" +
            "}";
    }
}
