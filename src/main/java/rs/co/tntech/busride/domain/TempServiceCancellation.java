package rs.co.tntech.busride.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import rs.co.tntech.busride.domain.enumeration.StudentServiceType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TempServiceCancellation.
 */
@Entity
@Table(name = "temp_service_cancellation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TempServiceCancellation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "cancellation_type")
    private StudentServiceType cancellationType;

    @Column(name = "reason")
    private String reason;

    @OneToOne
    @JoinColumn(unique = true)
    private Student student;

    @ManyToOne
    private Spot spot;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StudentServiceType getCancellationType() {
        return cancellationType;
    }

    public TempServiceCancellation cancellationType(StudentServiceType cancellationType) {
        this.cancellationType = cancellationType;
        return this;
    }

    public void setCancellationType(StudentServiceType cancellationType) {
        this.cancellationType = cancellationType;
    }

    public String getReason() {
        return reason;
    }

    public TempServiceCancellation reason(String reason) {
        this.reason = reason;
        return this;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Student getStudent() {
        return student;
    }

    public TempServiceCancellation student(Student student) {
        this.student = student;
        return this;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Spot getSpot() {
        return spot;
    }

    public TempServiceCancellation spot(Spot spot) {
        this.spot = spot;
        return this;
    }

    public void setSpot(Spot spot) {
        this.spot = spot;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TempServiceCancellation tempServiceCancellation = (TempServiceCancellation) o;
        if (tempServiceCancellation.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tempServiceCancellation.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TempServiceCancellation{" +
            "id=" + getId() +
            ", cancellationType='" + getCancellationType() + "'" +
            ", reason='" + getReason() + "'" +
            "}";
    }
}
