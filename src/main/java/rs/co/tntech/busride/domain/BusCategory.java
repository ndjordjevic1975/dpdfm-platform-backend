package rs.co.tntech.busride.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A BusCategory.
 */
@Entity
@Table(name = "bus_category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BusCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "no_of_buses")
    private Integer noOfBuses;

    @Column(name = "capacity")
    private Integer capacity;

    @ManyToOne
    private School school;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNoOfBuses() {
        return noOfBuses;
    }

    public BusCategory noOfBuses(Integer noOfBuses) {
        this.noOfBuses = noOfBuses;
        return this;
    }

    public void setNoOfBuses(Integer noOfBuses) {
        this.noOfBuses = noOfBuses;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public BusCategory capacity(Integer capacity) {
        this.capacity = capacity;
        return this;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public School getSchool() {
        return school;
    }

    public BusCategory school(School school) {
        this.school = school;
        return this;
    }

    public void setSchool(School school) {
        this.school = school;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BusCategory busCategory = (BusCategory) o;
        if (busCategory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), busCategory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BusCategory{" +
            "id=" + getId() +
            ", noOfBuses='" + getNoOfBuses() + "'" +
            ", capacity='" + getCapacity() + "'" +
            "}";
    }
}
