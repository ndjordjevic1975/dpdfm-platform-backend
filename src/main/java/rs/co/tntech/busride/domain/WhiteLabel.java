package rs.co.tntech.busride.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A WhiteLabel.
 */
@Entity
@Table(name = "white_label")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class WhiteLabel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    @Column(name = "parent_white_label")
    private byte[] parentWhiteLabel;

    @Column(name = "parent_white_label_content_type")
    private String parentWhiteLabelContentType = "application/zip";

    @Lob
    @Column(name = "driver_white_label")
    private byte[] driverWhiteLabel;

    @Column(name = "driver_white_label_content_type")
    private String driverWhiteLabelContentType = "application/zip";

    @Column(name = "parent_white_label_version")
    private Integer parentWhiteLabelVersion;

    @Column(name = "driver_white_label_version")
    private Integer driverWhiteLabelVersion;

    @Lob
    @Column(name = "splash_screen")
    private byte[] splashScreen;

    @Column(name = "splash_screen_content_type")
    private String splashScreenContentType = "image/png";

    @Lob
    @Column(name = "icn_bus")
    private byte[] icnBus;

    @Column(name = "icn_bus_content_type")
    private String icnBusContentType = "image/png";

    @Lob
    @Column(name = "logo_bg")
    private byte[] logoBg;

    @Column(name = "logo_bg_content_type")
    private String logoBgContentType = "image/png";

    @Lob
    @Column(name = "logo_txt")
    private byte[] logoTxt;

    @Column(name = "logo_txt_content_type")
    private String logoTxtContentType = "image/png";

    @OneToOne
    @JoinColumn(unique = true)
    private School school;

    public WhiteLabel(School school) {
        this.school = school;
    }

    public WhiteLabel() {

    }

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getParentWhiteLabel() {
        return parentWhiteLabel;
    }

    public WhiteLabel parentWhiteLabel(byte[] parentWhiteLabel) {
        this.parentWhiteLabel = parentWhiteLabel;
        return this;
    }

    public void setParentWhiteLabel(byte[] parentWhiteLabel) {
        this.parentWhiteLabel = parentWhiteLabel;
    }

    public String getParentWhiteLabelContentType() {
        return parentWhiteLabelContentType;
    }

    public WhiteLabel parentWhiteLabelContentType(String parentWhiteLabelContentType) {
        this.parentWhiteLabelContentType = parentWhiteLabelContentType;
        return this;
    }

    public void setParentWhiteLabelContentType(String parentWhiteLabelContentType) {
        this.parentWhiteLabelContentType = parentWhiteLabelContentType;
    }

    public byte[] getDriverWhiteLabel() {
        return driverWhiteLabel;
    }

    public WhiteLabel driverWhiteLabel(byte[] driverWhiteLabel) {
        this.driverWhiteLabel = driverWhiteLabel;
        return this;
    }

    public void setDriverWhiteLabel(byte[] driverWhiteLabel) {
        this.driverWhiteLabel = driverWhiteLabel;
    }

    public String getDriverWhiteLabelContentType() {
        return driverWhiteLabelContentType;
    }

    public WhiteLabel driverWhiteLabelContentType(String driverWhiteLabelContentType) {
        this.driverWhiteLabelContentType = driverWhiteLabelContentType;
        return this;
    }

    public void setDriverWhiteLabelContentType(String driverWhiteLabelContentType) {
        this.driverWhiteLabelContentType = driverWhiteLabelContentType;
    }

    public Integer getParentWhiteLabelVersion() {
        return parentWhiteLabelVersion;
    }

    public WhiteLabel parentWhiteLabelVersion(Integer parentWhiteLabelVersion) {
        this.parentWhiteLabelVersion = parentWhiteLabelVersion;
        return this;
    }

    public void setParentWhiteLabelVersion(Integer parentWhiteLabelVersion) {
        this.parentWhiteLabelVersion = parentWhiteLabelVersion;
    }

    public Integer getDriverWhiteLabelVersion() {
        return driverWhiteLabelVersion;
    }

    public WhiteLabel driverWhiteLabelVersion(Integer driverWhiteLabelVersion) {
        this.driverWhiteLabelVersion = driverWhiteLabelVersion;
        return this;
    }

    public void setDriverWhiteLabelVersion(Integer driverWhiteLabelVersion) {
        this.driverWhiteLabelVersion = driverWhiteLabelVersion;
    }

    public byte[] getSplashScreen() {
        return splashScreen;
    }

    public WhiteLabel splashScreen(byte[] splashScreen) {
        this.splashScreen = splashScreen;
        return this;
    }

    public void setSplashScreen(byte[] splashScreen) {
        this.splashScreen = splashScreen;
    }

    public String getSplashScreenContentType() {
        return splashScreenContentType;
    }

    public WhiteLabel splashScreenContentType(String splashScreenContentType) {
        this.splashScreenContentType = splashScreenContentType;
        return this;
    }

    public void setSplashScreenContentType(String splashScreenContentType) {
        this.splashScreenContentType = splashScreenContentType;
    }

    public byte[] getIcnBus() {
        return icnBus;
    }

    public WhiteLabel icnBus(byte[] icnBus) {
        this.icnBus = icnBus;
        return this;
    }

    public void setIcnBus(byte[] icnBus) {
        this.icnBus = icnBus;
    }

    public String getIcnBusContentType() {
        return icnBusContentType;
    }

    public WhiteLabel icnBusContentType(String icnBusContentType) {
        this.icnBusContentType = icnBusContentType;
        return this;
    }

    public void setIcnBusContentType(String icnBusContentType) {
        this.icnBusContentType = icnBusContentType;
    }

    public byte[] getLogoBg() {
        return logoBg;
    }

    public WhiteLabel logoBg(byte[] logoBg) {
        this.logoBg = logoBg;
        return this;
    }

    public void setLogoBg(byte[] logoBg) {
        this.logoBg = logoBg;
    }

    public String getLogoBgContentType() {
        return logoBgContentType;
    }

    public WhiteLabel logoBgContentType(String logoBgContentType) {
        this.logoBgContentType = logoBgContentType;
        return this;
    }

    public void setLogoBgContentType(String logoBgContentType) {
        this.logoBgContentType = logoBgContentType;
    }

    public byte[] getLogoTxt() {
        return logoTxt;
    }

    public WhiteLabel logoTxt(byte[] logoTxt) {
        this.logoTxt = logoTxt;
        return this;
    }

    public void setLogoTxt(byte[] logoTxt) {
        this.logoTxt = logoTxt;
    }

    public String getLogoTxtContentType() {
        return logoTxtContentType;
    }

    public WhiteLabel logoTxtContentType(String logoTxtContentType) {
        this.logoTxtContentType = logoTxtContentType;
        return this;
    }

    public void setLogoTxtContentType(String logoTxtContentType) {
        this.logoTxtContentType = logoTxtContentType;
    }

    public School getSchool() {
        return school;
    }

    public WhiteLabel school(School school) {
        this.school = school;
        return this;
    }

    public void setSchool(School school) {
        this.school = school;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WhiteLabel whiteLabel = (WhiteLabel) o;
        if (whiteLabel.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), whiteLabel.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WhiteLabel{" +
            "id=" + getId() +
            ", parentWhiteLabel='" + getParentWhiteLabel() + "'" +
            ", parentWhiteLabelContentType='" + parentWhiteLabelContentType + "'" +
            ", driverWhiteLabel='" + getDriverWhiteLabel() + "'" +
            ", driverWhiteLabelContentType='" + driverWhiteLabelContentType + "'" +
            ", parentWhiteLabelVersion='" + getParentWhiteLabelVersion() + "'" +
            ", driverWhiteLabelVersion='" + getDriverWhiteLabelVersion() + "'" +
            ", splashScreen='" + getSplashScreen() + "'" +
            ", splashScreenContentType='" + splashScreenContentType + "'" +
            ", icnBus='" + getIcnBus() + "'" +
            ", icnBusContentType='" + icnBusContentType + "'" +
            ", logoBg='" + getLogoBg() + "'" +
            ", logoBgContentType='" + logoBgContentType + "'" +
            ", logoTxt='" + getLogoTxt() + "'" +
            ", logoTxtContentType='" + logoTxtContentType + "'" +
            "}";
    }
}
