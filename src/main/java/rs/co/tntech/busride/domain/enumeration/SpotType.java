package rs.co.tntech.busride.domain.enumeration;

/**
 * The SpotType enumeration.
 */
public enum SpotType {
    SCHOOL, PICKUP_DROPOFF
}
