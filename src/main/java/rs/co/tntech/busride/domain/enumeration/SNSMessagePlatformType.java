package rs.co.tntech.busride.domain.enumeration;

public enum SNSMessagePlatformType {
    GCM, APNS
}
