package rs.co.tntech.busride.domain.enumeration;

/**
 * The ParentNotificationType enumeration.
 */
public enum ParentNotificationType {
    ANNOUNCEMENT, POLICY_CHANGE_ANNOUNCEMENT, ACCOUNT_NOTIFICATION, MEETING_REQUEST, EMERGENCY, DRIVER
}
