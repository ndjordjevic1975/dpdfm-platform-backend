package rs.co.tntech.busride.domain.enumeration;

/**
 * The BusRouteType enumeration.
 */
public enum BusRouteType {
    PICKUP, DROPOFF
}
