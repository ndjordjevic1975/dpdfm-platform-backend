package rs.co.tntech.busride.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import rs.co.tntech.busride.domain.enumeration.BusRouteType;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A BusRoute.
 */
@Entity
@Table(name = "bus_route")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BusRoute implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    @Column(name = "details")
    private String details;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type")
    private BusRouteType type;

    @Column(name = "capacity")
    private Integer capacity;

    @Column(name = "created_on")
    private Instant createdOn;

    @Column(name = "inactive_on")
    private Instant inactiveOn;

    @ManyToOne
    private School school;

    @ManyToOne
    private Driver driver;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "bus_route_spot",
               joinColumns = @JoinColumn(name="bus_routes_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="spots_id", referencedColumnName="id"))
    private Set<Spot> spots = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetails() {
        return details;
    }

    public BusRoute details(String details) {
        this.details = details;
        return this;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public BusRouteType getType() {
        return type;
    }

    public BusRoute type(BusRouteType type) {
        this.type = type;
        return this;
    }

    public void setType(BusRouteType type) {
        this.type = type;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public BusRoute capacity(Integer capacity) {
        this.capacity = capacity;
        return this;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Instant getCreatedOn() {
        return createdOn;
    }

    public BusRoute createdOn(Instant createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public Instant getInactiveOn() {
        return inactiveOn;
    }

    public BusRoute inactiveOn(Instant inactiveOn) {
        this.inactiveOn = inactiveOn;
        return this;
    }

    public void setInactiveOn(Instant inactiveOn) {
        this.inactiveOn = inactiveOn;
    }

    public School getSchool() {
        return school;
    }

    public BusRoute school(School school) {
        this.school = school;
        return this;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public Driver getDriver() {
        return driver;
    }

    public BusRoute driver(Driver driver) {
        this.driver = driver;
        return this;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Set<Spot> getSpots() {
        return spots;
    }

    public BusRoute spots(Set<Spot> spots) {
        this.spots = spots;
        return this;
    }

    public BusRoute addSpot(Spot spot) {
        this.spots.add(spot);
        spot.getBusRoutes().add(this);
        return this;
    }

    public BusRoute removeSpot(Spot spot) {
        this.spots.remove(spot);
        spot.getBusRoutes().remove(this);
        return this;
    }

    public void setSpots(Set<Spot> spots) {
        this.spots = spots;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BusRoute busRoute = (BusRoute) o;
        if (busRoute.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), busRoute.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BusRoute{" +
            "id=" + getId() +
            ", details='" + getDetails() + "'" +
            ", type='" + getType() + "'" +
            ", capacity='" + getCapacity() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", inactiveOn='" + getInactiveOn() + "'" +
            "}";
    }
}
