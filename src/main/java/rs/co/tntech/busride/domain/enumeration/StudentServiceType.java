package rs.co.tntech.busride.domain.enumeration;

/**
 * The StudentServiceType enumeration.
 */
public enum StudentServiceType {
    ROUND_TRIP, PICKUP, DROPOFF, OFF
}
