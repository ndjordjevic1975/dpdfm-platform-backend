package rs.co.tntech.busride.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import rs.co.tntech.busride.domain.enumeration.DriverNotificationType;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DriverNotification.
 */
@Entity
@Table(name = "driver_notification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DriverNotification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "notification")
    private String notification;

    @Column(name = "created_on_device")
    private Instant createdOnDevice;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type")
    private DriverNotificationType type;

    @ManyToOne
    private Driver driver;

    @ManyToOne
    private Spot spot;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNotification() {
        return notification;
    }

    public DriverNotification notification(String notification) {
        this.notification = notification;
        return this;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public Instant getCreatedOnDevice() {
        return createdOnDevice;
    }

    public DriverNotification createdOnDevice(Instant createdOnDevice) {
        this.createdOnDevice = createdOnDevice;
        return this;
    }

    public void setCreatedOnDevice(Instant createdOnDevice) {
        this.createdOnDevice = createdOnDevice;
    }

    public DriverNotificationType getType() {
        return type;
    }

    public DriverNotification type(DriverNotificationType type) {
        this.type = type;
        return this;
    }

    public void setType(DriverNotificationType type) {
        this.type = type;
    }

    public Driver getDriver() {
        return driver;
    }

    public DriverNotification driver(Driver driver) {
        this.driver = driver;
        return this;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Spot getSpot() {
        return spot;
    }

    public DriverNotification spot(Spot spot) {
        this.spot = spot;
        return this;
    }

    public void setSpot(Spot spot) {
        this.spot = spot;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DriverNotification driverNotification = (DriverNotification) o;
        if (driverNotification.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), driverNotification.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DriverNotification{" +
            "id=" + getId() +
            ", notification='" + getNotification() + "'" +
            ", createdOnDevice='" + getCreatedOnDevice() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
