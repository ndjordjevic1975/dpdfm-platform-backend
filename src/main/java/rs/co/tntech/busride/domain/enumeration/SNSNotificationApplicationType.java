package rs.co.tntech.busride.domain.enumeration;

public enum SNSNotificationApplicationType {
    PARENT_ANDROID, PARENT_IOS, DRIVER
}
