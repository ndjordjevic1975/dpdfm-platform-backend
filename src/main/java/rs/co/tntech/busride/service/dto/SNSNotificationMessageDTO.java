package rs.co.tntech.busride.service.dto;


import rs.co.tntech.busride.domain.enumeration.DriverNotificationMsgType;
import rs.co.tntech.busride.domain.enumeration.ParentNotificationMsgType;

import java.io.Serializable;

/**
 * A DTO for the DriverNotificationMessage.
 */
public class SNSNotificationMessageDTO implements Serializable {

    private DriverNotificationMsgType driverNotificationMsgType;
    private ParentNotificationMsgType parentNotificationMsgType;
    private String userLang;
    private String titleKey;
    private String[] titleArgs;
    private String contentKey;
    private String[] contentArgs;
    private Long canceled_stop;
    private String user;
    private Long notificationId;

    public SNSNotificationMessageDTO() {
    }

	/**
	 * Constructor for message which is send from Driver to Parent
	 *
	 * @param driverNotificationMsgType
	 * @param titleKey
	 * @param contentArgs
	 * @param contentKey
	 */
	public SNSNotificationMessageDTO(DriverNotificationMsgType driverNotificationMsgType, String titleKey, String contentKey, String[] contentArgs) {
        this.driverNotificationMsgType = driverNotificationMsgType;
        this.titleKey = titleKey;
        this.contentKey = contentKey;
		this.contentArgs = contentArgs;
    }

	public SNSNotificationMessageDTO(DriverNotificationMsgType driverNotificationMsgType) {
        this.driverNotificationMsgType = driverNotificationMsgType;
    }

	/**
	 * Constructor for message which is send from Parent to Driver
	 *
	 * @param parentNotificationMsgType
	 * @param contentKey
	 * @param contentArgs
	 * @param canceled_stop
	 */
	public SNSNotificationMessageDTO(ParentNotificationMsgType parentNotificationMsgType, String contentKey, String[] contentArgs, Long canceled_stop) {
        this.parentNotificationMsgType = parentNotificationMsgType;
        this.contentKey = contentKey;
        this.contentArgs = contentArgs;
        this.canceled_stop = canceled_stop;
    }

	public SNSNotificationMessageDTO(ParentNotificationMsgType parentNotificationMsgType) {
        this.parentNotificationMsgType = parentNotificationMsgType;
    }

    /**
	 * @return the driverNotificationMsgType
	 */
	public DriverNotificationMsgType getDriverNotificationMsgType() {
		return driverNotificationMsgType;
	}

	/**
	 * @param driverNotificationMsgType the driverNotificationMsgType to set
	 */
	public void setDriverNotificationMsgType(DriverNotificationMsgType driverNotificationMsgType) {
		this.driverNotificationMsgType = driverNotificationMsgType;
	}

	/**
	 * @return the parentNotificationMsgType
	 */
	public ParentNotificationMsgType getParentNotificationMsgType() {
		return parentNotificationMsgType;
	}

	/**
	 * @param parentNotificationMsgType the parentNotificationMsgType to set
	 */
	public void setParentNotificationMsgType(ParentNotificationMsgType parentNotificationMsgType) {
		this.parentNotificationMsgType = parentNotificationMsgType;
	}

	/**
	 * @return the userLang
	 */
	public String getUserLang() {
		return userLang;
	}

	/**
	 * @param userLang the userLang to set
	 */
	public void setUserLang(String userLang) {
		this.userLang = userLang;
	}

	/**
	 * @return the titleKey
	 */
	public String getTitleKey() {
		return titleKey;
	}

	/**
	 * @param titleKey the titleKey to set
	 */
	public void setTitleKey(String titleKey) {
		this.titleKey = titleKey;
	}

	/**
	 * @return the titleArgs
	 */
	public String[] getTitleArgs() {
		return titleArgs;
	}

	/**
	 * @param titleArgs the titleArgs to set
	 */
	public void setTitleArgs(String[] titleArgs) {
		this.titleArgs = titleArgs;
	}

	/**
	 * @return the contentKey
	 */
	public String getContentKey() {
		return contentKey;
	}

	/**
	 * @param contentKey the contentKey to set
	 */
	public void setContentKey(String contentKey) {
		this.contentKey = contentKey;
	}

	/**
	 * @return the contentArgs
	 */
	public String[] getContentArgs() {
		return contentArgs;
	}

	/**
	 * @param contentArgs the contentArgs to set
	 */
	public void setContentArgs(String[] contentArgs) {
		this.contentArgs = contentArgs;
	}

	/**
	 * @return the canceled_stop
	 */
	public Long getCanceled_stop() {
		return canceled_stop;
	}

	/**
	 * @param canceled_stop the canceled_stop to set
	 */
	public void setCanceled_stop(Long canceled_stop) {
		this.canceled_stop = canceled_stop;
	}

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
    
    /**
	 * @return the notificationId
	 */
	public Long getNotificationId() {
		return notificationId;
	}

	/**
	 * @param notificationId the notificationId to set
	 */
	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	@Override
    public String toString() {
        return "SNSNotificationMessageDTO{" +
            "driverNotificationMsgType=" + driverNotificationMsgType +
            ", parentNotificationMsgType=" + parentNotificationMsgType +
            ", userLang='" + userLang + '\'' +
            ", titleKey='" + titleKey + '\'' +
            ", contentKey='" + contentKey + '\'' +
            ", canceled_stop=" + canceled_stop +
            ", notificationId=" + notificationId +
            '}';
    }
}
