package rs.co.tntech.busride.service.dto;


import rs.co.tntech.busride.domain.enumeration.StudentServiceType;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the TempServiceCancellation entity.
 */
public class TempServiceCancellationDTO implements Serializable {

    private Long id;

    private StudentServiceType cancellationType;

    private String reason;

    private Long studentId;

    private Long spotId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StudentServiceType getCancellationType() {
        return cancellationType;
    }

    public void setCancellationType(StudentServiceType cancellationType) {
        this.cancellationType = cancellationType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getSpotId() {
        return spotId;
    }

    public void setSpotId(Long spotId) {
        this.spotId = spotId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TempServiceCancellationDTO tempServiceCancellationDTO = (TempServiceCancellationDTO) o;
        if(tempServiceCancellationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tempServiceCancellationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TempServiceCancellationDTO{" +
            "id=" + getId() +
            ", cancellationType='" + getCancellationType() + "'" +
            ", reason='" + getReason() + "'" +
            "}";
    }
}
