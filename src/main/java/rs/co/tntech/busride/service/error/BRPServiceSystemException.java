/**
 *
 */
package rs.co.tntech.busride.service.error;

import java.util.UUID;

/**
 * This exception is thrown in service layer when a system/unexpected exception occurs
 *
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class BRPServiceSystemException extends BRPBaseServiceException {

	private static final long serialVersionUID = 1L;
	private String errorId;

	/**
	 * @param message
	 */
	public BRPServiceSystemException(String message) {
		super(message);
		errorId = UUID.randomUUID().toString();
	}

	/**
	 * @return the errorId
	 */
	public String getErrorId() {
		return errorId;
	}

	/*
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		return "errorId: " + errorId + ", error: " + super.getMessage();
	}

}
