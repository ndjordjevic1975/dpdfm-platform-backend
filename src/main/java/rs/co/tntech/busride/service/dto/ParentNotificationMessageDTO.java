package rs.co.tntech.busride.service.dto;


import rs.co.tntech.busride.domain.enumeration.DriverNotificationMsgType;
import rs.co.tntech.busride.domain.enumeration.ParentNotificationMsgType;

import java.io.Serializable;

/**
 * A DTO for the ParentNotificationMessage.
 */
public class ParentNotificationMessageDTO implements Serializable {

    ParentNotificationMsgType message;
    String title;
    String content;


    public ParentNotificationMessageDTO() {
    }

    public ParentNotificationMessageDTO(ParentNotificationMsgType message, String title, String content) {
        this.message = message;
        this.title = title;
        this.content = content;
    }

    public ParentNotificationMsgType getMessage() {
        return message;
    }

    public void setMessage(ParentNotificationMsgType message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "ParentNotificationMessageDTO{" +
            "message=" + message +
            ", title='" + title + '\'' +
            ", content='" + content + '\'' +
            '}';
    }
}
