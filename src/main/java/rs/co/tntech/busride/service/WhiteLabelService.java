package rs.co.tntech.busride.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import rs.co.tntech.busride.domain.School;
import rs.co.tntech.busride.domain.WhiteLabel;
import rs.co.tntech.busride.repository.WhiteLabelRepository;
import rs.co.tntech.busride.security.AuthoritiesConstants;
import rs.co.tntech.busride.security.SecurityUtils;
import rs.co.tntech.busride.service.dto.FileDTO;
import rs.co.tntech.busride.service.dto.WhiteLabelDTO;
import rs.co.tntech.busride.service.error.BRPServiceSecurityException;
import rs.co.tntech.busride.service.mapper.WhiteLabelMapper;
import rs.co.tntech.busride.service.util.BRPServiceUtil;
import rs.co.tntech.busride.web.rest.util.PngImageName;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Service Implementation for managing WhiteLabel.
 */
@Service
@Transactional
public class WhiteLabelService {

    private final Logger log = LoggerFactory.getLogger(WhiteLabelService.class);

    private final WhiteLabelRepository whiteLabelRepository;

    private final WhiteLabelMapper whiteLabelMapper;

    @Autowired
    BRPServiceUtil serviceUtil;

    public WhiteLabelService(WhiteLabelRepository whiteLabelRepository, WhiteLabelMapper whiteLabelMapper) {
        this.whiteLabelRepository = whiteLabelRepository;
        this.whiteLabelMapper = whiteLabelMapper;
    }

    /**
     * Save a whiteLabel.
     *
     * @param whiteLabelDTO the entity to save
     * @return the persisted entity
     */
    public WhiteLabelDTO save(WhiteLabelDTO whiteLabelDTO) {
        log.debug("Request to save WhiteLabel : {}", whiteLabelDTO);
        WhiteLabel whiteLabel = whiteLabelMapper.toEntity(whiteLabelDTO);
        whiteLabel = whiteLabelRepository.save(whiteLabel);
        return whiteLabelMapper.toDto(whiteLabel);
    }

    /**
     * Get all the whiteLabels.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<WhiteLabelDTO> findAll() {
        log.debug("Request to get all WhiteLabels");
        return whiteLabelRepository.findAll().stream()
            .map(whiteLabelMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one whiteLabel by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public WhiteLabelDTO findOne(Long id) {
        log.debug("Request to get WhiteLabel : {}", id);
        WhiteLabel whiteLabel = whiteLabelRepository.findOne(id);
        return whiteLabelMapper.toDto(whiteLabel);
    }

    /**
     * Get whiteLabel (content and content type) for given school id and authenticated user type.<br>
     * If authenticated user is driver driverWhiteLabel will be returned, if authenticated user is parent parentWhiteLabel will be returned.
     *
     * @param schoolId the id of the school
     * @return the file object with content and content type
     */
    @Transactional(readOnly = true)
    public FileDTO findWhiteLabelBySchoolAndAUserType(Long schoolId) {
        log.debug("API:GET:IN:white-labels/by-school-id/{}: User {} made request to find WhiteLabel", schoolId, serviceUtil.getCurrentUser().getLogin());
        FileDTO file = null;

        Optional<WhiteLabel> whiteLabelOpt = whiteLabelRepository.findOneBySchool_id(schoolId);
        if (whiteLabelOpt.isPresent()) {
            WhiteLabel whiteLabel = whiteLabelOpt.get();
            if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.DRIVER)) {
                file = new FileDTO(whiteLabel.getDriverWhiteLabel(), whiteLabel.getDriverWhiteLabelContentType());
            } else if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.PARENT)) {
                file = new FileDTO(whiteLabel.getParentWhiteLabel(), whiteLabel.getParentWhiteLabelContentType());
            } else {
                log.warn("User with unsupported role tried to get white label by school #{}. Supported roles are {} and {}", schoolId,
                    AuthoritiesConstants.DRIVER, AuthoritiesConstants.PARENT);
            }
        }

        return file;
    }

    /**
     * Delete the  whiteLabel by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete WhiteLabel : {}", id);
        whiteLabelRepository.delete(id);
    }

    /**
     * Creates/Updated splash_screen.png
     *
     * @param splashScreenPng to update
     */
    public void saveSplashScreenPng(MultipartFile splashScreenPng) throws IOException {
        WhiteLabel whiteLabel = getWhiteLabelFromDb();

        whiteLabel.setSplashScreen(splashScreenPng.getBytes());

        byte[] zippedSplashScreenPng = zipSplashScreen(splashScreenPng);

        whiteLabel.setDriverWhiteLabel(zippedSplashScreenPng);
        whiteLabel.setDriverWhiteLabelVersion(whiteLabel.getDriverWhiteLabelVersion() != null ? whiteLabel.getDriverWhiteLabelVersion() + 1 : 1);

        whiteLabelRepository.save(whiteLabel);
    }

    /**
     * Creates/Updates logo_txt.png
     *
     * @param logoTxtPng to update
     */
    public void saveLogoTxtPng(MultipartFile logoTxtPng) throws IOException {
        WhiteLabel whiteLabel = getWhiteLabelFromDb();

        whiteLabel.setLogoTxt(logoTxtPng.getBytes());

        saveWhiteLabelForParent(whiteLabel);
    }

    /**
     * Creates/Updates logo_bg.png
     *
     * @param logoBgPng to update
     */
    public void saveLogoBgPng(MultipartFile logoBgPng) throws IOException {
        WhiteLabel whiteLabel = getWhiteLabelFromDb();

        whiteLabel.setLogoBg(logoBgPng.getBytes());

        saveWhiteLabelForParent(whiteLabel);
    }

    /**
     * Creates/Updates icn_bus.png
     *
     * @param icnBusPng to update
     */
    public void saveIcnBusPng(MultipartFile icnBusPng) throws IOException {
        WhiteLabel whiteLabel = getWhiteLabelFromDb();

        whiteLabel.setIcnBus(icnBusPng.getBytes());

        saveWhiteLabelForParent(whiteLabel);
    }

    /**
     * Returns a png image of a parent and driver white-labels
     *
     * @param image SPLASH_SCREEN|LOGO_TXT|LOGO_BG|ICN_BUS
     * @return a png images
     */
    public byte[] getSchoolDriverOrParentPng(PngImageName image) {
        School school = serviceUtil.getCurrentSchoolAdmin().getSchool();

        Optional<WhiteLabel> whiteLabelOptional = whiteLabelRepository.findOneBySchool_id(school.getId());

        if (whiteLabelOptional.isPresent()) {
            WhiteLabel whiteLabel = whiteLabelOptional.get();

            switch (image) {
                case SPLASH_SCREEN:
                    return whiteLabel.getSplashScreen();
                case LOGO_TXT:
                    return whiteLabel.getLogoTxt();
                case LOGO_BG:
                    return whiteLabel.getLogoBg();
                case ICN_BUS:
                    return whiteLabel.getIcnBus();
            }
        }

        return null;
    }

    /**
     * Returns parent white-label version
     *
     * @param schoolId school id
     * @return version
     */
    public Integer getParentWhiteLabelVersion(Long schoolId) throws BRPServiceSecurityException {
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.PARENT)) {
            Optional<WhiteLabel> whiteLabelOptional = whiteLabelRepository.findOneBySchool_id(schoolId);

            if (whiteLabelOptional.isPresent()) {
                WhiteLabel whiteLabel = whiteLabelOptional.get();

                return whiteLabel.getParentWhiteLabelVersion();
            } else {
                return null;
            }
        } else {
            String msg = String.format("User does not have any of required roles: '%s'", AuthoritiesConstants.PARENT);
            throw new BRPServiceSecurityException(AuthoritiesConstants.PARENT, msg);
        }
    }

    /**
     * Returns driver white-label version
     *
     * @param schoolId school id
     * @return version
     */
    public Integer getDriverWhiteLabelVersion(Long schoolId) throws BRPServiceSecurityException {
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.DRIVER)) {
            Optional<WhiteLabel> whiteLabelOptional = whiteLabelRepository.findOneBySchool_id(schoolId);

            if (whiteLabelOptional.isPresent()) {
                WhiteLabel whiteLabel = whiteLabelOptional.get();

                return whiteLabel.getDriverWhiteLabelVersion();
            } else {
                return null;
            }
        } else {
            String msg = String.format("User does not have any of required roles: '%s'", AuthoritiesConstants.DRIVER);
            throw new BRPServiceSecurityException(AuthoritiesConstants.DRIVER, msg);
        }
    }

    private void saveWhiteLabelForParent(WhiteLabel whiteLabel) throws IOException {
        if (allParentPngExist(whiteLabel)) {
            whiteLabel.setParentWhiteLabel(zipParentPng(whiteLabel));
            whiteLabel.setParentWhiteLabelVersion(whiteLabel.getParentWhiteLabelVersion() != null ? whiteLabel.getParentWhiteLabelVersion() + 1 : 1);
        }

        whiteLabelRepository.save(whiteLabel);
    }

    private WhiteLabel getWhiteLabelFromDb() {
        School school = serviceUtil.getCurrentSchoolAdmin().getSchool();

        final Optional<WhiteLabel> whiteLabelOptional = whiteLabelRepository.findOneBySchool_id(school.getId());
        return whiteLabelOptional.orElse(new WhiteLabel(school));
    }

    private byte[] zipParentPng(WhiteLabel whiteLabel) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zipOutputStream = new ZipOutputStream(bos);

        ZipEntry zipentry = new ZipEntry("content/logo_txt.png");
        zipOutputStream.putNextEntry(zipentry);
        zipOutputStream.write(whiteLabel.getLogoTxt());

        zipentry = new ZipEntry("content/logo_bg.png");
        zipOutputStream.putNextEntry(zipentry);
        zipOutputStream.write(whiteLabel.getLogoBg());

        zipentry = new ZipEntry("content/icn_bus.png");
        zipOutputStream.putNextEntry(zipentry);
        zipOutputStream.write(whiteLabel.getIcnBus());

        zipOutputStream.close();

        return bos.toByteArray();
    }

    private boolean allParentPngExist(WhiteLabel whiteLabel) {
        return whiteLabel.getLogoTxt() != null && whiteLabel.getLogoBg() != null && whiteLabel.getIcnBus() != null;
    }

    private byte[] zipSplashScreen(MultipartFile splashScreenPng) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zipOutputStream = new ZipOutputStream(bos);
        ZipEntry zipentry = new ZipEntry("content/splash_screen.png");

        zipOutputStream.putNextEntry(zipentry);
        zipOutputStream.write(splashScreenPng.getBytes());
        zipOutputStream.close();

        return bos.toByteArray();
    }
}
