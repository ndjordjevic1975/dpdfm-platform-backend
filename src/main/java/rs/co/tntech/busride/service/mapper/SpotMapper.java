package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.SpotDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Spot and its DTO SpotDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SpotMapper extends EntityMapper <SpotDTO, Spot> {
    
    @Mapping(target = "busRoutes", ignore = true)
    Spot toEntity(SpotDTO spotDTO); 
    default Spot fromId(Long id) {
        if (id == null) {
            return null;
        }
        Spot spot = new Spot();
        spot.setId(id);
        return spot;
    }
}
