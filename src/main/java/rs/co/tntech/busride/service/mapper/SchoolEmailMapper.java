package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.SchoolEmailDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SchoolEmail and its DTO SchoolEmailDTO.
 */
@Mapper(componentModel = "spring", uses = {SchoolMapper.class, })
public interface SchoolEmailMapper extends EntityMapper <SchoolEmailDTO, SchoolEmail> {

    @Mapping(source = "school.id", target = "schoolId")
    SchoolEmailDTO toDto(SchoolEmail schoolEmail); 

    @Mapping(source = "schoolId", target = "school")
    SchoolEmail toEntity(SchoolEmailDTO schoolEmailDTO); 
    default SchoolEmail fromId(Long id) {
        if (id == null) {
            return null;
        }
        SchoolEmail schoolEmail = new SchoolEmail();
        schoolEmail.setId(id);
        return schoolEmail;
    }
}
