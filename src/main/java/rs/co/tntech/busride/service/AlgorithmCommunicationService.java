package rs.co.tntech.busride.service;

import com.amazonaws.services.lambda.AWSLambdaAsync;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import gherkin.deps.com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.co.tntech.busride.config.ApplicationProperties;
import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.domain.enumeration.BusRouteType;
import rs.co.tntech.busride.domain.enumeration.StudentServiceType;
import rs.co.tntech.busride.repository.*;

import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by nemanja on 06-Jul-17.
 */
@Service
public class AlgorithmCommunicationService {

    private final Logger log = LoggerFactory.getLogger(AlgorithmCommunicationService.class);

    @Autowired
    AWSLambdaAsync awsLambda;
    @Autowired
    private ApplicationProperties applicationProperties;
    @Autowired
    private SchoolRepository schoolRepository;
    @Autowired
    private BusCategoryRepository busCategoryRepository;
    @Autowired
    private BusRouteRepository busRouteRepository;
    @Autowired
    private ParentRepository parentRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private SpotRepository spotRepository;
    @Autowired
    private DriverRepository driverRepository;

    public AlgorithmCommunicationService() {
    }

   @Transactional
    public void invokeAlgorithm(SchoolAdmin currentSchoolAdmin) throws Exception {
        String lambdaFunction = applicationProperties.getAwsLambdaRoutingFunction();
        log.debug("API:PUT:bus-routes/trigger-routes-calc: Trigger lambda function: {}", lambdaFunction);

       School school = schoolRepository.findOne(currentSchoolAdmin.getSchool().getId());

        if (school == null) {
            new ResponseEntity<>("Invoking of algorithm has failed, no school was found", HttpStatus.NOT_FOUND);
        } else {
            invokeAlgorithmPerSchool(lambdaFunction, school);
        }
    }

    private void invokeAlgorithmPerSchool(String lambdaFunction, School school) throws Exception {

        log.debug("API:LAMBDA:PUT:bus-routes/trigger-routes-calc: Trigger lambda function for School: {}", school.getName());

        String requestJSON = createLambdaRequestForSchool(school);

        InvokeResult result = invokeLambdaFunction(lambdaFunction, requestJSON);

        if (result != null) {
            String formattedResponsePayload = formatOutputResponsePayload(result);

            setInactiveBusRoutes(school);
            processLambdaResponse(formattedResponsePayload);
        } else {
            log.error("API:LAMBDA:PUT:bus-routes/trigger-routes-calc: Algorithm output is not a valid JSON object!");
        }
    }

    private void setInactiveBusRoutes(School school) {
        Instant inactiveOn = Instant.now();
        busRouteRepository.setInactiveOnExistingRoutes(inactiveOn, school);
    }

    private void processLambdaResponse(String payload) throws Exception {
        JsonObject mainResponseObject;

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(payload);
        if (element.isJsonObject()) {
            mainResponseObject = element.getAsJsonObject();
            Gson gson = new Gson();
            JsonArray allSchoolsRoutes = mainResponseObject.getAsJsonArray("allSchoolsRoutes");

            if (allSchoolsRoutes == null || allSchoolsRoutes.size() == 0) {
                throw new Exception("Lambda returned null or no routes");
            }

            persistDriverBusRoutes(gson, allSchoolsRoutes);

        } else {
            throw new IllegalArgumentException("API:LAMBDA:PUT:bus-routes/trigger-routes-calc: Algorithm output is not a valid JSON object!");
        }
    }

    private String formatOutputResponsePayload(InvokeResult result) {
        ByteBuffer byteBuffer = result.getPayload();
        String rawJson = null;
        try {
            rawJson = new String(byteBuffer.array(), "UTF-8");
            rawJson = rawJson.replace("\\\"", "\"");
            rawJson = rawJson.replaceAll("^\"|\"$", "");
        } catch (Exception e) {
            log.error("Got an exception with the following cause: {}", e.getCause());
        }
        return rawJson;
    }

    private InvokeResult invokeLambdaFunction(String lambdaFunction, String inputJSON) {
        InvokeResult invokeResult = null;
        InvokeRequest invokeRequest = new InvokeRequest()
            .withFunctionName(lambdaFunction)
            .withPayload(inputJSON);

        try {
            invokeResult = awsLambda.invoke(invokeRequest);
        } catch (Exception e) {
            log.error("API:LAMBDA:PUT:bus-routes/trigger-routes-calc: Exception {} ", e.getMessage());
        }
        return invokeResult;
    }

    private String createLambdaRequestForSchool(School school) {

        JsonObject mainJsonObject = new JsonObject();
        JsonArray schoolsDataArray = new JsonArray();
        Gson gson = new Gson();

        JsonObject schoolsDataObject = mapInputSchoolJson(school);

        if (schoolsDataObject != null) {
            schoolsDataArray.add(schoolsDataObject);
        }

        mainJsonObject.add("schoolsData", schoolsDataArray);

        return gson.toJson(mainJsonObject);
    }

    private JsonObject mapInputSchoolJson(School school) {
        log.debug("API:LAMBDA: Map the School with SchoolId: {} to the JSON", school.getId());
        JsonObject schoolsDataObject = new JsonObject();

        schoolsDataObject.addProperty("schoolName", school.getName());
        schoolsDataObject.addProperty("schoolId", school.getId());

        JsonObject schoolCoordinates = new JsonObject();
        schoolCoordinates.addProperty("latitude", school.getSpot().getGpsLatitude());
        schoolCoordinates.addProperty("longitude", school.getSpot().getGpsLongitude());
        schoolsDataObject.add("schoolCoordinates", schoolCoordinates);


        List<BusCategory> allBusCategoriesForSchool = busCategoryRepository.findAllBySchool(school);

        JsonArray allBusesInventory = new JsonArray();
        for (BusCategory busCategory : allBusCategoriesForSchool) {
            allBusesInventory.add(mapInputBusCategory(busCategory));
            schoolsDataObject.add("busesInventory", allBusesInventory);
        }

        JsonArray pickUps = mapPickupsDropOffs(school, Arrays.asList(StudentServiceType.ROUND_TRIP, StudentServiceType.PICKUP));
        schoolsDataObject.add("pickups", pickUps);

        JsonArray dropOffs = mapPickupsDropOffs(school, Arrays.asList(StudentServiceType.ROUND_TRIP, StudentServiceType.DROPOFF));
        schoolsDataObject.add("dropoffs", dropOffs);

        if (pickUps.size() == 0 || dropOffs.size() == 0) {
            log.debug("API:LAMBDA: Not sending data for school {}. There are no pickup/dropoff services registered", school.getName());
            return null;
        }

        return schoolsDataObject;
    }

    private JsonObject mapInputBusCategory(BusCategory busCategory) {
        log.debug("API:LAMBDA: Map the BusCategory with capacity: {} to the JSON", busCategory.getCapacity());
        JsonObject busesInventory = new JsonObject();
        busesInventory.addProperty("capacity", busCategory.getCapacity());
        busesInventory.addProperty("totalNumberOfBuses", busCategory.getNoOfBuses());
        return busesInventory;
    }

    private JsonArray mapPickupsDropOffs(School school, List<StudentServiceType> studentServiceTypes) {
        log.debug("API:LAMBDA: Map pickups/dropoffs for SchoolId: {}, and StudentServiceType: {}", school.getId(),
            studentServiceTypes.get(1));

        Set<Student> allStudentsBySchoolAndTypeOfService = studentRepository.findAllBySchoolAndTypeOfServiceIn(school, studentServiceTypes);

        Set<Parent> parents = allStudentsBySchoolAndTypeOfService.stream().map(student -> parentRepository.findOne(student.getParent().getId())).collect(Collectors.toSet());

        JsonArray pickUpsDropOffs = new JsonArray();
        for (Parent parent : parents) {
            if (parent.getUser().getActivated()) {
                JsonObject pickUpDropOff = mapInputPickUpDropOffStudents(studentServiceTypes, parent);
                pickUpsDropOffs.add(pickUpDropOff);
            }
        }
        return pickUpsDropOffs;
    }

    private JsonObject mapInputPickUpDropOffStudents(List<StudentServiceType> studentServiceTypes, Parent parent) {
        JsonObject pickUpDropOff = new JsonObject();
        pickUpDropOff.addProperty("id", parent.getSpot().getId());

        JsonObject gpsCoordinate = new JsonObject();
        gpsCoordinate.addProperty("latitude", parent.getSpot().getGpsLatitude());
        gpsCoordinate.addProperty("longitude", parent.getSpot().getGpsLongitude());
        pickUpDropOff.add("gpsCoordinate", gpsCoordinate);

        int studentsCount = studentRepository.countByParentAndTypeOfServiceIn(parent, studentServiceTypes);
        pickUpDropOff.addProperty("studentsCountAtPoint", studentsCount);

        JsonArray studentsArray = new JsonArray();
        List<Student> students = studentRepository.findByParentAndTypeOfServiceIn(parent, studentServiceTypes);
        students.stream().map(student -> new JsonPrimitive(student.getName())).forEach(studentsArray::add);

        pickUpDropOff.add("students", studentsArray);
        return pickUpDropOff;
    }

    private void persistDriverBusRoutes(Gson gson, JsonArray allSchoolsRoutes) {
        for (JsonElement allSchoolsRoute : allSchoolsRoutes) {

            JsonObject school = allSchoolsRoute.getAsJsonObject();
            Long schoolId = school.get("schoolId").getAsLong();

            JsonArray routes = school.getAsJsonArray("routes");

            Instant createdOn = Instant.now();

            for (JsonElement route : routes) {
                BusRoute busRoute = mapOutputBusRoute(gson, schoolId, route, createdOn);
                busRouteRepository.save(busRoute);
            }

            log.debug("API:LAMBDA: " + routes.size() + " routes generated for school: " + school.get("schoolName"));
        }
    }

    private BusRoute mapOutputBusRoute(Gson gson, Long schoolId, JsonElement route, Instant createdOn) {
        BusRoute busRoute = new BusRoute();
        busRoute.setSchool(schoolRepository.findOne(schoolId));
        JsonObject oneRoute = route.getAsJsonObject();

        Integer oneRouteCapacity = oneRoute.get("routeCapacity").getAsInt();
        busRoute.setCapacity(oneRouteCapacity);
        Integer oneRouteBusCapacity = oneRoute.get("bus_capacity").getAsInt();

        Driver driverToAssign = assignDriverToRoute(oneRouteBusCapacity, busRoute);
        if (driverToAssign != null) {
            busRoute.setDriver(driverToAssign);
        } else {
            log.debug("API:LAMBDA: Bus Route for school {} with route capacity {} and bus capacity {} could not be assigned a driver",
                busRoute.getSchool().getName(), oneRouteCapacity,oneRouteBusCapacity);
        }

        busRoute.setType(mapOutputRouteType(oneRoute));

        busRoute.setSpots(mapOutputSpots(oneRoute));

        String details = gson.toJson(route);
        busRoute.setDetails(details);
        busRoute.setCreatedOn(createdOn);

        return busRoute;
    }

    private BusRouteType mapOutputRouteType(JsonObject oneRoute) {
        Integer oneRouteType = oneRoute.get("route_type").getAsInt();

        if (oneRouteType == 0) {
            return BusRouteType.PICKUP;
        } else {
            return BusRouteType.DROPOFF;
        }
    }

    private Set<Spot> mapOutputSpots(JsonObject oneRoute) {
        JsonArray oneRoutePoints = oneRoute.getAsJsonArray("route_points");
        Set<Spot> spots = new HashSet<>();
        for (JsonElement oneRoutePoint : oneRoutePoints) {
            JsonObject routePoint = oneRoutePoint.getAsJsonObject();
            if (routePoint.get("is_stop").getAsBoolean()) {
                Long routePointId = routePoint.get("id").getAsLong();

                Spot spot;
                if (routePoint.get("studentsCountAtPoint").getAsInt() == 0) {
                    School school = schoolRepository.findOne(routePointId);

                    spot = school.getSpot();
                } else {
                    spot = spotRepository.findOne(routePointId);
                }

                if (spot != null) {
                    spot.setGpsLatitude(routePoint.get("Lat").getAsDouble());
                    spot.setGpsLongitude(routePoint.get("Lng").getAsDouble());
                    spots.add(spot);
                }
            }
        }

        return spots;
    }

    private Driver assignDriverToRoute(Integer busCapacity, BusRoute busRoute) {
        List<Driver> allDrivers = driverRepository.findAllBySchoolAndCapacity(busRoute.getSchool(), busCapacity);

        return allDrivers.stream().min(Comparator.comparing(driver -> busRouteRepository.findAllByDriver(driver).stream().filter(busRoute1 -> busRoute1.getInactiveOn() == null).collect(Collectors.toList()).size())).orElse(null);

    }
}
