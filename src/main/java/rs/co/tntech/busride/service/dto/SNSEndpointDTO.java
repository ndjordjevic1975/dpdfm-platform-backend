package rs.co.tntech.busride.service.dto;

import javax.validation.constraints.NotNull;

/**
 * Created by smirkovic on 7/17/2017.
 */
public class SNSEndpointDTO {

    @NotNull
    String token;
    Boolean isEnabled = true;
    String customUserData;
    String deviceARN;

    public SNSEndpointDTO() {
    }

    public SNSEndpointDTO(String customUserData, Boolean isEnabled, String token, String deviceARN) {
        this.customUserData = customUserData;
        this.isEnabled = isEnabled;
        this.token = token;
        this.deviceARN = deviceARN;
    }

    public String getCustomUserData() {
        return customUserData;
    }

    public void setCustomUserData(String customUserData) {
        this.customUserData = customUserData;
    }

    public Boolean getEnabled() {
        return isEnabled;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDeviceARN() {
        return deviceARN;
    }

    public void setDeviceARN(String deviceARN) {
        this.deviceARN = deviceARN;
    }

    @Override
    public String toString() {
        return "{" +
            "\"token\": \"" + token + "\"," +
            "\"isEnabled\": \"" + isEnabled +"\"," +
            "\"customUserData\": \"" + customUserData + "\"," +
            "\"deviceARN\": \"" + deviceARN + "\"" +
            '}';
    }
}
