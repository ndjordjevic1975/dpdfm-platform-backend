/**
 *
 */
package rs.co.tntech.busride.service.util;

import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.amazonaws.services.sns.model.DeleteEndpointResult;
import com.amazonaws.services.sns.model.Endpoint;
import com.amazonaws.services.sns.model.SetEndpointAttributesResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rs.co.tntech.busride.domain.Driver;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.SchoolAdmin;
import rs.co.tntech.busride.domain.User;
import rs.co.tntech.busride.domain.enumeration.DeviceType;
import rs.co.tntech.busride.domain.enumeration.SNSNotificationApplicationType;
import rs.co.tntech.busride.repository.DriverRepository;
import rs.co.tntech.busride.repository.ParentRepository;
import rs.co.tntech.busride.repository.SchoolAdminRepository;
import rs.co.tntech.busride.repository.UserRepository;
import rs.co.tntech.busride.security.SecurityUtils;
import rs.co.tntech.busride.service.SNSNotificationService;
import rs.co.tntech.busride.service.dto.SNSEndpointDTO;
import rs.co.tntech.busride.service.error.BRPFieldsExcpetion;
import rs.co.tntech.busride.service.error.BRPServiceExcpetion;
import rs.co.tntech.busride.service.error.BRPServiceSystemException;

import java.util.List;
import java.util.Optional;

/**
 * Contains utilities methods for service layer.
 *
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
@Component
public class BRPServiceUtil {

    private final Logger log = LoggerFactory.getLogger(BRPServiceUtil.class);

    @Autowired
    private ParentRepository parentRepository;
    @Autowired
    private DriverRepository driverRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SchoolAdminRepository schoolAdminRepository;
    @Autowired
    private SNSNotificationService snsNotificationService;


	/**
	 * Gets primary parent for current authenticated user if it has PARENT role.<br>
	 * If authenticated user is primary it will be returned, otherwise it's primary parent will be returned.
	 *
	 * @return primary parent
	 */
    public Parent getCurrentPrimaryParent() {
        Parent parent = getCurrentParent();

        if (parent != null && !isParentPrimary(parent)) {
            parent = parent.getLinkToPrimary();
        }

        return parent;
    }

	/**
	 * Gets parent for current authenticated user if it has PARENT role.
	 *
	 * @return current parent
	 */
    public Parent getCurrentParent() {
        return  parentRepository.findOneByUser_login(SecurityUtils.getCurrentUserLogin());
    }

	/**
	 * Checks if given parent is primary or not.
	 *
	 * @param parent
	 * @return true if given parent is primary, otherwise false
	 */
	public boolean isParentPrimary(Parent parent) {
		boolean isPrimary = false;
		if (parent != null && parent.getLinkToPrimary() == null) {
			isPrimary = true;
		}
		return isPrimary;
	}

	/**
	 * Gets driver for current authenticated user if it has DRIVER role.
	 *
	 * @return current driver
	 */
	public Driver getCurrentDriver() {
		return driverRepository.findDriverByUser_Login(SecurityUtils.getCurrentUserLogin());
	}

	/**
	 * Gets current user object.
	 *
	 * @return
	 */
	public User getCurrentUser() {
		User user = null;
		Optional<User> userOption = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
		if (userOption != null && userOption.isPresent()) {
			user = userOption.get();
		}
		return user;
	}

	/**
	 * Gets School Administrator for current authenticated user if it has SCHOOL_ADMIN role.
	 *
	 * @return current school administrator
	 */
	public SchoolAdmin getCurrentSchoolAdmin() {
		return schoolAdminRepository.findOneByUserLogin(SecurityUtils.getCurrentUserLogin());
	}

    public Optional<Driver> updateDriverEndpoint(Driver driver, String gcmSessionId) throws BRPServiceSystemException, BRPFieldsExcpetion {
        SNSEndpointDTO endpointDTO = new SNSEndpointDTO();

        if (driver != null && driver.getUser() != null) {
            endpointDTO.setCustomUserData(driver.getUser().getLogin());
            endpointDTO.setToken(gcmSessionId);
            if (driver.getEndpointARN() == null) {
                endpointDTO.setDeviceARN(findDeviceARN(driver.getUser().getLogin(), SNSNotificationApplicationType.DRIVER));
            } else {
                endpointDTO.setDeviceARN(driver.getEndpointARN());
            }
        }

        Optional<SetEndpointAttributesResult> setEndpointAttributesResult = snsNotificationService.updateDeviceSubscriptionAttributes(endpointDTO, DeviceType.ANDROID);

        if (setEndpointAttributesResult.isPresent() && setEndpointAttributesResult.get().getSdkHttpMetadata().getHttpStatusCode() == 200) {
            driver.setGcmSessionId(gcmSessionId);
            driver.setEndpointARN(endpointDTO.getDeviceARN());
            return Optional.ofNullable(driverRepository.save(driver));

        } else if (setEndpointAttributesResult.isPresent()) {
            throw new BRPServiceSystemException("Updating Driver " + driver.getUser().getLogin() + ", GCM SessionID endpoint failed ");
        } else {
            throw new BRPServiceSystemException("no response from amazon sns while updating Driver " + driver.getUser().getLogin());
        }
    }

    public Optional<Parent> updateParentEndpoint(Parent parent, String gcmSessionId) throws BRPServiceSystemException, BRPFieldsExcpetion {

	    SNSEndpointDTO endpointDTO = new SNSEndpointDTO();

        if (parent != null && parent.getUser() != null) {
            endpointDTO.setCustomUserData(parent.getUser().getLogin());
            endpointDTO.setToken(gcmSessionId);
            if (parent.getEndpointARN() == null) {
            	SNSNotificationApplicationType snsNotificationApplicationType = mapSNSNotificationParentApplicationType(parent);
                endpointDTO.setDeviceARN(findDeviceARN(parent.getUser().getLogin(), snsNotificationApplicationType));
            } else {
                endpointDTO.setDeviceARN(parent.getEndpointARN());
            }
        }

        Optional<SetEndpointAttributesResult> setEndpointAttributesResult = snsNotificationService.updateDeviceSubscriptionAttributes(endpointDTO, parent.getDeviceType());

        if (setEndpointAttributesResult.isPresent() && setEndpointAttributesResult.get().getSdkHttpMetadata().getHttpStatusCode() == 200) {
            parent.setGcmSessionId(gcmSessionId);
            parent.setEndpointARN(endpointDTO.getDeviceARN());
            return Optional.ofNullable(parentRepository.save(parent));

        } else if (setEndpointAttributesResult.isPresent()) {
            throw new BRPServiceSystemException("Updating Parent " + parent.getUser().getLogin() + ", GCM SessionID endpoint failed ");
        } else {
            throw new BRPServiceSystemException("no response from amazon sns while updating Parent " + parent.getUser().getLogin());
        }
    }

    public Optional<Driver> createDriverEndpoint(Driver driver, String gcmSessionId) throws BRPServiceSystemException {
        SNSEndpointDTO endpointDTO = new SNSEndpointDTO();

        if (driver != null && driver.getUser() != null) {
            endpointDTO.setCustomUserData(driver.getUser().getLogin());
            endpointDTO.setToken(gcmSessionId);
            endpointDTO.setEnabled(true);
        }
        Optional<CreatePlatformEndpointResult> createPlatformEndpointResult = snsNotificationService.registerDevice(endpointDTO, SNSNotificationApplicationType.DRIVER);

        if (createPlatformEndpointResult.isPresent() && createPlatformEndpointResult.get().getSdkHttpMetadata().getHttpStatusCode() == 200) {
            driver.setEndpointARN(createPlatformEndpointResult.get().getEndpointArn());
            driver.setGcmSessionId(gcmSessionId);
            return Optional.ofNullable(driverRepository.save(driver));
        } else if (createPlatformEndpointResult.isPresent()) {
            throw new BRPServiceSystemException("Creating Driver " + driver.getUser().getLogin() + " endpoint failed for GCM SessionID " + gcmSessionId);

        } else {
            throw new BRPServiceSystemException("no response from amazon sns while creating endpoint for Driver " + driver.getUser().getLogin());
        }

    }

    public Optional<Parent> createParentEndpoint(Parent parent, String gcmSessionId) throws BRPServiceSystemException {

	    SNSEndpointDTO endpointDTO = new SNSEndpointDTO();

        if (parent != null && parent.getUser() != null) {
            endpointDTO.setCustomUserData(parent.getUser().getLogin());
            endpointDTO.setToken(gcmSessionId);
            endpointDTO.setEnabled(true);
        }
        SNSNotificationApplicationType snsNotificationApplicationType = mapSNSNotificationParentApplicationType(parent);
        Optional<CreatePlatformEndpointResult> createPlatformEndpointResult = snsNotificationService.registerDevice(endpointDTO, snsNotificationApplicationType);

        if (createPlatformEndpointResult.isPresent() && createPlatformEndpointResult.get().getSdkHttpMetadata().getHttpStatusCode() == 200) {
            parent.setEndpointARN(createPlatformEndpointResult.get().getEndpointArn());
            parent.setGcmSessionId(gcmSessionId);
            return Optional.ofNullable(parentRepository.save(parent));
        } else if (createPlatformEndpointResult.isPresent()) {
            throw new BRPServiceSystemException("Creating Parent " + parent.getUser().getLogin() + " endpoint failed for GCM SessionID " + gcmSessionId);

        } else {
            throw new BRPServiceSystemException("no response from amazon sns while creating endpoint for Parent " + parent.getUser().getLogin());
        }

    }

    private String findDeviceARN(String login, SNSNotificationApplicationType applicationType) {
        Optional<List<Endpoint>> endpointList = snsNotificationService.getAllRegisteredDevices(applicationType);
        final String[] result = {null};
        if (endpointList.isPresent()) {
            endpointList.get().stream()
                .filter(endpoint -> endpoint.getAttributes().get("CustomUserData").equalsIgnoreCase(login))
                .findFirst().ifPresent((Endpoint endpoint) -> result[0] = endpoint.getEndpointArn());
        }
        return result[0];
    }

	private SNSNotificationApplicationType mapSNSNotificationParentApplicationType(Parent parent) throws BRPServiceSystemException {
        switch (parent.getDeviceType()) {
            case ANDROID:
                return SNSNotificationApplicationType.PARENT_ANDROID;
            case IOS:
                return SNSNotificationApplicationType.PARENT_IOS;
            default:
                throw new BRPServiceSystemException("Mapping Application type failed since device type in not correct");
        }
    }

    public Optional<DeleteEndpointResult> deleteParentEndpoint(String endpointARN, DeviceType deviceType) throws BRPServiceExcpetion {
	    return snsNotificationService.unregisterDevice(endpointARN,deviceType);
    }
}
