package rs.co.tntech.busride.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.co.tntech.busride.domain.Driver;
import rs.co.tntech.busride.domain.DriverNotification;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.Spot;
import rs.co.tntech.busride.domain.enumeration.DriverNotificationMsgType;
import rs.co.tntech.busride.domain.enumeration.DriverNotificationType;
import rs.co.tntech.busride.repository.*;
import rs.co.tntech.busride.service.dto.DriverNotificationDTO;
import rs.co.tntech.busride.service.dto.SNSNotificationMessageDTO;
import rs.co.tntech.busride.service.error.*;
import rs.co.tntech.busride.service.mapper.DriverNotificationMapper;
import rs.co.tntech.busride.service.util.BRPServiceUtil;
import rs.co.tntech.busride.service.util.BRPServiceValidationUtils;
import rs.co.tntech.busride.service.util.EnumUtils;
import rs.co.tntech.busride.web.rest.vm.SkipSpotVM;

import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing DriverNotification.
 */
@Service
@Transactional
public class DriverNotificationService extends NotificationService {

    private final Logger log = LoggerFactory.getLogger(DriverNotificationService.class);

    @Autowired
    private DriverNotificationRepository driverNotificationRepository;
    @Autowired
    private SpotRepository spotRepository;
    @Autowired
    private ParentRepository parentRepository;
    @Autowired
    private DriverNotificationMapper driverNotificationMapper;
    @Autowired
    private BRPServiceUtil serviceUtil;
    @Autowired
    private EnumUtils enumUtils;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private SNSNotificationService snsNotificationService;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private TempServiceCancellationRepository tempServiceCancellationRepository;

    /**
     * Save a driverNotification.
     *
     * @param driverNotificationDTO the entity to save
     * @return the persisted entity
     */
    public DriverNotificationDTO save(DriverNotificationDTO driverNotificationDTO) {
        log.debug("Request to save DriverNotification : {}", driverNotificationDTO);
        DriverNotification driverNotification = driverNotificationMapper.toEntity(driverNotificationDTO);
        driverNotification = driverNotificationRepository.save(driverNotification);
        return driverNotificationMapper.toDto(driverNotification);
    }

    /**
     * Save a driverNotification for current driver.
     *
     * @param driverNotificationDTO the entity to save
     * @return the persisted entity
     * @throws BRPBaseServiceException
     */
    public DriverNotificationDTO saveForCurrentDriver(DriverNotificationDTO driverNotificationDTO) throws BRPBaseServiceException {
        validateSaveForCurrentDriver(driverNotificationDTO);
        DriverNotification driverNotification = driverNotificationMapper.toEntity(driverNotificationDTO);
        Driver currentDriver = serviceUtil.getCurrentDriver();
        log.debug("API:POST:driver-notifications/current-driver: Save DriverNotification:{} for Driver:{}", driverNotificationDTO, currentDriver.getUser().getId());
        driverNotification.setDriver(currentDriver);
        // generate notification message, use locale of primary parent connected to spot id
        Parent primaryParentBySpotId = parentRepository.findOneBySpotId(driverNotificationDTO.getSpotId());
        // validate parent retrieved by spot id
        if (primaryParentBySpotId == null) {
            throw new BRPServiceSystemException("There is no primary parent for spot id #" + driverNotificationDTO.getSpotId());
        }
        if (primaryParentBySpotId.getLinkToPrimary() != null) {
            throw new BRPServiceSystemException(
                "Non primary parent #" + primaryParentBySpotId.getId() + " connected to spot #" + driverNotificationDTO.getSpotId());
        }
        Locale locale = Locale.forLanguageTag(primaryParentBySpotId.getUser().getLangKey());

        String notification;
        String key;

        if (primaryParentBySpotId.getAddress() == null || primaryParentBySpotId.getAddress().isEmpty()) {
            key = String.format("driver.notification.%s.generic", driverNotificationDTO.getType().toString().toLowerCase());
            notification = messageSource.getMessage(key, null, locale);
        }
        else {
            key = String.format("driver.notification.%s", driverNotificationDTO.getType().toString().toLowerCase());
            notification = messageSource.getMessage(key, new String[]{primaryParentBySpotId.getAddress()}, locale);
        }

        driverNotification.setNotification(notification);

        driverNotification = driverNotificationRepository.save(driverNotification);


        // find users by spot id (primary parent and it's relatives)
        List<Parent> parents = parentRepository.findAllBySpotId(driverNotificationDTO.getSpotId());

        parents.forEach(parent -> saveParentNotification(parent, notification));

        // send push notifications to all parents
        if (!parents.isEmpty()) {
            SNSNotificationMessageDTO snsNotificationMsg = new SNSNotificationMessageDTO(DriverNotificationMsgType.BUS);
            snsNotificationMsg.setTitleKey("notification.student.title");
            snsNotificationMsg.setTitleArgs(new String[]{enumUtils.localized(driverNotificationDTO.getType(), locale)});
            snsNotificationMsg.setContentKey(key);
            if (primaryParentBySpotId.getAddress() == null || primaryParentBySpotId.getAddress().isEmpty()) {
                snsNotificationMsg.setContentArgs(null);
            } else {
                snsNotificationMsg.setContentArgs(new String[]{primaryParentBySpotId.getAddress()});
            }
            snsNotificationMsg.setNotificationId(driverNotification.getId());
            snsNotificationService.publishMessageToParentApp(parents, snsNotificationMsg);
        }

        log.debug("API:POST:OUT:driver-notifications/current-driver: {}", driverNotification);

        return driverNotificationMapper.toDto(driverNotification);
    }

    /**
     * Get all the driverNotifications.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<DriverNotificationDTO> findAll() {
        log.debug("Request to get all DriverNotifications");
        return driverNotificationRepository.findAll().stream()
            .map(driverNotificationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one driverNotification by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public DriverNotificationDTO findOne(Long id) {
        log.debug("Request to get DriverNotification : {}", id);
        DriverNotification driverNotification = driverNotificationRepository.findOne(id);
        return driverNotificationMapper.toDto(driverNotification);
    }

    /**
     * Delete the  driverNotification by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DriverNotification : {}", id);
        driverNotificationRepository.delete(id);
    }

    /**
     * This method validates input data for method {@link #saveForCurrentDriver(DriverNotificationDTO)}
     *
     * @param driverNotificationDTO
     * @throws BRPBaseServiceException
     */
    private void validateSaveForCurrentDriver(DriverNotificationDTO driverNotificationDTO) throws BRPBaseServiceException {
        // TODO validate required role
        // if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.DRIVER)) {
        // String msg = String.format("User does not have any of required roles: '%s'", AuthoritiesConstants.DRIVER);
        // throw new BRPServiceSecurityException(AuthoritiesConstants.DRIVER, msg);
        // }
        // validate input data
        List<BRPFieldException> fieldErrors = new ArrayList<>();
        // validate not allowed fields
        BRPServiceValidationUtils.validateNotAllowedField(fieldErrors, "DriverNotificationDTO", "id", driverNotificationDTO.getId());
        BRPServiceValidationUtils.validateNotAllowedField(fieldErrors, "DriverNotificationDTO", "driverId", driverNotificationDTO.getDriverId());
        BRPServiceValidationUtils.validateNotAllowedField(fieldErrors, "DriverNotificationDTO", "notification", driverNotificationDTO.getNotification());
        // validate mandatory fields
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "DriverNotificationDTO", "spotId", driverNotificationDTO.getSpotId());
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "DriverNotificationDTO", "type", driverNotificationDTO.getType());
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "DriverNotificationDTO", "createdOnDevice", driverNotificationDTO.getCreatedOnDevice());
        // validate spot id
        if (fieldErrors.isEmpty()) {
            if (spotRepository.getOne(driverNotificationDTO.getSpotId()) == null) {
                fieldErrors.add(new BRPFieldException("DriverNotificationDTO", "spotId", BRPServiceErrorIds.SPOT_DB_MISSING,
                    "Spot data for id #" + driverNotificationDTO.getSpotId() + " does not exist."));
            }
        }
        if (!fieldErrors.isEmpty()) {
            throw new BRPFieldsExcpetion(fieldErrors, "Some of input fields are not valid");
        }
        // validate current driver and route
        Driver currentDriver = serviceUtil.getCurrentDriver();
        if (currentDriver == null) {
            throw new BRPServiceSystemException("There is no current driver user.");
        }
    }

    public void skipSpotForCurrentDriver(SkipSpotVM skipSpotVM) {
        Driver currentDriver = serviceUtil.getCurrentDriver();
        Long spotIdToSkip = skipSpotVM.getSpotId();

        log.debug("API:POST:driver-notifications/skip-spot/current-driver: Skipping spot:{} for Driver:{}", spotIdToSkip, currentDriver.getUser().getId());

        Spot spotToSkip = spotRepository.findOne(spotIdToSkip);
        Parent primaryParentBySpotId = parentRepository.findOneBySpotId(spotIdToSkip);
        Locale locale = Locale.forLanguageTag(primaryParentBySpotId.getUser().getLangKey());

        studentRepository.findAllByParent(primaryParentBySpotId).forEach(student -> {
            if (tempServiceCancellationRepository.findByStudent(student) == null) {
                String notification = messageSource.getMessage("driver.notification.skip_spot", new String[]{student.getName()}, locale);

                DriverNotification skipSpotDriverNotification = new DriverNotification();

                skipSpotDriverNotification.setNotification(notification);
                skipSpotDriverNotification.setCreatedOnDevice(Instant.now());
                skipSpotDriverNotification.setType(DriverNotificationType.SKIP_SPOT);
                skipSpotDriverNotification.setDriver(currentDriver);
                skipSpotDriverNotification.setSpot(spotToSkip);

                DriverNotification savedDriverNotification = driverNotificationRepository.save(skipSpotDriverNotification);

                List<Parent> parents = parentRepository.findAllBySpotId(spotIdToSkip);

                parents.forEach(parent -> saveParentNotification(parent, notification));

                if (!parents.isEmpty()) {
                    SNSNotificationMessageDTO snsNotificationMsg = new SNSNotificationMessageDTO(DriverNotificationMsgType.BUS);
                    snsNotificationMsg.setTitleKey("notification.student.title");
                    snsNotificationMsg.setTitleArgs(new String[]{enumUtils.localized(skipSpotDriverNotification.getType(), locale)});
                    snsNotificationMsg.setContentKey("driver.notification.skip_spot");
                    snsNotificationMsg.setContentArgs(new String[]{student.getName()});

                    snsNotificationMsg.setNotificationId(savedDriverNotification.getId());
                    snsNotificationService.publishMessageToParentApp(parents, snsNotificationMsg);
                }
            }
        });
    }
}
