package rs.co.tntech.busride.service;

import com.amazonaws.AmazonWebServiceResult;
import com.amazonaws.ResponseMetadata;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.co.tntech.busride.config.ApplicationProperties;
import rs.co.tntech.busride.domain.Driver;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.enumeration.DeviceType;
import rs.co.tntech.busride.domain.enumeration.SNSMessagePlatformType;
import rs.co.tntech.busride.domain.enumeration.SNSNotificationApplicationType;
import rs.co.tntech.busride.service.dto.SNSEndpointDTO;
import rs.co.tntech.busride.service.dto.SNSNotificationMessageDTO;
import rs.co.tntech.busride.service.error.BRPFieldsExcpetion;
import rs.co.tntech.busride.service.error.BRPServiceErrorIds;
import rs.co.tntech.busride.service.error.BRPServiceExcpetion;
import rs.co.tntech.busride.service.util.SNSModelUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing SNS Notifications.
 */
@Service
@Transactional
public class SNSNotificationService {

    private final Logger log = LoggerFactory.getLogger(SNSNotificationService.class);

    @Autowired
    @Qualifier("android")
    private AmazonSNS amazonSNSClientGCM;
    @Autowired
    @Qualifier("ios")
    private AmazonSNS amazonSNSClientAPNS;
    @Autowired
    private SNSModelUtil snsModelUtil;
    @Autowired
    private MailService mailService;
    @Autowired
    private ApplicationProperties applicationProperties;

    private final ObjectMapper mapper = new ObjectMapper();

    public SNSNotificationService() {
    }

    public Optional<CreatePlatformEndpointResult> registerDevice(SNSEndpointDTO snsEndpointDTO, SNSNotificationApplicationType applicationType) {
        log.debug("AWS:SNS:CALL:REGISTER: register endpoint");
        log.debug("AWS:SNS:IN: " + snsEndpointDTO.toString());
        CreatePlatformEndpointRequest platformEndpointRequest = snsModelUtil.mapRegisterDeviceReq(snsEndpointDTO, applicationType);
        CreatePlatformEndpointResult result = null;
        switch (applicationType) {
            case DRIVER:
            case PARENT_ANDROID:
                result = amazonSNSClientGCM.createPlatformEndpoint(platformEndpointRequest);
                break;
            case PARENT_IOS:
                result = amazonSNSClientAPNS.createPlatformEndpoint(platformEndpointRequest);
                break;
            default:
                log.error("AWS:OUT:Application type is not supported " + applicationType);
                break;
        }

        isResponseOK(result);

        return Optional.of(result);
    }

    public Optional<DeleteEndpointResult> unregisterDevice(String endpointARN, DeviceType deviceType) throws BRPServiceExcpetion {
        log.debug("AWS:SNS:CALL:UNREGISTER: Unregister endpoint");
        log.debug("AWS:SNS:IN: " + endpointARN);
        DeleteEndpointResult result = null;
        switch (deviceType) {
            case ANDROID:
                result = amazonSNSClientGCM.deleteEndpoint(snsModelUtil.mapUnregisterDeviceReq(endpointARN));
                break;
            case IOS:
                result = amazonSNSClientAPNS.deleteEndpoint(snsModelUtil.mapUnregisterDeviceReq(endpointARN));
                break;
            default:
                log.error("AWS:OUT:Application type is not supported " + deviceType);
                break;
        }

        if (result != null && result.getSdkHttpMetadata() != null && result.getSdkResponseMetadata() != null) {
            log.debug("AWS:OUT:StatusCode: " + result.getSdkHttpMetadata().getHttpStatusCode() + ":RequestId: " + result.getSdkResponseMetadata().getRequestId());
            int statusCode = result.getSdkHttpMetadata().getHttpStatusCode();
            if(statusCode!= HttpStatus.OK.value()) {
                throw new BRPServiceExcpetion(BRPServiceErrorIds.SNS_UNREGISTER_DEVICE_FAILED,"received sns status code " +statusCode + "for endpoint #"+ endpointARN);
            }
        }else {
            throw new BRPServiceExcpetion(BRPServiceErrorIds.SNS_UNREGISTER_DEVICE_FAILED,"result not received for endpoint #" + endpointARN);
        }

        return Optional.ofNullable(result);
    }

    @Transactional(noRollbackFor = NotFoundException.class)
    public Optional<SetEndpointAttributesResult> updateDeviceSubscriptionAttributes(SNSEndpointDTO snsEndpointDTO, DeviceType deviceType) throws BRPFieldsExcpetion {
        log.debug("AWS:SNS:CALL:UPDATE: Update device subscription attributes");
        SetEndpointAttributesResult  result = null;
        switch (deviceType) {
            case ANDROID:
                result = amazonSNSClientGCM.setEndpointAttributes(snsModelUtil.mapUpdateDeviceAttributesReq(snsEndpointDTO));
                break;
            case IOS:
                result = amazonSNSClientAPNS.setEndpointAttributes(snsModelUtil.mapUpdateDeviceAttributesReq(snsEndpointDTO));
                break;
            default:
                log.error("AWS:OUT:Application type is not supported " + deviceType);
                break;
        }

        isResponseOK(result);

        return Optional.of(result);
    }

    public Optional<List<Endpoint>> getAllRegisteredDevices(SNSNotificationApplicationType applicationType) {
        String appARN = snsModelUtil.mapSNSApplicationARN(applicationType);

        ListEndpointsByPlatformApplicationRequest req = new ListEndpointsByPlatformApplicationRequest()
            .withPlatformApplicationArn(appARN);

        ListEndpointsByPlatformApplicationResult result = null;

        switch (applicationType) {
            case PARENT_ANDROID:
            case DRIVER:
                result = amazonSNSClientGCM.listEndpointsByPlatformApplication(req);
                break;
            case PARENT_IOS:
                result = amazonSNSClientAPNS.listEndpointsByPlatformApplication(req);
                break;
            default:
                log.error("AWS:OUT:Application type is not supported " + applicationType);
                break;
        }

        isResponseOK(result);

        return Optional.of(result.getEndpoints());
    }

    /**
     * Public given message to all parents
     *
     * @param parents
     * @param snsNotificationMessageDTO
     */
    @Async
    public void publishMessageToParentApp(List<Parent> parents, SNSNotificationMessageDTO snsNotificationMessageDTO) {
        List<Endpoint> allEndpoints = new ArrayList<>();
        Optional<List<Endpoint>> parentAppAndroidEndpoints = getAllRegisteredDevices(SNSNotificationApplicationType.PARENT_ANDROID);

        if(parents.stream().filter(parent -> parent.getDeviceType()==DeviceType.IOS).findFirst().isPresent()) {
            getAllRegisteredDevices(SNSNotificationApplicationType.PARENT_IOS).ifPresent(endpoints -> allEndpoints.addAll(endpoints));
        }
        if(parents.stream().filter(parent -> parent.getDeviceType()==DeviceType.ANDROID).findFirst().isPresent()) {
            getAllRegisteredDevices(SNSNotificationApplicationType.PARENT_ANDROID).ifPresent(endpoints -> allEndpoints.addAll(endpoints));
        }

    	parents.stream().forEach(p -> publishMessageToParentApp(p, snsNotificationMessageDTO,Optional.ofNullable(allEndpoints)));
    }


    /**
     * Publish given message to the parent
     *  @param parent
     * @param snsNotificationMessageDTO
     * @param parentAppEndpoints
     */
    @Async
    public void publishMessageToParentApp(Parent parent, SNSNotificationMessageDTO snsNotificationMessageDTO, Optional<List<Endpoint>> parentAppEndpoints) {
        log.debug("AWS:SNS:CALL:PUBLISH: publish message to parent app");

        Optional<Endpoint> parentEndpoint = findParentEndpoint(parent, parentAppEndpoints);

        if (!isParentEndpointValidForPublishing(parent, parentEndpoint)) {
            return;
        }

        if(parent.getUser()!=null) {
            snsNotificationMessageDTO.setUser(parent.getUser().getLogin());
            snsNotificationMessageDTO.setUserLang(parent.getUser().getLangKey());
        }

		long retryPeriodInMs = applicationProperties.getSnsRetryPeriodInSec() * 1000l;
		boolean msgSended = false;
		int counter = 0;
		do {
			counter++;
			try {
				publishMessage(parent.getEndpointARN(), snsNotificationMessageDTO, mapPlatformType(parent.getDeviceType()));
				msgSended = true;
			} catch (Exception e) {
                log.error("AWS:SNS:ERROR:PUBLISH: " + "Error while publishing SNS notification to parent #" + parent.getId() + ": " + e.getMessage());
				msgSended = false;
				if (counter < applicationProperties.getSnsRetryMaxCount()) {
					try {
						Thread.sleep(retryPeriodInMs);
					} catch (InterruptedException e1) {
					}
				}
			}
		} while (!msgSended && counter < applicationProperties.getSnsRetryMaxCount());
		if(!msgSended) {
			// sending email instead of SNS
			String title = snsModelUtil.translateTitle(snsNotificationMessageDTO);
			String content = snsModelUtil.translateContent(snsNotificationMessageDTO);
			mailService.sendSnsNotificationFailedEmail(parent.getUser(), title, content);
		}
	}

    private boolean isParentEndpointValidForPublishing(Parent parent, Optional<Endpoint> parentEndpoint) {
        if (parentEndpoint.isPresent()) {
            String isEnabledAttribute = parentEndpoint.get().getAttributes().get("Enabled");
            if (StringUtils.equalsAnyIgnoreCase(isEnabledAttribute, "True")) {
                return true;
            } else {
                log.debug("AWS:SNS: Message cannot be published to parent {}. EndpointARN {} is not enabled", parent.getUser().getLogin(), parent.getEndpointARN());
                return false;
            }
        } else {
            log.debug("AWS:SNS: Message cannot be published to parent {}. EndpointARN {} cannot be found", parent.getUser().getLogin(), parent.getEndpointARN());
            return false;
        }
    }

    private Optional<Endpoint> findParentEndpoint(Parent parent, Optional<List<Endpoint>> parentAppEndpoints) {
        if(parentAppEndpoints.isPresent()) {
            List<Endpoint> allEndpoints = parentAppEndpoints.get();
            return allEndpoints.stream().filter(endpoint -> endpoint.getEndpointArn().equals(parent.getEndpointARN())).findFirst();
        }
        return Optional.empty();
    }

    /**
     * Publish given message to the driver
     *
     * @param driver
     * @param snsNotificationMessageDTO
     */
    @Async
	public void publishMessageToDriverApp(Driver driver, SNSNotificationMessageDTO snsNotificationMessageDTO) {
        log.debug("AWS:SNS:CALL:PUBLISH: publish message to driver app");

        Optional<List<Endpoint>> driverAppEndpoints = getAllRegisteredDevices(SNSNotificationApplicationType.DRIVER);
        Optional<Endpoint> driverEndpoint = findDriverEndpoint(driver,driverAppEndpoints);

        if (!isDriverEndpointValidForPublishing(driver, driverEndpoint)) {
            return;
        }

        snsNotificationMessageDTO.setUserLang(driver.getUser().getLangKey());
    	long retryPeriodInMs = applicationProperties.getSnsRetryPeriodInSec() * 1000l;
		boolean msgSended = false;
		int counter = 0;
		do {
			counter++;
			try {
				publishMessage(driver.getEndpointARN(), snsNotificationMessageDTO, mapPlatformType(DeviceType.ANDROID));
				msgSended = true;
			} catch (Exception e) {
			    log.error("AWS:SNS:ERROR:PUBLISH: " + "Error while publishing SNS notification to driver #" + driver.getId() + ": " + e.getMessage() );
				msgSended = false;
				if (counter < applicationProperties.getSnsRetryMaxCount()) {
					try {
						// TODO make this configurable
						Thread.sleep(retryPeriodInMs);
					} catch (InterruptedException e1) {
					}
				}
			}
		} while (!msgSended && counter < applicationProperties.getSnsRetryMaxCount());
		if (!msgSended) {
			// TODO log to DB
		}
	}

    private boolean isDriverEndpointValidForPublishing(Driver driver, Optional<Endpoint> driverEndpoint) {
        if (driverEndpoint.isPresent()) {
            String isEnabledAttribute = driverEndpoint.get().getAttributes().get("Enabled");
            if (StringUtils.equalsAnyIgnoreCase(isEnabledAttribute, "True")) {
                return true;
            } else {
                log.debug("AWS:SNS: Message cannot be published to driver {}. EndpointARN {} is not enabled", driver.getUser().getLogin(), driver.getEndpointARN());
                return false;
            }
        } else {
            log.debug("AWS:SNS: Message cannot be published to driver {}. EndpointARN {} cannot be found", driver.getUser().getLogin(), driver.getEndpointARN());
            return false;
        }

    }

    private Optional<Endpoint> findDriverEndpoint(Driver driver, Optional<List<Endpoint>> driverAppEndpoints) {
        if(driverAppEndpoints.isPresent()) {
            List<Endpoint> allEndpoints = driverAppEndpoints.get();
            return allEndpoints.stream().filter(endpoint -> endpoint.getEndpointArn().equals(driver.getEndpointARN())).findFirst();
        }
        return Optional.empty();
    }

    private Optional<PublishResult> publishMessage(String endpointARN, SNSNotificationMessageDTO snsNotificationMessageDTO, SNSMessagePlatformType platformType) {

        PublishResult result = null;
        switch (platformType) {
            case GCM:
                result = amazonSNSClientGCM.publish(snsModelUtil.mapPublishRequest(endpointARN, snsNotificationMessageDTO, platformType));
                break;
            case APNS:
                result = amazonSNSClientAPNS.publish(snsModelUtil.mapPublishRequest(endpointARN, snsNotificationMessageDTO, platformType));
                break;
            default:
                log.error("AWS:OUT:Platform type is not supported " + platformType);
                break;

        }

       isResponseOK(result);

        return Optional.of(result);
    }

    private SNSMessagePlatformType mapPlatformType(DeviceType deviceType) {
        switch (deviceType) {
            case ANDROID:
                return SNSMessagePlatformType.GCM;
            case IOS:
                return SNSMessagePlatformType.APNS;
        }
        return null;
    }

    private void isResponseOK(AmazonWebServiceResult<ResponseMetadata> result) {
        if (result != null && result.getSdkHttpMetadata() != null && result.getSdkResponseMetadata() != null) {
            log.debug("AWS:SNS:OUT:StatusCode: " + result.getSdkHttpMetadata().getHttpStatusCode() + ":RequestId:" + result.getSdkResponseMetadata().getRequestId());
        } else {
            log.debug("AWS:SNS:OUT: {}", result);
        }
    }
}
