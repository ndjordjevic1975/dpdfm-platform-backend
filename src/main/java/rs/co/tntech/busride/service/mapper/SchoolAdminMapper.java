package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.SchoolAdminDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SchoolAdmin and its DTO SchoolAdminDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, SchoolMapper.class, })
public interface SchoolAdminMapper extends EntityMapper <SchoolAdminDTO, SchoolAdmin> {

    @Mapping(source = "user.id", target = "userId")

    @Mapping(source = "school.id", target = "schoolId")
    SchoolAdminDTO toDto(SchoolAdmin schoolAdmin); 

    @Mapping(source = "userId", target = "user")

    @Mapping(source = "schoolId", target = "school")
    SchoolAdmin toEntity(SchoolAdminDTO schoolAdminDTO); 
    default SchoolAdmin fromId(Long id) {
        if (id == null) {
            return null;
        }
        SchoolAdmin schoolAdmin = new SchoolAdmin();
        schoolAdmin.setId(id);
        return schoolAdmin;
    }
}
