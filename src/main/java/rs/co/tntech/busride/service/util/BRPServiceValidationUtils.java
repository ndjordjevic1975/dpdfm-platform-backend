/**
 *
 */
package rs.co.tntech.busride.service.util;

import rs.co.tntech.busride.service.error.BRPFieldException;

import java.util.Collection;
import java.util.List;

/**
 * This class contains helper methods to validate input data.
 *
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public abstract class BRPServiceValidationUtils {

	/**
	 * This method validates if the mandatory input field is present. If not it adds error info to list of all field errors.
	 *
	 * @param fieldErrors
	 * @param objectName
	 * @param fieldName
	 * @param value
	 */
	public static void validateMandatoryField(List<BRPFieldException> fieldErrors, String objectName, String fieldName, Object value) {
		if (isValueEmpty(value)) {
			fieldErrors.add(new BRPFieldException(objectName, fieldName, "mandatoryField", fieldName + " is mandatory field"));
		}
	}

	/**
	 * This method validates if given input field (which is not allowed) is present. If it is, error info is added to list of all field errors.
	 *
	 * @param fieldErrors
	 * @param objectName
	 * @param fieldName
	 * @param value
	 */
	public static void validateNotAllowedField(List<BRPFieldException> fieldErrors, String objectName, String fieldName, Object value) {
		if (!isValueEmpty(value)) {
			fieldErrors.add(new BRPFieldException(objectName, fieldName, "notAllowed", fieldName + " is not allowed"));
		}
	}

	/**
	 * This method checks if given value empty
	 *
	 * @param value
	 * @return
	 */
	public static boolean isValueEmpty(final Object value) {
		boolean emptyValue = false;
		if (value == null) {
			emptyValue = true;
		} else if (value instanceof String && ((String) value).isEmpty()) {
			emptyValue = true;
		} else if (value instanceof Collection<?> && ((Collection<?>) value).isEmpty()) {
			emptyValue = true;
		}
		return emptyValue;
	}

}
