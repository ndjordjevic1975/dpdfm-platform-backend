package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.BusCategoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity BusCategory and its DTO BusCategoryDTO.
 */
@Mapper(componentModel = "spring", uses = {SchoolMapper.class, })
public interface BusCategoryMapper extends EntityMapper <BusCategoryDTO, BusCategory> {

    @Mapping(source = "school.id", target = "schoolId")
    @Mapping(source = "school.name", target = "schoolName")
    BusCategoryDTO toDto(BusCategory busCategory); 

    @Mapping(source = "schoolId", target = "school")
    BusCategory toEntity(BusCategoryDTO busCategoryDTO); 
    default BusCategory fromId(Long id) {
        if (id == null) {
            return null;
        }
        BusCategory busCategory = new BusCategory();
        busCategory.setId(id);
        return busCategory;
    }
}
