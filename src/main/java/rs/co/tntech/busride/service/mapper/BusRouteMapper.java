package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.BusRouteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity BusRoute and its DTO BusRouteDTO.
 */
@Mapper(componentModel = "spring", uses = {SchoolMapper.class, DriverMapper.class, SpotMapper.class, })
public interface BusRouteMapper extends EntityMapper <BusRouteDTO, BusRoute> {

    @Mapping(source = "school.id", target = "schoolId")

    @Mapping(source = "driver.id", target = "driverId")
    BusRouteDTO toDto(BusRoute busRoute); 

    @Mapping(source = "schoolId", target = "school")

    @Mapping(source = "driverId", target = "driver")
    BusRoute toEntity(BusRouteDTO busRouteDTO); 
    default BusRoute fromId(Long id) {
        if (id == null) {
            return null;
        }
        BusRoute busRoute = new BusRoute();
        busRoute.setId(id);
        return busRoute;
    }
}
