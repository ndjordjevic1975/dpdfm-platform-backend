package rs.co.tntech.busride.service.util;

import com.amazonaws.services.sns.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import rs.co.tntech.busride.config.ApplicationProperties;
import rs.co.tntech.busride.domain.enumeration.SNSMessagePlatformType;
import rs.co.tntech.busride.domain.enumeration.SNSNotificationApplicationType;
import rs.co.tntech.busride.service.dto.SNSEndpointDTO;
import rs.co.tntech.busride.service.dto.SNSNotificationMessageDTO;
import rs.co.tntech.busride.service.error.BRPFieldException;
import rs.co.tntech.busride.service.error.BRPFieldsExcpetion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component
public class SNSModelUtil {

    private final Logger log = LoggerFactory.getLogger(SNSModelUtil.class);

    private final String MESSAGE_STRUCTURE_JSON = "json";
    private final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private ApplicationProperties applicationProperties;
    @Autowired
    private MessageSource messageSource;

    public SNSModelUtil() {
    }

    public PublishRequest mapPublishRequest(String endpointARN, SNSNotificationMessageDTO snsMessage, SNSMessagePlatformType platformType) {

        String platformSpecificMsg = null;

        switch (platformType) {
            case GCM:
                platformSpecificMsg = mapAndroidMessage(snsMessage);
                break;
            case APNS:
                platformSpecificMsg = mapAppleMessage(snsMessage);
        }

        log.debug("AWS:SNS:IN: User: {}, EndpointARN: {}",snsMessage.getUser(),endpointARN);
        log.debug("AWS:SNS:IN: {}",platformSpecificMsg);

        return new PublishRequest()
            .withMessage(platformSpecificMsg)
            .withMessageStructure(MESSAGE_STRUCTURE_JSON)
            .withTargetArn(endpointARN);
    }

    public SetEndpointAttributesRequest mapUpdateDeviceAttributesReq(SNSEndpointDTO snsEndpointDTO) throws BRPFieldsExcpetion {
        SetEndpointAttributesRequest req = new SetEndpointAttributesRequest();

        req.addAttributesEntry("Token", snsEndpointDTO.getToken());

        if (snsEndpointDTO.getEnabled() != null) {
            req.addAttributesEntry("Enabled", String.valueOf(snsEndpointDTO.getEnabled()));
        }

        if (StringUtils.isNotEmpty(snsEndpointDTO.getCustomUserData())) {
            req.addAttributesEntry("CustomUserData", snsEndpointDTO.getCustomUserData());
        }

        if (StringUtils.isNotEmpty(snsEndpointDTO.getDeviceARN())) {
            req.setEndpointArn(snsEndpointDTO.getDeviceARN());
        } else {
            BRPFieldException brpFieldException = new BRPFieldException("SNSEndpointDTO", "EndpointARN", "mandatoryField", "EndpointARN is mandatory field");
            ArrayList<BRPFieldException> fieldExceptionList = new ArrayList<>();
            fieldExceptionList.add(brpFieldException);
            throw new BRPFieldsExcpetion(fieldExceptionList, "bad update endpoint request");
        }

        return req;

    }

    public CreatePlatformEndpointRequest mapRegisterDeviceReq(SNSEndpointDTO snsEndpointDTO, SNSNotificationApplicationType applicationType) {
        CreatePlatformEndpointRequest req = new CreatePlatformEndpointRequest()
            .withPlatformApplicationArn(mapSNSApplicationARN(applicationType))
            .withToken(snsEndpointDTO.getToken())
            .withCustomUserData(snsEndpointDTO.getCustomUserData())

            .withAttributes(mapEndpointAttributes(snsEndpointDTO));

        return req;
    }

    public DeleteEndpointRequest mapUnregisterDeviceReq(String endpointARN) {
        DeleteEndpointRequest req = new DeleteEndpointRequest();
        if (StringUtils.isNotEmpty(endpointARN)) {
            req.setEndpointArn(endpointARN);
            return req;
        } else {
            //TODO ExceptionHandling ARN cannot be null
            return null;
        }
    }

    public GetEndpointAttributesRequest mapGetDeviceAttributesReq(SNSEndpointDTO snsEndpointDTO) {
        GetEndpointAttributesRequest req = new GetEndpointAttributesRequest();
        if (StringUtils.isNotEmpty(snsEndpointDTO.getDeviceARN())) {
            req.setEndpointArn(snsEndpointDTO.getDeviceARN());
            return req;
        } else {
            //TODO ExceptionHandling ARN cannot be null
            return null;
        }
    }

    private Map mapEndpointAttributes(SNSEndpointDTO snsEndpointDTO) {
        Map<String, String> snsAttributeMap = new HashMap<>();
        if (snsEndpointDTO.getEnabled()) {
            snsAttributeMap.put("Enabled", String.valueOf(snsEndpointDTO.getEnabled()));
        }

        return snsAttributeMap;
    }

    public String translateTitle(SNSNotificationMessageDTO notificationMessage) {
    	Locale locale = Locale.forLanguageTag(notificationMessage.getUserLang());
    	String titleKey = notificationMessage.getTitleKey();
		return messageSource.getMessage(titleKey, notificationMessage.getTitleArgs(), titleKey, locale);
    }

    public String translateContent(SNSNotificationMessageDTO notificationMessage) {
    	Locale locale = Locale.forLanguageTag(notificationMessage.getUserLang());
    	String contentKey = notificationMessage.getContentKey();
		return messageSource.getMessage(contentKey, notificationMessage.getContentArgs(), contentKey, locale);
    }

    private String mapAndroidMessage(SNSNotificationMessageDTO notificationMessage) {
        Map<String, String> messagePayload = new HashMap<>();

        if (notificationMessage.getDriverNotificationMsgType() != null) {
            messagePayload.put("message", notificationMessage.getDriverNotificationMsgType().name());
            messagePayload.put("title", translateTitle(notificationMessage));
        } else {
            messagePayload.put("message", notificationMessage.getParentNotificationMsgType().name());
            if (notificationMessage.getCanceled_stop()!=null) {
                messagePayload.put("canceled_stop", String.valueOf(notificationMessage.getCanceled_stop()));
            }
        }

		messagePayload.put("content", translateContent(notificationMessage));


        Map<String, Object> dataMessageMap = new HashMap<>();
        dataMessageMap.put("data", messagePayload);
        dataMessageMap.put("priority", 10);

        Map<String, String> gcmMessage = new HashMap<>();
        gcmMessage.put(SNSMessagePlatformType.GCM.name(), mapToString(dataMessageMap));

        return mapToString(gcmMessage);
    }

	public String mapAppleMessage(SNSNotificationMessageDTO notificationMessage) {
		Map<String, Object> apmsPayload = new HashMap<>();

		Map<String, String> alertPayload = new HashMap<>();
		alertPayload.put("title", translateTitle(notificationMessage));
		alertPayload.put("body", translateContent(notificationMessage));

		Map<String, Object> apsPayload = new HashMap<>();
		apsPayload.put("alert", alertPayload);
		if (notificationMessage.getDriverNotificationMsgType() != null) {
			apsPayload.put("category", notificationMessage.getDriverNotificationMsgType().name());
			apmsPayload.put("type", notificationMessage.getDriverNotificationMsgType().name());
        } else {
        	apsPayload.put("category", notificationMessage.getParentNotificationMsgType().name());
        	apmsPayload.put("type", notificationMessage.getParentNotificationMsgType().name());
            if (notificationMessage.getCanceled_stop()!=null) {
                apmsPayload.put("canceled_stop", String.valueOf(notificationMessage.getCanceled_stop()));
            }
        }
		apmsPayload.put("aps", apsPayload);
		if (notificationMessage.getNotificationId() != null) {
			apmsPayload.put("notificationID", notificationMessage.getNotificationId());
		}

		Map<String, Object> messagePayload = new HashMap<>();

		String apnsPrefix = applicationProperties.getAwsSNSParentIOSARN();
		apnsPrefix = apnsPrefix.substring(apnsPrefix.indexOf(":app/") + 5);
		apnsPrefix = apnsPrefix.substring(0, apnsPrefix.indexOf("/"));
		messagePayload.put(apnsPrefix, mapToString(apmsPayload));


		String message = mapToString(messagePayload);
		log.debug("iOS SNS message: " + message);
		return message;
	}

    private String mapToString(Object obj) {

        try {
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            log.error("Mapping to string failed", e);
            throw (RuntimeException) e;
        }
    }

    public String mapSNSApplicationARN(SNSNotificationApplicationType applicationType) {
        switch (applicationType) {
            case PARENT_ANDROID:
                return applicationProperties.getAwsSNSParentAppARN();
            case PARENT_IOS:
                return applicationProperties.getAwsSNSParentIOSARN();
            case DRIVER:
                return applicationProperties.getAwsSNSDriverAppARN();
            default:
               return null;
        }
    }
}
