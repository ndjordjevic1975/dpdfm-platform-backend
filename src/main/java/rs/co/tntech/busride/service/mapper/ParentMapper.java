package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.ParentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Parent and its DTO ParentDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, SpotMapper.class, })
public interface ParentMapper extends EntityMapper <ParentDTO, Parent> {

    @Mapping(source = "user.id", target = "userId")

    @Mapping(source = "spot.id", target = "spotId")

    @Mapping(source = "linkToPrimary.id", target = "linkToPrimaryId")
    ParentDTO toDto(Parent parent); 

    @Mapping(source = "userId", target = "user")

    @Mapping(source = "spotId", target = "spot")

    @Mapping(source = "linkToPrimaryId", target = "linkToPrimary")
    @Mapping(target = "schools", ignore = true)
    Parent toEntity(ParentDTO parentDTO); 
    default Parent fromId(Long id) {
        if (id == null) {
            return null;
        }
        Parent parent = new Parent();
        parent.setId(id);
        return parent;
    }
}
