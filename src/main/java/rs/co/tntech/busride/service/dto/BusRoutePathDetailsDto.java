package rs.co.tntech.busride.service.dto;

import rs.co.tntech.busride.domain.enumeration.BusRouteType;

import java.util.ArrayList;
import java.util.List;

public class BusRoutePathDetailsDto {

    private List<BusRouteSinglePointDto> path = new ArrayList<>();
    private BusRouteType routeType;
    private Long tabletId;
    private Long routeId;
    private boolean updated;
    private BusRouteSinglePointDto currentBusLocation;

    public void addToPath(BusRouteSinglePointDto routePoint){
        path.add(routePoint);
    }

    public List<BusRouteSinglePointDto> getPath() {
        return path;
    }

    public void setPath(List<BusRouteSinglePointDto> path) {
        this.path = path;
    }

    public BusRouteType getRouteType() {
        return routeType;
    }

    public void setRouteType(BusRouteType routeType) {
        this.routeType = routeType;
    }

    public Long getTabletId() {
        return tabletId;
    }

    public void setTabletId(Long tabletId) {
        this.tabletId = tabletId;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public BusRouteSinglePointDto getCurrentBusLocation() {
        return currentBusLocation;
    }

    public void setCurrentBusLocation(BusRouteSinglePointDto currentBusLocation) {
        this.currentBusLocation = currentBusLocation;
    }

    @Override
    public String toString() {
        return "BusRoutePathDetailsDto{" +
            "path=" + path +
            ", routeType=" + routeType +
            ", tabletId=" + tabletId +
            ", routeId=" + routeId +
            ", updated=" + updated +
            ", currentBusLocation=" + currentBusLocation +
            '}';
    }
}
