package rs.co.tntech.busride.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.co.tntech.busride.domain.Driver;
import rs.co.tntech.busride.domain.DropoffReport;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.enumeration.BusRouteType;
import rs.co.tntech.busride.domain.enumeration.DriverNotificationMsgType;
import rs.co.tntech.busride.repository.DropoffReportRepository;
import rs.co.tntech.busride.repository.ParentRepository;
import rs.co.tntech.busride.repository.SpotRepository;
import rs.co.tntech.busride.security.AuthoritiesConstants;
import rs.co.tntech.busride.security.SecurityUtils;
import rs.co.tntech.busride.service.dto.DropoffReportDTO;
import rs.co.tntech.busride.service.dto.SNSNotificationMessageDTO;
import rs.co.tntech.busride.service.error.*;
import rs.co.tntech.busride.service.mapper.DropoffReportMapper;
import rs.co.tntech.busride.service.util.BRPServiceUtil;
import rs.co.tntech.busride.service.util.BRPServiceValidationUtils;
import rs.co.tntech.busride.service.util.EnumUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing DropoffReport.
 */
@Service
@Transactional
public class DropoffReportService extends NotificationService {


    private final Logger log = LoggerFactory.getLogger(DropoffReportService.class);

    @Autowired
    private DropoffReportRepository dropoffReportRepository;
    @Autowired
    private SpotRepository spotRepository;
    @Autowired
    private ParentRepository parentRepository;
    @Autowired
    private DropoffReportMapper dropoffReportMapper;
    @Autowired
    private BRPServiceUtil serviceUtil;
    @Autowired
    private EnumUtils enumUtils;
    @Autowired
    private SNSNotificationService snsNotificationService;
    @Autowired
    private MessageSource messageSource;

    /**
     * Save a dropoffReport.
     *
     * @param dropoffReportDTO the entity to save
     * @return the persisted entity
     */
    public DropoffReportDTO save(DropoffReportDTO dropoffReportDTO) {
        log.debug("Request to save DropoffReport : {}", dropoffReportDTO);
        DropoffReport dropoffReport = dropoffReportMapper.toEntity(dropoffReportDTO);
        dropoffReport = dropoffReportRepository.save(dropoffReport);
        return dropoffReportMapper.toDto(dropoffReport);
    }

    /**
     * Save a dropoffReport for current driver.
     *
     * @param dropoffReportDTO the entity to save
     * @return the persisted entity
     * @throws BRPBaseServiceException
     */
    public DropoffReportDTO saveForCurrentDriver(DropoffReportDTO dropoffReportDTO) throws BRPBaseServiceException {
        validateSaveForCurrentDriver(dropoffReportDTO);
        DropoffReportDTO savedDropoffReport;
        DropoffReport dropoffReport = dropoffReportMapper.toEntity(dropoffReportDTO);
        // set current driver's bus route to the saving dropoff report object
        Driver currentDriver = serviceUtil.getCurrentDriver();
        log.debug("API:POST:dropoff-reports/current-driver: Save DropoffReport:{} for Driver:{}", dropoffReportDTO, currentDriver.getUser().getId());
        dropoffReport.setBusRoute(currentDriver.getActiveBusRoute());

        dropoffReport = dropoffReportRepository.save(dropoffReport);
        savedDropoffReport = dropoffReportMapper.toDto(dropoffReport);

        // find users by spot id (primary parent and it's relatives)
        List<Parent> parents = parentRepository.findAllBySpotId(dropoffReportDTO.getSpotId());

        Locale locale = Locale.forLanguageTag(parents.get(0).getUser().getLangKey());

        String notification = messageSource.getMessage("notification.student.dropoff", null, locale);
        parents.forEach(parent -> saveParentNotification(parent, notification));

        if (!parents.isEmpty()) {
            SNSNotificationMessageDTO notificationMsg = new SNSNotificationMessageDTO(DriverNotificationMsgType.BUS);
            notificationMsg.setContentKey("notification.student.dropoff");
            notificationMsg.setTitleKey("notification.student.dropoff.title");
            notificationMsg.setNotificationId(dropoffReport.getId());
            snsNotificationService.publishMessageToParentApp(parents, notificationMsg);
        }

        return savedDropoffReport;
    }

    /**
     * Get all the dropoffReports.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<DropoffReportDTO> findAll() {
        log.debug("Request to get all DropoffReports");
        return dropoffReportRepository.findAll().stream()
            .map(dropoffReportMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one dropoffReport by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public DropoffReportDTO findOne(Long id) {
        log.debug("Request to get DropoffReport : {}", id);
        DropoffReport dropoffReport = dropoffReportRepository.findOne(id);
        return dropoffReportMapper.toDto(dropoffReport);
    }

    /**
     * Delete the  dropoffReport by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DropoffReport : {}", id);
        dropoffReportRepository.delete(id);
    }

    /**
     * This method validates input data for method {@link #saveForCurrentDriver(DropoffReportDTO)}
     *
     * @param dropoffReportDTO
     * @throws BRPBaseServiceException
     */
    private void validateSaveForCurrentDriver(DropoffReportDTO dropoffReportDTO) throws BRPBaseServiceException {
        // validate required role
        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.DRIVER)) {
            String msg = String.format("User does not have any of required roles: '%s'", AuthoritiesConstants.DRIVER);
            throw new BRPServiceSecurityException(AuthoritiesConstants.DRIVER, msg);
        }
        // validate input data
        List<BRPFieldException> fieldErrors = new ArrayList<>();
        // validate not allowed fields
        BRPServiceValidationUtils.validateNotAllowedField(fieldErrors, "DropoffReportDTO", "id", dropoffReportDTO.getId());
        BRPServiceValidationUtils.validateNotAllowedField(fieldErrors, "DropoffReportDTO", "busRouteId", dropoffReportDTO.getBusRouteId());
        // validate mandatory fields
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "DropoffReportDTO", "status", dropoffReportDTO.getStatus());
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "DropoffReportDTO", "planned", dropoffReportDTO.getPlanned());
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "DropoffReportDTO", "accomplished", dropoffReportDTO.getAccomplished());
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "DropoffReportDTO", "spotId", dropoffReportDTO.getSpotId());
        // validate spot id
        if (fieldErrors.isEmpty()) {
            if (spotRepository.getOne(dropoffReportDTO.getSpotId()) == null) {
                fieldErrors.add(new BRPFieldException("DropoffReportDTO", "spotId", BRPServiceErrorIds.SPOT_DB_MISSING,
                    "Spot data for id #" + dropoffReportDTO.getSpotId() + " does not exist."));
            }
        }
        if (!fieldErrors.isEmpty()) {
            throw new BRPFieldsExcpetion(fieldErrors, "Some of input fields are not valid");
        }
        // validate current driver and route
        Driver currentDriver = serviceUtil.getCurrentDriver();
        if (currentDriver == null) {
            throw new BRPServiceSystemException("There is no current driver user");
        }
        if (currentDriver.getActiveBusRoute() == null) {
            throw new BRPServiceExcpetion(BRPServiceErrorIds.CURRENT_DRIVER_ACTIVE_ROUTE_MISSING,
                "Current driver #" + currentDriver.getId() + " does not have associated bus route.");
        }
        if (!currentDriver.getActiveBusRoute().getType().equals(BusRouteType.DROPOFF)) {
            throw new BRPServiceExcpetion(BRPServiceErrorIds.ACTIVE_BUS_ROUTE_DROPOFF_REQUIRED,
                "Current drivers #" + currentDriver.getId() + " route #" + currentDriver.getActiveBusRoute().getId() + " is not DROPOFF rute.");
        }
    }

}
