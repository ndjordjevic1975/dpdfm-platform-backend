package rs.co.tntech.busride.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.ParentNotification;
import rs.co.tntech.busride.repository.ParentNotificationRepository;
import rs.co.tntech.busride.service.dto.ParentNotificationDTO;
import rs.co.tntech.busride.service.mapper.ParentNotificationMapper;
import rs.co.tntech.busride.service.util.BRPServiceUtil;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ParentNotification.
 */
@Service
@Transactional
public class ParentNotificationService {

    private final Logger log = LoggerFactory.getLogger(ParentNotificationService.class);

    @Autowired
    private ParentNotificationRepository parentNotificationRepository;
    @Autowired
    private ParentNotificationMapper parentNotificationMapper;
    @Autowired
    private BRPServiceUtil serviceUtil;

    /**
     * Save a parentNotification.
     *
     * @param parentNotificationDTO the entity to save
     * @return the persisted entity
     */
    public ParentNotificationDTO save(ParentNotificationDTO parentNotificationDTO) {
        log.debug("Request to save ParentNotification : {}", parentNotificationDTO);
        ParentNotification parentNotification = parentNotificationMapper.toEntity(parentNotificationDTO);
        parentNotification = parentNotificationRepository.save(parentNotification);
        return parentNotificationMapper.toDto(parentNotification);
    }

    /**
     *  Get all the parentNotifications.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ParentNotificationDTO> findAll() {
        log.debug("Request to get all ParentNotifications");
        return parentNotificationRepository.findAll().stream()
            .map(parentNotificationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one parentNotification by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ParentNotificationDTO findOne(Long id) {
        log.debug("Request to get ParentNotification : {}", id);
        ParentNotification parentNotification = parentNotificationRepository.findOne(id);
        return parentNotificationMapper.toDto(parentNotification);
    }

    /**
     *  Delete the  parentNotification by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ParentNotification : {}", id);
        parentNotificationRepository.delete(id);
    }

    /**
     * Delete all parentNotifications with IDs
     * @param ids
     */
    @Transactional()
    public void deleteByIds(Long[] ids) {
        log.debug("API:DELETE:IN:/parent-notifications/[{}]: Request to delete multiple ParentNotification", Arrays.toString(ids));
        for(Long id : ids) {
            delete(id);
        }
    }

	/**
	 * Gets all parentNotifications for current parent.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<ParentNotificationDTO> getAllNotificationForCurrentParent() {
		Parent currentParent = serviceUtil.getCurrentParent();
        log.debug("API:GET:IN:parent-notifications/current-parent: ParentId: {}", currentParent.getUser().getLogin());
        LinkedList<ParentNotificationDTO> result;
        result = parentNotificationRepository.findAllByParent(currentParent).stream()
                .map(parentNotificationMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
        log.debug("API:GET:OUT:parent-notifications/current-parent: Parent: {}, Parent Notifications: {}", currentParent, result.toString());
		return result;
	}
}
