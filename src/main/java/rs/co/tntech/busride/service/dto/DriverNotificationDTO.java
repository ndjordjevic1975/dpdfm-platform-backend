package rs.co.tntech.busride.service.dto;


import rs.co.tntech.busride.domain.enumeration.DriverNotificationType;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the DriverNotification entity.
 */
public class DriverNotificationDTO implements Serializable {

    private Long id;

    private String notification;

    private Instant createdOnDevice;

    private DriverNotificationType type;

    private Long driverId;

    private Long spotId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public Instant getCreatedOnDevice() {
        return createdOnDevice;
    }

    public void setCreatedOnDevice(Instant createdOnDevice) {
        this.createdOnDevice = createdOnDevice;
    }

    public DriverNotificationType getType() {
        return type;
    }

    public void setType(DriverNotificationType type) {
        this.type = type;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public Long getSpotId() {
        return spotId;
    }

    public void setSpotId(Long spotId) {
        this.spotId = spotId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DriverNotificationDTO driverNotificationDTO = (DriverNotificationDTO) o;
        if(driverNotificationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), driverNotificationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DriverNotificationDTO{" +
            "id=" + getId() +
            ", notification='" + getNotification() + "'" +
            ", createdOnDevice='" + getCreatedOnDevice() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
