package rs.co.tntech.busride.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.SchoolAdmin;
import rs.co.tntech.busride.domain.Student;
import rs.co.tntech.busride.repository.SchoolRepository;
import rs.co.tntech.busride.repository.StudentRepository;
import rs.co.tntech.busride.security.SecurityUtils;
import rs.co.tntech.busride.service.dto.StudentDTO;
import rs.co.tntech.busride.service.error.*;
import rs.co.tntech.busride.service.mapper.StudentMapper;
import rs.co.tntech.busride.service.util.BRPServiceUtil;
import rs.co.tntech.busride.service.util.BRPServiceValidationUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Student.
 */
@Service
@Transactional
public class StudentService {

    private final Logger log = LoggerFactory.getLogger(StudentService.class);

    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private SchoolRepository schoolRepository;
    @Autowired
    private StudentMapper studentMapper;
    @Autowired
    private BRPServiceUtil serviceUtil;

    /**
     * Save a student.
     *
     * @param studentDTO the entity to save
     * @return the persisted entity
     */
    public StudentDTO save(StudentDTO studentDTO) {
        log.debug("Request to save Student : {}", studentDTO);
        Student student = studentMapper.toEntity(studentDTO);
        student = studentRepository.save(student);
        return studentMapper.toDto(student);
    }

    /**
     * Updates an existing student for a current logged in (primary) parent.
     *
     * @param studentDTO
     * @return
     * @throws BRPBaseServiceException
     */
    public StudentDTO updateForCurrentPrimaryParent(StudentDTO studentDTO) throws BRPBaseServiceException {
        Parent currentPrimaryParent = serviceUtil.getCurrentPrimaryParent();
        log.debug("API:PUT:IN:students/current-parent for parent: {}, student: {}", currentPrimaryParent.getUser().getLogin(), studentDTO);

        Student student = validateUpdateForCurrentPrimaryParent(studentDTO);

        student.setImage(studentDTO.getImage());
        student.setImageContentType(studentDTO.getImageContentType());
        student.setName(studentDTO.getName());
        student.setSchool(schoolRepository.findOne(studentDTO.getSchoolId()));

        return studentMapper.toDto(studentRepository.save(student));
    }

    /**
     * Get all the students.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<StudentDTO> findAll() {
        log.debug("Request to get all Students");
        return studentRepository.findAll().stream()
            .map(studentMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one student by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public StudentDTO findOne(Long id) {
        Student student = studentRepository.findOne(id);
        return studentMapper.toDto(student);
    }

    /**
     * Delete the  student by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Student : {}", id);
        studentRepository.delete(id);
    }

    /**
     * Gets all students for a current logged in parent. If the parent is NOT a primary one this returns the students from the primary parent.
     *
     * @return the list of entities
     * @throws BRPBaseServiceException
     */
    @Transactional(readOnly = true)
    public List<StudentDTO> getAllByCurrentPrimaryParent() {
        Parent primaryParent = serviceUtil.getCurrentPrimaryParent();
        log.debug("API:GET:IN:students/current-parent: Primary parent: {}", primaryParent.getUser().getLogin());

        LinkedList<StudentDTO> result = studentRepository.findAllByParent(primaryParent).stream()
            .map(studentMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));

        log.debug("API:GET:OUT:students/current-parent: Primary parent: {}, Students: {}", primaryParent.getUser().getLogin(), result);

        return result;
    }

    /**
     * @return Total number of students for currently logged school admin
     */
    public int getTotalByCurrentSchoolAdmin(){
        SchoolAdmin currentSchoolAdmin = serviceUtil.getCurrentSchoolAdmin();
        Long schoolId = currentSchoolAdmin.getSchool().getId();
        return studentRepository.countBySchoolId(schoolId);
    }

    /**
     * @return Total number of students for current school admin with non canceled service
     */
    public int getTotalNonCanceledBySchoolAdmin(){
        SchoolAdmin currentSchoolAdmin = serviceUtil.getCurrentSchoolAdmin();
        Long schoolId = currentSchoolAdmin.getSchool().getId();
        return studentRepository.countNonCanceledBySchoolId(schoolId);
    }

    /**
     * This method validates input data for method {@link #updateForCurrentPrimaryParent}
     *
     * @param studentDTO
     * @throws BRPBaseServiceException
     */
    private Student validateUpdateForCurrentPrimaryParent(StudentDTO studentDTO) throws BRPBaseServiceException {
        Student studentDB = null;
        List<BRPFieldException> fieldErrors = validateSaveUpdateCommonForCurrentPrimaryParent(studentDTO);
        // validate mandatory fields
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "StudentDTO", "id", studentDTO.getId());
        // validate if student exists for given id
        if (fieldErrors.isEmpty()) {
        	studentDB = studentRepository.findOne(studentDTO.getId());
            if (studentDB == null) {
                fieldErrors.add(new BRPFieldException("StudentDTO", "id", BRPServiceErrorIds.STUDENT_DB_MISSING,
                    "Student data for id #" + studentDTO.getId() + " does not exist."));
            }
        }
        // validate if student is connected to current primary parent
        if (fieldErrors.isEmpty()) {
            Parent currentPrimaryParent = serviceUtil.getCurrentPrimaryParent();
            if (!studentDB.getParent().getId().equals(currentPrimaryParent.getId())) {
                fieldErrors.add(new BRPFieldException("StudentDTO", "id", BRPServiceErrorIds.NOT_CURRENT_PARENT_STUDENT,
                    "Student id #" + studentDTO.getId() + " is not connected to current primary parent #" + currentPrimaryParent.getId()));
            }
        }
        if (!fieldErrors.isEmpty()) {
            throw new BRPFieldsExcpetion(fieldErrors, "Some of input fields are not valid");
        }
        return studentDB;
    }

    private List<BRPFieldException> validateSaveUpdateCommonForCurrentPrimaryParent(StudentDTO studentDTO) throws BRPBaseServiceException {
        // TODO validate required role
        // if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.PARENT)) {
        // String msg = String.format("User does not have any of required roles: '%s'", AuthoritiesConstants.PARENT);
        // throw new BRPServiceSecurityException(AuthoritiesConstants.PARENT, msg);
        // }
        // validate current parent
        Parent currentPrimaryParent = serviceUtil.getCurrentPrimaryParent();
        if (currentPrimaryParent == null) {
            throw new BRPServiceSystemException("There is no primary parent user. (AUser: " + SecurityUtils.getCurrentUserLogin() + ")");
        }
        // validate input data
        List<BRPFieldException> fieldErrors = new ArrayList<>();
        // validate not allowed fields
        BRPServiceValidationUtils.validateNotAllowedField(fieldErrors, "StudentDTO", "parentId", studentDTO.getParentId());
        // validate mandatory fields
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "StudentDTO", "schoolId", studentDTO.getSchoolId());
        // validate if school exists for given id
        if (fieldErrors.isEmpty()) {
            if (schoolRepository.findOne(studentDTO.getSchoolId()) == null) {
                fieldErrors.add(new BRPFieldException("StudentDTO", "schoolId", BRPServiceErrorIds.SCHOOL_DB_MISSING,
                    "School data for id #" + studentDTO.getSchoolId() + " does not exist."));
            }
        }
        return fieldErrors;
    }

}
