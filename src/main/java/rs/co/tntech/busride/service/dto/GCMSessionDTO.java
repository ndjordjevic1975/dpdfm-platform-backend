package rs.co.tntech.busride.service.dto;

import org.hibernate.validator.constraints.NotEmpty;

import rs.co.tntech.busride.domain.enumeration.DeviceType;

public class GCMSessionDTO {

    @NotEmpty
    private String gcmSessionId;
    private DeviceType deviceType;

    public GCMSessionDTO() {
    }

    /**
	 * @return the gcmSessionId
	 */
	public String getGcmSessionId() {
		return gcmSessionId;
	}

	/**
	 * @param gcmSessionId the gcmSessionId to set
	 */
	public void setGcmSessionId(String gcmSessionId) {
		this.gcmSessionId = gcmSessionId;
	}

	/**
	 * @return the deviceType
	 */
	public DeviceType getDeviceType() {
		return deviceType;
	}

	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	@Override
    public String toString() {
        return "{" +
            "\"gcmSessionId\": \"" + gcmSessionId + "\"" +
            "\"deviceType\": \"" + deviceType + "\"" +
            '}';
    }
}
