package rs.co.tntech.busride.service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.co.tntech.busride.domain.BusRoute;
import rs.co.tntech.busride.domain.LiveViewReportData;
import rs.co.tntech.busride.web.rest.vm.UpdateLiveViewReportVM;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
public class LiveViewReportStorageService {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    public IMap<Long, Map<Long, LiveViewReportData>> getLiveViewReportStorage() {
        return hazelcastInstance.getMap("LiveViewReportStorage");
    }

    public void updateLiveViewReport(Long schoolId, UpdateLiveViewReportVM updateLiveViewReportVM, Long activeBusRouteId) {
        IMap<Long, Map<Long, LiveViewReportData>> liveViewReportStorage = hazelcastInstance.getMap("LiveViewReportStorage");
        Map<Long, LiveViewReportData> liveViewReportForSchool = liveViewReportStorage.get(schoolId);

        if (liveViewReportForSchool == null) {
            HashMap<Long, LiveViewReportData> updatedBusPosition = new HashMap<>();
            updatedBusPosition.put(activeBusRouteId, new LiveViewReportData(updateLiveViewReportVM.getGpsLatitude(), updateLiveViewReportVM.getGpsLongitude(), LocalDateTime.now()));

            liveViewReportStorage.put(schoolId, updatedBusPosition);
        } else {
            if(liveViewReportForSchool.get(activeBusRouteId) == null) {
                liveViewReportForSchool.put(activeBusRouteId, new LiveViewReportData(updateLiveViewReportVM.getGpsLatitude(), updateLiveViewReportVM.getGpsLongitude(), LocalDateTime.now()));
                liveViewReportStorage.put(schoolId, liveViewReportForSchool);
            } else {
                LiveViewReportData liveViewReportData = liveViewReportForSchool.get(activeBusRouteId);
                liveViewReportData.setGpsLatitude(updateLiveViewReportVM.getGpsLatitude());
                liveViewReportData.setGpsLongitude(updateLiveViewReportVM.getGpsLongitude());
                liveViewReportData.setUpdatedOn(LocalDateTime.now());
                liveViewReportForSchool.put(activeBusRouteId, liveViewReportData);
                liveViewReportStorage.put(schoolId, liveViewReportForSchool);
            }
        }
    }

    public void deleteLiveViewReport(Long schoolId, BusRoute busRoute) {
        IMap<Long, Map<Long, LiveViewReportData>> liveViewReportStorage = hazelcastInstance.getMap("LiveViewReportStorage");
        Map<Long, LiveViewReportData> liveViewReportForSchool = liveViewReportStorage.get(schoolId);

        if (liveViewReportForSchool != null) {
            liveViewReportForSchool.remove(busRoute.getId());
            liveViewReportStorage.put(schoolId, liveViewReportForSchool);
        }
    }
}
