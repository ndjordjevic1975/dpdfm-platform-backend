package rs.co.tntech.busride.service.dto;


import rs.co.tntech.busride.domain.enumeration.ParentNotificationType;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the ParentNotification entity.
 */
public class ParentNotificationDTO implements Serializable {

    private Long id;

    private String notification;

    private Instant timestamp;

    private ParentNotificationType type;

    private Long parentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public ParentNotificationType getType() {
        return type;
    }

    public void setType(ParentNotificationType type) {
        this.type = type;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ParentNotificationDTO parentNotificationDTO = (ParentNotificationDTO) o;
        if(parentNotificationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), parentNotificationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ParentNotificationDTO{" +
            "id=" + getId() +
            ", notification='" + getNotification() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
