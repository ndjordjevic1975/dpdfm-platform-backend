/**
 *
 */
package rs.co.tntech.busride.service.error;

/**
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public interface BRPServiceErrorIds {

	String CURRENT_DRIVER_ACTIVE_ROUTE_MISSING = "currentDriver.activeBusRoute.missing";
	String ACTIVE_BUS_ROUTE_PICKUP_REQUIRED =  "activeBusRoute.type.pickup.required";
	String ACTIVE_BUS_ROUTE_DROPOFF_REQUIRED =  "activeBusRoute.type.dropoff.required";
	String SPOT_DB_MISSING = "spot.id.db.missing";
	String STUDENT_DB_MISSING = "student.id.db.missing";
	String SCHOOL_DB_MISSING = "school.id.db.missing";
	String SCHOOL_EMAIL_DB_MISSING = "school.email.id.db.missing";
	String SCHOOL_EMAIL_NOTRELATED_SCHOOL_ADMIN = "school.email.notrelated.school.admin";
	String BUS_ROUTE_DB_MISSING = "busRoute.id.db.missing";
	String NOT_DRIVERS_BUS_ROUTE = "not.drivers.busRoute";
	String NOT_CURRENT_PARENT_STUDENT = "not.current.parents.student";
	String PARENT_MAX_STUDENTS_NO_REACHED = "parent.max.students.no.reached";
	String USER_EMAIL_ALREADY_EXIST = "user.email.already.exist";
	String USER_NATIONAL_ID_ALREADY_EXIST = "user.nationalId.already.exist";
	String DEVICE_TYPE_MISSING = "device.type.missing";

    String SNS_UNREGISTER_DEVICE_FAILED = "sns.device.unregister.failed" ;
    String PARENT_SPOT_MUST_HAVE_LAT_AND_LON = "parent.spot.no.coordinates";
}
