package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.SchoolDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity School and its DTO SchoolDTO.
 */
@Mapper(componentModel = "spring", uses = {SpotMapper.class, ParentMapper.class, })
public interface SchoolMapper extends EntityMapper <SchoolDTO, School> {

    @Mapping(source = "spot.id", target = "spotId")
    SchoolDTO toDto(School school); 

    @Mapping(source = "spotId", target = "spot")
    School toEntity(SchoolDTO schoolDTO); 
    default School fromId(Long id) {
        if (id == null) {
            return null;
        }
        School school = new School();
        school.setId(id);
        return school;
    }
}
