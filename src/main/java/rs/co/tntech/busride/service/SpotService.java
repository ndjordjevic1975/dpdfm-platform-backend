package rs.co.tntech.busride.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.co.tntech.busride.domain.SchoolAdmin;
import rs.co.tntech.busride.domain.Spot;
import rs.co.tntech.busride.repository.SpotRepository;
import rs.co.tntech.busride.service.dto.SpotDTO;
import rs.co.tntech.busride.service.mapper.SpotMapper;
import rs.co.tntech.busride.service.util.BRPServiceUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Spot.
 */
@Service
@Transactional
public class SpotService {

    private final Logger log = LoggerFactory.getLogger(SpotService.class);

    private final SpotRepository spotRepository;

    private final SpotMapper spotMapper;

    private final BRPServiceUtil serviceUtil;


    public SpotService(SpotRepository spotRepository, SpotMapper spotMapper, BRPServiceUtil serviceUtil) {
        this.spotRepository = spotRepository;
        this.spotMapper = spotMapper;
        this.serviceUtil = serviceUtil;
    }

    /**
     * Save a spot.
     *
     * @param spotDTO the entity to save
     * @return the persisted entity
     */
    public SpotDTO save(SpotDTO spotDTO) {
        log.debug("Request to save Spot : {}", spotDTO);
        Spot spot = spotMapper.toEntity(spotDTO);
        spot = spotRepository.save(spot);
        return spotMapper.toDto(spot);
    }

    /**
     *  Get all the spots.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SpotDTO> findAll() {
        log.debug("Request to get all Spots");
        return spotRepository.findAll().stream()
            .map(spotMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one spot by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SpotDTO findOne(Long id) {
        log.debug("Request to get Spot : {}", id);
        Spot spot = spotRepository.findOne(id);
        return spotMapper.toDto(spot);
    }

    /**
     *  Delete the  spot by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Spot : {}", id);
        spotRepository.delete(id);
    }

    public SpotDTO getForCurrentSchool(){
        SchoolAdmin schoolAdmin = serviceUtil.getCurrentSchoolAdmin();
        return spotMapper.toDto(schoolAdmin.getSchool().getSpot());
    }
}
