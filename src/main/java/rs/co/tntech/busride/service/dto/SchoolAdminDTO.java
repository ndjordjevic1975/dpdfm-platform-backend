package rs.co.tntech.busride.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the SchoolAdmin entity.
 */
public class SchoolAdminDTO implements Serializable {

    private Long id;

    private Long userId;

    private Long schoolId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SchoolAdminDTO schoolAdminDTO = (SchoolAdminDTO) o;
        if(schoolAdminDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), schoolAdminDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SchoolAdminDTO{" +
            "id=" + getId() +
            "}";
    }
}
