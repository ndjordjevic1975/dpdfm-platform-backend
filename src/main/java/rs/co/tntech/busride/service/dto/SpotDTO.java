package rs.co.tntech.busride.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import rs.co.tntech.busride.domain.enumeration.SpotType;

/**
 * A DTO for the Spot entity.
 */
public class SpotDTO implements Serializable {

    private Long id;

    private Double gpsLatitude;

    private Double gpsLongitude;

    private SpotType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(Double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public Double getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(Double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public SpotType getType() {
        return type;
    }

    public void setType(SpotType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SpotDTO spotDTO = (SpotDTO) o;
        if(spotDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), spotDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SpotDTO{" +
            "id=" + getId() +
            ", gpsLatitude='" + getGpsLatitude() + "'" +
            ", gpsLongitude='" + getGpsLongitude() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
