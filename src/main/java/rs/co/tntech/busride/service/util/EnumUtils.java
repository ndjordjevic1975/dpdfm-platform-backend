package rs.co.tntech.busride.service.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import rs.co.tntech.busride.domain.enumeration.BusRouteType;
import rs.co.tntech.busride.domain.enumeration.PickupDropoffType;

import java.util.Locale;

@Component
public class EnumUtils {

    @Autowired
    private MessageSource messageSource;

    public final <E extends Enum<E>> String localized(E enumValue, Locale locale){
        String enumKey = "enum."+enumValue.getClass().getCanonicalName()+"."+enumValue.name();
        return messageSource.getMessage(enumKey, null, locale);
    }
}
