package rs.co.tntech.busride.service.dto;


import rs.co.tntech.busride.domain.enumeration.PickupDropoffType;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the PickupReport entity.
 */
public class PickupReportDTO implements Serializable {

    private Long id;

    private PickupDropoffType status;

    private Double delay;

    private Integer planned;

    private Integer accomplished;

    private Long busRouteId;

    private Long spotId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PickupDropoffType getStatus() {
        return status;
    }

    public void setStatus(PickupDropoffType status) {
        this.status = status;
    }

    public Double getDelay() {
        return delay;
    }

    public void setDelay(Double delay) {
        this.delay = delay;
    }

    public Integer getPlanned() {
        return planned;
    }

    public void setPlanned(Integer planned) {
        this.planned = planned;
    }

    public Integer getAccomplished() {
        return accomplished;
    }

    public void setAccomplished(Integer accomplished) {
        this.accomplished = accomplished;
    }

    public Long getBusRouteId() {
        return busRouteId;
    }

    public void setBusRouteId(Long busRouteId) {
        this.busRouteId = busRouteId;
    }

    public Long getSpotId() {
        return spotId;
    }

    public void setSpotId(Long spotId) {
        this.spotId = spotId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PickupReportDTO pickupReportDTO = (PickupReportDTO) o;
        if(pickupReportDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pickupReportDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PickupReportDTO{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", delay='" + getDelay() + "'" +
            ", planned='" + getPlanned() + "'" +
            ", accomplished='" + getAccomplished() + "'" +
            "}";
    }
}
