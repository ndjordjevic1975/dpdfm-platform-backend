package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.PickupReportDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PickupReport and its DTO PickupReportDTO.
 */
@Mapper(componentModel = "spring", uses = {BusRouteMapper.class, SpotMapper.class, })
public interface PickupReportMapper extends EntityMapper <PickupReportDTO, PickupReport> {

    @Mapping(source = "busRoute.id", target = "busRouteId")

    @Mapping(source = "spot.id", target = "spotId")
    PickupReportDTO toDto(PickupReport pickupReport); 

    @Mapping(source = "busRouteId", target = "busRoute")

    @Mapping(source = "spotId", target = "spot")
    PickupReport toEntity(PickupReportDTO pickupReportDTO); 
    default PickupReport fromId(Long id) {
        if (id == null) {
            return null;
        }
        PickupReport pickupReport = new PickupReport();
        pickupReport.setId(id);
        return pickupReport;
    }
}
