package rs.co.tntech.busride.service;

import gherkin.deps.com.google.gson.JsonArray;
import gherkin.deps.com.google.gson.JsonElement;
import gherkin.deps.com.google.gson.JsonObject;
import gherkin.deps.com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.co.tntech.busride.config.ApplicationProperties;
import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.domain.enumeration.BusRouteType;
import rs.co.tntech.busride.repository.BusRouteRepository;
import rs.co.tntech.busride.repository.DriverRepository;
import rs.co.tntech.busride.service.dto.BusRouteDTO;
import rs.co.tntech.busride.service.dto.BusRoutePathDetailsDto;
import rs.co.tntech.busride.service.dto.BusRouteSinglePointDto;
import rs.co.tntech.busride.service.mapper.BusRouteMapper;
import rs.co.tntech.busride.service.util.BRPServiceUtil;
import rs.co.tntech.busride.web.rest.vm.TotalBusesOnRoutesVM;
import rs.co.tntech.busride.web.rest.vm.TotalPickupDropoffVM;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing BusRoute.
 */
@Service
@Transactional
public class BusRouteService {

    private final Logger log = LoggerFactory.getLogger(BusRouteService.class);

    private final BusRouteRepository busRouteRepository;

    private final BusRouteMapper busRouteMapper;

    private final BRPServiceUtil serviceUtil;

    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private LiveViewReportStorageService liveViewReportStorageService;

    @Autowired
    private DriverRepository driverRepository;

    public BusRouteService(BusRouteRepository busRouteRepository, BusRouteMapper busRouteMapper, BRPServiceUtil serviceUtil) {
        this.busRouteRepository = busRouteRepository;
        this.busRouteMapper = busRouteMapper;
        this.serviceUtil = serviceUtil;
    }

    /**
     * Save a busRoute.
     *
     * @param busRouteDTO the entity to save
     * @return the persisted entity
     */
    public BusRouteDTO save(BusRouteDTO busRouteDTO) {
        log.debug("Request to save BusRoute : {}", busRouteDTO);
        BusRoute busRoute = busRouteMapper.toEntity(busRouteDTO);
        busRoute = busRouteRepository.save(busRoute);
        return busRouteMapper.toDto(busRoute);
    }

    /**
     * Get all the busRoutes.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<BusRouteDTO> findAll() {
        log.debug("Request to get all BusRoutes");
        return busRouteRepository.findAllWithEagerRelationships().stream()
            .map(busRouteMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get all the busRoutes.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<BusRoutePathDetailsDto> findDetailedPathForCurrentSchool() {
        log.debug("Request to find bus route path details for current school");
        SchoolAdmin currentSchoolAdmin = serviceUtil.getCurrentSchoolAdmin();

        List<BusRoutePathDetailsDto> details = new ArrayList<>();

        busRouteRepository.findAllBySchoolIdAndInactiveOnIsNull(currentSchoolAdmin.getSchool().getId()).forEach(busRoute -> {

            BusRoutePathDetailsDto detail = fillBusRoutePathDetail(busRoute);

            details.add(detail);
        });

        return details;
    }

    /**
     * Get one busRoute by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BusRouteDTO findOne(Long id) {
        log.debug("Request to get BusRoute : {}", id);
        BusRoute busRoute = busRouteRepository.findOneWithEagerRelationships(id);
        return busRouteMapper.toDto(busRoute);
    }

    /**
     * Delete the  busRoute by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete BusRoute : {}", id);
        busRouteRepository.delete(id);
    }

    /**
     * Gets all bus routes the driver is associated with.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<BusRouteDTO> findAllByCurrentDriver() {
        Driver driver = serviceUtil.getCurrentDriver();
        log.debug("API:GET:IN:bus-routes/current-driver: Driver: {}", driver.getUser().getLogin());
        LinkedList<BusRouteDTO> result;
        Long driverId = driver.getId();
        result = busRouteRepository.findAllByDriver_IdAndInactiveOnIsNull(driverId).stream()
            .map(busRouteMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
        log.debug("API:GET:OUT:bus-routes/current-driver: Driver: {}, No of routes returned: {}", driver, result.size());
        return result;
    }

    public List<BusRoutePathDetailsDto> getAllActiveBusLocations() {
        log.debug("Request to get details for all active routes and bus locations");

        SchoolAdmin currentSchoolAdmin = serviceUtil.getCurrentSchoolAdmin();

        List<BusRoutePathDetailsDto> details = new ArrayList<>();

        List<Driver> assignedSchoolRoutes =
            driverRepository.findAllBySchoolIdAndActiveBusRouteIsNotNull(currentSchoolAdmin.getSchool().getId());

        List<BusRoute> routes = assignedSchoolRoutes.stream().map(Driver::getActiveBusRoute).collect(Collectors.toList());

        routes.forEach(busRoute -> {

            BusRoutePathDetailsDto detail = fillBusRoutePathDetail(busRoute);
            log.debug("BusRoutePathDetailsDto detail = fillBusRoutePathDetail(busRoute): " + detail);

            Map<Long, LiveViewReportData> activeBusLocationsForCurrentSchool = liveViewReportStorageService.getLiveViewReportStorage().get(currentSchoolAdmin.getSchool().getId());

            if (activeBusLocationsForCurrentSchool != null) {
                LiveViewReportData liveViewReportData = activeBusLocationsForCurrentSchool.get(busRoute.getId());

                if (liveViewReportData != null) {
                    log.debug("LiveViewReportData liveViewReportData = activeBusLocationsForCurrentSchool.get(busRoute.getId()):" + liveViewReportData);

                    detail.setCurrentBusLocation(new BusRouteSinglePointDto(liveViewReportData.getGpsLatitude(), liveViewReportData.getGpsLongitude()));

                    long secondsSinceLastUpdate = ChronoUnit.SECONDS.between(liveViewReportData.getUpdatedOn(), LocalDateTime.now());
                    boolean updated = secondsSinceLastUpdate <= 2 * Long.parseLong(applicationProperties.getMgmPortalLiveReportRefreshRate());

                    detail.setUpdated(updated);

                    details.add(detail);
                }
            }
        });

        return details;
    }

    private BusRoutePathDetailsDto fillBusRoutePathDetail(BusRoute busRoute) {
        BusRoutePathDetailsDto detail = new BusRoutePathDetailsDto();

        detail.setRouteType(busRoute.getType());
        detail.setTabletId(busRoute.getDriver() != null ? busRoute.getDriver().getId() : null);
        detail.setRouteId(busRoute.getId());

        JsonParser parser = new JsonParser();
        JsonObject obj = parser.parse(busRoute.getDetails()).getAsJsonObject();
        JsonArray pointsArray = obj.get("route_points").getAsJsonArray();

        for (JsonElement jsonElement : pointsArray) {
            BusRouteSinglePointDto singlePoint = new BusRouteSinglePointDto();
            singlePoint.setLat(jsonElement.getAsJsonObject().get("Lat").getAsDouble());
            singlePoint.setLng(jsonElement.getAsJsonObject().get("Lng").getAsDouble());
            detail.addToPath(singlePoint);
        }

        return detail;
    }

    public TotalPickupDropoffVM getTotalPickupDropOff() {
        School school = serviceUtil.getCurrentSchoolAdmin().getSchool();

        TotalPickupDropoffVM totalPickupDropoffVM = new TotalPickupDropoffVM(busRouteRepository.countByTypeAndSchoolAndInactiveOnIsNull(BusRouteType.PICKUP, school), busRouteRepository.countByTypeAndSchoolAndInactiveOnIsNull(BusRouteType.DROPOFF, school));

        log.debug("API:GET:OUT:bus-routes/total-pickup-dropoff: {}", totalPickupDropoffVM);

        return totalPickupDropoffVM;
    }

    public TotalBusesOnRoutesVM getTotalBusesOnRoutes() {
        School school = serviceUtil.getCurrentSchoolAdmin().getSchool();

        TotalBusesOnRoutesVM totalBusesOnRoutesVM = new TotalBusesOnRoutesVM(driverRepository.countBySchoolAndActiveBusRoute_idGreaterThan(school, 0L));

        log.debug("API:GET:OUT:bus-routes/total-buses-on-routes: {}", totalBusesOnRoutesVM);

        return totalBusesOnRoutesVM;
    }
}
