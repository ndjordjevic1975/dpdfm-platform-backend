package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.DriverDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Driver and its DTO DriverDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, BusRouteMapper.class, SchoolMapper.class, })
public interface DriverMapper extends EntityMapper <DriverDTO, Driver> {

    @Mapping(source = "user.id", target = "userId")

    @Mapping(source = "activeBusRoute.id", target = "activeBusRouteId")

    @Mapping(source = "school.id", target = "schoolId")
    DriverDTO toDto(Driver driver); 

    @Mapping(source = "userId", target = "user")

    @Mapping(source = "activeBusRouteId", target = "activeBusRoute")

    @Mapping(source = "schoolId", target = "school")
    Driver toEntity(DriverDTO driverDTO); 
    default Driver fromId(Long id) {
        if (id == null) {
            return null;
        }
        Driver driver = new Driver();
        driver.setId(id);
        return driver;
    }
}
