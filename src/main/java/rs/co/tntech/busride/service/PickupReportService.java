package rs.co.tntech.busride.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.co.tntech.busride.domain.Driver;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.PickupReport;
import rs.co.tntech.busride.domain.enumeration.BusRouteType;
import rs.co.tntech.busride.domain.enumeration.DriverNotificationMsgType;
import rs.co.tntech.busride.repository.ParentNotificationRepository;
import rs.co.tntech.busride.repository.ParentRepository;
import rs.co.tntech.busride.repository.PickupReportRepository;
import rs.co.tntech.busride.repository.SpotRepository;
import rs.co.tntech.busride.security.AuthoritiesConstants;
import rs.co.tntech.busride.security.SecurityUtils;
import rs.co.tntech.busride.service.dto.PickupReportDTO;
import rs.co.tntech.busride.service.dto.SNSNotificationMessageDTO;
import rs.co.tntech.busride.service.error.*;
import rs.co.tntech.busride.service.mapper.PickupReportMapper;
import rs.co.tntech.busride.service.util.BRPServiceUtil;
import rs.co.tntech.busride.service.util.BRPServiceValidationUtils;
import rs.co.tntech.busride.service.util.EnumUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing PickupReport.
 */
@Service
@Transactional
public class PickupReportService extends NotificationService {

    private final Logger log = LoggerFactory.getLogger(PickupReportService.class);

    @Autowired
    private PickupReportRepository pickupReportRepository;
    @Autowired
    private SpotRepository spotRepository;
    @Autowired
    private ParentRepository parentRepository;
    @Autowired
    private PickupReportMapper pickupReportMapper;
    @Autowired
    private BRPServiceUtil serviceUtil;
    @Autowired
    private EnumUtils enumUtils;
    @Autowired
    private SNSNotificationService snsNotificationService;
    @Autowired
    private ParentNotificationRepository parentNotificationRepository;
    @Autowired
    private MessageSource messageSource;

    /**
     * Save a pickupReport.
     *
     * @param pickupReportDTO the entity to save
     * @return the persisted entity
     */
    public PickupReportDTO save(PickupReportDTO pickupReportDTO) {
        log.debug("Request to save PickupReport : {}", pickupReportDTO);
        PickupReport pickupReport = pickupReportMapper.toEntity(pickupReportDTO);
        pickupReport = pickupReportRepository.save(pickupReport);
        return pickupReportMapper.toDto(pickupReport);
    }

    /**
     * Save a pickupReport for current driver.
     *
     * @param pickupReportDTO the entity to save
     * @return the persisted entity
     * @throws BRPBaseServiceException
     */
    public PickupReportDTO saveForCurrentDriver(PickupReportDTO pickupReportDTO) throws BRPBaseServiceException {
        validateSaveForCurrentDriver(pickupReportDTO);
        PickupReportDTO savedPickupReport;
        PickupReport pickupReport = pickupReportMapper.toEntity(pickupReportDTO);
        // set current driver's bus route to the saving pickup report object
        Driver currentDriver = serviceUtil.getCurrentDriver();
        log.debug("API:POST:pickup-reports/current-driver: Save PickupReport:{} for Driver:{}", pickupReportDTO, currentDriver.getUser().getId());
        pickupReport.setBusRoute(currentDriver.getActiveBusRoute());

        pickupReport = pickupReportRepository.save(pickupReport);
        savedPickupReport = pickupReportMapper.toDto(pickupReport);

        // find users by spot id (primary parent and it's relatives)
        List<Parent> parents = parentRepository.findAllBySpotId(pickupReportDTO.getSpotId());

        Locale locale = Locale.forLanguageTag(parents.get(0).getUser().getLangKey());
        String[] notificationArguments = {
            enumUtils.localized(pickupReportDTO.getStatus(), locale),
            pickupReportDTO.getAccomplished().toString(),
            pickupReportDTO.getPlanned().toString()
        };

        String notification = messageSource.getMessage("notification.student.pickup", notificationArguments, locale);
        parents.forEach(parent -> saveParentNotification(parent, notification));

        // send push notifications to all parents
        if (!parents.isEmpty()) {
            SNSNotificationMessageDTO notificationMsg = new SNSNotificationMessageDTO(DriverNotificationMsgType.BUS);
            notificationMsg.setContentKey("notification.student.pickup");
            notificationMsg.setContentArgs(notificationArguments);
            notificationMsg.setTitleKey("notification.student.pickup.title");
            notificationMsg.setNotificationId(pickupReport.getId());
//            snsNotificationService.publishMessageToParentApp(parents, notificationMsg);
        }
        return savedPickupReport;
    }

    /**
     * Get all the pickupReports.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<PickupReportDTO> findAll() {
        log.debug("Request to get all PickupReports");
        return pickupReportRepository.findAll().stream()
            .map(pickupReportMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one pickupReport by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PickupReportDTO findOne(Long id) {
        log.debug("Request to get PickupReport : {}", id);
        PickupReport pickupReport = pickupReportRepository.findOne(id);
        return pickupReportMapper.toDto(pickupReport);
    }

    /**
     * Delete the  pickupReport by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PickupReport : {}", id);
        pickupReportRepository.delete(id);
    }

    /**
     * This method validates input data for method {@link #saveForCurrentDriver(PickupReportDTO)}
     *
     * @param pickupReportDTO
     * @throws BRPBaseServiceException
     */
    private void validateSaveForCurrentDriver(PickupReportDTO pickupReportDTO) throws BRPBaseServiceException {
        // validate required role
        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.DRIVER)) {
            String msg = String.format("User does not have any of required roles: '%s'", AuthoritiesConstants.DRIVER);
            throw new BRPServiceSecurityException(AuthoritiesConstants.DRIVER, msg);
        }
        // validate input data
        List<BRPFieldException> fieldErrors = new ArrayList<>();
        // validate not allowed fields
        BRPServiceValidationUtils.validateNotAllowedField(fieldErrors, "PickupReportDTO", "id", pickupReportDTO.getId());
        BRPServiceValidationUtils.validateNotAllowedField(fieldErrors, "PickupReportDTO", "busRouteId", pickupReportDTO.getBusRouteId());
        // validate mandatory fields
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "PickupReportDTO", "status", pickupReportDTO.getStatus());
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "PickupReportDTO", "delay", pickupReportDTO.getDelay());
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "PickupReportDTO", "planned", pickupReportDTO.getPlanned());
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "PickupReportDTO", "accomplished", pickupReportDTO.getAccomplished());
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "PickupReportDTO", "spotId", pickupReportDTO.getSpotId());
        // validate spot id
        if (fieldErrors.isEmpty()) {
            if (spotRepository.getOne(pickupReportDTO.getSpotId()) == null) {
                fieldErrors.add(new BRPFieldException("PickupReportDTO", "spotId", BRPServiceErrorIds.SPOT_DB_MISSING,
                    "Spot data for id #" + pickupReportDTO.getSpotId() + " does not exist."));
            }
        }
        if (!fieldErrors.isEmpty()) {
            throw new BRPFieldsExcpetion(fieldErrors, "Some of input fields are not valid");
        }
        // validate current driver and route
        Driver currentDriver = serviceUtil.getCurrentDriver();
        if (currentDriver == null) {
            throw new BRPServiceSystemException("There is no current driver user");
        }
        if (currentDriver.getActiveBusRoute() == null) {
            throw new BRPServiceExcpetion(BRPServiceErrorIds.CURRENT_DRIVER_ACTIVE_ROUTE_MISSING,
                "Current driver #" + currentDriver.getId() + " does not have associated bus route.");
        }
        if (!currentDriver.getActiveBusRoute().getType().equals(BusRouteType.PICKUP)) {
            throw new BRPServiceExcpetion(BRPServiceErrorIds.ACTIVE_BUS_ROUTE_PICKUP_REQUIRED,
                "Current drivers #" + currentDriver.getId() + " route #" + currentDriver.getActiveBusRoute().getId() + " is not PICKUP rute.");
        }
    }
}
