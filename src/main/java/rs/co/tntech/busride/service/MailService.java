package rs.co.tntech.busride.service;

import io.github.jhipster.config.JHipsterProperties;
import org.apache.commons.lang3.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import rs.co.tntech.busride.config.ApplicationProperties;
import rs.co.tntech.busride.domain.School;
import rs.co.tntech.busride.domain.SchoolEmail;
import rs.co.tntech.busride.domain.User;
import rs.co.tntech.busride.repository.ParentRepository;

import javax.mail.internet.MimeMessage;
import java.util.Locale;

/**
 * Service for sending emails.
 * <p>
 * We use the @Async annotation to send emails asynchronously.
 */
@Service
public class MailService {

    private static final String PARENT_LOGIN_SCHEMA_BASE = "http://parent.credentials/";

    private static final String PARENT_LOGIN_SCHEMA = "parentLoginSchema";

    private static final String PARENT_DOWNLOAD_LINK_VAR = "parentDownloadLinkVar";

    private static final String PARENT_LOGIN_SCHEMA_IOS = "parentLoginSchemaIOS";

    private static final String PASSWORD = "randomPassword";

    private static final String SCHOOL_MGM_PORTAL_URL = "schoolMgmPortalUrl";

    // TODO: extract this to a property file
    private static final String PARENT_DOWNLOAD_LINK = "http://BusRideParentAppdDownload.com/download";

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    private static final String USER = "user";

    private static final String BASE_URL = "baseUrl";

    private final JHipsterProperties jHipsterProperties;

    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private ParentRepository parentRepository;

    public MailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
                       MessageSource messageSource, SpringTemplateEngine templateEngine) {

        this.jHipsterProperties = jHipsterProperties;
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
    }

    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
            message.setTo(to);
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        } catch (Exception e) {
            log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
        }
    }

    @Async
    public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        context.setVariable(SCHOOL_MGM_PORTAL_URL, applicationProperties.getSchoolMgmPortalUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);

    }

    @Async
    public void sendActivationEmail(User user) {
        log.debug("Sending activation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "activationEmail", "email.activation.title");
    }

    @Async
    public void sendCreationEmail(User user) {
        log.debug("Sending creation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "creationEmail", "email.activation.title");
    }

    @Async
    public void sendPasswordResetMail(User user) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "passwordResetEmail", "email.reset.title");
    }

    @Async
    public void sendParentPasswordResetMail(User user, String randomPassword) {
        log.debug("Sending parent password reset email to '{}'", user.getEmail());
        sendParentPasswordResetEmailFromTemplate(user, randomPassword);
    }

    @Async
    public void sendParentCreationEmail(User user, String randomPassword) {
        log.debug("Sending Parent creation email to '{}'", user.getEmail());
        sendParentEmailFromTemplate(user, randomPassword);
    }

    @Async
    public void sendSnsNotificationFailedEmail(User user, String subject, String message) {
        log.debug("Sending SNS notification failed email to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        context.setVariable(USER, user);
        context.setVariable("subject", subject);
        context.setVariable("message", message);
        String content = templateEngine.process("snsNotificationFailedEmail", context);
        templateEngine.process("creationEmail", context);

        sendEmail(user.getEmail(), subject, content, false, true);
    }

    @Async
    public void sendSchoolEmailToAllSchoolParents(SchoolEmail schoolEmail) {
        log.debug("Sending School email '{}'", schoolEmail);
        School school = schoolEmail.getSchool();
        parentRepository.findAllBySchools(school).stream().
        	forEach(p -> sendSchoolEmailToParent(p.getUser(), schoolEmail.getSubject(), schoolEmail.getContent(), school.getName()));
    }

    @Async
    public void sendSchoolAdminCreationEmail(User user, String randomPassword) {
        log.debug("Sending School Admin creation email to '{}'", user.getEmail());
        sendSchoolAdminEmailFromTemplate(user, randomPassword);
    }

    private void sendSchoolAdminEmailFromTemplate(User user, String randomPassword) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        context.setVariable(USER, user);
        context.setVariable(PASSWORD, randomPassword);
        context.setVariable(SCHOOL_MGM_PORTAL_URL, applicationProperties.getSchoolMgmPortalUrl());
        String content = templateEngine.process("creationSchoolAdminEmail", context);
        String subject = messageSource.getMessage("email.schoolAdmin.creation.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    private void sendSchoolEmailToParent(User user, String subject, String message, String schoolName) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        context.setVariable(USER, user);
        context.setVariable("subject", subject);
        context.setVariable("message", message);
        context.setVariable("schoolName", schoolName);
        String content = templateEngine.process("schoolEmail", context);

        sendEmail(user.getEmail(), subject, content, false, true);
    }

    private void sendParentEmailFromTemplate(User user, String randomPassword) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        context.setVariable(PARENT_DOWNLOAD_LINK_VAR, PARENT_DOWNLOAD_LINK);
        context.setVariable(PARENT_LOGIN_SCHEMA, constructSchema(user, randomPassword));
        context.setVariable(PARENT_LOGIN_SCHEMA_IOS, user.getLogin() + "/" + randomPassword);
        String content = templateEngine.process("creationParentEmail", context);
        String subject = messageSource.getMessage("email.parent.creation.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    private void sendParentPasswordResetEmailFromTemplate(User user, String randomPassword) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        context.setVariable(PARENT_LOGIN_SCHEMA, constructSchema(user, randomPassword));
        context.setVariable(PARENT_LOGIN_SCHEMA_IOS, user.getLogin() + "/" + randomPassword);
        String content = templateEngine.process("parentPasswordResetEmail", context);
        String subject = messageSource.getMessage("email.parent.creation.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    private String constructSchema(User user, String randomPassword) {
        return PARENT_LOGIN_SCHEMA_BASE + user.getLogin() + "/" + randomPassword;
    }
}
