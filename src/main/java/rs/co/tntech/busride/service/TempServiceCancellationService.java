package rs.co.tntech.busride.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.co.tntech.busride.domain.BusRoute;
import rs.co.tntech.busride.domain.Driver;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.Spot;
import rs.co.tntech.busride.domain.Student;
import rs.co.tntech.busride.domain.TempServiceCancellation;
import rs.co.tntech.busride.domain.enumeration.ParentNotificationMsgType;
import rs.co.tntech.busride.domain.enumeration.StudentServiceType;
import rs.co.tntech.busride.repository.BusRouteRepository;
import rs.co.tntech.busride.repository.ParentRepository;
import rs.co.tntech.busride.repository.StudentRepository;
import rs.co.tntech.busride.repository.TempServiceCancellationRepository;
import rs.co.tntech.busride.service.dto.SNSNotificationMessageDTO;
import rs.co.tntech.busride.service.dto.TempServiceCancellationDTO;
import rs.co.tntech.busride.service.dto.TempServiceCancellationMessageDTO;
import rs.co.tntech.busride.service.mapper.TempServiceCancellationMapper;
import rs.co.tntech.busride.service.util.BRPServiceUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing TempServiceCancellation.
 */
@Service
@Transactional
public class TempServiceCancellationService {

    private final Logger log = LoggerFactory.getLogger(TempServiceCancellationService.class);

    @Autowired
    private TempServiceCancellationRepository tempServiceCancellationRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private BusRouteRepository busRouteRepository;
    @Autowired
    private SNSNotificationService snsNotificationService;
    @Autowired
    private BRPServiceUtil serviceUtil;
    @Autowired
    private TempServiceCancellationMapper tempServiceCancellationMapper;
    @Autowired
    private ParentRepository parentRepository;

    public TempServiceCancellationService() {
    }

    /**
     * Save a tempServiceCancellation.
     *
     * @param tempServiceCancellationDTO the entity to save
     * @return the persisted entity
     */
    public TempServiceCancellationDTO save(TempServiceCancellationDTO tempServiceCancellationDTO) {
        log.debug("Request to save TempServiceCancellation : {}", tempServiceCancellationDTO);
        TempServiceCancellation tempServiceCancellation = tempServiceCancellationMapper.toEntity(tempServiceCancellationDTO);
        tempServiceCancellation = tempServiceCancellationRepository.save(tempServiceCancellation);
        return tempServiceCancellationMapper.toDto(tempServiceCancellation);
    }

    /**
     * Get all the tempServiceCancellations.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<TempServiceCancellationDTO> findAll() {
        log.debug("Request to get all TempServiceCancellations");
        return tempServiceCancellationRepository.findAll().stream()
            .map(tempServiceCancellationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one tempServiceCancellation by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public TempServiceCancellationDTO findOne(Long id) {
        log.debug("Request to get TempServiceCancellation : {}", id);
        TempServiceCancellation tempServiceCancellation = tempServiceCancellationRepository.findOne(id);
        return tempServiceCancellationMapper.toDto(tempServiceCancellation);
    }

    /**
     * Delete the  tempServiceCancellation by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TempServiceCancellation : {}", id);
        tempServiceCancellationRepository.delete(id);
    }

    @Transactional
    public Optional<List<TempServiceCancellationMessageDTO>> findAllTempCancellationsForActiveRoute() {
        Driver driver = serviceUtil.getCurrentDriver();

        String driverLogin = driver.getUser() != null ? driver.getUser().getLogin() : null;
        Long driverActiveRoute = driver.getActiveBusRoute() != null ? driver.getActiveBusRoute().getId() : null;
        log.debug("API:GET:IN:temp-service-cancellations/current-driver: get for driver:{}, and activeRouteID: {}", driverLogin, driverActiveRoute);

        List<Long> spotIds = new ArrayList<>();
        List<TempServiceCancellationMessageDTO> resultList;

        BusRoute activeRoute = driver.getActiveBusRoute();
        if (driver.getActiveBusRoute() != null) {

            spotIds = activeRoute.getSpots()
                .stream()
                .map(Spot::getId)
                .collect(Collectors.toList());

        }

        log.debug("API:GET:IN:temp-service-cancellations/current-driver: spots: {}", spotIds.toString());
        log.debug("API:GET:IN:temp-service-cancellations/current-driver: activeRoute.Type: {}", activeRoute.getType());

        List<TempServiceCancellation> allBySpotIdInAndCancellationTypeIn = tempServiceCancellationRepository.findAllBySpotIdInAndCancellationTypeIn(spotIds, Arrays.asList(StudentServiceType.ROUND_TRIP, StudentServiceType.valueOf(activeRoute.getType().name())));
        log.debug("API:GET:IN:temp-service-cancellations/current-driver: cancellations from DB: {}", allBySpotIdInAndCancellationTypeIn);

        resultList = allBySpotIdInAndCancellationTypeIn
            .stream().map(tempServiceCancellation ->
                new TempServiceCancellationMessageDTO(tempServiceCancellation, mapCancellationMsg(tempServiceCancellation))).collect(Collectors.toList());

        log.debug("API:GET:OUT:temp-service-cancellations/current-driver: returning list of cancellations: {}", resultList);

        return Optional.ofNullable(resultList);

    }

    /**
     * Returns all service cancellations for logged in parent
     *
     * @return List of TempServiceCancellationDTO
     */
    @Transactional(readOnly = true)
    public Optional<List<TempServiceCancellationDTO>> findAllTempCancellationsForLoggedInParent() {
        Parent parent = serviceUtil.getCurrentPrimaryParent();
        String parentLogin = parent.getUser() != null ? parent.getUser().getLogin() : null;
        log.debug("API:GET:IN:temp-service-cancellations/current-parent: get for parent:{} all TempServiceCancellations", parentLogin);

        List<TempServiceCancellationDTO> result = tempServiceCancellationRepository.findAllByParentId(parent.getId())
            .stream().map(tempServiceCancellationMapper::toDto).collect(Collectors.toList());

        log.debug("API:GET:OUT:temp-service-cancellations/current-parent: {}", result);

        return Optional.ofNullable(result);
    }

    @Transactional
    public Optional<TempServiceCancellationDTO> saveTempServiceCancellationParentRecord(Long studentId, StudentServiceType cancellationType, String reason) {
        Parent parent = serviceUtil.getCurrentPrimaryParent();
        String parentLogin = parent.getUser() != null ? parent.getUser().getLogin() : null;
        log.debug("API:POST:IN:temp-service-cancellations/current-parent: Save TempServiceCancellations for parent:{}, studentId:{}, cancellationType:{}, reason:{}", parentLogin, studentId, cancellationType, reason);

        List<Student> students = studentRepository.findAllByParent(parent);

        TempServiceCancellation tempServiceCancellation = null;
        if (students != null) {
            Optional<Student> student = students.stream().filter(st -> st.getId().compareTo(studentId) == 0).findFirst();
            if (student.isPresent()) {

                tempServiceCancellation = tempServiceCancellationRepository.findByStudent(student.get());
                if (tempServiceCancellation != null) {
                    tempServiceCancellation.setCancellationType(cancellationType);
                    tempServiceCancellation.setReason(reason);

                } else {
                    tempServiceCancellation = new TempServiceCancellation();
                    tempServiceCancellation.setStudent(student.get());
                    tempServiceCancellation.setSpot(parent.getSpot());
                    tempServiceCancellation.setCancellationType(cancellationType);
                    tempServiceCancellation.setReason(reason);
                }
                tempServiceCancellation = tempServiceCancellationRepository.save(tempServiceCancellation);

                // check if all parent's students, who are going to the same school, are canceled
                int noOfNonCanceledStudents = studentRepository.countNonCanceledStudentsForParentAndSchool(parent.getId(), student.get().getSchool().getId());

                Long targetSpotId = parent.getSpot().getId();
                // if all canceled, get all active route for the student school
                List<BusRoute> activeRoutes = busRouteRepository.findAllActiveRoutesBySchoolId(student.get().getSchool().getId());
                // for each route check if the spot is in it
                BusRoute busRoute = null;
                for (BusRoute busRouteTmp : activeRoutes) {
                    Optional<Spot> spotOp = busRouteTmp.getSpots().stream().filter(sp -> sp.getId().equals(targetSpotId)).findFirst();
                    if (spotOp.isPresent()) {
                        busRoute = busRouteTmp;
                        break;
                    }
                }

                // if active bus route exists for the spot, send push notification to driver
                //TODO test driver push notifications - TempServiceCancellation
                if (busRoute != null) {
                    SNSNotificationMessageDTO notificationMsg = new SNSNotificationMessageDTO(ParentNotificationMsgType.NOTIFICATION);
                    // All students at specific spot have canceled - send spotId information
                    if (noOfNonCanceledStudents == 0) {
                        notificationMsg.setContentKey("notification.student.cancellation");
                        notificationMsg.setContentArgs(new String[]{parent.getAddress(), String.valueOf(targetSpotId)});
                        notificationMsg.setCanceled_stop(targetSpotId);
                        // Only one of All students at specific spot have canceled - send spotId information
                    } else {
                        notificationMsg.setContentKey("notification.student.cancellation.nospotid");
                        notificationMsg.setContentArgs(new String[]{student.get().getName(), parent.getAddress(), String.valueOf(noOfNonCanceledStudents)});
                    }
                    if (busRoute.getDriver().getUser() != null) {
                        notificationMsg.setUser(busRoute.getDriver().getUser().getLogin());
                    }
                    snsNotificationService.publishMessageToDriverApp(busRoute.getDriver(), notificationMsg);
                }

            } else {
                log.debug("API:POST:temp-service-cancellations/current-parent: : Parent does not have student with id: " + studentId);
            }
        } else {
            log.debug("API:POST:temp-service-cancellations/current-parent:  Parent does not have any children assigned to him");
        }

        TempServiceCancellationDTO result = tempServiceCancellationMapper.toDto(tempServiceCancellation);
        log.debug("API:POST:OUT:temp-service-cancellations/current-parent:  {}", result == null ? "null" : result.toString());

        return Optional.ofNullable(result);
    }

    @Transactional
    public Integer deleteTempServiceCancellationParentRecord(Long studentId) {
        Parent parent = serviceUtil.getCurrentPrimaryParent();
        String parentLogin = parent.getUser() != null ? parent.getUser().getLogin() : null;
        log.debug("API:POST:IN:temp-service-cancellations/current-parent: delete TempServiceCancellations for parent:{} and studentId:{}", parentLogin, studentId);
        int deleteResult = tempServiceCancellationRepository.deleteByStudent(studentRepository.getOne(studentId));
        return deleteResult;
    }

    private String mapCancellationMsg(TempServiceCancellation tempServiceCancellation) {

        Parent parent = tempServiceCancellation.getStudent().getParent();

        return "Student " + tempServiceCancellation.getStudent().getName() +
            " who lives at " + parent.getAddress()
            + " shouldn't be picked up." + checkIfSpotShouldBeSkipped(tempServiceCancellation);
    }

    private String checkIfSpotShouldBeSkipped(TempServiceCancellation tempServiceCancellation) {
        long countAllBySpotIdSpot = tempServiceCancellationRepository.countAllBySpot_Id(tempServiceCancellation.getSpot().getId());
        Parent parentBySpot = parentRepository.findOneBySpotId(tempServiceCancellation.getSpot().getId());

        if (countAllBySpotIdSpot == parentBySpot.getNoOfStudents()) {
            return "(Cancelled Spot ID: " + tempServiceCancellation.getSpot().getId() + ")";
        }

        return "";
    }

}
