package rs.co.tntech.busride.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.service.dto.ParentDTO;
import rs.co.tntech.busride.service.dto.ParentExtendedDTO;

/**
 * Mapper for the entity Parent and its VM ParentVM.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, SpotMapper.class})
public interface ParentExtendedMapper extends EntityMapper<ParentExtendedDTO, Parent> {

	@Mapping(source = "user.firstName", target = "firstName")
	@Mapping(source = "user.lastName", target = "lastName")
	@Mapping(source = "user.email", target = "email")
    @Mapping(source = "spot.gpsLongitude", target = "longitude")
    @Mapping(source = "spot.gpsLatitude", target = "latitude")
    @Mapping(target = "linkToPrimaryId", ignore = true)
    @Mapping(target = "spotId", ignore = true)
    @Mapping(target = "userId", ignore = true)
    ParentExtendedDTO toDto(Parent parent);


    @Mapping(source = "spotId", target = "spot")
    @Mapping(target = "user", ignore = true)
    Parent toEntity(ParentDTO parentDTO);

}
