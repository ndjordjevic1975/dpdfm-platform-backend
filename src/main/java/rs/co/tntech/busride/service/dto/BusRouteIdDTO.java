package rs.co.tntech.busride.service.dto;

import javax.validation.constraints.NotNull;

public class BusRouteIdDTO {

    @NotNull
    private Long busRouteId;

    public BusRouteIdDTO() {
    }

    public BusRouteIdDTO(Long busRouteId) {
        this.busRouteId = busRouteId;
    }

    public Long getBusRouteId() {
        return busRouteId;
    }

    public void setBusRouteId(Long busRouteId) {
        this.busRouteId = busRouteId;
    }

    @Override
    public String toString() {
        return "BusRouteIdDTO{" +
            "busRouteId=" + busRouteId +
            '}';
    }
}
