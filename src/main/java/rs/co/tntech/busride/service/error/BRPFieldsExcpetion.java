/**
 *
 */
package rs.co.tntech.busride.service.error;

import java.util.Collections;
import java.util.List;

/**
 * This exception is thrown in service layer when an input value is invalid. It contains list of invalid input fields errors.
 *
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class BRPFieldsExcpetion extends BRPBaseServiceException {

	private static final long serialVersionUID = 1L;

	private final List<BRPFieldException> fields;

	/**
	 * @param fields
	 * @param message
	 */
	public BRPFieldsExcpetion(List<BRPFieldException> fields, String message) {
		super(message);
		this.fields = Collections.unmodifiableList(fields);
	}

	/**
	 * @return the fields
	 */
	public List<BRPFieldException> getFields() {
		return fields;
	}

}
