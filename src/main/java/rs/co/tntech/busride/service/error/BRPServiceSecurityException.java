/**
 *
 */
package rs.co.tntech.busride.service.error;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * This exception is thrown in service layer when user does not appropriate role to execute target action
 *
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class BRPServiceSecurityException extends BRPBaseServiceException {

	private static final long serialVersionUID = 1L;

	private final List<String> requiredRoles;

	/**
	 * @param requiredRole
	 * @param message
	 */
	public BRPServiceSecurityException(String requiredRole, String message) {
		super(message);
		this.requiredRoles = Collections.unmodifiableList(Arrays.asList(requiredRole));
	}

	/**
	 * @param requiredRoles
	 * @param message
	 */
	public BRPServiceSecurityException(List<String> requiredRoles, String message) {
		super(message);
		this.requiredRoles = Collections.unmodifiableList(requiredRoles);
	}

	/**
	 * @return the requiredRoles
	 */
	public List<String> getRequiredRoles() {
		return requiredRoles;
	}

}
