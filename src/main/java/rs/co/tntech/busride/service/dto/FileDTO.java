/**
 * 
 */
package rs.co.tntech.busride.service.dto;

/**
 * A DTO representing a file to download, with his content and content type.
 * 
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class FileDTO {

	private byte[] content;
	private String contentType;

	/**
	 * Default constructor
	 */
	public FileDTO() {
	}

	/**
	 * @param content
	 * @param contentType
	 */
	public FileDTO(byte[] content, String contentType) {
		super();
		this.content = content;
		this.contentType = contentType;
	}

	/**
	 * @return the content
	 */
	public byte[] getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(byte[] content) {
		this.content = content;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType
	 *            the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

}
