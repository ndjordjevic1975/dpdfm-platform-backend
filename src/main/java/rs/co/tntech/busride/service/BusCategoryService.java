package rs.co.tntech.busride.service;

import org.springframework.beans.factory.annotation.Autowired;
import rs.co.tntech.busride.domain.BusCategory;
import rs.co.tntech.busride.domain.SchoolAdmin;
import rs.co.tntech.busride.repository.BusCategoryRepository;
import rs.co.tntech.busride.service.dto.BusCategoryDTO;
import rs.co.tntech.busride.service.mapper.BusCategoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.co.tntech.busride.service.util.BRPServiceUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing BusCategory.
 */
@Service
@Transactional
public class BusCategoryService {

    private final Logger log = LoggerFactory.getLogger(BusCategoryService.class);

    private final BusCategoryRepository busCategoryRepository;

    @Autowired
    private BusCategoryMapper busCategoryMapper;

    @Autowired
    private BRPServiceUtil serviceUtil;

    public BusCategoryService(BusCategoryRepository busCategoryRepository) {
        this.busCategoryRepository = busCategoryRepository;
    }

    /**
     * Save a busCategory.
     *
     * @param busCategoryDTO the entity to save
     * @return the persisted entity
     */
    public BusCategoryDTO save(BusCategoryDTO busCategoryDTO) {
        log.debug("Request to save BusCategory : {}", busCategoryDTO);
        BusCategory busCategory = busCategoryMapper.toEntity(busCategoryDTO);
        busCategory = busCategoryRepository.save(busCategory);
        busCategoryRepository.flush();
        return busCategoryMapper.toDto(busCategory);
    }

    /**
     *  Get all the busCategories.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<BusCategoryDTO> findAll() {
        log.debug("Request to get all BusCategories");
        return busCategoryRepository.findAll().stream()
            .map(busCategoryMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Transactional(readOnly = true)
    public List<BusCategoryDTO> findAllBusCategoriesBySchoolAdminUserId() {
        SchoolAdmin currentSchoolAdmin = serviceUtil.getCurrentSchoolAdmin();
        LinkedList<BusCategoryDTO> busCategoriesDTO = busCategoryRepository.findAllBySchool(currentSchoolAdmin.getSchool())
            .stream()
            .map(busCategoryMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));

        log.debug("API:GET:OUT:/bus-categories/current-school-admin/: Bus categories for school {} are : {}",currentSchoolAdmin.getSchool().getName(), busCategoriesDTO);

        return busCategoriesDTO;
    }

    public BusCategoryDTO saveBusCategoriesBySchoolAdmin(BusCategoryDTO busCategoryDTO) {

        log.debug("API:POST:IN:/bus-categories/current-school-admin/: Bus Capacity: {}, Number of buses: {}", busCategoryDTO.getCapacity(), busCategoryDTO.getNoOfBuses());

        SchoolAdmin currentSchoolAdmin = serviceUtil.getCurrentSchoolAdmin();

        busCategoryDTO.setSchoolId(currentSchoolAdmin.getSchool().getId());
        busCategoryDTO.setSchoolName(currentSchoolAdmin.getSchool().getName());

        BusCategoryDTO result = save(busCategoryDTO);

        return result;
    }

    /**
     *  Get one busCategory by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public BusCategoryDTO findOne(Long id) {
        log.debug("Request to get BusCategory : {}", id);
        BusCategory busCategory = busCategoryRepository.findOne(id);
        return busCategoryMapper.toDto(busCategory);
    }

    /**
     *  Delete the  busCategory by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete BusCategory : {}", id);
        busCategoryRepository.delete(id);
    }
}
