/**
 * 
 */
package rs.co.tntech.busride.service.error;

/**
 * This exception is thrown in service layer when an error, which is not related to input field, is detected
 * 
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class BRPServiceExcpetion extends BRPBaseServiceException {

	private static final long serialVersionUID = 1L;

	private final String errorId;

	/**
	 * @param errorId
	 * @param message
	 */
	public BRPServiceExcpetion(String errorId, String message) {
		super(message);
		this.errorId = errorId;
	}

	/**
	 * @return the errorId
	 */
	public String getErrorId() {
		return errorId;
	}

}
