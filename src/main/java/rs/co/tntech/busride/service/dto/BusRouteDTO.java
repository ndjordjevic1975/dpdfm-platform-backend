package rs.co.tntech.busride.service.dto;


import rs.co.tntech.busride.domain.enumeration.BusRouteType;

import javax.persistence.Lob;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the BusRoute entity.
 */
public class BusRouteDTO implements Serializable {

    private Long id;

    @Lob
    private String details;

    private BusRouteType type;

    private Integer capacity;

    private Instant createdOn;

    private Instant inactiveOn;

    private Long schoolId;

    private Long driverId;

    private Set<SpotDTO> spots = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public BusRouteType getType() {
        return type;
    }

    public void setType(BusRouteType type) {
        this.type = type;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Instant getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public Instant getInactiveOn() {
        return inactiveOn;
    }

    public void setInactiveOn(Instant inactiveOn) {
        this.inactiveOn = inactiveOn;
    }

    public Long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public Set<SpotDTO> getSpots() {
        return spots;
    }

    public void setSpots(Set<SpotDTO> spots) {
        this.spots = spots;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BusRouteDTO busRouteDTO = (BusRouteDTO) o;
        if(busRouteDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), busRouteDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BusRouteDTO{" +
            "id=" + getId() +
            ", details='" + getDetails() + "'" +
            ", type='" + getType() + "'" +
            ", capacity='" + getCapacity() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", inactiveOn='" + getInactiveOn() + "'" +
            "}";
    }
}
