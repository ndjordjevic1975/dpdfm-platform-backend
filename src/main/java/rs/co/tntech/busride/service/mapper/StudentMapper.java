package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.StudentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Student and its DTO StudentDTO.
 */
@Mapper(componentModel = "spring", uses = {SchoolMapper.class, ParentMapper.class, })
public interface StudentMapper extends EntityMapper <StudentDTO, Student> {

    @Mapping(source = "school.id", target = "schoolId")

    @Mapping(source = "parent.id", target = "parentId")
    StudentDTO toDto(Student student); 

    @Mapping(source = "schoolId", target = "school")

    @Mapping(source = "parentId", target = "parent")
    Student toEntity(StudentDTO studentDTO); 
    default Student fromId(Long id) {
        if (id == null) {
            return null;
        }
        Student student = new Student();
        student.setId(id);
        return student;
    }
}
