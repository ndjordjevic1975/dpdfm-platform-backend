/**
 *
 */
package rs.co.tntech.busride.service.error;

/**
 * This exception is thrown in service layer when an input value is invalid
 *
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class BRPFieldException extends BRPBaseServiceException {

	private static final long serialVersionUID = 1L;

	private final String objectName;
	private final String field;
	private final String errorId;

	/**
	 * @param objectName
	 * @param field
	 * @param errorId
	 */
	public BRPFieldException(String objectName, String field, String errorId, String message) {
		super(message);
		this.objectName = objectName;
		this.field = field;
		this.errorId = errorId;
	}

	/**
	 * @return the objectName
	 */
	public String getObjectName() {
		return objectName;
	}

	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}

	/**
	 * @return the errorId
	 */
	public String getErrorId() {
		return errorId;
	}

}
