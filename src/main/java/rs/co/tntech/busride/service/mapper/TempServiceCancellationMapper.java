package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.TempServiceCancellationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity TempServiceCancellation and its DTO TempServiceCancellationDTO.
 */
@Mapper(componentModel = "spring", uses = {StudentMapper.class, SpotMapper.class, })
public interface TempServiceCancellationMapper extends EntityMapper <TempServiceCancellationDTO, TempServiceCancellation> {

    @Mapping(source = "student.id", target = "studentId")
    @Mapping(source = "spot.id", target = "spotId")
    @Mapping(source = "reason", target = "reason")
    @Mapping(source = "cancellationType", target = "cancellationType")
    TempServiceCancellationDTO toDto(TempServiceCancellation tempServiceCancellation);

    @Mapping(source = "studentId", target = "student")

    @Mapping(source = "spotId", target = "spot")
    TempServiceCancellation toEntity(TempServiceCancellationDTO tempServiceCancellationDTO);
    default TempServiceCancellation fromId(Long id) {
        if (id == null) {
            return null;
        }
        TempServiceCancellation tempServiceCancellation = new TempServiceCancellation();
        tempServiceCancellation.setId(id);
        return tempServiceCancellation;
    }
}
