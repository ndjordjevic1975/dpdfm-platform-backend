package rs.co.tntech.busride.service;

import rs.co.tntech.busride.domain.SchoolAdmin;
import rs.co.tntech.busride.repository.SchoolAdminRepository;
import rs.co.tntech.busride.service.dto.SchoolAdminDTO;
import rs.co.tntech.busride.service.mapper.SchoolAdminMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing SchoolAdmin.
 */
@Service
@Transactional
public class SchoolAdminService {

    private final Logger log = LoggerFactory.getLogger(SchoolAdminService.class);

    private final SchoolAdminRepository schoolAdminRepository;

    private final SchoolAdminMapper schoolAdminMapper;
    public SchoolAdminService(SchoolAdminRepository schoolAdminRepository, SchoolAdminMapper schoolAdminMapper) {
        this.schoolAdminRepository = schoolAdminRepository;
        this.schoolAdminMapper = schoolAdminMapper;
    }

    /**
     * Save a schoolAdmin.
     *
     * @param schoolAdminDTO the entity to save
     * @return the persisted entity
     */
    public SchoolAdminDTO save(SchoolAdminDTO schoolAdminDTO) {
        log.debug("Request to save SchoolAdmin : {}", schoolAdminDTO);
        SchoolAdmin schoolAdmin = schoolAdminMapper.toEntity(schoolAdminDTO);
        schoolAdmin = schoolAdminRepository.save(schoolAdmin);
        return schoolAdminMapper.toDto(schoolAdmin);
    }

    /**
     *  Get all the schoolAdmins.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SchoolAdminDTO> findAll() {
        log.debug("Request to get all SchoolAdmins");
        return schoolAdminRepository.findAll().stream()
            .map(schoolAdminMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one schoolAdmin by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SchoolAdminDTO findOne(Long id) {
        log.debug("Request to get SchoolAdmin : {}", id);
        SchoolAdmin schoolAdmin = schoolAdminRepository.findOne(id);
        return schoolAdminMapper.toDto(schoolAdmin);
    }

    /**
     *  Delete the  schoolAdmin by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SchoolAdmin : {}", id);
        schoolAdminRepository.delete(id);
    }
}
