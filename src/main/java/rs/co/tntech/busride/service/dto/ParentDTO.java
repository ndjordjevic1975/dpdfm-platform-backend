package rs.co.tntech.busride.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import rs.co.tntech.busride.domain.enumeration.DeviceType;

/**
 * A DTO for the Parent entity.
 */
public class ParentDTO implements Serializable {

    private Long id;

    private String address;

    private String houseNumber;

    private String city;

    private String zipCode;

    private String state;

    private String country;

    private String phone;

    private String landPhone;

    private Integer noOfStudents;

    private String deviceIdentifier;

    private DeviceType deviceType;

    private String gcmSessionId;

    private String endpointARN;

    private String nationalId;

    private Long userId;

    private Long spotId;

    private Long linkToPrimaryId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLandPhone() {
        return landPhone;
    }

    public void setLandPhone(String landPhone) {
        this.landPhone = landPhone;
    }

    public Integer getNoOfStudents() {
        return noOfStudents;
    }

    public void setNoOfStudents(Integer noOfStudents) {
        this.noOfStudents = noOfStudents;
    }

    public String getDeviceIdentifier() {
        return deviceIdentifier;
    }

    public void setDeviceIdentifier(String deviceIdentifier) {
        this.deviceIdentifier = deviceIdentifier;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public String getGcmSessionId() {
        return gcmSessionId;
    }

    public void setGcmSessionId(String gcmSessionId) {
        this.gcmSessionId = gcmSessionId;
    }

    public String getEndpointARN() {
        return endpointARN;
    }

    public void setEndpointARN(String endpointARN) {
        this.endpointARN = endpointARN;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getSpotId() {
        return spotId;
    }

    public void setSpotId(Long spotId) {
        this.spotId = spotId;
    }

    public Long getLinkToPrimaryId() {
        return linkToPrimaryId;
    }

    public void setLinkToPrimaryId(Long parentId) {
        this.linkToPrimaryId = parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ParentDTO parentDTO = (ParentDTO) o;
        if(parentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), parentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ParentDTO{" +
            "id=" + getId() +
            ", address='" + getAddress() + "'" +
            ", houseNumber='" + getHouseNumber() + "'" +
            ", city='" + getCity() + "'" +
            ", zipCode='" + getZipCode() + "'" +
            ", state='" + getState() + "'" +
            ", country='" + getCountry() + "'" +
            ", phone='" + getPhone() + "'" +
            ", landPhone='" + getLandPhone() + "'" +
            ", noOfStudents='" + getNoOfStudents() + "'" +
            ", deviceIdentifier='" + getDeviceIdentifier() + "'" +
            ", deviceType='" + getDeviceType() + "'" +
            ", gcmSessionId='" + getGcmSessionId() + "'" +
            ", endpointARN='" + getEndpointARN() + "'" +
            ", nationalId='" + getNationalId() + "'" +
            "}";
    }
}
