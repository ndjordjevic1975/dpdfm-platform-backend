package rs.co.tntech.busride.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.repository.ParentRepository;
import rs.co.tntech.busride.service.error.BRPServiceExcpetion;
import rs.co.tntech.busride.service.util.BRPServiceUtil;

/**
 * Service Implementation for managing LogOutResource.
 */
@Service
@Transactional
public class LogOutService {

    private final Logger log = LoggerFactory.getLogger(LogOutService.class);

    @Autowired
    private ParentRepository parentRepository;

    @Autowired
    private BRPServiceUtil serviceUtil;

    public boolean logOutCurrentParent() throws BRPServiceExcpetion {
        Parent currentParent = serviceUtil.getCurrentParent();
        if (currentParent != null) {
            log.debug("API:PUT:IN:logout/current-parent: {}", currentParent.getUser().getLogin());
            if(currentParent.getEndpointARN()!=null) {
                serviceUtil.deleteParentEndpoint(currentParent.getEndpointARN(),currentParent.getDeviceType());
            }else {
                log.debug("API:PUT:IN:logout/current-parent: {} endpoint not set", currentParent.getUser().getLogin());
            }

            currentParent.setEndpointARN(null);
            currentParent.setGcmSessionId(null);
            currentParent.setDeviceIdentifier(null);

            parentRepository.save(currentParent);

            return true;
        }

        return false;
    }
}
