package rs.co.tntech.busride.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.co.tntech.busride.domain.Driver;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.School;
import rs.co.tntech.busride.repository.SchoolRepository;
import rs.co.tntech.busride.service.dto.SchoolDTO;
import rs.co.tntech.busride.service.mapper.SchoolMapper;
import rs.co.tntech.busride.service.util.BRPServiceUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing School.
 */
@Service
@Transactional
public class SchoolService {

    private final Logger log = LoggerFactory.getLogger(SchoolService.class);

    private final SchoolRepository schoolRepository;

    private final SchoolMapper schoolMapper;

    private final BRPServiceUtil serviceUtil;

    public SchoolService(SchoolRepository schoolRepository, SchoolMapper schoolMapper, BRPServiceUtil serviceUtil) {
        this.schoolRepository = schoolRepository;
        this.schoolMapper = schoolMapper;
        this.serviceUtil = serviceUtil;
    }

    /**
     * Save a school.
     *
     * @param schoolDTO the entity to save
     * @return the persisted entity
     */
    public SchoolDTO save(SchoolDTO schoolDTO) {
        log.debug("Request to save School : {}", schoolDTO);
        School school = schoolMapper.toEntity(schoolDTO);
        school = schoolRepository.save(school);
        return schoolMapper.toDto(school);
    }

    /**
     * Get all the schools.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SchoolDTO> findAll() {
        log.debug("Request to get all Schools");
        return schoolRepository.findAllWithEagerRelationships().stream()
            .map(schoolMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Gets all schools the primary parent (of authenticated parent) is associated with.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SchoolDTO> findAllByCurrentPrimaryParent() {
        Parent primaryParent = serviceUtil.getCurrentPrimaryParent();
        log.debug("API:GET:IN:schools/current-parent: Parent: {}", primaryParent.getUser().getLogin());
        LinkedList<SchoolDTO> result;
        result = schoolRepository.findAllByParents(primaryParent).stream()
            .map(schoolMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));

        log.debug("API:GET:OUT:schools/current-parent: Parent: {}, Schools: {}", primaryParent, result);
        return result;
    }

    /**
     * Gets a school's data for the current driver.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public SchoolDTO findByCurrentDriver() {
        SchoolDTO schoolDTO = null;
        Driver driver = serviceUtil.getCurrentDriver();
        log.debug("API:GET:IN:schools/current-driver: Driver: {}", driver.getUser().getLogin());
        schoolDTO = schoolMapper.toDto(driver.getSchool());

        log.debug("API:GET:OUT:schools/current-driver: Driver: {}, School: {}", driver, schoolDTO.toString());

        return schoolDTO;
    }

    /**
     * Get one school by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SchoolDTO findOne(Long id) {
        log.debug("Request to get School : {}", id);
        School school = schoolRepository.findOneWithEagerRelationships(id);
        return schoolMapper.toDto(school);
    }

    /**
     * Delete the  school by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete School : {}", id);
        schoolRepository.delete(id);
    }
}
