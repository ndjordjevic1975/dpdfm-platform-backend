package rs.co.tntech.busride.service.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A extended DTO for the Parent entity.
 */
public class ParentExtendedDTO extends ParentDTO {

	private String firstName;
	private String lastName;
	private String email;
    private Double latitude;
    private Double longitude;
    private String password;

    public ParentExtendedDTO() {
    }

    public ParentExtendedDTO(Double latitude, Double longitude) {
        super();
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    @JsonIgnore
    public Long getSpotId() {
        return super.getSpotId();
    }

    @Override
    @JsonIgnore
    public Long getUserId() {
        return super.getUserId();
    }

    @Override
    @JsonIgnore
    public Long getLinkToPrimaryId() {
        return super.getLinkToPrimaryId();
    }

    @Override
    @JsonIgnore
    public String getEndpointARN() {
        return super.getEndpointARN();
    }

    /**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "{" +
            "\"id\": \"" + getId() + "\"," +
            "\"firstName\": \"" + getFirstName() + "\"," +
            "\"lastName\": \"" + getLastName() + "\"," +
            "\"email\": \"" + getEmail() + "\"," +
            "\"address\": \"" + getAddress() + "\"," +
            "\"houseNumber\": \"" + getHouseNumber() + "\"," +
            "\"city\": \"" + getCity() + "\"," +
            "\"zipCode\" : \"" + getZipCode() + "\"," +
            "\"state\": \"" + getState() + "\"," +
            "\"country\": \"" + getCountry() + "\"," +
            "\"phone\": \"" + getPhone() + "\"," +
            "\"landPhone\": \"" + getLandPhone() + "\"," +
            "\"noOfStudents\": \"" + getNoOfStudents() + "\"," +
            "\"deviceIdentifier\": \"" + getDeviceIdentifier() + "\"," +
            "\"deviceType\": \"" + getDeviceType() + "\"," +
            "\"gcmSessionId\": \"" + getGcmSessionId() + "\"," +
            "\"endpointARN\": \"" + getEndpointARN() + "\"," +
            "\"latitude\": \"" + getLatitude() + "\"," +
            "\"longitude\": \"" + getLongitude() + "\"" +
            "\"nationalId\": \"" + getNationalId() + "\"" +
            "}";
    }
}
