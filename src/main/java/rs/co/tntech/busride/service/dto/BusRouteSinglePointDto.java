package rs.co.tntech.busride.service.dto;

import java.io.Serializable;

public class BusRouteSinglePointDto implements Serializable{

    private double lat;
    private double lng;

    public BusRouteSinglePointDto() {
    }

    public BusRouteSinglePointDto(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "BusRouteSinglePointDto{" +
            "lat=" + lat +
            ", lng=" + lng +
            '}';
    }
}
