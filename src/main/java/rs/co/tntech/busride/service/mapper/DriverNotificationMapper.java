package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.DriverNotificationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity DriverNotification and its DTO DriverNotificationDTO.
 */
@Mapper(componentModel = "spring", uses = {DriverMapper.class, SpotMapper.class, })
public interface DriverNotificationMapper extends EntityMapper <DriverNotificationDTO, DriverNotification> {

    @Mapping(source = "driver.id", target = "driverId")

    @Mapping(source = "spot.id", target = "spotId")
    DriverNotificationDTO toDto(DriverNotification driverNotification); 

    @Mapping(source = "driverId", target = "driver")

    @Mapping(source = "spotId", target = "spot")
    DriverNotification toEntity(DriverNotificationDTO driverNotificationDTO); 
    default DriverNotification fromId(Long id) {
        if (id == null) {
            return null;
        }
        DriverNotification driverNotification = new DriverNotification();
        driverNotification.setId(id);
        return driverNotification;
    }
}
