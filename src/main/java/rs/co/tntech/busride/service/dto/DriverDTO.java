package rs.co.tntech.busride.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Driver entity.
 */
public class DriverDTO implements Serializable {

    private Long id;

    private Integer capacity;

    private String deviceIdentifier;

    private String gcmSessionId;

    private String endpointARN;

    private String tabletNumber;

    private Long userId;

    private Long activeBusRouteId;

    private Long schoolId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getDeviceIdentifier() {
        return deviceIdentifier;
    }

    public void setDeviceIdentifier(String deviceIdentifier) {
        this.deviceIdentifier = deviceIdentifier;
    }

    public String getGcmSessionId() {
        return gcmSessionId;
    }

    public void setGcmSessionId(String gcmSessionId) {
        this.gcmSessionId = gcmSessionId;
    }

    public String getEndpointARN() {
        return endpointARN;
    }

    public void setEndpointARN(String endpointARN) {
        this.endpointARN = endpointARN;
    }

    public String getTabletNumber() {
        return tabletNumber;
    }

    public void setTabletNumber(String tabletNumber) {
        this.tabletNumber = tabletNumber;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getActiveBusRouteId() {
        return activeBusRouteId;
    }

    public void setActiveBusRouteId(Long busRouteId) {
        this.activeBusRouteId = busRouteId;
    }

    public Long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DriverDTO driverDTO = (DriverDTO) o;
        if(driverDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), driverDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DriverDTO{" +
            "id=" + getId() +
            ", capacity='" + getCapacity() + "'" +
            ", deviceIdentifier='" + getDeviceIdentifier() + "'" +
            ", gcmSessionId='" + getGcmSessionId() + "'" +
            ", endpointARN='" + getEndpointARN() + "'" +
            ", tabletNumber='" + getTabletNumber() + "'" +
            "}";
    }
}
