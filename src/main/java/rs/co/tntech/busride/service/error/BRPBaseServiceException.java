/**
 * 
 */
package rs.co.tntech.busride.service.error;

/**
 * Base BRP service exception class. This exception is extended by all other specific exception classes.
 * 
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public abstract class BRPBaseServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public BRPBaseServiceException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public BRPBaseServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public BRPBaseServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public BRPBaseServiceException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public BRPBaseServiceException(Throwable cause) {
		super(cause);
	}

}
