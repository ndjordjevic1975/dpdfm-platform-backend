package rs.co.tntech.busride.service;

import rs.co.tntech.busride.domain.SchoolAdmin;
import rs.co.tntech.busride.domain.SchoolEmail;
import rs.co.tntech.busride.repository.SchoolEmailRepository;
import rs.co.tntech.busride.security.AuthoritiesConstants;
import rs.co.tntech.busride.security.SecurityUtils;
import rs.co.tntech.busride.service.dto.PickupReportDTO;
import rs.co.tntech.busride.service.dto.SchoolEmailDTO;
import rs.co.tntech.busride.service.error.*;
import rs.co.tntech.busride.service.error.BRPServiceSecurityException;
import rs.co.tntech.busride.service.mapper.SchoolEmailMapper;
import rs.co.tntech.busride.service.util.BRPServiceUtil;
import rs.co.tntech.busride.service.util.BRPServiceValidationUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing SchoolEmail.
 */
@Service
@Transactional
public class SchoolEmailService {

    private final Logger log = LoggerFactory.getLogger(SchoolEmailService.class);

    @Autowired
    private SchoolEmailRepository schoolEmailRepository;
    @Autowired
    private SchoolEmailMapper schoolEmailMapper;
    @Autowired
    private MailService mailService;
    @Autowired
    private BRPServiceUtil serviceUtil;

    /**
     * Save a schoolEmail.
     *
     * @param schoolEmailDTO the entity to save
     * @return the persisted entity
     */
    public SchoolEmailDTO save(SchoolEmailDTO schoolEmailDTO) {
        log.debug("Request to save SchoolEmail : {}", schoolEmailDTO);
        SchoolEmail schoolEmail = schoolEmailMapper.toEntity(schoolEmailDTO);
        schoolEmail = schoolEmailRepository.save(schoolEmail);
        return schoolEmailMapper.toDto(schoolEmail);
    }

    /**
     *  Get all the schoolEmails.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<SchoolEmailDTO> findAll() {
        log.debug("Request to get all SchoolEmails");
        return schoolEmailRepository.findAll().stream()
            .map(schoolEmailMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one schoolEmail by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SchoolEmailDTO findOne(Long id) {
        log.debug("Request to get SchoolEmail : {}", id);
        SchoolEmail schoolEmail = schoolEmailRepository.findOne(id);
        return schoolEmailMapper.toDto(schoolEmail);
    }

    /**
     *  Delete the  schoolEmail by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SchoolEmail : {}", id);
        schoolEmailRepository.delete(id);
    }

    /**
     * Creates new email by current school administrator and sends it to all school parents.
     *
     * @param schoolEmailDTO the entity to save
     * @return the persisted entity
     * @throws BRPBaseServiceException
     */
    public SchoolEmailDTO saveCurrentSchoolAdminAndSendToAllSchoolParents(SchoolEmailDTO schoolEmailDTO) throws BRPBaseServiceException {
        log.debug("API:POST:school-emails/current-school-admin: SchoolEmailDTO: {}", schoolEmailDTO);
        validateSaveCurrentSchoolAdmin(schoolEmailDTO);

        SchoolEmail schoolEmail = schoolEmailMapper.toEntity(schoolEmailDTO);
        schoolEmail.setTimestamp(Instant.now());
        schoolEmail.setSchool(serviceUtil.getCurrentSchoolAdmin().getSchool());
        schoolEmail = schoolEmailRepository.save(schoolEmail);

		mailService.sendSchoolEmailToAllSchoolParents(schoolEmail);

        SchoolEmailDTO savedDTO = schoolEmailMapper.toDto(schoolEmail);
        log.debug("API:PUT:OUT:school-emails/current-school-admin: SchoolEmailDTO: {}", savedDTO);
		return savedDTO;
    }

    /**
     *  Delete the  schoolEmail by id for current school admin..
     *
     *  @param id the id of the entity
     */
	public void deleteForCurrentSchoolAdmin(Long id) throws BRPBaseServiceException {
		log.debug("API:DELETE:school-emails/current-school-admin/{}", id);
		// validate required role
		if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.SCHOOL_ADMIN)) {
			String msg = String.format("User does not have any of required roles: '%s'", AuthoritiesConstants.SCHOOL_ADMIN);
			throw new BRPServiceSecurityException(AuthoritiesConstants.SCHOOL_ADMIN, msg);
		}
		// validate current school admin
		SchoolAdmin schoolAdmin = serviceUtil.getCurrentSchoolAdmin();
		if (schoolAdmin == null) {
			throw new BRPServiceSystemException("There is no current school admin user");
		}
		SchoolEmail schoolEmail = schoolEmailRepository.findOne(id);
		if (schoolEmail == null) {
			throw new BRPServiceExcpetion(BRPServiceErrorIds.SCHOOL_EMAIL_DB_MISSING, "There is no school email for id #" + id);
		}
		if (!schoolEmail.getSchool().getId().equals(schoolAdmin.getSchool().getId())) {
			String msg = String.format("Given school email #%d belongs to school #%d which is not related to current school admin #%d", id,
					schoolEmail.getSchool().getId(), schoolAdmin.getId());
			throw new BRPServiceExcpetion(BRPServiceErrorIds.SCHOOL_EMAIL_NOTRELATED_SCHOOL_ADMIN, msg);
		}

		schoolEmailRepository.delete(id);
		log.debug("API:DELETE:OUT:school-emails/current-school-admin/{}", id);
	}

    /**
	 * Retrieves list of all school emails for current school admin.
	 *
	 * @return
	 * @throws BRPBaseServiceException
	 */
	public void deleteAllForCurrentSchoolAdmin() throws BRPBaseServiceException {
		log.debug("API:DELETE:school-emails/current-school-admin");
		// validate required role
		if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.SCHOOL_ADMIN)) {
			String msg = String.format("User does not have any of required roles: '%s'", AuthoritiesConstants.SCHOOL_ADMIN);
			throw new BRPServiceSecurityException(AuthoritiesConstants.SCHOOL_ADMIN, msg);
		}
		// validate current school admin
		SchoolAdmin schoolAdmin = serviceUtil.getCurrentSchoolAdmin();
		if (schoolAdmin == null) {
			throw new BRPServiceSystemException("There is no current school admin user");
		}
		schoolEmailRepository.deleteAllBySchoolId(schoolAdmin.getSchool().getId());
		log.debug("API:DELETE:OUT:school-emails/current-school-admin");
	}

	/**
	 * Retrieves list of all school emails for current school admin.
	 *
	 * @return
	 * @throws BRPBaseServiceException
	 */
	public List<SchoolEmailDTO> getAllForCurrentSchoolAdmin() throws BRPBaseServiceException {
		log.debug("API:GET:school-emails/current-school-admin: ");
		// validate required role
		if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.SCHOOL_ADMIN)) {
			String msg = String.format("User does not have any of required roles: '%s'", AuthoritiesConstants.SCHOOL_ADMIN);
			throw new BRPServiceSecurityException(AuthoritiesConstants.SCHOOL_ADMIN, msg);
		}
		// validate current school admin
		SchoolAdmin schoolAdmin = serviceUtil.getCurrentSchoolAdmin();
		if (schoolAdmin == null) {
			throw new BRPServiceSystemException("There is no current school admin user");
		}
		List<SchoolEmailDTO> result = schoolEmailRepository.findAllBySchoolId(schoolAdmin.getSchool().getId())
				.stream()
				.map(schoolEmailMapper::toDto)
				.collect(Collectors.toCollection(LinkedList::new));
		log.debug("API:GET:OUT:school-emails/current-school-admin: SchoolEmailDTOs: []", result);
		return result;
	}

    /**
     * This method validates input data for method {@link #saveForCurrentDriver(PickupReportDTO)}
     *
     * @param pickupReportDTO
     * @throws BRPBaseServiceException
     */
    private void validateSaveCurrentSchoolAdmin(SchoolEmailDTO schoolEmailDTO) throws BRPBaseServiceException {
        // validate required role
        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.SCHOOL_ADMIN)) {
            String msg = String.format("User does not have any of required roles: '%s'", AuthoritiesConstants.SCHOOL_ADMIN);
            throw new BRPServiceSecurityException(AuthoritiesConstants.SCHOOL_ADMIN, msg);
        }
        // validate input data
        List<BRPFieldException> fieldErrors = new ArrayList<>();
        // validate not allowed fields
        BRPServiceValidationUtils.validateNotAllowedField(fieldErrors, "SchoolEmailDTO", "id", schoolEmailDTO.getId());
        BRPServiceValidationUtils.validateNotAllowedField(fieldErrors, "SchoolEmailDTO", "schoolId", schoolEmailDTO.getSchoolId());
        BRPServiceValidationUtils.validateNotAllowedField(fieldErrors, "SchoolEmailDTO", "timestamp", schoolEmailDTO.getTimestamp());
        // validate mandatory fields
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "SchoolEmailDTO", "subject", schoolEmailDTO.getSubject());
        BRPServiceValidationUtils.validateMandatoryField(fieldErrors, "SchoolEmailDTO", "subject", schoolEmailDTO.getContent());
        if (!fieldErrors.isEmpty()) {
            throw new BRPFieldsExcpetion(fieldErrors, "Some of input fields are not valid");
        }
        // validate current school admin
        SchoolAdmin schoolAdmin = serviceUtil.getCurrentSchoolAdmin();
        if (schoolAdmin == null) {
            throw new BRPServiceSystemException("There is no current school admin user");
        }
    }
}
