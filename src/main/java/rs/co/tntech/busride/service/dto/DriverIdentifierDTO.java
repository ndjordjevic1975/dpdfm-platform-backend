package rs.co.tntech.busride.service.dto;

import javax.validation.constraints.NotNull;

/**
 * DTO for Information needed to retrieve and update Driver and Driver User.
 */
public class DriverIdentifierDTO {

    @NotNull
    private String deviceIdentifier;

    @NotNull
    private String tabletNumber;

    public DriverIdentifierDTO() {}

    public DriverIdentifierDTO(String deviceIdentifier, String tabletNumber) {
        this.deviceIdentifier = deviceIdentifier;
        this.tabletNumber = tabletNumber;
    }

    public String getDeviceIdentifier() {
        return deviceIdentifier;
    }

    public void setDeviceIdentifier(String deviceIdentifier) {
        this.deviceIdentifier = deviceIdentifier;
    }

    public String getTabletNumber() {
        return tabletNumber;
    }

    public void setTabletNumber(String tabletNumber) {
        this.tabletNumber = tabletNumber;
    }

    @Override
    public String toString() {
        return "DriverIdentifierDTO{" +
            ", deviceIdentifier='" + deviceIdentifier + '\'' +
            ", tabletNumber='" + tabletNumber + '\'' +
            '}';
    }
}
