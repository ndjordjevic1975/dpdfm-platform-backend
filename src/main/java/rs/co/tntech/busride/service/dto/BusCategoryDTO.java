package rs.co.tntech.busride.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the BusCategory entity.
 */
public class BusCategoryDTO implements Serializable {

    private Long id;

    private Integer noOfBuses;

    private Integer capacity;

    private Long schoolId;

    private String schoolName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNoOfBuses() {
        return noOfBuses;
    }

    public void setNoOfBuses(Integer noOfBuses) {
        this.noOfBuses = noOfBuses;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BusCategoryDTO busCategoryDTO = (BusCategoryDTO) o;
        if(busCategoryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), busCategoryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BusCategoryDTO{" +
            "id=" + getId() +
            ", noOfBuses='" + getNoOfBuses() + "'" +
            ", capacity='" + getCapacity() + "'" +
            "}";
    }
}
