package rs.co.tntech.busride.service;

import org.springframework.beans.factory.annotation.Autowired;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.ParentNotification;
import rs.co.tntech.busride.domain.enumeration.ParentNotificationType;
import rs.co.tntech.busride.repository.ParentNotificationRepository;

import java.time.Instant;

class NotificationService {

    @Autowired
    private ParentNotificationRepository parentNotificationRepository;

    void saveParentNotification(Parent parent, String notification) {
        ParentNotification parentNotification = new ParentNotification();
        parentNotification.setParent(parent);
        parentNotification.setNotification(notification);
        parentNotification.setTimestamp(Instant.now());
        parentNotification.setType(ParentNotificationType.DRIVER);
        parentNotificationRepository.save(parentNotification);
    }
}
