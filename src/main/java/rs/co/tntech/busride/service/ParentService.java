package rs.co.tntech.busride.service;

import com.amazonaws.services.sns.model.DeleteEndpointResult;
import com.amazonaws.services.sns.model.NotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.School;
import rs.co.tntech.busride.domain.Spot;
import rs.co.tntech.busride.domain.User;
import rs.co.tntech.busride.domain.enumeration.DeviceType;
import rs.co.tntech.busride.domain.enumeration.SpotType;
import rs.co.tntech.busride.domain.enumeration.StudentServiceType;
import rs.co.tntech.busride.repository.ParentRepository;
import rs.co.tntech.busride.repository.SchoolRepository;
import rs.co.tntech.busride.repository.SpotRepository;
import rs.co.tntech.busride.repository.UserRepository;
import rs.co.tntech.busride.security.AuthoritiesConstants;
import rs.co.tntech.busride.security.SecurityUtils;
import rs.co.tntech.busride.service.dto.ParentDTO;
import rs.co.tntech.busride.service.dto.ParentExtendedDTO;
import rs.co.tntech.busride.service.dto.StudentDTO;
import rs.co.tntech.busride.service.dto.UserDTO;
import rs.co.tntech.busride.service.error.BRPBaseServiceException;
import rs.co.tntech.busride.service.error.BRPFieldException;
import rs.co.tntech.busride.service.error.BRPFieldsExcpetion;
import rs.co.tntech.busride.service.error.BRPServiceErrorIds;
import rs.co.tntech.busride.service.error.BRPServiceExcpetion;
import rs.co.tntech.busride.service.error.BRPServiceSecurityException;
import rs.co.tntech.busride.service.mapper.ParentExtendedMapper;
import rs.co.tntech.busride.service.mapper.ParentMapper;
import rs.co.tntech.busride.service.util.BRPServiceUtil;
import rs.co.tntech.busride.service.util.BRPServiceValidationUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Parent.
 */
@Service
@Transactional
public class ParentService {

    private final Logger log = LoggerFactory.getLogger(ParentService.class);

    private final ParentRepository parentRepository;

    @Autowired
    private SpotRepository spotRepository;
    @Autowired
    private SchoolRepository schoolRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ParentMapper parentMapper;
    @Autowired
    private ParentExtendedMapper parentExtendedMapper;
    @Autowired
    private BRPServiceUtil serviceUtil;

    public ParentService(ParentRepository parentRepository) {
        this.parentRepository = parentRepository;
    }

    /**
     * Save a parent.
     *
     * @param parentDTO the entity to save
     * @return the persisted entity
     */
    public ParentDTO save(ParentDTO parentDTO) {
        log.debug("Request to save Parent : {}", parentDTO);
        Parent parent = parentMapper.toEntity(parentDTO);
        parent = parentRepository.save(parent);
        return parentMapper.toDto(parent);
    }

    /**
     * Get all the parents.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ParentDTO> findAll() {
        log.debug("Request to get all Parents");
        return parentRepository.findAll().stream()
            .map(parentMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one parent by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ParentDTO findOne(Long id) {
        log.debug("Request to get Parent : {}", id);
        Parent parent = parentRepository.findOne(id);
        return parentMapper.toDto(parent);
    }

    /**
     * Get one parent by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Parent findOneParent(Long id) {
        log.debug("Request to get Parent : {}", id);
        Parent parent = parentRepository.findOne(id);
        return parent;
    }

    /**
     * Delete the  parent by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Parent : {}", id);
        parentRepository.delete(id);
    }

	/**
	 * Saves list of parents. First parent in list is primary parent.
	 *
	 * @param parentDTOs
	 * @param subscriptionTypes
     * @return list of saved ParentDTO
	 * @throws BRPBaseServiceException
	 */
	public List<ParentExtendedDTO> saveList(List<ParentExtendedDTO> parentDTOs, List<StudentServiceType> subscriptionTypes) throws BRPBaseServiceException {
		log.debug("API:POST:IN:parents/list: save parents list {}", parentDTOs);
		validateSaveList(parentDTOs);

		List<Parent> savedParents = new ArrayList<>();
		// save primary parent (the first parent in input array)
		Parent primaryParent = saveOne(parentDTOs.get(0), null);
		savedParents.add(primaryParent);
		// save all other parents
		for (int i = 1; i < parentDTOs.size(); i++) {
			savedParents.add(saveOne(parentDTOs.get(i), primaryParent));
		}
		// assign parents to school
		School school = serviceUtil.getCurrentSchoolAdmin().getSchool();
		savedParents.forEach(p -> schoolRepository.addParent(school.getId(), p.getId()));

		List<ParentExtendedDTO> result = parentExtendedMapper.toDto(savedParents);
		log.debug("API:POST:OUT:parents/list: {}" + result);

		// save students with subscriptions
        subscriptionTypes.forEach(subscriptionType -> {
            StudentDTO student = new StudentDTO();
            student.setParentId(primaryParent.getId());
            student.setTypeOfService(subscriptionType);
            student.setSchoolId(school.getId());
            studentService.save(student);
        });

        return result;
	}


    /**
     * Get logged in parent
     *
     * @return ParentDTO of currently logged in user
     */
    @Transactional(readOnly = true)
    public ParentExtendedDTO getCurrentParent() {
        Parent loggedInParent = serviceUtil.getCurrentParent();
        log.debug("API:GET:IN:parents/current-parent: get logged in user {}", loggedInParent.getUser().getLogin());

        ParentExtendedDTO result = parentExtendedMapper.toDto(loggedInParent);
        log.debug("API:GET:OUT:parents/current-parent: {}" + result);
        return result;
    }

	@Transactional()
    public Optional<ParentExtendedDTO> updateCurrentParentGCMSessionId(String gcmSessionId, DeviceType deviceType) throws BRPBaseServiceException {
        Parent parent = serviceUtil.getCurrentParent();
        log.debug("API:PUT:IN:parents/refresh-GCM-Session-Id: GCMSessionId: {}, deviceType: {}, for Parent: {}  ", gcmSessionId, deviceType, parent.getUser().getLogin());
        Optional<Parent> updateOrRegisterParentEndpoint;

		if (deviceType != null) {
			if (parent.getDeviceType() != null && parent.getDeviceType() != deviceType) {
				// if device type exist for current parent and input device type is different, delete current endpoint
	            if (parent.getGcmSessionId() != null && parent.getEndpointARN() != null) {
					try {
						Optional<DeleteEndpointResult> deleteEndpointResult = serviceUtil.deleteParentEndpoint(parent.getEndpointARN(), parent.getDeviceType());
						deleteEndpointResult.ifPresent(delResult -> {
							log.debug("API:PUT:IN:parents/current-parent: device type changed. Send request to delete old endpoint: {} with status: {}",
									parent.getEndpointARN(), delResult.getSdkHttpMetadata().getHttpStatusCode());
						});
						parent.setEndpointARN(null);
					} catch (Exception e) {
						log.error(e.getMessage(), e);
					}
	            }
	        }
			if (parent.getDeviceType() == null || parent.getDeviceType() != deviceType) {
				// TODO for now deviceType is optional in V1 this should be mandatory value
				parent.setDeviceType(deviceType);
			}
		}

		if (parent.getDeviceType() == null) {
			throw new BRPServiceExcpetion(BRPServiceErrorIds.DEVICE_TYPE_MISSING,
					String.format("GCM session id cannot be updated because device type not defined for current parent #%s", parent.getUser().getLogin()));
		}
        if (StringUtils.isNotEmpty(parent.getEndpointARN())) {
			try {
				updateOrRegisterParentEndpoint = serviceUtil.updateParentEndpoint(parent, gcmSessionId);
			} catch (NotFoundException e) {
				updateOrRegisterParentEndpoint = serviceUtil.createParentEndpoint(parent, gcmSessionId);
			}
        } else {
            updateOrRegisterParentEndpoint = serviceUtil.createParentEndpoint(parent, gcmSessionId);
        }
        Optional<ParentExtendedDTO> parentExtendedDTO = updateOrRegisterParentEndpoint.map(parentExtendedMapper::toDto);

        return parentExtendedDTO;
    }

    @Transactional()
    public Optional<ParentExtendedDTO> updateCurrentParent(ParentExtendedDTO parentExtendedDTO) throws BRPServiceExcpetion, BRPFieldException {
        Parent loggedParent = serviceUtil.getCurrentParent();
        log.debug("API:PUT:IN:parents/current-parent: Update logged in user {} with a {}", loggedParent.getUser().getLogin(), parentExtendedDTO);

        if (parentExtendedDTO.getLatitude() == null || parentExtendedDTO.getLongitude()==null){
            throw new BRPServiceExcpetion(
                BRPServiceErrorIds.PARENT_SPOT_MUST_HAVE_LAT_AND_LON,"Parent spot must have both latitude and longitude");
        }

        if(isExistingEmail(loggedParent.getUser().getEmail(), parentExtendedDTO.getEmail())) {
            throw new BRPFieldException("ParentExtendedDto", "email", BRPServiceErrorIds.USER_EMAIL_ALREADY_EXIST, "Can't update parent configuration. Email already exists");
        }

        if (parentExtendedDTO.getDeviceType() != loggedParent.getDeviceType()) {
            if (loggedParent.getGcmSessionId() != null && loggedParent.getEndpointARN() != null) {
                Optional<DeleteEndpointResult> deleteEndpointResult = serviceUtil.deleteParentEndpoint(loggedParent.getEndpointARN(), loggedParent.getDeviceType());
                deleteEndpointResult.ifPresent(delResult -> {
                    log.debug("API:PUT:IN:parents/current-parent: device type changed. Send request to delete old endpoint: {} with status: {}", loggedParent.getEndpointARN(), delResult.getSdkHttpMetadata().getHttpStatusCode());
                });
            }
        }

        Optional<ParentExtendedDTO> result = Optional.ofNullable(loggedParent).map(parent -> mapParent(parentExtendedDTO, parent))
            .map(parentExtendedMapper::toDto);

        return result;
    }

    private boolean isExistingEmail(String existingEmail, String newEmail) {
        if (newEmail == null){
            return false;
        }

        return !newEmail.equals(existingEmail) &&
            userRepository.findOneByEmail(newEmail).isPresent();
    }

    private Parent mapParent(ParentExtendedDTO parentExtendedDTO, Parent p) {
        p.setAddress(parentExtendedDTO.getAddress());
        p.setHouseNumber(parentExtendedDTO.getHouseNumber());
        p.setCity(parentExtendedDTO.getCity());
        p.setZipCode(parentExtendedDTO.getZipCode());
        p.setState(parentExtendedDTO.getState());
        p.setCountry(parentExtendedDTO.getCountry());
        p.setNoOfStudents(parentExtendedDTO.getNoOfStudents());
        p.setDeviceIdentifier(parentExtendedDTO.getDeviceIdentifier());
        p.setDeviceType(parentExtendedDTO.getDeviceType());
        p.setGcmSessionId(parentExtendedDTO.getGcmSessionId());
        mapPrimaryParentSpot(parentExtendedDTO, p);

        // Android parent app sends empty value for phones as phone field does not exist in the UI in the moment
        // so keep existing phones if value in update is empty
        if (parentExtendedDTO.getPhone() != null && !parentExtendedDTO.getPhone().isEmpty()){
            p.setPhone(parentExtendedDTO.getPhone());
        }

        if (parentExtendedDTO.getLandPhone() != null && !parentExtendedDTO.getLandPhone().isEmpty()){
            p.setLandPhone(parentExtendedDTO.getLandPhone());
        }

        User user = p.getUser();
        user.setEmail(parentExtendedDTO.getEmail());
        userRepository.save(user);

        parentRepository.save(p);

        return p;
    }

    private void mapPrimaryParentSpot(ParentExtendedDTO parentExtendedDTO, Parent parent) {

        if (parent.getLinkToPrimary() == null) {
            if (parent.getSpot() == null) {
                Spot newSpot = new Spot(parentExtendedDTO.getLatitude(), parentExtendedDTO.getLongitude());
                newSpot.setType(SpotType.PICKUP_DROPOFF);
                newSpot = spotRepository.save(newSpot);
                parent.setSpot(newSpot);
            } else {
                parent.getSpot().setGpsLatitude(parentExtendedDTO.getLatitude());
                parent.getSpot().setGpsLongitude(parentExtendedDTO.getLongitude());
                parent.getSpot().setType(SpotType.PICKUP_DROPOFF);
            }
        }
    }

	/**
	 * Validate list of parents for save action
	 *
	 * @param parentDTOs
	 * @throws BRPBaseServiceException
	 */
	private void validateSaveList(List<ParentExtendedDTO> parentDTOs) throws BRPBaseServiceException {
		// validate required role
		if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.SCHOOL_ADMIN)) {
			String msg = String.format("User does not have any of required roles: '%s'", AuthoritiesConstants.SCHOOL_ADMIN);
			throw new BRPServiceSecurityException(AuthoritiesConstants.SCHOOL_ADMIN, msg);
		}
		// validate input data
		List<BRPFieldException> fieldErrors = new ArrayList<>();
		for (int i = 0; i < parentDTOs.size(); i++) {
			validateSaveEachFromList(fieldErrors, parentDTOs.get(i), i);
		}
		if (!fieldErrors.isEmpty()) {
			throw new BRPFieldsExcpetion(fieldErrors, "Some of input fields are not valid");
		}
	}

	/**
	 * Validate each parent from list
	 *
	 * @param fieldErrors
	 * @param parent
	 * @param index
	 */
	private void validateSaveEachFromList(List<BRPFieldException> fieldErrors, ParentExtendedDTO parent, int index) {
		String objectName = "ParentDTO[" + index + "]";

		if (index == 0) {
			BRPServiceValidationUtils.validateMandatoryField(fieldErrors, objectName, "noOfStudents", parent.getNoOfStudents());
		}

		if (fieldErrors.isEmpty() && parent.getEmail() != null && !parent.getEmail().isEmpty()) {
			Optional<User> userByEmail = userRepository.findOneByEmail(parent.getEmail());

			if (userByEmail.isPresent()) {
				fieldErrors.add(new BRPFieldException(objectName, "email", BRPServiceErrorIds.USER_EMAIL_ALREADY_EXIST,
						"Parent email " + parent.getEmail() + " already exist."));
			}
		}

        if (fieldErrors.isEmpty()) {
            Optional<User> userByNationalId = userRepository.findOneByLogin(parent.getNationalId());

            if (userByNationalId.isPresent()) {
                fieldErrors.add(new BRPFieldException(objectName, "nationalId", BRPServiceErrorIds.USER_NATIONAL_ID_ALREADY_EXIST,
                    "Parent nationalId " + parent.getNationalId() + " already exist."));
            }
        }
    }

	/**
	 * Save each parent from input list
	 *
	 * @param parentDTO
	 * @param primaryParent
	 * @return
	 */
	private Parent saveOne(ParentExtendedDTO parentDTO, Parent primaryParent) {
		Parent parent = parentExtendedMapper.toEntity(parentDTO);
		// create User entity
		Set<String> authorities = new HashSet<String>();
		authorities.add(AuthoritiesConstants.USER);
		authorities.add(AuthoritiesConstants.PARENT);
		UserDTO userDTO = new UserDTO(null, parentDTO.getNationalId(), parentDTO.getFirstName(), parentDTO.getLastName(), parentDTO.getEmail(), true, null, null,
				null, null, null, null, authorities);
		User user = userService.createUser(userDTO, parentDTO.getPassword() != null && parentDTO.getPassword().isEmpty() ? null : parentDTO.getPassword());
		parent.setUser(user);
		// create spot entity only for primary parent
		if (primaryParent == null) {
			Spot spot = null;
			if (parentDTO.getLongitude() != null && parentDTO.getLatitude() != null) {
				spot = new Spot(parentDTO.getLatitude(), parentDTO.getLongitude());
				spot.setType(SpotType.PICKUP_DROPOFF);
				spotRepository.save(spot);
			}
			parent.setSpot(spot);
		} else {
			parent.setNoOfStudents(null);
			// set link to primary parent
			parent.setLinkToPrimary(primaryParent);
		}
		return parentRepository.save(parent);
	}

}
