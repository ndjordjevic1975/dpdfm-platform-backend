package rs.co.tntech.busride.service;

import com.codahale.metrics.annotation.Timed;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.domain.enumeration.BusRouteType;
import rs.co.tntech.busride.domain.enumeration.DriverNotificationMsgType;
import rs.co.tntech.busride.domain.enumeration.DriverNotificationType;
import rs.co.tntech.busride.repository.*;
import rs.co.tntech.busride.security.SecurityUtils;
import rs.co.tntech.busride.service.dto.DriverDTO;
import rs.co.tntech.busride.service.dto.DriverIdentifierDTO;
import rs.co.tntech.busride.service.dto.SNSNotificationMessageDTO;
import rs.co.tntech.busride.service.error.BRPFieldsExcpetion;
import rs.co.tntech.busride.service.error.BRPServiceSystemException;
import rs.co.tntech.busride.service.mapper.DriverMapper;
import rs.co.tntech.busride.service.util.BRPServiceUtil;
import rs.co.tntech.busride.web.rest.vm.UpdateLiveViewReportVM;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Driver.
 */
@Service
@Transactional
public class DriverService {

    private final Logger log = LoggerFactory.getLogger(DriverService.class);

    @Autowired
    private DriverRepository driverRepository;
    @Autowired
    private ParentRepository parentRepository;
    @Autowired
    private BusRouteRepository busRouteRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private DriverNotificationRepository driverNotificationRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private DriverMapper driverMapper;
    @Autowired
    private BRPServiceUtil serviceUtil;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private SNSNotificationService snsNotificationService;
    @Autowired
    private DriverNotificationService driverNotificationService;
    @Autowired
    private LiveViewReportStorageService liveViewReportStorageService;

    public DriverService() {
    }

    /**
     * Save a driver.
     *
     * @param driverDTO the entity to save
     * @return the persisted entity
     */
    public DriverDTO save(DriverDTO driverDTO) {
        log.debug("Request to save Driver : {}", driverDTO);
        Driver driver = driverMapper.toEntity(driverDTO);
        driver = driverRepository.save(driver);
        return driverMapper.toDto(driver);
    }

    /**
     * Get all the drivers.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<DriverDTO> findAll() {
        log.debug("Request to get all Drivers");
        return driverRepository.findAll().stream()
            .map(driverMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one driver by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public DriverDTO findOne(Long id) {
        log.debug("Request to get Driver : {}", id);
        Driver driver = driverRepository.findOne(id);
        return driverMapper.toDto(driver);
    }

    /**
     * Delete the  driver by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Driver : {}", id);
        driverRepository.delete(id);
    }

    /**
     * Update Driver user password and Driver deviIdentifier value
     *
     * @param driverIdentifier
     * @return DriverDTO
     */
    @Transactional
    @Timed
    public Optional<DriverDTO> updateDriverPwdAndIdentifier(DriverIdentifierDTO driverIdentifier) {
        Optional<Driver> driverOptional = Optional.ofNullable(driverRepository.findDriverByUser_Login(mapDriverLoginBySchoolAdmin(driverIdentifier)));
        log.debug("API:PUT:drivers/current-school-admin: Driver: {}, DriverIdentifier: {}", driverOptional.map(Driver::getUser).map(User::getLogin), driverIdentifier);
        return driverOptional
            .map(driver -> updatedDriver(driverIdentifier, driver))
            .map(driver -> driverRepository.save(driver))
            .map(driverMapper::toDto);

    }

    @Transactional()
    public Optional<DriverDTO> updateDriverGCMSessionId(String gcmSessionId) throws BRPServiceSystemException, BRPFieldsExcpetion {
        Driver driver = serviceUtil.getCurrentDriver();
        log.debug("API:PUT:IN:drivers/refresh-GCM-Session-Id: GCMSessionId: {}, for Driver: {}  ", gcmSessionId, driver.getUser().getLogin());
        Optional<Driver> updateOrRegisterDriverEndpoint;
        if (StringUtils.isNotEmpty(driver.getGcmSessionId())) {
            updateOrRegisterDriverEndpoint = serviceUtil.updateDriverEndpoint(driver, gcmSessionId);
        } else {
            updateOrRegisterDriverEndpoint = serviceUtil.createDriverEndpoint(driver, gcmSessionId);
        }
        if (updateOrRegisterDriverEndpoint.isPresent()) {
            Driver updatedDriver = updateOrRegisterDriverEndpoint.get();
            log.debug("API:PUT:OUT:drivers/refresh-GCM-Session-Id: EndpointARN: {}, for Driver: {}  ", updatedDriver.getEndpointARN(), updatedDriver.getUser().getLogin());
        }
        return updateOrRegisterDriverEndpoint.map(driverMapper::toDto);
    }

    /**
     * Assigns a route if provided id is present in the Set of BusRoutes assigned to the Driver
     *
     * @param routeId
     * @return DriverDTO
     */
    @Transactional()
    public Optional<DriverDTO> assignRouteToDriver(Long routeId) {
        Driver currentDriver = serviceUtil.getCurrentDriver();
        log.debug("API:PUT:drivers/assign-route-to-driver: RouteId: {}, for Driver: {}  ", routeId, currentDriver.getUser().getLogin());
        final BusRoute busRoute;
        final DriverNotificationType notifyType;
        final String notificationKey;

        // create driver notification and store it to DB
        DriverNotification driverNotification = new DriverNotification();
        driverNotification.setCreatedOnDevice(Instant.now());
        driverNotification.setDriver(currentDriver);

        if (routeId == 0) {
            notifyType = DriverNotificationType.SCHOOL_END;
            busRoute = currentDriver.getActiveBusRoute();
            liveViewReportStorageService.deleteLiveViewReport(currentDriver.getSchool().getId(), busRoute);
        } else {
            notifyType = DriverNotificationType.SCHOOL_START;
            busRoute = busRouteRepository.getOne(routeId);
        }

        notificationKey = String.format("driver.notification.%s", notifyType.toString().toLowerCase());
        driverNotification.setType(notifyType);
        driverNotification.setSpot(busRoute.getSchool().getSpot());

        Locale locale = Locale.forLanguageTag(currentDriver.getUser().getLangKey());
        String schoolName = busRoute.getSchool().getName();
        driverNotification.setNotification(messageSource.getMessage(notificationKey, new String[]{schoolName}, locale));

        driverNotification = driverNotificationRepository.save(driverNotification);

        // send push notifications to all parents which children are selected for the route
        // do not send if dropoff route ended
        if (routeId != 0 || !busRoute.getType().equals(BusRouteType.DROPOFF)) {
            Long schoolId = busRoute.getSchool().getId();
            // convert list of spots to list of spot ids, filter spots where not all students are canceled for the school
            List<Long> spotIds = busRoute.getSpots()
                .stream()
                .filter(spot -> hasOnGoingStudents(spot, schoolId))
                .map(Spot::getId)
                .collect(Collectors.toList());
            // find all parents (primary and relatives, for filtered spot ids), and send push notifications
            List<Parent> parents = parentRepository.findAllBySpotIds(spotIds);
            // send SNS message to all parents
            SNSNotificationMessageDTO notificationMsg = new SNSNotificationMessageDTO(DriverNotificationMsgType.BUS);
            notificationMsg.setTitleKey("title.bus.notification");
            notificationMsg.setContentKey(String.format("driver.notification.%s", notifyType.toString().toLowerCase()));
            notificationMsg.setContentArgs(new String[]{schoolName});
            notificationMsg.setNotificationId(driverNotification.getId());

            parents.forEach(parent -> {
                Locale parentLocale = Locale.forLanguageTag(parent.getUser().getLangKey());
                String message = messageSource.getMessage(notificationMsg.getContentKey(), notificationMsg.getContentArgs(), parentLocale);
                driverNotificationService.saveParentNotification(parent, message);
            });

            if (driverNotification.getType() != DriverNotificationType.SCHOOL_END) {
                snsNotificationService.publishMessageToParentApp(parents, notificationMsg);
            }
        }

        return Optional.of(currentDriver).map(
            (Driver driver) -> mapDriverBusRoute(routeId, driver))
            .map(driverMapper::toDto);
    }

    /**
     * This method checks if on given spot, exists any student who is going to the school. If exists, the method returns true, otherwise returns false.
     *
     * @param spot
     * @param schoolId
     * @return
     */
    private boolean hasOnGoingStudents(Spot spot, Long schoolId) {
        boolean result = false;

        Parent parent = parentRepository.findOneBySpotId(spot.getId());

        if (parent != null) {
            result = studentRepository.countNonCanceledStudentsForParentAndSchool(parent.getId(), schoolId) > 0;
        }
        return result;
    }

    /**
     * Get Driver by id
     *
     * @param id of a driver
     * @return optional driver
     */
    @Transactional(readOnly = true)
    public Optional<DriverDTO> getDriverById(Long id) {
        return Optional.ofNullable(driverRepository.findOne(id))
            .map(driverMapper::toDto);
    }

    private Driver mapDriverBusRoute(Long routeId, Driver driver) {
        BusRoute route = busRouteRepository.findOne(routeId);

        driver.setActiveBusRoute(route);

        driverRepository.save(driver);

        return driver;
    }

    private Driver updatedDriver(DriverIdentifierDTO driverIdentifier, Driver d) {
        d.setTabletNumber(driverIdentifier.getTabletNumber());
        d.setDeviceIdentifier(driverIdentifier.getDeviceIdentifier());
        d.getUser().setPassword(passwordEncoder.encode(driverIdentifier.getDeviceIdentifier()));
        return d;
    }

    private String mapDriverLoginBySchoolAdmin(DriverIdentifierDTO driverIdentifier) {
        return SecurityUtils.getCurrentUserLogin() + "_tablet_" + driverIdentifier.getTabletNumber();
    }

    public void updateLiveReportView(UpdateLiveViewReportVM updateLiveViewReportVM) {
        Driver currentDriver = serviceUtil.getCurrentDriver();
        Long schoolId = currentDriver.getSchool().getId();

        liveViewReportStorageService.updateLiveViewReport(schoolId, updateLiveViewReportVM, currentDriver.getActiveBusRoute().getId());
    }

    public HashMap<Long, LiveViewReportData> getLiveViewReport() {
        Driver driver = serviceUtil.getCurrentDriver();
        Long schoolId = driver.getSchool().getId();
        return new HashMap<>(liveViewReportStorageService.getLiveViewReportStorage().get(schoolId));
    }
}
