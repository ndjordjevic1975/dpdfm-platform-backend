package rs.co.tntech.busride.service.dto;


import javax.persistence.Lob;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WhiteLabel entity.
 */
public class WhiteLabelDTO implements Serializable {

    private Long id;

    @Lob
    private byte[] parentWhiteLabel;
    private String parentWhiteLabelContentType;

    @Lob
    private byte[] driverWhiteLabel;
    private String driverWhiteLabelContentType;

    private Integer parentWhiteLabelVersion;

    private Integer driverWhiteLabelVersion;

    @Lob
    private byte[] splashScreen;
    private String splashScreenContentType;

    @Lob
    private byte[] icnBus;
    private String icnBusContentType;

    @Lob
    private byte[] logoBg;
    private String logoBgContentType;

    @Lob
    private byte[] logoTxt;
    private String logoTxtContentType;

    private Long schoolId;

    private String schoolName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getParentWhiteLabel() {
        return parentWhiteLabel;
    }

    public void setParentWhiteLabel(byte[] parentWhiteLabel) {
        this.parentWhiteLabel = parentWhiteLabel;
    }

    public String getParentWhiteLabelContentType() {
        return parentWhiteLabelContentType;
    }

    public void setParentWhiteLabelContentType(String parentWhiteLabelContentType) {
        this.parentWhiteLabelContentType = parentWhiteLabelContentType;
    }

    public byte[] getDriverWhiteLabel() {
        return driverWhiteLabel;
    }

    public void setDriverWhiteLabel(byte[] driverWhiteLabel) {
        this.driverWhiteLabel = driverWhiteLabel;
    }

    public String getDriverWhiteLabelContentType() {
        return driverWhiteLabelContentType;
    }

    public void setDriverWhiteLabelContentType(String driverWhiteLabelContentType) {
        this.driverWhiteLabelContentType = driverWhiteLabelContentType;
    }

    public Integer getParentWhiteLabelVersion() {
        return parentWhiteLabelVersion;
    }

    public void setParentWhiteLabelVersion(Integer parentWhiteLabelVersion) {
        this.parentWhiteLabelVersion = parentWhiteLabelVersion;
    }

    public Integer getDriverWhiteLabelVersion() {
        return driverWhiteLabelVersion;
    }

    public void setDriverWhiteLabelVersion(Integer driverWhiteLabelVersion) {
        this.driverWhiteLabelVersion = driverWhiteLabelVersion;
    }

    public byte[] getSplashScreen() {
        return splashScreen;
    }

    public void setSplashScreen(byte[] splashScreen) {
        this.splashScreen = splashScreen;
    }

    public String getSplashScreenContentType() {
        return splashScreenContentType;
    }

    public void setSplashScreenContentType(String splashScreenContentType) {
        this.splashScreenContentType = splashScreenContentType;
    }

    public byte[] getIcnBus() {
        return icnBus;
    }

    public void setIcnBus(byte[] icnBus) {
        this.icnBus = icnBus;
    }

    public String getIcnBusContentType() {
        return icnBusContentType;
    }

    public void setIcnBusContentType(String icnBusContentType) {
        this.icnBusContentType = icnBusContentType;
    }

    public byte[] getLogoBg() {
        return logoBg;
    }

    public void setLogoBg(byte[] logoBg) {
        this.logoBg = logoBg;
    }

    public String getLogoBgContentType() {
        return logoBgContentType;
    }

    public void setLogoBgContentType(String logoBgContentType) {
        this.logoBgContentType = logoBgContentType;
    }

    public byte[] getLogoTxt() {
        return logoTxt;
    }

    public void setLogoTxt(byte[] logoTxt) {
        this.logoTxt = logoTxt;
    }

    public String getLogoTxtContentType() {
        return logoTxtContentType;
    }

    public void setLogoTxtContentType(String logoTxtContentType) {
        this.logoTxtContentType = logoTxtContentType;
    }

    public Long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WhiteLabelDTO whiteLabelDTO = (WhiteLabelDTO) o;
        if(whiteLabelDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), whiteLabelDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WhiteLabelDTO{" +
            "id=" + getId() +
            ", parentWhiteLabel='" + getParentWhiteLabel() + "'" +
            ", driverWhiteLabel='" + getDriverWhiteLabel() + "'" +
            ", parentWhiteLabelVersion='" + getParentWhiteLabelVersion() + "'" +
            ", driverWhiteLabelVersion='" + getDriverWhiteLabelVersion() + "'" +
            ", splashScreen='" + getSplashScreen() + "'" +
            ", icnBus='" + getIcnBus() + "'" +
            ", logoBg='" + getLogoBg() + "'" +
            ", logoTxt='" + getLogoTxt() + "'" +
            "}";
    }
}
