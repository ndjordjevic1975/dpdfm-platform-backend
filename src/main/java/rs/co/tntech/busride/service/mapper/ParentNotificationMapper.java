package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.ParentNotificationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ParentNotification and its DTO ParentNotificationDTO.
 */
@Mapper(componentModel = "spring", uses = {ParentMapper.class, })
public interface ParentNotificationMapper extends EntityMapper <ParentNotificationDTO, ParentNotification> {

    @Mapping(source = "parent.id", target = "parentId")
    ParentNotificationDTO toDto(ParentNotification parentNotification); 

    @Mapping(source = "parentId", target = "parent")
    ParentNotification toEntity(ParentNotificationDTO parentNotificationDTO); 
    default ParentNotification fromId(Long id) {
        if (id == null) {
            return null;
        }
        ParentNotification parentNotification = new ParentNotification();
        parentNotification.setId(id);
        return parentNotification;
    }
}
