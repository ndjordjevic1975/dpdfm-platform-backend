package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.DropoffReportDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity DropoffReport and its DTO DropoffReportDTO.
 */
@Mapper(componentModel = "spring", uses = {BusRouteMapper.class, SpotMapper.class, })
public interface DropoffReportMapper extends EntityMapper <DropoffReportDTO, DropoffReport> {

    @Mapping(source = "busRoute.id", target = "busRouteId")

    @Mapping(source = "spot.id", target = "spotId")
    DropoffReportDTO toDto(DropoffReport dropoffReport); 

    @Mapping(source = "busRouteId", target = "busRoute")

    @Mapping(source = "spotId", target = "spot")
    DropoffReport toEntity(DropoffReportDTO dropoffReportDTO); 
    default DropoffReport fromId(Long id) {
        if (id == null) {
            return null;
        }
        DropoffReport dropoffReport = new DropoffReport();
        dropoffReport.setId(id);
        return dropoffReport;
    }
}
