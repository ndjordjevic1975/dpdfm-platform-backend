package rs.co.tntech.busride.service.dto;


import rs.co.tntech.busride.domain.TempServiceCancellation;

import java.io.Serializable;

/**
 * A DTO for the TempServiceCancellation entity.
 */
public class TempServiceCancellationMessageDTO implements Serializable {

    Long id;
    String message;

    public TempServiceCancellationMessageDTO() {
    }

    public TempServiceCancellationMessageDTO(Long id, String message) {
        this.id = id;
        this.message = message;
    }

    public TempServiceCancellationMessageDTO(TempServiceCancellation tempServiceCancellation, String message) {
        setId(tempServiceCancellation.getId());
        setMessage(message);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "TempServiceCancellationExtendedDTO{" +
            "id=" + id +
            ", message='" + message + '\'' +
            '}';
    }
}
