package rs.co.tntech.busride.service.mapper;

import rs.co.tntech.busride.domain.*;
import rs.co.tntech.busride.service.dto.WhiteLabelDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WhiteLabel and its DTO WhiteLabelDTO.
 */
@Mapper(componentModel = "spring", uses = {SchoolMapper.class, })
public interface WhiteLabelMapper extends EntityMapper <WhiteLabelDTO, WhiteLabel> {

    @Mapping(source = "school.id", target = "schoolId")
    @Mapping(source = "school.name", target = "schoolName")
    WhiteLabelDTO toDto(WhiteLabel whiteLabel); 

    @Mapping(source = "schoolId", target = "school")
    WhiteLabel toEntity(WhiteLabelDTO whiteLabelDTO); 
    default WhiteLabel fromId(Long id) {
        if (id == null) {
            return null;
        }
        WhiteLabel whiteLabel = new WhiteLabel();
        whiteLabel.setId(id);
        return whiteLabel;
    }
}
