package rs.co.tntech.busride.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String DRIVER = "ROLE_DRIVER";

    public static final String PARENT = "ROLE_PARENT";

    public static final String SCHOOL_ADMIN = "ROLE_SCHOOL_ADMIN";

    private AuthoritiesConstants() {
    }
}
