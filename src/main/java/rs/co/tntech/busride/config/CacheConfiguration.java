package rs.co.tntech.busride.config;

import com.hazelcast.config.*;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import io.github.jhipster.config.JHipsterConstants;
import io.github.jhipster.config.JHipsterProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.annotation.PreDestroy;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final Logger log = LoggerFactory.getLogger(CacheConfiguration.class);

    private final Environment env;

    private ApplicationProperties applicationProperties;

    public CacheConfiguration(Environment env, ApplicationProperties applicationProperties) {
        this.env = env;
        this.applicationProperties = applicationProperties;
    }

    @PreDestroy
    public void destroy() {
        log.info("Closing Cache Manager");
        Hazelcast.shutdownAll();
    }

    @Bean
    public CacheManager cacheManager(HazelcastInstance hazelcastInstance) {
        log.debug("Starting HazelcastCacheManager");
        CacheManager cacheManager = new com.hazelcast.spring.cache.HazelcastCacheManager(hazelcastInstance);
        return cacheManager;
    }

    @Bean
    public HazelcastInstance hazelcastInstance(JHipsterProperties jHipsterProperties) {
        log.debug("Configuring Hazelcast");
        HazelcastInstance hazelCastInstance = Hazelcast.getHazelcastInstanceByName("busRidePlatformBackend");

        if (hazelCastInstance != null) {
            log.debug("Hazelcast already initialized");
            return hazelCastInstance;
        }

        Config config = new Config();
        config.setInstanceName("busRidePlatformBackend");
        config.getGroupConfig().setName(applicationProperties.getAwsHazelcastGroupName()).setPassword(applicationProperties.getAwsHazelcastGroupPassword());
        config.getNetworkConfig().setPort(5701);
        config.getNetworkConfig().setPortAutoIncrement(true);

        config.getManagementCenterConfig().setEnabled(true);
        config.getManagementCenterConfig().setUpdateInterval(3);
        config.getManagementCenterConfig().setUrl(applicationProperties.getAwsHazelcastManCenterURL());

        config.getNetworkConfig().getJoin().getAwsConfig().setEnabled(false);
        config.getNetworkConfig().getJoin().getMulticastConfig().setEnabled(false);
        config.getNetworkConfig().getJoin().getTcpIpConfig().setEnabled(false);

        if (env.acceptsProfiles(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT)) {
            System.setProperty("hazelcast.local.localAddress", "127.0.0.1");
        }

        if (env.acceptsProfiles(JHipsterConstants.SPRING_PROFILE_PRODUCTION)) {
            config.setProperty("hazelcast.discovery.enabled", "true");

            DiscoveryStrategyConfig discoveryStrategyConfig = new DiscoveryStrategyConfig("com.hazelcast.aws.AwsDiscoveryStrategy");
            discoveryStrategyConfig.addProperty("access-key", applicationProperties.getAwsHazelcastAccessKey());
            discoveryStrategyConfig.addProperty("secret-key", applicationProperties.getAwsHazelcastSecretKey());
            discoveryStrategyConfig.addProperty("region", applicationProperties.getAwsHazelcastRegion());
            discoveryStrategyConfig.addProperty("security-group-name", applicationProperties.getAwsHazelcastSecurityGroupName());
            discoveryStrategyConfig.addProperty("hz-port", applicationProperties.getAwsHazelcastHzPort());

            config.getNetworkConfig().getJoin().getDiscoveryConfig().addDiscoveryStrategyConfig(discoveryStrategyConfig);
        }

        config.getMapConfigs().put("default", initializeDefaultMapConfig());
        config.getMapConfigs().put("rs.co.tntech.busride.domain.*", initializeDomainMapConfig(jHipsterProperties));

        return Hazelcast.newHazelcastInstance(config);
    }

    private MapConfig initializeDefaultMapConfig() {
        MapConfig mapConfig = new MapConfig("default");

    /*
        Number of backups. If 1 is set as the backup-count for example,
        then all entries of the map will be copied to another JVM for
        fail-safety. Valid numbers are 0 (no backup), 1, 2, 3.
     */
        mapConfig.setBackupCount(0);

    /*
        Valid values are:
        NONE (no eviction),
        LRU (Least Recently Used),
        LFU (Least Frequently Used).
        NONE is the default.
     */
        mapConfig.setEvictionPolicy(EvictionPolicy.LRU);

    /*
        Maximum size of the map. When max size is reached,
        map is evicted based on the policy defined.
        Any integer between 0 and Integer.MAX_VALUE. 0 means
        Integer.MAX_VALUE. Default is 0.
     */
        mapConfig.setMaxSizeConfig(new MaxSizeConfig(0, MaxSizeConfig.MaxSizePolicy.USED_HEAP_SIZE));

        return mapConfig;
    }

    private MapConfig initializeDomainMapConfig(JHipsterProperties jHipsterProperties) {
        MapConfig mapConfig = new MapConfig();

        mapConfig.setBackupCount(0);
        mapConfig.setEvictionPolicy(EvictionPolicy.NONE);
        mapConfig.setMaxSizeConfig(new MaxSizeConfig(0, MaxSizeConfig.MaxSizePolicy.USED_HEAP_SIZE));

        mapConfig.setName("rs.co.tntech.busride.domain.*");
        mapConfig.setTimeToLiveSeconds(jHipsterProperties.getCache().getHazelcast().getTimeToLiveSeconds());
        mapConfig.setMaxIdleSeconds(300);

        EvictionConfig evictionConfig = new EvictionConfig()
            .setEvictionPolicy(EvictionPolicy.NONE)
            .setMaximumSizePolicy(EvictionConfig.MaxSizePolicy.ENTRY_COUNT)
            .setSize(5000);

        NearCacheConfig nearCacheConfig = new NearCacheConfig()
            .setInMemoryFormat(InMemoryFormat.OBJECT)
            .setInvalidateOnChange(false)
            .setTimeToLiveSeconds(600)
            .setEvictionConfig(evictionConfig);

        mapConfig.setInMemoryFormat(InMemoryFormat.BINARY).setNearCacheConfig(nearCacheConfig);

        return mapConfig;
    }
}
