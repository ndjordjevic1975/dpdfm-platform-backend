package rs.co.tntech.busride.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambdaAsync;
import com.amazonaws.services.lambda.AWSLambdaAsyncClientBuilder;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AWSConfiguration {

    private final Logger log = LoggerFactory.getLogger(AWSConfiguration.class);

    @Autowired
    private ApplicationProperties applicationProperties;

    public AWSConfiguration() {

    }

    @Bean()
    @Qualifier("android")
    public AmazonSNSAsync getAmazonSNSClientGCM() {
        log.debug("AWS:Service: Creating AWS SNS Client for GCM");

        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(applicationProperties.getAwsSNSClientGCMAccesskey(), applicationProperties.getGetAwsSNSClientGCMSecretKey());

        return AmazonSNSAsyncClientBuilder.standard()
            .withRegion(applicationProperties.getAwsSNSRegion())
            .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
            .build();
    }

    @Bean()
    @Qualifier("ios")
    public AmazonSNSAsync getAmazonSNSClientAPNS() {
        log.debug("AWS:Service: Creating AWS SNS Client for APNS");

        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(applicationProperties.getAwsSNSClientAPNSAccesskey(), applicationProperties.getGetAwsSNSClientAPNSSecretKey());

        return AmazonSNSAsyncClientBuilder.standard()
            .withRegion(applicationProperties.getAwsSNSRegion())
            .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
            .build();
    }

    @Bean()
    public AWSLambdaAsync getAmazonLambda() {
        log.debug("AWS:Service: Creating AWS Lambda Client");
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(applicationProperties.getAwsLambdaClientUsername(), applicationProperties.getAwsLambdaClientPassword());

        return AWSLambdaAsyncClientBuilder.standard()
            .withRegion(Regions.EU_WEST_1)
            .withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();

    }

}
