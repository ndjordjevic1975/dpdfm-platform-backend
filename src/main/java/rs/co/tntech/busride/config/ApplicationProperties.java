package rs.co.tntech.busride.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to JHipster.
 * <p>
 * Properties are configured in the application.yml file.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private String awsLambdaClientUsername;

    private String awsLambdaClientPassword;

    private String awsLambdaRoutingFunction;

    private String awsSNSClientGCMAccesskey;
    private String getAwsSNSClientGCMSecretKey;

    private String awsSNSClientAPNSAccesskey;
    private String getAwsSNSClientAPNSSecretKey;
    private String awsSNSRegion;

    private String awsSNSDriverAppARN;
    private String awsSNSParentAppARN;
    private String awsSNSParentIOSARN;

    private int snsRetryMaxCount;
    private int snsRetryPeriodInSec;

    private String schoolMgmPortalUrl;

    private String awsHazelcastManCenterURL;
    private String awsHazelcastGroupName;
    private String awsHazelcastGroupPassword;
    private String awsHazelcastAccessKey;
    private String awsHazelcastSecretKey;
    private String awsHazelcastIamRole;
    private String awsHazelcastRegion;
    private String awsHazelcastHostHeader;
    private String awsHazelcastSecurityGroupName;
    private String awsHazelcastTagKey;
    private String awsHazelcastTagValue;
    private String awsHazelcastHzPort;

    private String busDriverLiveReportRefreshRate;
    private String mgmPortalLiveReportRefreshRate;

    public String getAwsLambdaClientUsername() {
        return awsLambdaClientUsername;
    }

    public void setAwsLambdaClientUsername(String awsLambdaClientUsername) {
        this.awsLambdaClientUsername = awsLambdaClientUsername;
    }

    public String getAwsLambdaClientPassword() {
        return awsLambdaClientPassword;
    }

    public void setAwsLambdaClientPassword(String awsLambdaClientPassword) {
        this.awsLambdaClientPassword = awsLambdaClientPassword;
    }

    public String getAwsLambdaRoutingFunction() {
        return awsLambdaRoutingFunction;
    }

    public void setAwsLambdaRoutingFunction(String awsLambdaRoutingFunction) {
        this.awsLambdaRoutingFunction = awsLambdaRoutingFunction;
    }

    public String getAwsSNSClientGCMAccesskey() {
        return awsSNSClientGCMAccesskey;
    }

    public void setAwsSNSClientGCMAccesskey(String awsSNSClientGCMAccesskey) {
        this.awsSNSClientGCMAccesskey = awsSNSClientGCMAccesskey;
    }

    public String getGetAwsSNSClientGCMSecretKey() {
        return getAwsSNSClientGCMSecretKey;
    }

    public void setGetAwsSNSClientGCMSecretKey(String getAwsSNSClientGCMSecretKey) {
        this.getAwsSNSClientGCMSecretKey = getAwsSNSClientGCMSecretKey;
    }

    public String getAwsSNSClientAPNSAccesskey() {
        return awsSNSClientAPNSAccesskey;
    }

    public void setAwsSNSClientAPNSAccesskey(String awsSNSClientAPNSAccesskey) {
        this.awsSNSClientAPNSAccesskey = awsSNSClientAPNSAccesskey;
    }

    public String getGetAwsSNSClientAPNSSecretKey() {
        return getAwsSNSClientAPNSSecretKey;
    }

    public void setGetAwsSNSClientAPNSSecretKey(String getAwsSNSClientAPNSSecretKey) {
        this.getAwsSNSClientAPNSSecretKey = getAwsSNSClientAPNSSecretKey;
    }

    public String getAwsSNSRegion() {
        return awsSNSRegion;
    }

    public void setAwsSNSRegion(String awsSNSRegion) {
        this.awsSNSRegion = awsSNSRegion;
    }

    public String getAwsSNSParentAppARN() {
        return awsSNSParentAppARN;
    }

    public void setAwsSNSParentAppARN(String awsSNSParentAppARN) {
        this.awsSNSParentAppARN = awsSNSParentAppARN;
    }

	public String getAwsSNSParentIOSARN() {
		return awsSNSParentIOSARN;
	}

	public void setAwsSNSParentIOSARN(String awsSNSParentIOSARN) {
		this.awsSNSParentIOSARN = awsSNSParentIOSARN;
	}

	public String getAwsSNSDriverAppARN() {
        return awsSNSDriverAppARN;
    }

    public void setAwsSNSDriverAppARN(String awsSNSDriverAppARN) {
        this.awsSNSDriverAppARN = awsSNSDriverAppARN;
    }

	/**
	 * @return the snsRetryMaxCount
	 */
	public int getSnsRetryMaxCount() {
		return snsRetryMaxCount;
	}

	/**
	 * @param snsRetryMaxCount the snsRetryMaxCount to set
	 */
	public void setSnsRetryMaxCount(int snsRetryMaxCount) {
		this.snsRetryMaxCount = snsRetryMaxCount;
	}

	/**
	 * @return the snsRetryPeriodInSec
	 */
	public int getSnsRetryPeriodInSec() {
		return snsRetryPeriodInSec;
	}

	/**
	 * @param snsRetryPeriodInSec the snsRetryPeriodInSec to set
	 */
	public void setSnsRetryPeriodInSec(int snsRetryPeriodInSec) {
		this.snsRetryPeriodInSec = snsRetryPeriodInSec;
	}

    public String getSchoolMgmPortalUrl() {
        return schoolMgmPortalUrl;
    }

    public void setSchoolMgmPortalUrl(String schoolMgmPortalUrl) {
        this.schoolMgmPortalUrl = schoolMgmPortalUrl;
    }

    public String getAwsHazelcastManCenterURL() {
        return awsHazelcastManCenterURL;
    }

    public void setAwsHazelcastManCenterURL(String awsHazelcastManCenterURL) {
        this.awsHazelcastManCenterURL = awsHazelcastManCenterURL;
    }

    public String getAwsHazelcastAccessKey() {
        return awsHazelcastAccessKey;
    }

    public void setAwsHazelcastAccessKey(String awsHazelcastAccessKey) {
        this.awsHazelcastAccessKey = awsHazelcastAccessKey;
    }

    public String getAwsHazelcastSecretKey() {
        return awsHazelcastSecretKey;
    }

    public void setAwsHazelcastSecretKey(String awsHazelcastSecretKey) {
        this.awsHazelcastSecretKey = awsHazelcastSecretKey;
    }

    public String getAwsHazelcastIamRole() {
        return awsHazelcastIamRole;
    }

    public void setAwsHazelcastIamRole(String awsHazelcastIamRole) {
        this.awsHazelcastIamRole = awsHazelcastIamRole;
    }

    public String getAwsHazelcastRegion() {
        return awsHazelcastRegion;
    }

    public void setAwsHazelcastRegion(String awsHazelcastRegion) {
        this.awsHazelcastRegion = awsHazelcastRegion;
    }

    public String getAwsHazelcastHostHeader() {
        return awsHazelcastHostHeader;
    }

    public void setAwsHazelcastHostHeader(String awsHazelcastHostHeader) {
        this.awsHazelcastHostHeader = awsHazelcastHostHeader;
    }

    public String getAwsHazelcastSecurityGroupName() {
        return awsHazelcastSecurityGroupName;
    }

    public void setAwsHazelcastSecurityGroupName(String awsHazelcastSecurityGroupName) {
        this.awsHazelcastSecurityGroupName = awsHazelcastSecurityGroupName;
    }

    public String getAwsHazelcastTagKey() {
        return awsHazelcastTagKey;
    }

    public void setAwsHazelcastTagKey(String awsHazelcastTagKey) {
        this.awsHazelcastTagKey = awsHazelcastTagKey;
    }

    public String getAwsHazelcastTagValue() {
        return awsHazelcastTagValue;
    }

    public void setAwsHazelcastTagValue(String awsHazelcastTagValue) {
        this.awsHazelcastTagValue = awsHazelcastTagValue;
    }

    public String getAwsHazelcastHzPort() {
        return awsHazelcastHzPort;
    }

    public void setAwsHazelcastHzPort(String awsHazelcastHzPort) {
        this.awsHazelcastHzPort = awsHazelcastHzPort;
    }

    public String getAwsHazelcastGroupName() {
        return awsHazelcastGroupName;
    }

    public void setAwsHazelcastGroupName(String awsHazelcastGroupName) {
        this.awsHazelcastGroupName = awsHazelcastGroupName;
    }

    public String getAwsHazelcastGroupPassword() {
        return awsHazelcastGroupPassword;
    }

    public void setAwsHazelcastGroupPassword(String awsHazelcastGroupPassword) {
        this.awsHazelcastGroupPassword = awsHazelcastGroupPassword;
    }

    public String getBusDriverLiveReportRefreshRate() {
        return busDriverLiveReportRefreshRate;
    }

    public void setBusDriverLiveReportRefreshRate(String busDriverLiveReportRefreshRate) {
        this.busDriverLiveReportRefreshRate = busDriverLiveReportRefreshRate;
    }

    public String getMgmPortalLiveReportRefreshRate() {
        return mgmPortalLiveReportRefreshRate;
    }

    public void setMgmPortalLiveReportRefreshRate(String mgmPortalLiveReportRefreshRate) {
        this.mgmPortalLiveReportRefreshRate = mgmPortalLiveReportRefreshRate;
    }
}
