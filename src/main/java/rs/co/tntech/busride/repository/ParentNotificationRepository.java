package rs.co.tntech.busride.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.ParentNotification;

import java.util.List;


/**
 * Spring Data JPA repository for the ParentNotification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParentNotificationRepository extends JpaRepository<ParentNotification,Long> {

	List<ParentNotification> findAllByParent(Parent parent);

}
