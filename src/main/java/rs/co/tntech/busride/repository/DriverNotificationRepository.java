package rs.co.tntech.busride.repository;

import rs.co.tntech.busride.domain.DriverNotification;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DriverNotification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DriverNotificationRepository extends JpaRepository<DriverNotification, Long> {

}
