package rs.co.tntech.busride.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.co.tntech.busride.domain.Driver;
import rs.co.tntech.busride.domain.School;

import java.util.List;


/**
 * Spring Data JPA repository for the Driver entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DriverRepository extends JpaRepository<Driver, Long> {

    Driver findDriverByUser_Login(String userLogin);

    List<Driver> findAllBySchoolId(Long schoolId);

    List<Driver> findAllBySchoolAndCapacity(School school, Integer busCapacity);

    int countBySchoolAndActiveBusRoute_idGreaterThan(School school, long routeId);

    List<Driver> findAllBySchoolIdAndActiveBusRouteIsNotNull(long schoolId);
}
