package rs.co.tntech.busride.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.co.tntech.busride.domain.BusRoute;
import rs.co.tntech.busride.domain.Driver;
import rs.co.tntech.busride.domain.School;
import rs.co.tntech.busride.domain.enumeration.BusRouteType;

import java.time.Instant;
import java.util.List;
import java.util.Set;

/**
 * Spring Data JPA repository for the BusRoute entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BusRouteRepository extends JpaRepository<BusRoute, Long> {

    @Query("select distinct bus_route from BusRoute bus_route left join fetch bus_route.spots")
    List<BusRoute> findAllWithEagerRelationships();

    @Query("select bus_route from BusRoute bus_route left join fetch bus_route.spots where bus_route.id =:id")
    BusRoute findOneWithEagerRelationships(@Param("id") Long id);

    @Query("SELECT br FROM BusRoute br INNER JOIN Driver d ON br.id = d.activeBusRoute.id WHERE br.school.id = :schoolId")
    List<BusRoute> findAllActiveRoutesBySchoolId(@Param("schoolId") Long schoolId);

    List<BusRoute> findAllByDriver_IdAndInactiveOnIsNull(Long driverId);

    @Modifying(clearAutomatically = true)
    @Query("UPDATE BusRoute br SET br.inactiveOn = :inactiveOn where br.inactiveOn IS NULL AND br.school = :school")
    void setInactiveOnExistingRoutes(@Param("inactiveOn") Instant inactiveOn, @Param("school") School school);

    List<BusRoute> findAllBySchoolIdAndInactiveOnIsNull(long schoolId);

    int countByTypeAndSchoolAndInactiveOnIsNull(BusRouteType type, School school);

    Set<BusRoute> findAllByDriver(Driver driver);
}
