package rs.co.tntech.busride.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.co.tntech.busride.domain.WhiteLabel;

import java.util.Optional;


/**
 * Spring Data JPA repository for the WhiteLabel entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WhiteLabelRepository extends JpaRepository<WhiteLabel,Long> {

	Optional<WhiteLabel> findOneBySchool_id(Long schoolId);

}
