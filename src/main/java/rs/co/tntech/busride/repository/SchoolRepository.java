package rs.co.tntech.busride.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.School;

import java.util.List;

/**
 * Spring Data JPA repository for the School entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SchoolRepository extends JpaRepository<School,Long> {

    @Query("select distinct school from School school left join fetch school.parents")
    List<School> findAllWithEagerRelationships();

    @Query("select school from School school left join fetch school.parents where school.id =:id")
    School findOneWithEagerRelationships(@Param("id") Long id);

    List<School> findAllByParents(Parent parent);
    
    @Modifying
	@Query(value = "insert into school_parent (schools_id, parents_id) values (?1,?2)", nativeQuery = true)
	void addParent(Long schoolId, Long parentId);
}
