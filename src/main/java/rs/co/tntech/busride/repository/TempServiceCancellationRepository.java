package rs.co.tntech.busride.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.co.tntech.busride.domain.Student;
import rs.co.tntech.busride.domain.TempServiceCancellation;
import rs.co.tntech.busride.domain.enumeration.StudentServiceType;

import java.util.List;


/**
 * Spring Data JPA repository for the TempServiceCancellation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TempServiceCancellationRepository extends JpaRepository<TempServiceCancellation,Long> {

    List<TempServiceCancellation> findAllBySpotIdInAndCancellationTypeIn(List<Long> spotId, List<StudentServiceType> cancellationTypes);

    Long countAllBySpot_Id(Long spotId);

    @Query("FROM TempServiceCancellation WHERE student.id IN (SELECT id FROM Student WHERE parent.id = :parentId)")
    List<TempServiceCancellation> findAllByParentId(@Param("parentId") Long parentId);

    TempServiceCancellation findByStudent(Student student);

    Integer deleteByStudent(Student student);
}
