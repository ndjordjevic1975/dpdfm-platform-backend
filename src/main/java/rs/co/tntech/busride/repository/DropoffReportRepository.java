package rs.co.tntech.busride.repository;

import rs.co.tntech.busride.domain.DropoffReport;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DropoffReport entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DropoffReportRepository extends JpaRepository<DropoffReport, Long> {

}
