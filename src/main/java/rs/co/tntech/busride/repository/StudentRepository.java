package rs.co.tntech.busride.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.School;
import rs.co.tntech.busride.domain.Student;
import rs.co.tntech.busride.domain.enumeration.StudentServiceType;

import java.util.List;
import java.util.Set;


/**
 * Spring Data JPA repository for the Student entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {

    List<Student> findAllByParent(Parent primaryParent);

    int countByParentAndTypeOfServiceIn(Parent parent, List<StudentServiceType> studentServiceTypes);

    List<Student> findByParentAndTypeOfServiceIn(Parent parent, List<StudentServiceType> studentServiceTypes);

    @Query("SELECT count(s) FROM Student s LEFT JOIN TempServiceCancellation c ON s.id = c.student.id WHERE c.id IS NULL AND s.parent.id = :parentId AND s.school.id = :schoolId")
    int countNonCanceledStudentsForParentAndSchool(@Param("parentId") Long parentId, @Param("schoolId") Long schoolId);

    int countBySchoolId(Long schoolId);

    @Query("SELECT count(s) FROM Student s LEFT JOIN TempServiceCancellation c ON s.id = c.student.id WHERE c.id IS NULL AND  s.school.id = :schoolId")
    int countNonCanceledBySchoolId(@Param("schoolId") Long schoolId);

    Set<Student> findAllBySchoolAndTypeOfServiceIn(School school, List<StudentServiceType> studentServiceTypes);
}
