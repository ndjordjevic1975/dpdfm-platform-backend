package rs.co.tntech.busride.repository;

import rs.co.tntech.busride.domain.Spot;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Spot entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpotRepository extends JpaRepository<Spot,Long> {
}
