package rs.co.tntech.busride.repository;

import rs.co.tntech.busride.domain.BusCategory;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import rs.co.tntech.busride.domain.School;

import java.util.List;


/**
 * Spring Data JPA repository for the BusCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BusCategoryRepository extends JpaRepository<BusCategory,Long> {

    List<BusCategory> findAllBySchool(School school);
}
