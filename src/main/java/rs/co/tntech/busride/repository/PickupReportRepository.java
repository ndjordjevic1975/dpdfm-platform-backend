package rs.co.tntech.busride.repository;

import rs.co.tntech.busride.domain.PickupReport;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PickupReport entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PickupReportRepository extends JpaRepository<PickupReport, Long> {

}
