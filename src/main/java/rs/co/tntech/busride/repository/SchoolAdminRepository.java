package rs.co.tntech.busride.repository;

import rs.co.tntech.busride.domain.Driver;
import rs.co.tntech.busride.domain.SchoolAdmin;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SchoolAdmin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SchoolAdminRepository extends JpaRepository<SchoolAdmin,Long> {
    
	SchoolAdmin findOneByUserLogin(String userLogin);
	
}
