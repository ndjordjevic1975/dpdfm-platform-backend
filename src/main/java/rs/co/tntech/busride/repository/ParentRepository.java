package rs.co.tntech.busride.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.School;
import rs.co.tntech.busride.domain.enumeration.StudentServiceType;


/**
 * Spring Data JPA repository for the Parent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParentRepository extends JpaRepository<Parent,Long> {

	Parent findOneByUser_login(String userLogin);

	Parent findOneBySpotId(Long spotId);

	@Query("FROM Parent WHERE spot.id = :spotId OR linkToPrimary.id IN (SELECT id FROM Parent WHERE spot.id = :spotId)")
	List<Parent> findAllBySpotId(@Param("spotId") Long spotId);

	@Query("FROM Parent WHERE spot.id IN :spotIds OR linkToPrimary.id IN (SELECT id FROM Parent WHERE spot.id IN :spotIds)")
	List<Parent> findAllBySpotIds(@Param("spotIds") List<Long> spotIds);

    Set<Parent> findAllBySchoolsAndSpotNotNull(School school);

    List<Parent> findAllByLinkToPrimaryId(Long primaryParentId);

    List<Parent> findAllBySchools(School school);

    Optional<Parent> findOneByNationalId(String nationalId);
}
