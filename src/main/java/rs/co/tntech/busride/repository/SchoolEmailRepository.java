package rs.co.tntech.busride.repository;

import rs.co.tntech.busride.domain.SchoolEmail;
import org.springframework.stereotype.Repository;

import java.util.Set;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SchoolEmail entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SchoolEmailRepository extends JpaRepository<SchoolEmail,Long> {
    
	Set<SchoolEmail> findAllBySchoolId(Long schoolId);
	
	void deleteAllBySchoolId(Long schoolId);
	
}
