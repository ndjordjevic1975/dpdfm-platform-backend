package rs.co.tntech.busride.filter;

import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class AppleStaticFileFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String fileName = ".ios/apple-app-site-association";
        String line;

        servletResponse.setContentType("application/json");
        PrintWriter writer = servletResponse.getWriter();

        InputStream input = getServletContext().getResourceAsStream(fileName);

        BufferedReader bufferedReader =
            new BufferedReader(new InputStreamReader(input));

        while((line = bufferedReader.readLine()) != null) {
            System.out.println(line);
            writer.println(line);
        }

        servletResponse.flushBuffer();

        bufferedReader.close();
        input.close();
        writer.close();
    }
}
