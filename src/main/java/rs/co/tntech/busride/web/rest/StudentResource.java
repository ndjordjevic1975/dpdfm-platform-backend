package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.co.tntech.busride.service.StudentService;
import rs.co.tntech.busride.service.dto.StudentDTO;
import rs.co.tntech.busride.service.error.BRPBaseServiceException;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;
import rs.co.tntech.busride.web.rest.vm.StudentUpdateVM;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Student.
 */
@RestController
@RequestMapping("/api")
@Api(tags = { "students" }, description = "Students")
public class StudentResource {

    private final Logger log = LoggerFactory.getLogger(StudentResource.class);

    private static final String ENTITY_NAME = "student";

    private final StudentService studentService;

    public StudentResource(StudentService studentService) {
        this.studentService = studentService;
    }

    /**
     * POST  /students : Create a new student.
     *
     * @param studentDTO the studentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new studentDTO, or with status 400 (Bad Request) if the student has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/students")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<StudentDTO> createStudent(@RequestBody StudentDTO studentDTO) throws URISyntaxException {
        log.debug("REST request to save Student : {}", studentDTO);
        if (studentDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new student cannot already have an ID")).body(null);
        }
        StudentDTO result = studentService.save(studentDTO);
        return ResponseEntity.created(new URI("/api/students/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /students : Updates an existing student.
     *
     * @param studentDTO the studentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated studentDTO,
     * or with status 400 (Bad Request) if the studentDTO is not valid,
     * or with status 500 (Internal Server Error) if the studentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/students")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<StudentDTO> updateStudent(@RequestBody StudentDTO studentDTO) throws URISyntaxException {
        log.debug("REST request to update Student : {}", studentDTO);
        if (studentDTO.getId() == null) {
            return createStudent(studentDTO);
        }

        StudentDTO result = studentService.save(studentDTO);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, studentDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /students : get all the students.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of students in body
     */
    @GetMapping("/students")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public List<StudentDTO> getAllStudents() {
        log.debug("REST request to get all Students");
        List<StudentDTO> result = studentService.findAll();
        return result;
    }

    /**
     * GET  /students/:id : get the "id" student.
     *
     * @param id the id of the studentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the studentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/students/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<StudentDTO> getStudent(@PathVariable Long id) {
        log.debug("REST request to get Student : {}", id);
        StudentDTO result = studentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(result));
    }

    /**
     * DELETE  /students/:id : delete the "id" student.
     *
     * @param id the id of the studentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/students/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<Void> deleteStudent(@PathVariable Long id) {
        log.debug("REST request to delete Student : {}", id);
        studentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * PUT /students/current-parent : Updates an existing student for a current logged in (primary) parent.
     *
     * @param studentVM
     *            the studentVM to update
     * @return the ResponseEntity with status 200 (Ok) and with body the updated studentVM, or with status 400 (Bad Request) if an input data is incorrect
     * @throws URISyntaxException
     *             if the Location URI syntax is incorrect
     * @throws BRPBaseServiceException
     */
    @PutMapping("/students/current-parent")
    @Timed
    public ResponseEntity<StudentDTO> updateStudentForCurrentPrimaryParent(@RequestBody StudentUpdateVM studentVM) throws BRPBaseServiceException {
        log.debug("REST request to update a student for the current primary parent: {}", studentVM);

        StudentDTO result = studentService.updateForCurrentPrimaryParent(studentVM);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

	/**
	 * GET /students/current-parent : Gets all students for a current logged in parent. If the parent is NOT a primary one this returns the students from the
	 * primary parent.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of students in body
	 * @throws BRPBaseServiceException
	 */
	@GetMapping("/students/current-parent")
	@Timed
	public List<StudentDTO> getAllByCurrentPrimaryParent() throws BRPBaseServiceException {
        log.debug("REST request to get all students for the currently logged in parent");

		List<StudentDTO> result = studentService.getAllByCurrentPrimaryParent();
		return result;
	}

    /**
     * GET /students/current-school/totalActive : Gets total number of students for school of currently logged school admin
     *
     * @return the ResponseEntity with status 200 (OK) and the number of students in body
     * @throws BRPBaseServiceException
     */
    @GetMapping("/students/current-school/totalActive")
    @Timed
    public ResponseEntity<Integer> getTotalActiveForCurrentSchool() {
        log.debug("REST request to get total number of students for the current school admin");

        return  ResponseEntity.ok(studentService.getTotalByCurrentSchoolAdmin());
	}

    /**
     * GET /students/current-school/totalNonCanceled : Gets total number of students with non canceled service for
     * currently logged school admin
     *
     * @return the ResponseEntity with status 200 (OK) and the number of students in body
     * @throws BRPBaseServiceException
     */
    @GetMapping("/students/current-school/totalNonCanceled")
    @Timed
    public ResponseEntity<Integer>  getTotalNonCanceledForCurrentSchool() {
        log.debug("REST request to get total number of not temp cancelled students for the current school ");

        return  ResponseEntity.ok(studentService.getTotalNonCanceledBySchoolAdmin());
    }
}
