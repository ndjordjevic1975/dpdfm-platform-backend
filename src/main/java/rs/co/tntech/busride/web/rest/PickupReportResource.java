package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.co.tntech.busride.service.PickupReportService;
import rs.co.tntech.busride.service.dto.PickupReportDTO;
import rs.co.tntech.busride.service.error.BRPBaseServiceException;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;
import rs.co.tntech.busride.web.rest.vm.PickupReportVM;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PickupReport.
 */
@RestController
@RequestMapping("/api")
@Api(tags = {"pickup-reports"}, description = "Pick up reports")
public class PickupReportResource {

    private final Logger log = LoggerFactory.getLogger(PickupReportResource.class);

    private static final String ENTITY_NAME = "pickupReport";

    private final PickupReportService pickupReportService;

    public PickupReportResource(PickupReportService pickupReportService) {
        this.pickupReportService = pickupReportService;
    }

    /**
     * POST  /pickup-reports : Create a new pickupReport.
     *
     * @param pickupReportDTO the pickupReportDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pickupReportDTO, or with status 400 (Bad Request) if the pickupReport has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pickup-reports")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<PickupReportDTO> createPickupReport(@RequestBody PickupReportDTO pickupReportDTO) throws URISyntaxException {
        log.debug("REST request to save PickupReport : {}", pickupReportDTO);
        if (pickupReportDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new pickupReport cannot already have an ID")).body(null);
        }
        PickupReportDTO result = pickupReportService.save(pickupReportDTO);
        return ResponseEntity.created(new URI("/api/pickup-reports/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pickup-reports : Updates an existing pickupReport.
     *
     * @param pickupReportDTO the pickupReportDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pickupReportDTO,
     * or with status 400 (Bad Request) if the pickupReportDTO is not valid,
     * or with status 500 (Internal Server Error) if the pickupReportDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pickup-reports")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<PickupReportDTO> updatePickupReport(@RequestBody PickupReportDTO pickupReportDTO) throws URISyntaxException {
        log.debug("REST request to update PickupReport : {}", pickupReportDTO);
        if (pickupReportDTO.getId() == null) {
            return createPickupReport(pickupReportDTO);
        }
        PickupReportDTO result = pickupReportService.save(pickupReportDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pickupReportDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pickup-reports : get all the pickupReports.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pickupReports in body
     */
    @GetMapping("/pickup-reports")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public List<PickupReportDTO> getAllPickupReports() {
        log.debug("REST request to get all PickupReports");
        return pickupReportService.findAll();
    }

    /**
     * GET  /pickup-reports/:id : get the "id" pickupReport.
     *
     * @param id the id of the pickupReportDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pickupReportDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pickup-reports/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<PickupReportDTO> getPickupReport(@PathVariable Long id) {
        log.debug("REST request to get PickupReport : {}", id);
        PickupReportDTO pickupReportDTO = pickupReportService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pickupReportDTO));
    }

    /**
     * DELETE  /pickup-reports/:id : delete the "id" pickupReport.
     *
     * @param id the id of the pickupReportDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pickup-reports/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<Void> deletePickupReport(@PathVariable Long id) {
        log.debug("REST request to delete PickupReport : {}", id);
        pickupReportService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * POST  /pickup-reports/current-driver : Creates a new pickup report. Also triggers a push notification to Parents.
     *
     * @param pickupReportVM the pickupReportDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pickupReportDTO, or with status 400 (Bad Request) if the pickupReport has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     * @throws BRPBaseServiceException
     */
    @PostMapping("/pickup-reports/current-driver")
    @Timed
    public ResponseEntity<PickupReportDTO> createPickupReportForCurrentDriver(@RequestBody PickupReportVM pickupReportVM) throws URISyntaxException, BRPBaseServiceException {
        log.debug("REST request to create a new pickup report: {}", pickupReportVM);

        PickupReportDTO result = pickupReportService.saveForCurrentDriver(pickupReportVM);
        return ResponseEntity.created(new URI("/api/pickup-reports/current-driver/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

}
