package rs.co.tntech.busride.web.rest;

import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.amazonaws.services.sns.model.Endpoint;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.co.tntech.busride.domain.enumeration.DeviceType;
import rs.co.tntech.busride.domain.enumeration.SNSNotificationApplicationType;
import rs.co.tntech.busride.service.SNSNotificationService;
import rs.co.tntech.busride.service.dto.SNSEndpointDTO;
import rs.co.tntech.busride.service.error.BRPFieldsExcpetion;

import java.util.List;

/**
 * REST controller for managing Driver.
 */
@RestController
@RequestMapping("/api")
public class SNSNotificationResource {

    private final Logger log = LoggerFactory.getLogger(SNSNotificationResource.class);

    @Autowired
    private SNSNotificationService snsNotificationService;


    public SNSNotificationResource() {
    }

    @PostMapping("/sns/endpoints/parent")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<CreatePlatformEndpointResult> createParentSNSEndpoint(@RequestBody SNSEndpointDTO snsEndpointDTO) {
        log.debug("REST request to register parent app device endpoint", snsEndpointDTO);

        return ResponseUtil.wrapOrNotFound(snsNotificationService.registerDevice(snsEndpointDTO, SNSNotificationApplicationType.PARENT_ANDROID));

    }

    @PostMapping("/sns/endpoints/driver")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<CreatePlatformEndpointResult> createDriverSNSEndpoint(@RequestBody SNSEndpointDTO snsEndpointDTO) {
        log.debug("REST request to register driver app device endpoint", snsEndpointDTO);

        return ResponseUtil.wrapOrNotFound(snsNotificationService.registerDevice(snsEndpointDTO, SNSNotificationApplicationType.DRIVER));

    }

    @GetMapping("/sns/endpoints/parent")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<List<Endpoint>> getParentSNSEndpoints() {
        log.debug("REST request to GET all parent app endpoints");

        return ResponseUtil.wrapOrNotFound(snsNotificationService.getAllRegisteredDevices(SNSNotificationApplicationType.PARENT_ANDROID));
    }

    @GetMapping("/sns/endpoints/driver")
    @Timed
    public ResponseEntity<List<Endpoint>> getDriverSNSEndpoints() {
        log.debug("REST request to GET all driver app endpoints");

        return ResponseUtil.wrapOrNotFound(snsNotificationService.getAllRegisteredDevices(SNSNotificationApplicationType.DRIVER));
    }

    @PutMapping("/sns/endpoints")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity updateSNSEndpoint(@RequestBody SNSEndpointDTO snsEndpointDTO) throws BRPFieldsExcpetion {
        log.debug("REST Request to update endpoint");

        return ResponseUtil.wrapOrNotFound(snsNotificationService.updateDeviceSubscriptionAttributes(snsEndpointDTO, DeviceType.ANDROID));
    }
}
