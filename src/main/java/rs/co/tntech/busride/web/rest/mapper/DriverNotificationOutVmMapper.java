package rs.co.tntech.busride.web.rest.mapper;

import org.mapstruct.Mapper;

import rs.co.tntech.busride.service.dto.DriverNotificationDTO;
import rs.co.tntech.busride.web.rest.vm.DriverNotificationOutVM;

/**
 * Mapper for the DriverNotificationOutVM and its DTO DriverNotificationDTO.
 */
@Mapper(componentModel = "spring")
public interface DriverNotificationOutVmMapper extends VmDtoMapper <DriverNotificationOutVM, DriverNotificationDTO> {
    
}
