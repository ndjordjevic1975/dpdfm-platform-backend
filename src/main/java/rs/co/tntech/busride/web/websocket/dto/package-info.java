/**
 * Data Access Objects used by WebSocket services.
 */
package rs.co.tntech.busride.web.websocket.dto;
