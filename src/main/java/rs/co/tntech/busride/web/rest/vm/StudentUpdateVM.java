/**
 *
 */
package rs.co.tntech.busride.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.co.tntech.busride.domain.Student;
import rs.co.tntech.busride.domain.enumeration.StudentServiceType;
import rs.co.tntech.busride.service.dto.StudentDTO;

/**
 * View Model object of {@link Student} for update action
 *
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class StudentUpdateVM extends StudentDTO {

	private static final long serialVersionUID = 1L;

	@Override
	@JsonIgnore
	public Long getParentId() {
		return super.getParentId();
	}

    @Override
    @JsonIgnore
    public StudentServiceType getTypeOfService() {
        return super.getTypeOfService();
    }
}
