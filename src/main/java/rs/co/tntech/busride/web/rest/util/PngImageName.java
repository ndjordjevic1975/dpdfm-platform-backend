package rs.co.tntech.busride.web.rest.util;

public enum PngImageName {
    SPLASH_SCREEN,
    LOGO_TXT,
    LOGO_BG,
    ICN_BUS
}
