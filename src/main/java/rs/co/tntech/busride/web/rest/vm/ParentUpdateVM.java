/**
 *
 */
package rs.co.tntech.busride.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.service.dto.ParentExtendedDTO;

/**
 * View Model object of {@link Parent} for update action
 *
 */
public class ParentUpdateVM extends ParentExtendedDTO {

	private static final long serialVersionUID = 1L;

	@Override
	@JsonIgnore
	public Long getId() {
		return super.getId();
	}

    @Override
    @JsonIgnore
    public String getFirstName() { return super.getFirstName(); }

    @Override
    @JsonIgnore
    public String getLastName() { return super.getLastName(); }

    @Override
    @JsonIgnore
    public String getPassword() {
        return super.getEmail();
    }
}
