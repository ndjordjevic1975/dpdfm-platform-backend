package rs.co.tntech.busride.web.rest.vm;

/**
 * View Model object for storing a parent's email.
 */
public class ParentEmailVM {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "ParentEmailVM{" +
            "email='" + email + '\'' +
            '}';
    }
}
