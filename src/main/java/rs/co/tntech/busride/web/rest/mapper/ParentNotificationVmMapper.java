package rs.co.tntech.busride.web.rest.mapper;

import org.mapstruct.Mapper;

import rs.co.tntech.busride.service.dto.ParentNotificationDTO;
import rs.co.tntech.busride.web.rest.vm.ParentNotificationVM;

/**
 * Mapper for the ParentNotificationVM and its DTO ParentNotificationDTO.
 */
@Mapper(componentModel = "spring")
public interface ParentNotificationVmMapper extends VmDtoMapper <ParentNotificationVM, ParentNotificationDTO> {
    
}
