package rs.co.tntech.busride.web.rest.mapper;

import java.util.List;

/**
 * Contract for a generic vm to entity dto.
 @param <D> - VM type parameter.
 @param <E> - DTO type parameter.
 */

public interface VmDtoMapper <D, E> {

    public E toDto(D vm);

    public D toVm(E dto);

    public List <E> toDto(List<D> vmList);

    public List <D> toVm(List<E> dtoList);
}
