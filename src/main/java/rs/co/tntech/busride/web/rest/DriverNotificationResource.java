package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.co.tntech.busride.service.DriverNotificationService;
import rs.co.tntech.busride.service.dto.DriverNotificationDTO;
import rs.co.tntech.busride.service.error.BRPBaseServiceException;
import rs.co.tntech.busride.web.rest.mapper.DriverNotificationOutVmMapper;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;
import rs.co.tntech.busride.web.rest.vm.DriverNotificationInVM;
import rs.co.tntech.busride.web.rest.vm.DriverNotificationOutVM;
import rs.co.tntech.busride.web.rest.vm.SkipSpotVM;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DriverNotification.
 */
@RestController
@RequestMapping("/api")
@Api(tags = {"driver-notifications"}, description = "Driver Notifications")
public class DriverNotificationResource {

    private final Logger log = LoggerFactory.getLogger(DriverNotificationResource.class);

    private static final String ENTITY_NAME = "driverNotification";

    @Autowired
    private DriverNotificationOutVmMapper driverNotificationOutVmMapper;
    @Autowired
    private DriverNotificationService driverNotificationService;


    /**
     * POST  /driver-notifications : Create a new driverNotification.
     *
     * @param driverNotificationDTO the driverNotificationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new driverNotificationDTO, or with status 400 (Bad Request) if the driverNotification has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/driver-notifications")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<DriverNotificationDTO> createDriverNotification(@RequestBody DriverNotificationDTO driverNotificationDTO) throws URISyntaxException {
        log.debug("REST request to save DriverNotification : {}", driverNotificationDTO);
        if (driverNotificationDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new driverNotification cannot already have an ID")).body(null);
        }
        DriverNotificationDTO result = driverNotificationService.save(driverNotificationDTO);
        return ResponseEntity.created(new URI("/api/driver-notifications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /driver-notifications : Updates an existing driverNotification.
     *
     * @param driverNotificationDTO the driverNotificationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated driverNotificationDTO,
     * or with status 400 (Bad Request) if the driverNotificationDTO is not valid,
     * or with status 500 (Internal Server Error) if the driverNotificationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/driver-notifications")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<DriverNotificationDTO> updateDriverNotification(@RequestBody DriverNotificationDTO driverNotificationDTO) throws URISyntaxException {
        log.debug("REST request to update DriverNotification : {}", driverNotificationDTO);
        if (driverNotificationDTO.getId() == null) {
            return createDriverNotification(driverNotificationDTO);
        }
        DriverNotificationDTO result = driverNotificationService.save(driverNotificationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, driverNotificationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /driver-notifications : get all the driverNotifications.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of driverNotifications in body
     */
    @GetMapping("/driver-notifications")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public List<DriverNotificationDTO> getAllDriverNotifications() {
        log.debug("REST request to get all DriverNotifications");
        return driverNotificationService.findAll();
    }

    /**
     * GET  /driver-notifications/:id : get the "id" driverNotification.
     *
     * @param id the id of the driverNotificationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the driverNotificationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/driver-notifications/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<DriverNotificationDTO> getDriverNotification(@PathVariable Long id) {
        log.debug("REST request to get DriverNotification : {}", id);
        DriverNotificationDTO driverNotificationDTO = driverNotificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(driverNotificationDTO));
    }

    /**
     * DELETE  /driver-notifications/:id : delete the "id" driverNotification.
     *
     * @param id the id of the driverNotificationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/driver-notifications/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<Void> deleteDriverNotification(@PathVariable Long id) {
        log.debug("REST request to delete DriverNotification : {}", id);
        driverNotificationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * POST  /driver-notifications/skip-student/current-driver : Skips a spot (students) during the bus ride
     *
     * @param skipSpotVM the spot to skip
     * @return the ResponseEntity with status 201 (Created) and with body the new driverNotificationDTO, or with status 400 (Bad Request) if the driverNotificationDTO is not valid,
     */
    @PostMapping("/driver-notifications/skip-spot/current-driver")
    @Timed
    public ResponseEntity<Void> createCurrentDriverNotification(@RequestBody SkipSpotVM skipSpotVM)  {
        log.debug("REST request to skip a spot for the current driver: {}", skipSpotVM);

        driverNotificationService.skipSpotForCurrentDriver(skipSpotVM);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * POST  /driver-notifications/current-driver : Create a new driverNotification for current driver.
     *
     * @param driverNotificationVM the driverNotificationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new driverNotificationDTO, or with status 400 (Bad Request) if the driverNotificationDTO is not valid,
     * @throws URISyntaxException if the Location URI syntax is incorrect
     * @throws BRPBaseServiceException
     */
    @PostMapping("/driver-notifications/current-driver")
    @Timed
    public ResponseEntity<DriverNotificationOutVM> createCurrentDriverNotification(@RequestBody DriverNotificationInVM driverNotificationVM) throws URISyntaxException, BRPBaseServiceException {
        log.debug("REST request to create a new driver notification for the current driver: {}", driverNotificationVM);

        DriverNotificationDTO result = driverNotificationService.saveForCurrentDriver(driverNotificationVM);
        return ResponseEntity.created(new URI("/api/driver-notifications/current-driver/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(driverNotificationOutVmMapper.toVm(result));
    }
}
