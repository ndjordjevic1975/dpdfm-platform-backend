package rs.co.tntech.busride.web.rest.mapper;

import org.mapstruct.Mapper;

import rs.co.tntech.busride.service.dto.BusRouteDTO;
import rs.co.tntech.busride.web.rest.vm.BusRouteVM;

/**
 * Mapper for the BusRouteVM and its DTO BusRouteDTO.
 */
@Mapper(componentModel = "spring")
public interface BusRouteVmMapper extends VmDtoMapper <BusRouteVM, BusRouteDTO> {
    
}
