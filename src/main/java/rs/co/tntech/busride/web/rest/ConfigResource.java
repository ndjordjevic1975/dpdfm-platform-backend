package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.co.tntech.busride.config.ApplicationProperties;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
@Api(tags = {"config"}, description = "Config")
public class ConfigResource {

    private final Logger log = LoggerFactory.getLogger(ConfigResource.class);

    @Autowired
    private ApplicationProperties applicationProperties;

    /**
     * GET  /config/bus-driver-live-report-refresh-rate : returns bus driver live report refresh rate
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @GetMapping("/config/bus-driver-live-report-refresh-rate")
    @Timed
    public ResponseEntity<String> getBusDriverLiveReportRefreshRate() {
        log.info("REST request to get bus driver live report refresh rate: " + applicationProperties.getBusDriverLiveReportRefreshRate());
        return new ResponseEntity<>(applicationProperties.getBusDriverLiveReportRefreshRate(), HttpStatus.OK);
    }

    /**
     * GET  /config/mgm-portal-live-report-refresh-rate : returns school management portal live report refresh rate
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @GetMapping("/config/mgm-portal-live-report-refresh-rate")
    @Timed
    public ResponseEntity<String> getMgmPortalLiveReportRefreshRate() {
        log.info("REST request to get school management portal live report refresh rate retrieved: " + applicationProperties.getMgmPortalLiveReportRefreshRate());
        return new ResponseEntity<>(applicationProperties.getMgmPortalLiveReportRefreshRate(), HttpStatus.OK);
    }
}
