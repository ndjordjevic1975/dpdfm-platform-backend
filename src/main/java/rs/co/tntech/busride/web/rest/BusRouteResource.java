package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.co.tntech.busride.domain.SchoolAdmin;
import rs.co.tntech.busride.service.AlgorithmCommunicationService;
import rs.co.tntech.busride.service.BusRouteService;
import rs.co.tntech.busride.service.dto.BusRouteDTO;
import rs.co.tntech.busride.service.dto.BusRoutePathDetailsDto;
import rs.co.tntech.busride.service.util.BRPServiceUtil;
import rs.co.tntech.busride.web.rest.mapper.BusRouteVmMapper;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;
import rs.co.tntech.busride.web.rest.vm.BusRouteVM;
import rs.co.tntech.busride.web.rest.vm.TotalBusesOnRoutesVM;
import rs.co.tntech.busride.web.rest.vm.TotalPickupDropoffVM;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * REST controller for managing BusRoute.
 */
@RestController
@RequestMapping("/api")
@Api(tags = {"bus-routes"}, description = "Bus Routes")
public class BusRouteResource {

    private final Logger log = LoggerFactory.getLogger(BusRouteResource.class);

    private static final String ENTITY_NAME = "busRoute";

    private final BusRouteService busRouteService;

    private final AlgorithmCommunicationService algorithmCommunicationService;

    @Autowired
    private BRPServiceUtil serviceUtil;

    @Autowired
    private BusRouteVmMapper busRouteVmMapper;

    public BusRouteResource(BusRouteService busRouteService, AlgorithmCommunicationService algorithmCommunicationService) {
        this.busRouteService = busRouteService;
        this.algorithmCommunicationService = algorithmCommunicationService;
    }

    /**
     * POST  /bus-routes : Create a new busRoute.
     *
     * @param busRouteDTO the busRouteDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new busRouteDTO, or with status 400 (Bad Request) if the busRoute has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bus-routes")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<BusRouteDTO> createBusRoute(@RequestBody BusRouteDTO busRouteDTO) throws URISyntaxException {
        log.debug("REST request to save BusRoute : {}", busRouteDTO);
        if (busRouteDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new busRoute cannot already have an ID")).body(null);
        }
        BusRouteDTO result = busRouteService.save(busRouteDTO);
        return ResponseEntity.created(new URI("/api/bus-routes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /bus-routes : Updates an existing busRoute.
     *
     * @param busRouteDTO the busRouteDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated busRouteDTO,
     * or with status 400 (Bad Request) if the busRouteDTO is not valid,
     * or with status 500 (Internal Server Error) if the busRouteDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/bus-routes")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<BusRouteDTO> updateBusRoute(@RequestBody BusRouteDTO busRouteDTO) throws URISyntaxException {
        log.debug("REST request to update BusRoute : {}", busRouteDTO);
        if (busRouteDTO.getId() == null) {
            return createBusRoute(busRouteDTO);
        }
        BusRouteDTO result = busRouteService.save(busRouteDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, busRouteDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /bus-routes : get all the busRoutes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of busRoutes in body
     */
    @GetMapping("/bus-routes")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public List<BusRouteDTO> getAllBusRoutes() {
        log.debug("REST request to get all BusRoutes");
        return busRouteService.findAll();
    }

    /**
     * GET  /bus-routes : get all the busRoutes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of busRoutes in body
     */
    @GetMapping("/bus-routes/current-school/path-details")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public List<BusRoutePathDetailsDto> getDetailedBusPathForCurrentSchool() {
        log.debug("REST request to get details for all BusRoutes fot current school");
        return busRouteService.findDetailedPathForCurrentSchool();
    }


    /**
     * GET  /bus-routes/:id : get the "id" busRoute.
     *
     * @param id the id of the busRouteDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the busRouteDTO, or with status 404 (Not Found)
     */
    @GetMapping("/bus-routes/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<BusRouteDTO> getBusRoute(@PathVariable Long id) {
        log.debug("REST request to get BusRoute : {}", id);
        BusRouteDTO busRouteDTO = busRouteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(busRouteDTO));
    }

    /**
     * DELETE  /bus-routes/:id : delete the "id" busRoute.
     *
     * @param id the id of the busRouteDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/bus-routes/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<Void> deleteBusRoute(@PathVariable Long id) {
        log.debug("REST request to delete BusRoute : {}", id);
        busRouteService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /bus-routes : get all the busRoutes for this driver.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of busRoutes in body
     */
    @GetMapping("/bus-routes/current-driver")
    @Timed
    public List<BusRouteVM> getAllBusRoutesForDriver() {
        log.debug("REST request to get all bus routes for the current driver");
        return busRouteService.findAllByCurrentDriver().stream()
            .map(busRouteVmMapper::toVm)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * PUT  /bus-routes/trigger-routes-calc : Create a new invocation of algorithm.
     *
     * @return the ResponseEntity with status 200 (OK),
     * or with status 500 (Internal Server  Error) if the algorithm couldn't be invoked
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/bus-routes/trigger-routes-calc/current-school-admin")
    @Timed
    public Future<ResponseEntity<String>> triggerRoutesCalc() throws URISyntaxException {
        log.debug("REST request trigger routes calculation algorithm for the current school admin");

        SchoolAdmin currentSchoolAdmin = serviceUtil.getCurrentSchoolAdmin();

        CompletableFuture<ResponseEntity<Void>> future = new CompletableFuture<>();
        return CompletableFuture.supplyAsync(()-> {
            try {
                algorithmCommunicationService.invokeAlgorithm(currentSchoolAdmin);
                return new ResponseEntity<>(HttpStatus.OK);
            } catch (Exception e) {
                log.error(e.getMessage());
                e.printStackTrace();
                return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        });
    }

    @GetMapping("/bus-routes/live-view")
    @Timed
    public List<BusRoutePathDetailsDto> getLiveViewReportForSchool() {
        log.debug("REST request to get details for all active routes and bus locations");

        return busRouteService.getAllActiveBusLocations();
    }

    @GetMapping("/bus-routes/total-pickup-dropoff")
    @Timed
    public TotalPickupDropoffVM getTotalPickupDropoff() {
        log.debug("REST request to get total pickup/dropoff routes for the current school admin");

        return busRouteService.getTotalPickupDropOff();
    }

    @GetMapping("/bus-routes/total-buses-on-routes")
    @Timed
    public TotalBusesOnRoutesVM getTotalBusesOnRoutes() {
        log.debug("REST request to get total buses on routes for the current school admin");

        return busRouteService.getTotalBusesOnRoutes();
    }
}
