package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.co.tntech.busride.service.SpotService;
import rs.co.tntech.busride.service.dto.SpotDTO;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Spot.
 */
@RestController
@RequestMapping("/api")
public class SpotResource {

    private final Logger log = LoggerFactory.getLogger(SpotResource.class);

    private static final String ENTITY_NAME = "spot";

    private final SpotService spotService;

    public SpotResource(SpotService spotService) {
        this.spotService = spotService;
    }

    /**
     * POST  /spots : Create a new spot.
     *
     * @param spotDTO the spotDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new spotDTO, or with status 400 (Bad Request) if the spot has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/spots")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<SpotDTO> createSpot(@RequestBody SpotDTO spotDTO) throws URISyntaxException {
        log.debug("REST request to save Spot : {}", spotDTO);
        if (spotDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new spot cannot already have an ID")).body(null);
        }
        if (spotDTO.getGpsLatitude() == null || spotDTO.getGpsLongitude() == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("latitude and longitude", "NotNull", "A new spot must have latitude and longitude values")).body(null);
        }
        SpotDTO result = spotService.save(spotDTO);
        return ResponseEntity.created(new URI("/api/spots/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /spots : Updates an existing spot.
     *
     * @param spotDTO the spotDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated spotDTO,
     * or with status 400 (Bad Request) if the spotDTO is not valid,
     * or with status 500 (Internal Server Error) if the spotDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/spots")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<SpotDTO> updateSpot(@RequestBody SpotDTO spotDTO) throws URISyntaxException {
        log.debug("REST request to update Spot : {}", spotDTO);
        if (spotDTO.getId() == null) {
            return createSpot(spotDTO);
        }
        SpotDTO result = spotService.save(spotDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, spotDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /spots : get all the spots.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of spots in body
     */
    @GetMapping("/spots")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public List<SpotDTO> getAllSpots() {
        log.debug("REST request to get all Spots");
        return spotService.findAll();
    }

    /**
     * GET  /spots/:id : get the "id" spot.
     *
     * @param id the id of the spotDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the spotDTO, or with status 404 (Not Found)
     */
    @GetMapping("/spots/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<SpotDTO> getSpot(@PathVariable Long id) {
        log.debug("REST request to get Spot : {}", id);
        SpotDTO spotDTO = spotService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(spotDTO));
    }

    /**
     * DELETE  /spots/:id : delete the "id" spot.
     *
     * @param id the id of the spotDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/spots/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<Void> deleteSpot(@PathVariable Long id) {
        log.debug("REST request to delete Spot : {}", id);
        spotService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/spots/current-school")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<SpotDTO> getByCurrentSchool() {
        log.debug("REST request to get a spot for the school");

        return ResponseEntity.ok(spotService.getForCurrentSchool());
    }
}
