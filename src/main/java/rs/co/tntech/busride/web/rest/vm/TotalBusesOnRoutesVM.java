package rs.co.tntech.busride.web.rest.vm;

public class TotalBusesOnRoutesVM {
    private int totalBusesOnRoutes;

    public TotalBusesOnRoutesVM(int totalBusesOnRoutes) {
        this.totalBusesOnRoutes = totalBusesOnRoutes;
    }

    public int getTotalBusesOnRoutes() {
        return totalBusesOnRoutes;
    }

    public void setTotalBusesOnRoutes(int totalBusesOnRoutes) {
        this.totalBusesOnRoutes = totalBusesOnRoutes;
    }

    @Override
    public String toString() {
        return "TotalBusesOnRoutesVM{" +
            "totalBusesOnRoutes=" + totalBusesOnRoutes +
            '}';
    }
}
