package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import rs.co.tntech.busride.domain.enumeration.DeviceType;
import rs.co.tntech.busride.service.ParentService;
import rs.co.tntech.busride.service.dto.GCMSessionDTO;
import rs.co.tntech.busride.service.dto.ParentDTO;
import rs.co.tntech.busride.service.dto.ParentExtendedDTO;
import rs.co.tntech.busride.service.error.BRPBaseServiceException;
import rs.co.tntech.busride.service.error.BRPFieldException;
import rs.co.tntech.busride.service.error.BRPServiceExcpetion;
import rs.co.tntech.busride.web.rest.mapper.ParentCreateVmMapper;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;
import rs.co.tntech.busride.web.rest.vm.ParentCreateVM;
import rs.co.tntech.busride.web.rest.vm.ParentUpdateVM;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Parent.
 */
@RestController
@RequestMapping("/api")
@Api(tags = {"parents"}, description = "Parents")
public class ParentResource {

    private static final String ENTITY_NAME = "parent";
    private final Logger log = LoggerFactory.getLogger(ParentResource.class);
    @Autowired
    private ParentService parentService;
    @Autowired
    private ParentCreateVmMapper parentCreateVmMapper;

    /**
     * POST  /parents : Create a new parent.
     *
     * @param parentDTO the parentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new parentDTO, or with status 400 (Bad Request) if the parent has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/parents")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<ParentDTO> createParent(@RequestBody ParentDTO parentDTO) throws URISyntaxException {
        log.debug("REST request to save Parent : {}", parentDTO);
        if (parentDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new parent cannot already have an ID")).body(null);
        }
        ParentDTO result = parentService.save(parentDTO);
        return ResponseEntity.created(new URI("/api/parents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /parents : Updates an existing parent.
     *
     * @param parentDTO the parentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated parentDTO,
     * or with status 400 (Bad Request) if the parentDTO is not valid,
     * or with status 500 (Internal Server Error) if the parentDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/parents")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<ParentDTO> updateParent(@RequestBody ParentDTO parentDTO) throws URISyntaxException {
        log.debug("REST request to update Parent : {}", parentDTO);
        if (parentDTO.getId() == null) {
            log.debug("API:PUT:parents: parent id is null - create new parent");
            return createParent(parentDTO);
        }
        ParentDTO result = parentService.save(parentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, parentDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /parents : get all the parents.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of parents in body
     */
    @GetMapping("/parents")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public List<ParentDTO> getAllParents() {
        log.debug("REST request to get all Parents");
        return parentService.findAll();
    }

    /**
     * GET  /parents/:id : get the "id" parent.
     *
     * @param id the id of the parentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the parentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/parents/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<ParentDTO> getParent(@PathVariable Long id) {
        log.debug("REST request to get Parent : {}", id);
        ParentDTO parentDTO = parentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(parentDTO));
    }

    /**
     * DELETE  /parents/:id : delete the "id" parent.
     *
     * @param id the id of the parentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/parents/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<Void> deleteParent(@PathVariable Long id) {
        log.debug("REST request to delete Parent : {}", id);
        parentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * POST  /parents/list : Create new parents. First parent in the list is primary parent.
     *
     * @param parentVMs the list of parentVMs to create
     * @return the ResponseEntity with status 201 (Created) and with body the new parentDTOs
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/parents/list")
    @Timed
    public ResponseEntity<List<ParentExtendedDTO>> createListOfParents(@RequestBody List<ParentCreateVM> parentVMs) throws BRPBaseServiceException, URISyntaxException {
    	log.debug("REST request to create list of parents : {}", parentVMs);
    	List<ParentExtendedDTO> parentDTOs = parentCreateVmMapper.toDto(parentVMs);
        ParentCreateVM primaryParent = parentVMs.get(0);
        List<ParentExtendedDTO> result = parentService.saveList(parentDTOs, primaryParent.getSubscriptionTypes());
    	return ResponseEntity.created(new URI("/api/parents/list")).body(result);
    }

    /**
     * GET /parents/current-parent : Get logged in Parent
     *
     * @return ParentDTO
     */
    @GetMapping("/parents/current-parent")
    @Timed
    public ResponseEntity<ParentExtendedDTO> getCurrentParent() {
        log.debug("REST request to get currently logged in parent");

        ParentExtendedDTO parentDTO = parentService.getCurrentParent();
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(parentDTO));
    }

    @PutMapping("/parents/refresh-GCM-Session-Id")
    @Timed
    public ResponseEntity<ParentExtendedDTO> updateParentGCMSessionId(@Valid @RequestBody GCMSessionDTO gcmSessionDTO) throws BRPBaseServiceException {
        log.debug("REST request to update parent's gcm session id: {}", gcmSessionDTO);

        String gcmSessionId = gcmSessionDTO.getGcmSessionId();
        DeviceType deviceType = gcmSessionDTO.getDeviceType();

        Optional<ParentExtendedDTO> updatedParentDTO = parentService.updateCurrentParentGCMSessionId(gcmSessionId, deviceType);

        return ResponseUtil.wrapOrNotFound(updatedParentDTO);

    }

    /**
     * PUT /parents/current-parent : update logged in Parent
     *
     * @return extended parent object
     */
    @PutMapping("/parents/current-parent")
    @Timed
    public ResponseEntity<ParentExtendedDTO> updateCurrentParent(@RequestBody ParentUpdateVM parentUpdateVM) throws BRPServiceExcpetion, BRPFieldException {
        log.debug("REST request to update logged in parent: ", parentUpdateVM);

        Optional<ParentExtendedDTO> updatedParentDTO = parentService.updateCurrentParent(parentUpdateVM);

        return ResponseUtil.wrapOrNotFound(updatedParentDTO);
    }
}
