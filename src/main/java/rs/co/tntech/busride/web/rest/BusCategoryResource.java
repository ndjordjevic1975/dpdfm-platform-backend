package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.co.tntech.busride.service.BusCategoryService;
import rs.co.tntech.busride.service.dto.BusCategoryDTO;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BusCategory.
 */
@RestController
@RequestMapping("/api")
public class BusCategoryResource {

    private final Logger log = LoggerFactory.getLogger(BusCategoryResource.class);

    private static final String ENTITY_NAME = "busCategory";

    private final BusCategoryService busCategoryService;

    public BusCategoryResource(BusCategoryService busCategoryService) {
        this.busCategoryService = busCategoryService;
    }

    /**
     * POST  /bus-categories : Create a new busCategory.
     *
     * @param busCategoryDTO the busCategoryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new busCategoryDTO, or with status 400 (Bad Request) if the busCategory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bus-categories")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public ResponseEntity<BusCategoryDTO> createBusCategory(@RequestBody BusCategoryDTO busCategoryDTO) throws URISyntaxException {
        log.debug("REST request to save BusCategory : {}", busCategoryDTO);
        if (busCategoryDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new busCategory cannot already have an ID")).body(null);
        }
        BusCategoryDTO result = busCategoryService.save(busCategoryDTO);
        return ResponseEntity.created(new URI("/api/bus-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /bus-categories : Updates an existing busCategory.
     *
     * @param busCategoryDTO the busCategoryDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated busCategoryDTO,
     * or with status 400 (Bad Request) if the busCategoryDTO is not valid,
     * or with status 500 (Internal Server Error) if the busCategoryDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/bus-categories")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public ResponseEntity<BusCategoryDTO> updateBusCategory(@RequestBody BusCategoryDTO busCategoryDTO) throws URISyntaxException {
        log.debug("REST request to update BusCategory : {}", busCategoryDTO);
        if (busCategoryDTO.getId() == null) {
            return createBusCategory(busCategoryDTO);
        }
        BusCategoryDTO result = busCategoryService.save(busCategoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, busCategoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /bus-categories : get all the busCategories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of busCategories in body
     */
    @GetMapping("/bus-categories")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public List<BusCategoryDTO> getAllBusCategories() {
        log.debug("REST request to get all BusCategories");
        return busCategoryService.findAll();
    }

    /**
     * GET  /bus-categories/:id : get the "id" busCategory.
     *
     * @param id the id of the busCategoryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the busCategoryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/bus-categories/{id}")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public ResponseEntity<BusCategoryDTO> getBusCategory(@PathVariable Long id) {
        log.debug("REST request to get BusCategory : {}", id);
        BusCategoryDTO busCategoryDTO = busCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(busCategoryDTO));
    }

    /**
     * DELETE  /bus-categories/:id : delete the "id" busCategory.
     *
     * @param id the id of the busCategoryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/bus-categories/{id}")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public ResponseEntity<Void> deleteBusCategory(@PathVariable Long id) {
        log.debug("REST request to delete BusCategory : {}", id);
        busCategoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /bus-categories : get all the busCategories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of busCategories in body
     */
    @GetMapping("/bus-categories/current-school-admin")
    @Timed
    public ResponseEntity<List<BusCategoryDTO>> getAllBusCategoriesForSchoolAdmin() {
        log.debug("REST request to get all school BusCategories");

        List<BusCategoryDTO> result = busCategoryService.findAllBusCategoriesBySchoolAdminUserId();

        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(result));
    }

    @PostMapping("/bus-categories/current-school-admin")
    @Timed
    public ResponseEntity<BusCategoryDTO> saveBusCategoryForSchoolAdmin(@RequestBody BusCategoryDTO busCategoryDTO) {
        log.debug("REST request to save BusCategory by School admin: {}", busCategoryDTO);

        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(busCategoryService.saveBusCategoriesBySchoolAdmin(busCategoryDTO)));
    }
}
