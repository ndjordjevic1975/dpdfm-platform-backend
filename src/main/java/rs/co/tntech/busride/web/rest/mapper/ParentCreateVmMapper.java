package rs.co.tntech.busride.web.rest.mapper;

import org.mapstruct.Mapper;

import rs.co.tntech.busride.service.dto.ParentExtendedDTO;
import rs.co.tntech.busride.web.rest.vm.ParentCreateVM;

/**
 * Mapper for the ParentCreateVM and its DTO ParentExtendedDTO.
 */
@Mapper(componentModel = "spring")
public interface ParentCreateVmMapper extends VmDtoMapper <ParentCreateVM, ParentExtendedDTO> {
    
}
