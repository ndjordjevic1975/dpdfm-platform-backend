package rs.co.tntech.busride.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.co.tntech.busride.service.dto.TempServiceCancellationDTO;

import javax.validation.constraints.NotNull;

public class TempServiceCancellationParentVM extends TempServiceCancellationDTO {

	private static final long serialVersionUID = 1L;

    @Override
    @NotNull
    public Long getStudentId() {
        return super.getStudentId();
    }

    @Override
    @JsonIgnore
    public Long getSpotId() {
        return super.getSpotId();
    }

    @JsonIgnore
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    public String toString() {
        return "{ " +
            "\"cancellationType\": \"" + getCancellationType() + ", " +
            "\"reason\": \"" + getReason() + ", " +
            "\"studentId\": \"" + super.getStudentId() +"\""
            +"}";
    }
}
