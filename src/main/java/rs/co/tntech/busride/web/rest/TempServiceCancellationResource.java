package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.co.tntech.busride.service.TempServiceCancellationService;
import rs.co.tntech.busride.service.dto.TempServiceCancellationDTO;
import rs.co.tntech.busride.service.dto.TempServiceCancellationMessageDTO;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;
import rs.co.tntech.busride.web.rest.vm.TempServiceCancellationParentGetVM;
import rs.co.tntech.busride.web.rest.vm.TempServiceCancellationParentVM;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing TempServiceCancellation.
 */
@RestController
@RequestMapping("/api")
@Api(tags = {"temp-service-cancellations"}, description = "Temp Service Cancellations")
public class TempServiceCancellationResource {

    private final Logger log = LoggerFactory.getLogger(TempServiceCancellationResource.class);

    private static final String ENTITY_NAME = "tempServiceCancellation";

    @Autowired
    private TempServiceCancellationService tempServiceCancellationService;

    /**
     * POST  /temp-service-cancellations : Create a new tempServiceCancellation.
     *
     * @param tempServiceCancellationDTO the tempServiceCancellationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tempServiceCancellationDTO, or with status 400 (Bad Request) if the tempServiceCancellation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/temp-service-cancellations")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<TempServiceCancellationDTO> createTempServiceCancellation(@RequestBody TempServiceCancellationDTO tempServiceCancellationDTO) throws URISyntaxException {

        if (tempServiceCancellationDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tempServiceCancellation cannot already have an ID")).body(null);
        }
        TempServiceCancellationDTO result = tempServiceCancellationService.save(tempServiceCancellationDTO);

        return ResponseEntity.created(new URI("/api/temp-service-cancellations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /temp-service-cancellations : Updates an existing tempServiceCancellation.
     *
     * @param tempServiceCancellationDTO the tempServiceCancellationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tempServiceCancellationDTO,
     * or with status 400 (Bad Request) if the tempServiceCancellationDTO is not valid,
     * or with status 500 (Internal Server Error) if the tempServiceCancellationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/temp-service-cancellations")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<TempServiceCancellationDTO> updateTempServiceCancellation(@RequestBody TempServiceCancellationDTO tempServiceCancellationDTO) throws URISyntaxException {

        if (tempServiceCancellationDTO.getId() == null) {
            return createTempServiceCancellation(tempServiceCancellationDTO);
        }
        TempServiceCancellationDTO result = tempServiceCancellationService.save(tempServiceCancellationDTO);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tempServiceCancellationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /temp-service-cancellations : get all the tempServiceCancellations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tempServiceCancellations in body
     */
    @GetMapping("/temp-service-cancellations")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public List<TempServiceCancellationDTO> getAllTempServiceCancellations() {
        return tempServiceCancellationService.findAll();
    }

    /**
     * GET  /temp-service-cancellations/:id : get the "id" tempServiceCancellation.
     *
     * @param id the id of the tempServiceCancellationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tempServiceCancellationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/temp-service-cancellations/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<TempServiceCancellationDTO> getTempServiceCancellation(@PathVariable Long id) {
        TempServiceCancellationDTO tempServiceCancellationDTO = tempServiceCancellationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tempServiceCancellationDTO));
    }

    /**
     * DELETE  /temp-service-cancellations/:id : delete the "id" tempServiceCancellation.
     *
     * @param id the id of the tempServiceCancellationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/temp-service-cancellations/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<Void> deleteTempServiceCancellation(@PathVariable Long id) {
        tempServiceCancellationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * @return
     */
    @GetMapping("/temp-service-cancellations/current-driver")
    @Timed
    public ResponseEntity<List<TempServiceCancellationMessageDTO>> getAllTempCancellationsForActiveRoute() {
        log.debug("REST request to get a list of temp service cancellations for the current active route");

        Optional<List<TempServiceCancellationMessageDTO>> result = tempServiceCancellationService.findAllTempCancellationsForActiveRoute();

        return ResponseUtil.wrapOrNotFound(result);
    }

    @GetMapping("/temp-service-cancellations/current-parent")
    @Timed
    public ResponseEntity<List<TempServiceCancellationParentGetVM>> getAllTempCancellationsForCurrentParent() {
        log.debug("REST request to get a list of temp service cancellations for the current parent");

        List<TempServiceCancellationParentGetVM> result = null;

        Optional<List<TempServiceCancellationDTO>> list = tempServiceCancellationService.findAllTempCancellationsForLoggedInParent();
        if (list.isPresent()) {
            result = list.get().stream()
                .map(this::mapTempServiceCancellationParentGetVM)
                .collect(Collectors.toList());
        }

        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(result));
    }

    @PostMapping("/temp-service-cancellations/current-parent")
    @Timed
    public ResponseEntity createOrDeleteTempCancellationsForCurrentParent(@RequestBody @Valid TempServiceCancellationParentVM tempServiceCancellation) {
        log.debug("REST request to create or delete a new temp service cancellation for the current parent");

        switch (tempServiceCancellation.getCancellationType()) {
            case PICKUP:
            case DROPOFF:
            case ROUND_TRIP:
                Optional<TempServiceCancellationDTO> tempServiceCancellationDTO =
                    tempServiceCancellationService.saveTempServiceCancellationParentRecord(tempServiceCancellation.getStudentId(), tempServiceCancellation.getCancellationType(), tempServiceCancellation.getReason());

                return ResponseUtil.wrapOrNotFound(tempServiceCancellationDTO);
            case OFF:
                Integer deleteResult = tempServiceCancellationService.deleteTempServiceCancellationParentRecord(tempServiceCancellation.getStudentId());
                return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME,
                    "StudentId: " + tempServiceCancellation.getStudentId()
                        + ",SpotId:" + tempServiceCancellation.getSpotId()
                        + ",Deleted: " + deleteResult)).build();
        }

        return ResponseEntity.badRequest().build();

    }

    private TempServiceCancellationParentGetVM mapTempServiceCancellationParentGetVM(TempServiceCancellationDTO tmpCancellationDTO) {
        TempServiceCancellationParentGetVM tmpCancellationGetVM = new TempServiceCancellationParentGetVM();
        tmpCancellationGetVM.setStudentId(tmpCancellationDTO.getStudentId());
        tmpCancellationGetVM.setReason(tmpCancellationDTO.getReason());
        tmpCancellationGetVM.setCancellationType(tmpCancellationDTO.getCancellationType());
        tmpCancellationGetVM.setId(tmpCancellationDTO.getId());
        return tmpCancellationGetVM;
    }

}
