package rs.co.tntech.busride.web.rest.vm;

public class LocaleVM {

    private String locale;

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
