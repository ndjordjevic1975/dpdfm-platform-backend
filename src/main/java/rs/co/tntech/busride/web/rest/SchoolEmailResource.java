package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.co.tntech.busride.service.SchoolEmailService;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;
import rs.co.tntech.busride.web.rest.vm.SchoolEmailCreateVM;
import rs.co.tntech.busride.service.dto.SchoolEmailDTO;
import rs.co.tntech.busride.service.error.BRPBaseServiceException;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SchoolEmail.
 */
@RestController
@RequestMapping("/api")
@Api(tags = { "school-emails" }, description = "School Emails")
public class SchoolEmailResource {

    private final Logger log = LoggerFactory.getLogger(SchoolEmailResource.class);

    private static final String ENTITY_NAME = "schoolEmail";

    @Autowired
    private SchoolEmailService schoolEmailService;

    /**
     * POST  /school-emails : Create a new schoolEmail.
     *
     * @param schoolEmailDTO the schoolEmailDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new schoolEmailDTO, or with status 400 (Bad Request) if the schoolEmail has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/school-emails")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public ResponseEntity<SchoolEmailDTO> createSchoolEmail(@RequestBody SchoolEmailDTO schoolEmailDTO) throws URISyntaxException {
        log.debug("REST request to save SchoolEmail : {}", schoolEmailDTO);
        if (schoolEmailDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new schoolEmail cannot already have an ID")).body(null);
        }
        SchoolEmailDTO result = schoolEmailService.save(schoolEmailDTO);
        return ResponseEntity.created(new URI("/api/school-emails/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /school-emails : Updates an existing schoolEmail.
     *
     * @param schoolEmailDTO the schoolEmailDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated schoolEmailDTO,
     * or with status 400 (Bad Request) if the schoolEmailDTO is not valid,
     * or with status 500 (Internal Server Error) if the schoolEmailDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/school-emails")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public ResponseEntity<SchoolEmailDTO> updateSchoolEmail(@RequestBody SchoolEmailDTO schoolEmailDTO) throws URISyntaxException {
        log.debug("REST request to update SchoolEmail : {}", schoolEmailDTO);
        if (schoolEmailDTO.getId() == null) {
            return createSchoolEmail(schoolEmailDTO);
        }
        SchoolEmailDTO result = schoolEmailService.save(schoolEmailDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, schoolEmailDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /school-emails : get all the schoolEmails.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of schoolEmails in body
     */
    @GetMapping("/school-emails")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public List<SchoolEmailDTO> getAllSchoolEmails() {
        log.debug("REST request to get all SchoolEmails");
        return schoolEmailService.findAll();
    }

    /**
     * GET  /school-emails/:id : get the "id" schoolEmail.
     *
     * @param id the id of the schoolEmailDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the schoolEmailDTO, or with status 404 (Not Found)
     */
    @GetMapping("/school-emails/{id}")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public ResponseEntity<SchoolEmailDTO> getSchoolEmail(@PathVariable Long id) {
        log.debug("REST request to get SchoolEmail : {}", id);
        SchoolEmailDTO schoolEmailDTO = schoolEmailService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(schoolEmailDTO));
    }

    /**
     * DELETE  /school-emails/:id : delete the "id" schoolEmail.
     *
     * @param id the id of the schoolEmailDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/school-emails/{id}")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public ResponseEntity<Void> deleteSchoolEmail(@PathVariable Long id) {
        log.debug("REST request to delete SchoolEmail : {}", id);
        schoolEmailService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * POST  /school-emails/current-school-admin : Create a new school email by current school admin and send it to all school parents.
     *
     * @param schoolEmailVM the schoolEmailVM to create
     * @return the ResponseEntity with status 201 (Created) and with body the new schoolEmailDTO, or with status 400 (Bad Request) if the schoolEmail has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     * @throws BRPBaseServiceException
     */
    @PostMapping("/school-emails/current-school-admin")
    @Timed
    public ResponseEntity<SchoolEmailDTO> createSchoolEmailForCurrentSchoolAdmin(@RequestBody SchoolEmailCreateVM schoolEmailVM) throws URISyntaxException, BRPBaseServiceException {
        log.debug("REST request to save SchoolEmail for current school admin : {}", schoolEmailVM);
        SchoolEmailDTO result = schoolEmailService.saveCurrentSchoolAdminAndSendToAllSchoolParents(schoolEmailVM);
        return ResponseEntity.created(new URI("/api/school-emails/current-school-admin/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * DELETE  /school-emails/current-school-admin/all : delete all the school emails for current school admin.
     *
     * @return the ResponseEntity with status 200 (OK)
     * @throws BRPBaseServiceException
     */
    @DeleteMapping("/school-emails/current-school-admin")
    @Timed
	public ResponseEntity<Void> deleteAllSchoolEmailsForCurrentSchoolAdmin() throws BRPBaseServiceException {
		log.debug("REST request to delete all SchoolEmails for current school admin");
		schoolEmailService.deleteAllForCurrentSchoolAdmin();
		return ResponseEntity.ok().build();
	}

    /**
     * DELETE  /school-emails/current-school-admin/:id : delete the "id" schoolEmail.
     *
     * @param id the id of the schoolEmailDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     * @throws BRPBaseServiceException
     */
    @DeleteMapping("/school-emails/current-school-admin/{id:[0-9]+}")
    @Timed
    public ResponseEntity<Void> deleteSchoolEmailForCurrentSchoolAdmin(@PathVariable Long id) throws BRPBaseServiceException {
        log.debug("REST request to delete SchoolEmail for current school admin : {}", id);
        schoolEmailService.deleteForCurrentSchoolAdmin(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /school-emails/current-school-admin : get all the school emails for current school admin.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of schoolEmails in body
     * @throws BRPBaseServiceException
     */
    @GetMapping("/school-emails/current-school-admin")
    @Timed
    public List<SchoolEmailDTO> getAllSchoolEmailsForCurrentSchoolAdmin() throws BRPBaseServiceException {
        log.debug("REST request to get all SchoolEmails for current school admin");
        return schoolEmailService.getAllForCurrentSchoolAdmin();
    }

}
