package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.co.tntech.busride.domain.LiveViewReportData;
import rs.co.tntech.busride.service.DriverService;
import rs.co.tntech.busride.service.dto.BusRouteIdDTO;
import rs.co.tntech.busride.service.dto.DriverDTO;
import rs.co.tntech.busride.service.dto.DriverIdentifierDTO;
import rs.co.tntech.busride.service.dto.GCMSessionDTO;
import rs.co.tntech.busride.service.error.BRPFieldsExcpetion;
import rs.co.tntech.busride.service.error.BRPServiceSystemException;
import rs.co.tntech.busride.web.rest.mapper.DriverVmMapper;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;
import rs.co.tntech.busride.web.rest.vm.DriverVM;
import rs.co.tntech.busride.web.rest.vm.UpdateLiveViewReportVM;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Driver.
 */
@RestController
@RequestMapping("/api")
@Api(tags = {"drivers"}, description = "Drivers")
public class DriverResource {

    private final Logger log = LoggerFactory.getLogger(DriverResource.class);

    private static final String ENTITY_NAME = "driver";

    @Autowired
    private DriverVmMapper driverVmMapper;

    private DriverService driverService;

    public DriverResource(DriverService driverService) {
        this.driverService = driverService;
    }

    /**
     * POST  /drivers : Create a new driver.
     *
     * @param driverDTO the driverDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new driverDTO, or with status 400 (Bad Request) if the driver has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/drivers")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<DriverDTO> createDriver(@RequestBody DriverDTO driverDTO) throws URISyntaxException {
        log.debug("REST request to save Driver : {}", driverDTO);
        if (driverDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new driver cannot already have an ID")).body(null);
        }
        DriverDTO result = driverService.save(driverDTO);
        return ResponseEntity.created(new URI("/api/drivers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /drivers : Updates an existing driver.
     *
     * @param driverDTO the driverDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated driverDTO,
     * or with status 400 (Bad Request) if the driverDTO is not valid,
     * or with status 500 (Internal Server Error) if the driverDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/drivers")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<DriverDTO> updateDriver(@RequestBody DriverDTO driverDTO) throws URISyntaxException {
        log.debug("REST request to update Driver : {}", driverDTO);
        if (driverDTO.getId() == null) {
            return createDriver(driverDTO);
        }
        DriverDTO result = driverService.save(driverDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, driverDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /drivers : get all the drivers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of drivers in body
     */
    @GetMapping("/drivers")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public List<DriverDTO> getAllDrivers() {
        log.debug("REST request to get all Drivers");
        return driverService.findAll();
    }

    /**
     * GET  /drivers/:id : get the "id" driver.
     *
     * @param id the id of the driverDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the driverDTO, or with status 404 (Not Found)
     */
    @GetMapping("/drivers/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<DriverDTO> getDriver(@PathVariable Long id) {
        log.debug("REST request to get Driver : {}", id);
        DriverDTO driverDTO = driverService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(driverDTO));
    }

    /**
     * DELETE  /drivers/:id : delete the "id" driver.
     *
     * @param id the id of the driver to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/drivers/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<Void> deleteDriver(@PathVariable Long id) {
        log.debug("REST request to delete Driver : {}", id);
        driverService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * Updates DRIVER User password and deviceIdentifier
     *
     * @param driverIdentifier containing deviceIdentifier and driver id
     * @return DriverVM
     */
    @PutMapping("/drivers/current-school-admin")
    @Timed
    public ResponseEntity<DriverVM> updateCurrentDriver(@Valid @RequestBody DriverIdentifierDTO driverIdentifier) {
        log.debug("REST request to update current driver upon on-boarding: {}", driverIdentifier);

        Optional<DriverDTO> driver = driverService.updateDriverPwdAndIdentifier(driverIdentifier);
        return ResponseUtil.wrapOrNotFound(driver.map(driverVmMapper::toVm));
    }

    /**
     * PUT /drivers/refresh-GCM-Session-Id : assigns or updates gcmSessionId to the Driver
     *
     * @param gcmSessionDTO containing only gcmSessionId
     * @return DriverDTO
     */
    @PutMapping("/drivers/refresh-GCM-Session-Id")
    @Timed
    public ResponseEntity<DriverVM> refreshGCMSessionId(@Valid @RequestBody GCMSessionDTO gcmSessionDTO) throws BRPServiceSystemException, BRPFieldsExcpetion {
        log.debug("REST request to refresh gcm session id: {}", gcmSessionDTO);

        String gcmSessionId = gcmSessionDTO.getGcmSessionId();
        Optional<DriverVM> updatedDriverVM = driverService.updateDriverGCMSessionId(gcmSessionId).map(driverVmMapper::toVm);
        return ResponseUtil.wrapOrNotFound(updatedDriverVM,
            HeaderUtil.createAlert("driver.gcmSessionId.updated", String.valueOf(gcmSessionDTO.getGcmSessionId())));
    }

    /**
     * PUT /api/drivers/assign-route-to-driver : assigns or updates route to the Driver
     *
     * @param busRouteIdDTO containing only routeId
     * @return
     */
    @PutMapping("/drivers/assign-route-to-driver")
    @Timed
    public ResponseEntity<DriverVM> assignRouteToDriver(@Valid @RequestBody BusRouteIdDTO busRouteIdDTO) {
        log.debug("REST request to assign route to a driver: {}", busRouteIdDTO);

        Optional<DriverVM> assignedDriverRoute = driverService.assignRouteToDriver(busRouteIdDTO.getBusRouteId()).map(driverVmMapper::toVm);
        return ResponseUtil.wrapOrNotFound(assignedDriverRoute,
            HeaderUtil.createEntityUpdateAlert("driver.busRoute.id", String.valueOf(busRouteIdDTO.getBusRouteId())));
    }

    /**
     * PUT /api/drivers/update-live-view-report : updated live view report
     *
     * @param updateLiveViewReportVM containing data for updating live view report
     */
    @PutMapping("/drivers/update-live-view-report")
    @Timed
    public ResponseEntity<Void> updateLiveViewReport(@Valid @RequestBody UpdateLiveViewReportVM updateLiveViewReportVM) {
        log.debug("REST request to update live view report data: {}", updateLiveViewReportVM);

        driverService.updateLiveReportView(updateLiveViewReportVM);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * GET  /drivers/get-live-view-report : get the live view report data for a current driver
     *
     * @return the ResponseEntity with status 200 (OK) and live report data with a current driver
     */
    @GetMapping("/drivers/get-live-view-report")
    @Timed
    public ResponseEntity<HashMap<Long, LiveViewReportData>> getLiveViewReport() {
        log.debug("REST request to get live view report data for a current driver");

        return new ResponseEntity<>(driverService.getLiveViewReport(), HttpStatus.OK);
    }
}
