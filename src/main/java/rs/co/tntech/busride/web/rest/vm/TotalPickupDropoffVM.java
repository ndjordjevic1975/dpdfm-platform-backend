package rs.co.tntech.busride.web.rest.vm;

public class TotalPickupDropoffVM {
    private int totalPickup;
    private int totalDropoff;

    public TotalPickupDropoffVM(int totalPickup, int totalDropoff) {
        this.totalPickup = totalPickup;
        this.totalDropoff = totalDropoff;
    }

    public int getTotalPickup() {
        return totalPickup;
    }

    public void setTotalPickup(int totalPickup) {
        this.totalPickup = totalPickup;
    }

    public int getTotalDropoff() {
        return totalDropoff;
    }

    public void setTotalDropoff(int totalDropoff) {
        this.totalDropoff = totalDropoff;
    }

    @Override
    public String toString() {
        return "TotalPickupDropoffVM{" +
            "totalPickup=" + totalPickup +
            ", totalDropoff=" + totalDropoff +
            '}';
    }
}
