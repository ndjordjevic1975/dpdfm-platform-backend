package rs.co.tntech.busride.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.co.tntech.busride.service.dto.DriverDTO;

public class DriverVM extends DriverDTO{

    private static final long serialVersionUID = 1L;

    @Override
    @JsonIgnore
    public Long getUserId() {
        return super.getUserId();
    }

    @Override
    @JsonIgnore
    public Long getSchoolId() {
        return super.getSchoolId();
    }

    @Override
    @JsonIgnore
    public Long getActiveBusRouteId() {
        return super.getActiveBusRouteId();
    }

}

