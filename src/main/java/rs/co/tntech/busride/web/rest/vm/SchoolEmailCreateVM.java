package rs.co.tntech.busride.web.rest.vm;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.co.tntech.busride.domain.SchoolEmail;
import rs.co.tntech.busride.service.dto.SchoolEmailDTO;

/**
 * View Model object of {@link SchoolEmail} for create action
 * 
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class SchoolEmailCreateVM extends SchoolEmailDTO {

	private static final long serialVersionUID = 1L;

	@Override
	@JsonIgnore
	public Long getId() {
		return super.getId();
	}

	@Override
	@JsonIgnore
	public Instant getTimestamp() {
		return super.getTimestamp();
	}

	@Override
	@JsonIgnore
	public Long getSchoolId() {
		return super.getSchoolId();
	}

}
