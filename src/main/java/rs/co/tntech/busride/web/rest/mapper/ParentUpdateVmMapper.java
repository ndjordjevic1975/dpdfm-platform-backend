package rs.co.tntech.busride.web.rest.mapper;

import org.mapstruct.Mapper;
import rs.co.tntech.busride.service.dto.ParentExtendedDTO;
import rs.co.tntech.busride.web.rest.vm.ParentUpdateVM;

/**
 * Mapper for the ParentUpdateVM and its DTO ParentExtendedDTO.
 */
@Mapper(componentModel = "spring")
public interface ParentUpdateVmMapper extends VmDtoMapper<ParentUpdateVM, ParentExtendedDTO> {

}
