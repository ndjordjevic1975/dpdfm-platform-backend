/**
 * 
 */
package rs.co.tntech.busride.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.co.tntech.busride.domain.ParentNotification;
import rs.co.tntech.busride.service.dto.ParentNotificationDTO;

/**
 * View Model object of {@link ParentNotification}
 * 
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class ParentNotificationVM extends ParentNotificationDTO {

	private static final long serialVersionUID = 1L;

	@Override
	@JsonIgnore
	public Long getParentId() {
		return super.getParentId();
	}

}
