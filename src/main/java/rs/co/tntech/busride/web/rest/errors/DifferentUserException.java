package rs.co.tntech.busride.web.rest.errors;

public class DifferentUserException extends IllegalArgumentException {
    public DifferentUserException(String msg) {
        super(msg);
    }
}



