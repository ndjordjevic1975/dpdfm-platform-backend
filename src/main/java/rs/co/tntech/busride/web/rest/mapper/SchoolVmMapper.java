package rs.co.tntech.busride.web.rest.mapper;

import org.mapstruct.Mapper;

import rs.co.tntech.busride.service.dto.SchoolDTO;
import rs.co.tntech.busride.web.rest.vm.SchoolVM;

/**
 * Mapper for the SchoolVM and its DTO SchoolDTO.
 */
@Mapper(componentModel = "spring")
public interface SchoolVmMapper extends VmDtoMapper <SchoolVM, SchoolDTO> {
    
}
