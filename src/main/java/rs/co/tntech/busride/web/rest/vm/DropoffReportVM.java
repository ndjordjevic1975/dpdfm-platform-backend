package rs.co.tntech.busride.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.co.tntech.busride.domain.DropoffReport;
import rs.co.tntech.busride.service.dto.DropoffReportDTO;

/**
 * View Model object of {@link DropoffReport}
 * 
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class DropoffReportVM extends DropoffReportDTO {

	private static final long serialVersionUID = 1L;

	@Override
	@JsonIgnore
	public Long getId() {
		return super.getId();
	}

	@Override
	@JsonIgnore
	public Long getBusRouteId() {
		return super.getBusRouteId();
	}

}
