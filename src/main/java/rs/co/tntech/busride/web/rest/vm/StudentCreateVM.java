/**
 *
 */
package rs.co.tntech.busride.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.co.tntech.busride.domain.Student;

/**
 * View Model object of {@link Student} for create action
 *
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class StudentCreateVM extends StudentUpdateVM {

	private static final long serialVersionUID = 1L;

	@Override
	@JsonIgnore
	public Long getId() {
		return super.getId();
	}

    @Override
    public String toString() {
        return "{" +
            "\"name\": " + "\"" + getName() + "\"," +
            "\"image\": " + "\"" + getImage() + "\"," +
            "\"imageContentType\": " + "\"" + getImageContentType() + "\"," +
            "\"typeOfService\": " + "\"" + getTypeOfService() + "\"," +
            "\"schoolId\": " + getSchoolId() + "," +
            "\"parentId\": " + getParentId() +
            "}";
    }
}
