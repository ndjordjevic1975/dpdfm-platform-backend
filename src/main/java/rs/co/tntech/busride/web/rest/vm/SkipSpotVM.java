package rs.co.tntech.busride.web.rest.vm;

public class SkipSpotVM {
    private Long spotId;

    public SkipSpotVM() {
    }

    public SkipSpotVM(Long spotId) {
        this.spotId = spotId;
    }

    public Long getSpotId() {
        return spotId;
    }

    public void setSpotId(Long spotId) {
        this.spotId = spotId;
    }

    @Override
    public String toString() {
        return "SkipSpotVM{" +
            "spotId=" + spotId +
            '}';
    }
}
