/**
 *
 */
package rs.co.tntech.busride.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.co.tntech.busride.domain.School;
import rs.co.tntech.busride.service.dto.ParentDTO;
import rs.co.tntech.busride.service.dto.SchoolDTO;

import java.util.Set;

/**
 * View Model object of {@link School}
 *
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class SchoolVM extends SchoolDTO {

	private static final long serialVersionUID = 1L;

	@Override
	@JsonIgnore
	public Long getSpotId() {
		return super.getSpotId();
	}

    @Override
    @JsonIgnore
    public Set<ParentDTO> getParents() {
        return super.getParents();
    }
}
