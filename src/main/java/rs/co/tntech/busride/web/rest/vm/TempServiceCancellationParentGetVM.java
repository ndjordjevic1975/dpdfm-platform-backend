package rs.co.tntech.busride.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.co.tntech.busride.service.dto.TempServiceCancellationDTO;

public class TempServiceCancellationParentGetVM extends TempServiceCancellationDTO {

	private static final long serialVersionUID = 1L;

    @JsonIgnore
    @Override
    public Long getSpotId() {
        return super.getSpotId();
    }
}
