package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.co.tntech.busride.service.LogOutService;
import rs.co.tntech.busride.service.error.BRPServiceExcpetion;

/**
 * REST controller for managing LoggingOut of parents.
 */
@RestController
@RequestMapping("/api")
@Api(tags = {"logout"}, description = "Logging out of currently logged-in parent")
public class LogOutResource {

    private final Logger log = LoggerFactory.getLogger(LogOutResource.class);

    @Autowired
    private LogOutService logOutService;

    /**
     * PUT  /logout/current-parent : Logging out of currently logged in parent.
     *
     * @return the ResponseEntity with status 200 (OK) if the parent is logged out,
     * or with status 404 current parent couldn't be found
     */
    @PutMapping("/logout/current-parent")
    @Timed
    public ResponseEntity logOutCurrentParent() throws BRPServiceExcpetion {
        log.debug("REST request to log out a currently logged in parent");

        if (logOutService.logOutCurrentParent()) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
