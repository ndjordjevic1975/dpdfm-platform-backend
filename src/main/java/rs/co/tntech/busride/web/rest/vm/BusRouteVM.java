package rs.co.tntech.busride.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.co.tntech.busride.service.dto.BusRouteDTO;
import rs.co.tntech.busride.service.dto.SpotDTO;

import java.util.Set;

/**
 * Created by nemanja on 19-Jul-17.
 */
public class BusRouteVM extends BusRouteDTO {

    private static final long serialVersionUID = 1L;

    @Override
    @JsonIgnore
    public Long getSchoolId() {return super.getSchoolId();}

    @Override
    @JsonIgnore
    public Long getDriverId() {return  super.getDriverId();}

    @Override
    @JsonIgnore
    public Set<SpotDTO> getSpots() {
        return super.getSpots();
    }
}
