/**
 *
 */
package rs.co.tntech.busride.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.co.tntech.busride.domain.Parent;
import rs.co.tntech.busride.domain.enumeration.StudentServiceType;
import rs.co.tntech.busride.service.dto.ParentExtendedDTO;

import java.util.List;

/**
 * View Model object of {@link Parent} for create action
 *
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class ParentCreateVM extends ParentExtendedDTO {

	private static final long serialVersionUID = 1L;


	private List<StudentServiceType>  subscriptionTypes;

	@Override
	@JsonIgnore
	public Long getId() {
		return super.getId();
	}

    public void setSubscriptionTypes(List<StudentServiceType> subscriptionTypes) {
        this.subscriptionTypes = subscriptionTypes;
    }

    public List<StudentServiceType> getSubscriptionTypes() {
        return subscriptionTypes;
    }
}
