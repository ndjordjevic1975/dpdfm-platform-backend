/**
 * 
 */
package rs.co.tntech.busride.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.co.tntech.busride.domain.DriverNotification;
import rs.co.tntech.busride.service.dto.DriverNotificationDTO;

/**
 * View Model object of {@link DriverNotification}
 * 
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class DriverNotificationOutVM extends DriverNotificationDTO {

	private static final long serialVersionUID = 1L;

	@Override
	@JsonIgnore
	public Long getDriverId() {
		return super.getDriverId();
	}

}
