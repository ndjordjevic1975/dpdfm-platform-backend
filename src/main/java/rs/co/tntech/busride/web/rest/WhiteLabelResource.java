package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import rs.co.tntech.busride.service.WhiteLabelService;
import rs.co.tntech.busride.service.dto.FileDTO;
import rs.co.tntech.busride.service.dto.WhiteLabelDTO;
import rs.co.tntech.busride.service.error.BRPServiceSecurityException;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;
import rs.co.tntech.busride.web.rest.util.PngImageName;
import rs.co.tntech.busride.web.rest.vm.WhiteLabelVersionVM;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing WhiteLabel.
 */
@RestController
@RequestMapping("/api")
@Api(tags = {"white-labels"}, description = "White Labels")
public class WhiteLabelResource {

    private static final String ENTITY_NAME = "whiteLabel";
    private final Logger log = LoggerFactory.getLogger(WhiteLabelResource.class);
    @Autowired
    private WhiteLabelService whiteLabelService;

    /**
     * POST  /white-labels : Create a new whiteLabel.
     *
     * @param whiteLabelDTO the whiteLabelDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new whiteLabelDTO, or with status 400 (Bad Request) if the whiteLabel has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/white-labels")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<WhiteLabelDTO> createWhiteLabel(@RequestBody WhiteLabelDTO whiteLabelDTO) throws URISyntaxException {
        log.debug("REST request to save WhiteLabel : {}", whiteLabelDTO);
        if (whiteLabelDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new whiteLabel cannot already have an ID")).body(null);
        }
        WhiteLabelDTO result = whiteLabelService.save(whiteLabelDTO);
        return ResponseEntity.created(new URI("/api/white-labels/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /white-labels : Updates an existing whiteLabel.
     *
     * @param whiteLabelDTO the whiteLabelDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated whiteLabelDTO,
     * or with status 400 (Bad Request) if the whiteLabelDTO is not valid,
     * or with status 500 (Internal Server Error) if the whiteLabelDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/white-labels")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<WhiteLabelDTO> updateWhiteLabel(@RequestBody WhiteLabelDTO whiteLabelDTO) throws URISyntaxException {
        log.debug("REST request to update WhiteLabel : {}", whiteLabelDTO);
        if (whiteLabelDTO.getId() == null) {
            log.debug("API::PUT:white-labels: WhiteLabel id is null, create new WhiteLabel");
            return createWhiteLabel(whiteLabelDTO);
        }
        WhiteLabelDTO result = whiteLabelService.save(whiteLabelDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, whiteLabelDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /white-labels : get all the whiteLabels.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of whiteLabels in body
     */
    @GetMapping("/white-labels")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public List<WhiteLabelDTO> getAllWhiteLabels() {
        log.debug("REST request to get all WhiteLabels");
        return whiteLabelService.findAll();
    }

    /**
     * GET  /white-labels/:id : get the "id" whiteLabel.
     *
     * @param id the id of the whiteLabelDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the whiteLabelDTO, or with status 404 (Not Found)
     */
    @GetMapping("/white-labels/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<WhiteLabelDTO> getWhiteLabel(@PathVariable Long id) {
        log.debug("REST request to get WhiteLabel : {}", id);
        WhiteLabelDTO whiteLabelDTO = whiteLabelService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(whiteLabelDTO));
    }

    /**
     * DELETE  /white-labels/:id : delete the "id" whiteLabel.
     *
     * @param id the id of the whiteLabelDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/white-labels/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<Void> deleteWhiteLabel(@PathVariable Long id) {
        log.debug("REST request to delete WhiteLabel : {}", id);
        whiteLabelService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET /white-labels/by-school-id/:schoolId : get whiteLabel for given school id and authenticated user type.<br>
     * If authenticated user is driver driverWhiteLabel will be returned, if authenticated user is parent parentWhiteLabel will be returned.
     *
     * @param schoolId the id of the school
     * @return the ResponseEntity with status 200 (OK) and with body the whiteLabel, or with status 404 (Not Found)
     */
    @GetMapping("/white-labels/by-school-id/{schoolId}")
    @Timed
    @ApiOperation(value = "", notes = "Returns white label file content (file content, length and mimetype) for given school id and authenticated user type (Parent or Driver). If white label has not been found, 204 No Content, is returned.", response = byte[].class, produces = "application/zip")
    @ApiResponses(value = {@ApiResponse(code = 204, message = "No Content"), @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Not Found")})
    public ResponseEntity<Resource> getWhiteLabelBySchoolIdAndAUserType(@PathVariable Long schoolId) {
        log.debug("REST request to get a white label for given school id: {}", schoolId);

        FileDTO whiteLabel = whiteLabelService.findWhiteLabelBySchoolAndAUserType(schoolId);
        if (whiteLabel.getContent() == null) {
            return ResponseEntity.noContent().build();
        } else {
            ByteArrayResource resource = new ByteArrayResource(whiteLabel.getContent());

            return ResponseEntity.ok().contentLength(whiteLabel.getContent().length).contentType(MediaType.parseMediaType(whiteLabel.getContentType()))
                .body(resource);
        }
    }

    /**
     * PUT /white-labels/current-school/splash_screen: Updates/creates a splash_screen.png
     *
     * @param splashScreenPng splash_screen.png to update/create
     * @return the ResponseEntity with status 200 (OK)
     */
    @PutMapping("/white-labels/current-school/splash_screen")
    @Timed
    @ApiOperation(value = "", notes = "Create/update splash_screen.png")
    public ResponseEntity<Void> updateSplashScreenPng(@RequestParam("file") MultipartFile splashScreenPng) throws IOException {
        log.debug("REST request to update splash_screen.png");

        whiteLabelService.saveSplashScreenPng(splashScreenPng);

        return ResponseEntity.ok().build();
    }

    /**
     * PUT /white-labels/current-school/logo_txt: Updates/creates a logo_txt.png
     *
     * @param logoTxtPng logo_txt.png to update/create
     * @return the ResponseEntity with status 200 (OK)
     */
    @PutMapping("/white-labels/current-school/logo_txt")
    @Timed
    @ApiOperation(value = "", notes = "Create/update logo_txt.png")
    public ResponseEntity<Void> updateLogoTxtPng(@RequestParam("file") MultipartFile logoTxtPng) throws IOException {
        log.debug("REST request to update logo_txt.png");

        whiteLabelService.saveLogoTxtPng(logoTxtPng);

        return ResponseEntity.ok().build();
    }

    /**
     * PUT /white-labels/current-school/logo_bg: Updates/creates a logo_bg.png
     *
     * @param logoBgPng logo_bg.png to update/create
     * @return the ResponseEntity with status 200 (OK)
     */
    @PutMapping("/white-labels/current-school/logo_bg")
    @Timed
    @ApiOperation(value = "", notes = "Create/update logo_bg.png")
    public ResponseEntity<Void> updateLogoBgPng(@RequestParam("file") MultipartFile logoBgPng) throws IOException {
        log.debug("REST request to update logo_bg.png");

        whiteLabelService.saveLogoBgPng(logoBgPng);

        return ResponseEntity.ok().build();
    }

    /**
     * PUT /white-labels/current-school/icn_bus: Updates/creates a icn_bus.png
     *
     * @param icnBusPng icn_bus.png to update/create
     * @return the ResponseEntity with status 200 (OK)
     */
    @PutMapping("/white-labels/current-school/icn_bus")
    @Timed
    @ApiOperation(value = "", notes = "Create/update icn_bus.png")
    public ResponseEntity<Void> updateIcnBusPng(@RequestParam("file") MultipartFile icnBusPng) throws IOException {
        log.debug("REST request to update icn_bus.png");

        whiteLabelService.saveIcnBusPng(icnBusPng);

        return ResponseEntity.ok().build();
    }

    /**
     * GET /white-labels/current-school/image: Returns a png image of a Driver or Parent apps.
     *
     * @param image SPLASH_SCREEN|LOGO_TXT|LOGO_BG|ICN_BUS to return
     * @return a png image
     */
    @GetMapping("/white-labels/current-school/image")
    @Timed
    @ApiOperation(value = "", notes = "Returns a png image of a Driver or Parent apps.", produces = "image/png")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Not Found")})
    public ResponseEntity<Resource> getSchoolDriverAndParentPng(@ApiParam("SPLASH_SCREEN|LOGO_TXT|LOGO_BG|ICN_BUS") @RequestParam("image") PngImageName image) {
        log.debug("REST request to get the following png image: {}", image);

        byte[] schoolDriverAndParentPng = whiteLabelService.getSchoolDriverOrParentPng(image);

        if (schoolDriverAndParentPng != null) {
            return ResponseEntity.ok().contentLength(schoolDriverAndParentPng.length).contentType(MediaType.parseMediaType("image/png")).body(new ByteArrayResource(schoolDriverAndParentPng));
        }

        return ResponseEntity.notFound().build();
    }

    /**
     * GET /white-labels/current-parent/version/{schoolId}
     *
     * @param schoolId school id
     * @return version
     */
    @GetMapping("/white-labels/current-parent/version/{schoolId}")
    @Timed
    @ApiOperation(value = "", notes = "Returns a parent's white-label version.", produces = "application/json")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 403, message = "User doesn't have required role or not logged in")})
    public ResponseEntity<WhiteLabelVersionVM> getParentAppWhiteLabelVersion(@PathVariable Long schoolId) throws BRPServiceSecurityException {
        log.debug("REST request to get parent app white-label version for a school id: {}", schoolId);

        Integer parentWhiteLabelVersion = whiteLabelService.getParentWhiteLabelVersion(schoolId);

        if (parentWhiteLabelVersion != null) {
            return ResponseEntity.ok(new WhiteLabelVersionVM(parentWhiteLabelVersion));
        }

        return ResponseEntity.notFound().build();
    }

    /**
     * GET /white-labels/current-driver/version/{schoolId}
     *
     * @param schoolId school id
     * @return version
     */
    @GetMapping("/white-labels/current-driver/version/{schoolId}")
    @Timed
    @ApiOperation(value = "", notes = "Returns a driver's white-label version.", produces = "application/json")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 403, message = "User doesn't have required role or not logged in")})
    public ResponseEntity<WhiteLabelVersionVM> getDriverAppWhiteLabelVersion(@PathVariable Long schoolId) throws BRPServiceSecurityException {
        log.debug("REST request to get driver app white-label version for a school id: {}", schoolId);

        Integer driverWhiteLabelVersion = whiteLabelService.getDriverWhiteLabelVersion(schoolId);

        if (driverWhiteLabelVersion != null) {
            return ResponseEntity.ok(new WhiteLabelVersionVM(driverWhiteLabelVersion));
        }

        return ResponseEntity.notFound().build();
    }
}
