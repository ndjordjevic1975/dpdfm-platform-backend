package rs.co.tntech.busride.web.rest.vm;

public class WhiteLabelVersionVM {
    private Integer version;

    public WhiteLabelVersionVM(Integer parentWhiteLabelVersion) {
        this.version = parentWhiteLabelVersion;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
