package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.co.tntech.busride.domain.User;
import rs.co.tntech.busride.repository.UserRepository;
import rs.co.tntech.busride.security.AuthoritiesConstants;
import rs.co.tntech.busride.security.UserNotActivatedException;
import rs.co.tntech.busride.security.jwt.JWTConfigurer;
import rs.co.tntech.busride.security.jwt.TokenProvider;
import rs.co.tntech.busride.service.UserService;
import rs.co.tntech.busride.web.rest.vm.LoginVM;
import rs.co.tntech.busride.web.rest.vm.UserFirstTimeChangePasswordVM;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collections;
import java.util.Optional;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
@Api(tags = {"authenticate"}, description = "Authenticate")
public class UserJWTController {

    private final Logger log = LoggerFactory.getLogger(UserJWTController.class);

    private final TokenProvider tokenProvider;

    private final AuthenticationManager authenticationManager;

    private final UserService userService;

    @Autowired
    private UserRepository userRepository;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManager authenticationManager, UserService userService) {
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
        this.userService = userService;
    }

    @PostMapping("/authenticate")
    @Timed
    public ResponseEntity authorize(@Valid @RequestBody LoginVM loginVM, HttpServletResponse response) {
        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

        try {
            Authentication authentication = this.authenticationManager.authenticate(authenticationToken);

            Optional<User> currentUser = userRepository.findOneByLogin(loginVM.getUsername());

            if ((authentication.getAuthorities().contains(new SimpleGrantedAuthority(AuthoritiesConstants.PARENT))
                || authentication.getAuthorities().contains(new SimpleGrantedAuthority(AuthoritiesConstants.SCHOOL_ADMIN))) && currentUser.isPresent() && currentUser.get().getResetDate() == null) {
                log.info("User must change password: " + loginVM.getUsername());
                throw new UserNotActivatedException("User never changed password. Please change the password");
            }

            SecurityContextHolder.getContext().setAuthentication(authentication);
            boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
            String jwt = tokenProvider.createToken(authentication, rememberMe);
            response.addHeader(JWTConfigurer.AUTHORIZATION_HEADER, "Bearer " + jwt);
            return ResponseEntity.ok(new JWTToken(jwt));
        } catch (AuthenticationException ae) {
            log.info("Failed to login user: " + loginVM.getUsername());
            log.trace("Authentication exception trace: {}", ae);
            return new ResponseEntity<>(Collections.singletonMap("AuthenticationException",
                ae.getLocalizedMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/authenticate/parent-first-time-login-change-password")
    @Timed
    public ResponseEntity parentFirstTimeLoginChangePassword(@Valid @RequestBody UserFirstTimeChangePasswordVM userFirstTimeChangePasswordVM, HttpServletResponse response) {
        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(userFirstTimeChangePasswordVM.getUsername(), userFirstTimeChangePasswordVM.getTempPassword());

        try {
            this.authenticationManager.authenticate(authenticationToken);

            userService.userFirstTimeLoginChangePassword(userFirstTimeChangePasswordVM.getUsername(), userFirstTimeChangePasswordVM.getNewPassword());

            return ResponseEntity.ok("Temp password successfully changed for parent: " + userFirstTimeChangePasswordVM.getUsername());
        } catch (AuthenticationException ae) {
            log.trace("Authentication exception trace: {}", ae);
            return new ResponseEntity<>(Collections.singletonMap("AuthenticationException",
                ae.getLocalizedMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
