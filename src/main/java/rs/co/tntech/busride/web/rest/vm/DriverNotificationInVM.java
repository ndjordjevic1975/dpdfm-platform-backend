/**
 * 
 */
package rs.co.tntech.busride.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.co.tntech.busride.domain.DriverNotification;

/**
 * View Model object of {@link DriverNotification}
 * 
 * @author Ivan Obradovic <ivanobradovic83@gmail.com>
 *
 */
public class DriverNotificationInVM extends DriverNotificationOutVM {

	private static final long serialVersionUID = 1L;

	@Override
	@JsonIgnore
	public Long getId() {
		return super.getId();
	}

	@Override
	@JsonIgnore
	public String getNotification() {
		return super.getNotification();
	}

}
