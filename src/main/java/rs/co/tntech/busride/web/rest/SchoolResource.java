package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.co.tntech.busride.service.SchoolService;
import rs.co.tntech.busride.service.dto.SchoolDTO;
import rs.co.tntech.busride.web.rest.mapper.SchoolVmMapper;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;
import rs.co.tntech.busride.web.rest.vm.SchoolVM;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing School.
 */
@RestController
@RequestMapping("/api")
@Api(tags = { "schools" }, description = "Schools")
public class SchoolResource {

    private final Logger log = LoggerFactory.getLogger(SchoolResource.class);

    private static final String ENTITY_NAME = "school";

    @Autowired
    private SchoolVmMapper schoolVmDtoMapper;

    private final SchoolService schoolService;

    public SchoolResource(SchoolService schoolService) {
        this.schoolService = schoolService;
    }

    /**
     * POST  /schools : Create a new school.
     *
     * @param schoolDTO the schoolDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new schoolDTO, or with status 400 (Bad Request) if the school has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/schools")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<SchoolDTO> createSchool(@RequestBody SchoolDTO schoolDTO) throws URISyntaxException {
        log.debug("REST request to save School : {}", schoolDTO);
        if (schoolDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new school cannot already have an ID")).body(null);
        }
        SchoolDTO result = schoolService.save(schoolDTO);
        return ResponseEntity.created(new URI("/api/schools/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /schools : Updates an existing school.
     *
     * @param schoolDTO the schoolDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated schoolDTO,
     * or with status 400 (Bad Request) if the schoolDTO is not valid,
     * or with status 500 (Internal Server Error) if the schoolDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/schools")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<SchoolDTO> updateSchool(@RequestBody SchoolDTO schoolDTO) throws URISyntaxException {
        log.debug("REST request to update School : {}", schoolDTO);
        if (schoolDTO.getId() == null) {
            return createSchool(schoolDTO);
        }
        SchoolDTO result = schoolService.save(schoolDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, schoolDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /schools : get all the schools.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of schools in body
     */
    @GetMapping("/schools")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public List<SchoolDTO> getAllSchools() {
        log.debug("REST request to get all Schools");
        return schoolService.findAll();
    }

    /**
     * GET  /schools/current-parent :  gets all schools the primary parent (of authenticated parent) is associated with
     *
     * @return the ResponseEntity with status 200 (OK) and the list of schools in body
     */
    @GetMapping("/schools/current-parent")
    @Timed
    public List<SchoolVM> getAllByCurrentPrimaryParent() {
        return schoolService.findAllByCurrentPrimaryParent().stream()
        		.map(schoolVmDtoMapper::toVm)
        		.collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * GET  /schools/current-driver :  gets a school's data for the current driver.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of schools in body
     */
    @GetMapping("/schools/current-driver")
    @Timed
    public ResponseEntity<SchoolVM> getByCurrentDriver() {
        SchoolDTO schoolDTO = schoolService.findByCurrentDriver();
		SchoolVM schoolVM = schoolDTO == null ? null : schoolVmDtoMapper.toVm(schoolDTO);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(schoolVM));
    }

    /**
     * GET  /schools/:id : get the "id" school.
     *
     * @param id the id of the schoolDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the schoolDTO, or with status 404 (Not Found)
     */
    @GetMapping("/schools/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<SchoolDTO> getSchool(@PathVariable Long id) {
        log.debug("REST request to get School : {}", id);
        SchoolDTO schoolDTO = schoolService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(schoolDTO));
    }

    /**
     * DELETE  /schools/:id : delete the "id" school.
     *
     * @param id the id of the schoolDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/schools/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<Void> deleteSchool(@PathVariable Long id) {
        log.debug("REST request to delete School : {}", id);
        schoolService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
