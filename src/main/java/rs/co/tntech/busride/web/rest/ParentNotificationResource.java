package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.co.tntech.busride.service.ParentNotificationService;
import rs.co.tntech.busride.service.dto.ParentNotificationDTO;
import rs.co.tntech.busride.web.rest.mapper.ParentNotificationVmMapper;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;
import rs.co.tntech.busride.web.rest.vm.ParentNotificationVM;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing ParentNotification.
 */
@RestController
@RequestMapping("/api")
@Api(tags = {"parent-notifications"}, description = "Parent Notifications")
public class ParentNotificationResource {

    private final Logger log = LoggerFactory.getLogger(ParentNotificationResource.class);

    private static final String ENTITY_NAME = "parentNotification";

    @Autowired
    private ParentNotificationVmMapper parentNotificationVmMapper;

    private final ParentNotificationService parentNotificationService;

    public ParentNotificationResource(ParentNotificationService parentNotificationService) {
        this.parentNotificationService = parentNotificationService;
    }

    /**
     * POST  /parent-notifications : Create a new parentNotification.
     *
     * @param parentNotificationDTO the parentNotificationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new parentNotificationDTO, or with status 400 (Bad Request) if the parentNotification has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/parent-notifications")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<ParentNotificationDTO> createParentNotification(@RequestBody ParentNotificationDTO parentNotificationDTO) throws URISyntaxException {
        log.debug("REST request to save ParentNotification : {}", parentNotificationDTO);
        if (parentNotificationDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new parentNotification cannot already have an ID")).body(null);
        }
        ParentNotificationDTO result = parentNotificationService.save(parentNotificationDTO);
        return ResponseEntity.created(new URI("/api/parent-notifications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /parent-notifications : Updates an existing parentNotification.
     *
     * @param parentNotificationDTO the parentNotificationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated parentNotificationDTO,
     * or with status 400 (Bad Request) if the parentNotificationDTO is not valid,
     * or with status 500 (Internal Server Error) if the parentNotificationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/parent-notifications")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<ParentNotificationDTO> updateParentNotification(@RequestBody ParentNotificationDTO parentNotificationDTO) throws URISyntaxException {
        log.debug("REST request to update ParentNotification : {}", parentNotificationDTO);
        if (parentNotificationDTO.getId() == null) {
            return createParentNotification(parentNotificationDTO);
        }
        ParentNotificationDTO result = parentNotificationService.save(parentNotificationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, parentNotificationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /parent-notifications : get all the parentNotifications.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of parentNotifications in body
     */
    @GetMapping("/parent-notifications")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public List<ParentNotificationDTO> getAllParentNotifications() {
        log.debug("REST request to get all ParentNotifications");
        return parentNotificationService.findAll();
    }

    /**
     * GET  /parent-notifications/:id : get the "id" parentNotification.
     *
     * @param id the id of the parentNotificationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the parentNotificationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/parent-notifications/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<ParentNotificationDTO> getParentNotification(@PathVariable Long id) {
        log.debug("REST request to get ParentNotification : {}", id);
        ParentNotificationDTO parentNotificationDTO = parentNotificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(parentNotificationDTO));
    }

    /**
     * DELETE  /parent-notifications/:id : delete the "id" parentNotification.
     *
     * @param id the id of the parentNotificationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/parent-notifications/{id}")
    @Timed
    public ResponseEntity<Void> deleteParentNotification(@PathVariable Long id) {
        log.debug("REST request to delete ParentNotification : {}", id);
        parentNotificationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /parent-notifications/current-parent : gets all notifications for curent parent
     *
     * @return the ResponseEntity with status 200 (OK) and the list of parentNotifications in body
     */
    @GetMapping("/parent-notifications/current-parent")
    @Timed
    public List<ParentNotificationVM> getAllNotificationForCurrentParent() {
        log.debug("REST request to get all notifications for the current parent");

        return parentNotificationService.getAllNotificationForCurrentParent().stream()
            .map(parentNotificationVmMapper::toVm)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @DeleteMapping("/parent-notifications/[{ids}]")
    @Timed
    public ResponseEntity<Void> deleteParentNotification(@PathVariable Long[] ids) {
        log.debug("REST request to delete parent notifications");

        parentNotificationService.deleteByIds(ids);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, Arrays.toString(ids))).build();
    }
}
