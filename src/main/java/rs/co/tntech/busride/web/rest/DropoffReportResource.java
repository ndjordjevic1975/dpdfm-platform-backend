package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.co.tntech.busride.service.DropoffReportService;
import rs.co.tntech.busride.service.dto.DropoffReportDTO;
import rs.co.tntech.busride.service.error.BRPBaseServiceException;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;
import rs.co.tntech.busride.web.rest.vm.DropoffReportVM;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DropoffReport.
 */
@RestController
@RequestMapping("/api")
@Api(tags = {"dropoff-reports"}, description = "Drop off reports")
public class DropoffReportResource {

    private final Logger log = LoggerFactory.getLogger(DropoffReportResource.class);

    private static final String ENTITY_NAME = "dropoffReport";

    private final DropoffReportService dropoffReportService;

    public DropoffReportResource(DropoffReportService dropoffReportService) {
        this.dropoffReportService = dropoffReportService;
    }

    /**
     * POST  /dropoff-reports : Create a new dropoffReport.
     *
     * @param dropoffReportDTO the dropoffReportDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dropoffReportDTO, or with status 400 (Bad Request) if the dropoffReport has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dropoff-reports")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<DropoffReportDTO> createDropoffReport(@RequestBody DropoffReportDTO dropoffReportDTO) throws URISyntaxException {
        log.debug("REST request to save DropoffReport : {}", dropoffReportDTO);
        if (dropoffReportDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dropoffReport cannot already have an ID")).body(null);
        }
        DropoffReportDTO result = dropoffReportService.save(dropoffReportDTO);
        return ResponseEntity.created(new URI("/api/dropoff-reports/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dropoff-reports : Updates an existing dropoffReport.
     *
     * @param dropoffReportDTO the dropoffReportDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dropoffReportDTO,
     * or with status 400 (Bad Request) if the dropoffReportDTO is not valid,
     * or with status 500 (Internal Server Error) if the dropoffReportDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dropoff-reports")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<DropoffReportDTO> updateDropoffReport(@RequestBody DropoffReportDTO dropoffReportDTO) throws URISyntaxException {
        log.debug("REST request to update DropoffReport : {}", dropoffReportDTO);
        if (dropoffReportDTO.getId() == null) {
            return createDropoffReport(dropoffReportDTO);
        }
        DropoffReportDTO result = dropoffReportService.save(dropoffReportDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dropoffReportDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dropoff-reports : get all the dropoffReports.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dropoffReports in body
     */
    @GetMapping("/dropoff-reports")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public List<DropoffReportDTO> getAllDropoffReports() {
        log.debug("REST request to get all DropoffReports");
        return dropoffReportService.findAll();
    }

    /**
     * GET  /dropoff-reports/:id : get the "id" dropoffReport.
     *
     * @param id the id of the dropoffReportDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dropoffReportDTO, or with status 404 (Not Found)
     */
    @GetMapping("/dropoff-reports/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<DropoffReportDTO> getDropoffReport(@PathVariable Long id) {
        log.debug("REST request to get DropoffReport : {}", id);
        DropoffReportDTO dropoffReportDTO = dropoffReportService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dropoffReportDTO));
    }

    /**
     * DELETE  /dropoff-reports/:id : delete the "id" dropoffReport.
     *
     * @param id the id of the dropoffReportDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dropoff-reports/{id}")
    @Timed
    @ApiOperation(value = "", hidden = true)
    public ResponseEntity<Void> deleteDropoffReport(@PathVariable Long id) {
        log.debug("REST request to delete DropoffReport : {}", id);
        dropoffReportService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * POST  /dropoff-reports/current-driver : Creates a new dropoff report. Also triggers a push notification to Parents.
     *
     * @param dropoffReportVM the dropoffReportDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dropoffReportDTO, or with status 400 (Bad Request) if the dropoffReport has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     * @throws BRPBaseServiceException
     */
    @PostMapping("/dropoff-reports/current-driver")
    @Timed
    public ResponseEntity<DropoffReportDTO> createDropoffReportForCurrentDriver(@RequestBody DropoffReportVM dropoffReportVM) throws URISyntaxException, BRPBaseServiceException {
        log.debug("REST request to create a new dropoff report: {}", dropoffReportVM);

        DropoffReportDTO result = dropoffReportService.saveForCurrentDriver(dropoffReportVM);
        return ResponseEntity.created(new URI("/api/dropoff-reports/current-driver/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
}
