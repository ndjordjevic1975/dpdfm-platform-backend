package rs.co.tntech.busride.web.rest.errors;

import com.amazonaws.AmazonServiceException;
import java.util.List;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import rs.co.tntech.busride.domain.User;
import rs.co.tntech.busride.service.error.*;
import rs.co.tntech.busride.service.util.BRPServiceUtil;

/**
 * Controller advice to translate the server side exceptions to client-friendly json structures.
 */
@ControllerAdvice
public class ExceptionTranslator {

    private final Logger log = LoggerFactory.getLogger(ExceptionTranslator.class);

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private BRPServiceUtil serviceUtil;

    @ExceptionHandler(ConcurrencyFailureException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public ErrorVM processConcurrencyError(ConcurrencyFailureException ex) {
        return new ErrorVM(ErrorConstants.ERR_CONCURRENCY_FAILURE);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorVM processValidationError(MethodArgumentNotValidException ex) {
    	Locale locale = getCurrentUserLocale();
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        ErrorVM dto = new ErrorVM(ErrorConstants.ERR_VALIDATION);
        for (FieldError fieldError : fieldErrors) {
			String translatedMsg = messageSource.getMessage(fieldError.getCode(), new String[0], fieldError.getCode(), locale);
            dto.add(fieldError.getObjectName(), fieldError.getField(), translatedMsg);
        }
        return dto;
    }

    @ExceptionHandler(BRPServiceExcpetion.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorVM processValidationError(BRPServiceExcpetion ex) {
    	String translatedMsg = messageSource.getMessage(ex.getErrorId(), new String[0], ex.getErrorId(), getCurrentUserLocale());
		return new ErrorVM(translatedMsg, ex.getMessage());
	}

	@ExceptionHandler(BRPFieldException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorVM processValidationError(BRPFieldException ex) {
		ErrorVM dto = new ErrorVM(ErrorConstants.ERR_VALIDATION);
		String translatedMsg = messageSource.getMessage(ex.getErrorId(), new String[0], ex.getErrorId(), getCurrentUserLocale());
		dto.add(ex.getObjectName(), ex.getField(), translatedMsg);
		return dto;
	}

	@ExceptionHandler(BRPFieldsExcpetion.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorVM processValidationError(BRPFieldsExcpetion ex) {
		Locale locale = getCurrentUserLocale();
		ErrorVM dto = new ErrorVM(ErrorConstants.ERR_VALIDATION);
		for (BRPFieldException brpFieldException : ex.getFields()) {
			String translatedMsg = messageSource.getMessage(brpFieldException.getErrorId(), new String[0], brpFieldException.getErrorId(), locale);
			dto.add(brpFieldException.getObjectName(), brpFieldException.getField(), translatedMsg);
		}
		return dto;
	}

    @ExceptionHandler(BRPServiceSecurityException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorVM processValidationError(BRPServiceSecurityException ex) {
		StringBuilder roles = new StringBuilder();
		for (int i = 0; i < ex.getRequiredRoles().size(); i++) {
			roles.append(ex.getRequiredRoles().get(i));
			if (i < (ex.getRequiredRoles().size() - 1)) {
				roles.append(",");
			}
		}
		String translatedMsg = messageSource.getMessage(ErrorConstants.ERR_REQUIRED_ROLE_MISSING, new String[0], ErrorConstants.ERR_REQUIRED_ROLE_MISSING, getCurrentUserLocale());
		return new ErrorVM(translatedMsg, "User does not have any of required roles: " + roles.toString());
	}


    @ExceptionHandler(AmazonServiceException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorVM processAWSRequestError(AmazonServiceException ex) {
        return new ErrorVM(ErrorConstants.ERR_INTERNAL_SERVER_ERROR, ex.getMessage());
    }

    @ExceptionHandler(BRPServiceSystemException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ErrorVM processValidationError(BRPServiceSystemException ex) {
		String translatedMsg = messageSource.getMessage(ErrorConstants.ERR_INTERNAL_SERVER_ERROR, new String[] { ex.getErrorId() }, ex.getMessage(),
				getCurrentUserLocale());
		return new ErrorVM(translatedMsg, ex.getMessage());
	}

    @ExceptionHandler(CustomParameterizedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ParameterizedErrorVM processParameterizedValidationError(CustomParameterizedException ex) {
        return ex.getErrorVM();
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorVM processAccessDeniedException(AccessDeniedException e) {
        return new ErrorVM(ErrorConstants.ERR_ACCESS_DENIED, e.getMessage());
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ErrorVM processMethodNotSupportedException(HttpRequestMethodNotSupportedException exception) {
        return new ErrorVM(ErrorConstants.ERR_METHOD_NOT_SUPPORTED, exception.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorVM> processException(Exception ex) {
        if (log.isDebugEnabled()) {
            log.debug("An unexpected error occurred: {}", ex.getMessage(), ex);
        } else {
            log.error("An unexpected error occurred: {}", ex.getMessage());
        }
        BodyBuilder builder;
        ErrorVM errorVM;
        ResponseStatus responseStatus = AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class);
        if (responseStatus != null) {
            builder = ResponseEntity.status(responseStatus.value());
            errorVM = new ErrorVM("error." + responseStatus.value().value(), responseStatus.reason());
        } else {
            builder = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
            errorVM = new ErrorVM(ErrorConstants.ERR_INTERNAL_SERVER_ERROR, "Internal server error");
        }
        return builder.body(errorVM);
    }

    @ExceptionHandler(value = DifferentUserException.class)
    public ResponseEntity<String> processDifferentUserException(Exception ex) {
        return new ResponseEntity(ex.getMessage(),HttpStatus.FORBIDDEN);
    }

    private Locale getCurrentUserLocale() {
		User currentUser = serviceUtil.getCurrentUser();
		if (currentUser != null) {
			return Locale.forLanguageTag(currentUser.getLangKey());
		} else {
			return Locale.ENGLISH;
		}
    }

}
