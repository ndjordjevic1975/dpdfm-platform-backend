package rs.co.tntech.busride.web.rest.mapper;

import org.mapstruct.Mapper;

import rs.co.tntech.busride.service.dto.DriverDTO;
import rs.co.tntech.busride.web.rest.vm.DriverVM;

/**
 * Mapper for the DriverVM and its DTO DriverDTO.
 */
@Mapper(componentModel = "spring")
public interface DriverVmMapper extends VmDtoMapper <DriverVM, DriverDTO> {
    
}
