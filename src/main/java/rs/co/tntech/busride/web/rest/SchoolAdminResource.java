package rs.co.tntech.busride.web.rest;

import com.codahale.metrics.annotation.Timed;
import rs.co.tntech.busride.service.SchoolAdminService;
import rs.co.tntech.busride.web.rest.util.HeaderUtil;
import rs.co.tntech.busride.service.dto.SchoolAdminDTO;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SchoolAdmin.
 */
@RestController
@RequestMapping("/api")
@Api(tags = {"school-admins"}, description = "School Administrators")
public class SchoolAdminResource {

    private final Logger log = LoggerFactory.getLogger(SchoolAdminResource.class);

    private static final String ENTITY_NAME = "schoolAdmin";

    private final SchoolAdminService schoolAdminService;

    public SchoolAdminResource(SchoolAdminService schoolAdminService) {
        this.schoolAdminService = schoolAdminService;
    }

    /**
     * POST  /school-admins : Create a new schoolAdmin.
     *
     * @param schoolAdminDTO the schoolAdminDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new schoolAdminDTO, or with status 400 (Bad Request) if the schoolAdmin has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/school-admins")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public ResponseEntity<SchoolAdminDTO> createSchoolAdmin(@RequestBody SchoolAdminDTO schoolAdminDTO) throws URISyntaxException {
        log.debug("REST request to save SchoolAdmin : {}", schoolAdminDTO);
        if (schoolAdminDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new schoolAdmin cannot already have an ID")).body(null);
        }
        SchoolAdminDTO result = schoolAdminService.save(schoolAdminDTO);
        return ResponseEntity.created(new URI("/api/school-admins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /school-admins : Updates an existing schoolAdmin.
     *
     * @param schoolAdminDTO the schoolAdminDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated schoolAdminDTO,
     * or with status 400 (Bad Request) if the schoolAdminDTO is not valid,
     * or with status 500 (Internal Server Error) if the schoolAdminDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/school-admins")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public ResponseEntity<SchoolAdminDTO> updateSchoolAdmin(@RequestBody SchoolAdminDTO schoolAdminDTO) throws URISyntaxException {
        log.debug("REST request to update SchoolAdmin : {}", schoolAdminDTO);
        if (schoolAdminDTO.getId() == null) {
            return createSchoolAdmin(schoolAdminDTO);
        }
        SchoolAdminDTO result = schoolAdminService.save(schoolAdminDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, schoolAdminDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /school-admins : get all the schoolAdmins.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of schoolAdmins in body
     */
    @GetMapping("/school-admins")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public List<SchoolAdminDTO> getAllSchoolAdmins() {
        log.debug("REST request to get all SchoolAdmins");
        return schoolAdminService.findAll();
    }

    /**
     * GET  /school-admins/:id : get the "id" schoolAdmin.
     *
     * @param id the id of the schoolAdminDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the schoolAdminDTO, or with status 404 (Not Found)
     */
    @GetMapping("/school-admins/{id}")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public ResponseEntity<SchoolAdminDTO> getSchoolAdmin(@PathVariable Long id) {
        log.debug("REST request to get SchoolAdmin : {}", id);
        SchoolAdminDTO schoolAdminDTO = schoolAdminService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(schoolAdminDTO));
    }

    /**
     * DELETE  /school-admins/:id : delete the "id" schoolAdmin.
     *
     * @param id the id of the schoolAdminDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/school-admins/{id}")
    @ApiOperation(value = "", hidden = true)
    @Timed
    public ResponseEntity<Void> deleteSchoolAdmin(@PathVariable Long id) {
        log.debug("REST request to delete SchoolAdmin : {}", id);
        schoolAdminService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
