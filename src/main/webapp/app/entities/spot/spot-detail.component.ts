import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Spot } from './spot.model';
import { SpotService } from './spot.service';

@Component({
    selector: 'jhi-spot-detail',
    templateUrl: './spot-detail.component.html'
})
export class SpotDetailComponent implements OnInit, OnDestroy {

    spot: Spot;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private spotService: SpotService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSpots();
    }

    load(id) {
        this.spotService.find(id).subscribe((spot) => {
            this.spot = spot;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSpots() {
        this.eventSubscriber = this.eventManager.subscribe(
            'spotListModification',
            (response) => this.load(this.spot.id)
        );
    }
}
