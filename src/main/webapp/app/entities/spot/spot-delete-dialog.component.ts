import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Spot } from './spot.model';
import { SpotPopupService } from './spot-popup.service';
import { SpotService } from './spot.service';

@Component({
    selector: 'jhi-spot-delete-dialog',
    templateUrl: './spot-delete-dialog.component.html'
})
export class SpotDeleteDialogComponent {

    spot: Spot;

    constructor(
        private spotService: SpotService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.spotService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'spotListModification',
                content: 'Deleted an spot'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-spot-delete-popup',
    template: ''
})
export class SpotDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private spotPopupService: SpotPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.spotPopupService
                .open(SpotDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
