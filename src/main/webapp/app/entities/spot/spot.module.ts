import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BusRidePlatformBackendSharedModule } from '../../shared';
import {
    SpotService,
    SpotPopupService,
    SpotComponent,
    SpotDetailComponent,
    SpotDialogComponent,
    SpotPopupComponent,
    SpotDeletePopupComponent,
    SpotDeleteDialogComponent,
    spotRoute,
    spotPopupRoute,
} from './';

const ENTITY_STATES = [
    ...spotRoute,
    ...spotPopupRoute,
];

@NgModule({
    imports: [
        BusRidePlatformBackendSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        SpotComponent,
        SpotDetailComponent,
        SpotDialogComponent,
        SpotDeleteDialogComponent,
        SpotPopupComponent,
        SpotDeletePopupComponent,
    ],
    entryComponents: [
        SpotComponent,
        SpotDialogComponent,
        SpotPopupComponent,
        SpotDeleteDialogComponent,
        SpotDeletePopupComponent,
    ],
    providers: [
        SpotService,
        SpotPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusRidePlatformBackendSpotModule {}
