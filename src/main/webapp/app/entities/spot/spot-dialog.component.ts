import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Spot } from './spot.model';
import { SpotPopupService } from './spot-popup.service';
import { SpotService } from './spot.service';
import { BusRoute, BusRouteService } from '../bus-route';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-spot-dialog',
    templateUrl: './spot-dialog.component.html'
})
export class SpotDialogComponent implements OnInit {

    spot: Spot;
    isSaving: boolean;

    busroutes: BusRoute[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private spotService: SpotService,
        private busRouteService: BusRouteService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.busRouteService.query()
            .subscribe((res: ResponseWrapper) => { this.busroutes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.spot.id !== undefined) {
            this.subscribeToSaveResponse(
                this.spotService.update(this.spot));
        } else {
            this.subscribeToSaveResponse(
                this.spotService.create(this.spot));
        }
    }

    private subscribeToSaveResponse(result: Observable<Spot>) {
        result.subscribe((res: Spot) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Spot) {
        this.eventManager.broadcast({ name: 'spotListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackBusRouteById(index: number, item: BusRoute) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-spot-popup',
    template: ''
})
export class SpotPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private spotPopupService: SpotPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.spotPopupService
                    .open(SpotDialogComponent as Component, params['id']);
            } else {
                this.spotPopupService
                    .open(SpotDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
