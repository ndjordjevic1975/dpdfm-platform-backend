import { BaseEntity } from './../../shared';

const enum SpotType {
    'SCHOOL',
    'PICKUP_DROPOFF'
}

export class Spot implements BaseEntity {
    constructor(
        public id?: number,
        public gpsLatitude?: number,
        public gpsLongitude?: number,
        public type?: SpotType,
        public busRoutes?: BaseEntity[],
    ) {
    }
}
