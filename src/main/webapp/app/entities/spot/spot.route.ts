import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SpotComponent } from './spot.component';
import { SpotDetailComponent } from './spot-detail.component';
import { SpotPopupComponent } from './spot-dialog.component';
import { SpotDeletePopupComponent } from './spot-delete-dialog.component';

export const spotRoute: Routes = [
    {
        path: 'spot',
        component: SpotComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.spot.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'spot/:id',
        component: SpotDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.spot.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const spotPopupRoute: Routes = [
    {
        path: 'spot-new',
        component: SpotPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.spot.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'spot/:id/edit',
        component: SpotPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.spot.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'spot/:id/delete',
        component: SpotDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.spot.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
