import { BaseEntity } from './../../shared';

export class Driver implements BaseEntity {
    constructor(
        public id?: number,
        public capacity?: number,
        public deviceIdentifier?: string,
        public gcmSessionId?: string,
        public endpointARN?: string,
        public tabletNumber?: string,
        public userId?: number,
        public activeBusRouteId?: number,
        public schoolId?: number,
    ) {
    }
}
