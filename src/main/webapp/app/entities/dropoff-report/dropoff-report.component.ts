import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { DropoffReport } from './dropoff-report.model';
import { DropoffReportService } from './dropoff-report.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-dropoff-report',
    templateUrl: './dropoff-report.component.html'
})
export class DropoffReportComponent implements OnInit, OnDestroy {
dropoffReports: DropoffReport[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private dropoffReportService: DropoffReportService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.dropoffReportService.query().subscribe(
            (res: ResponseWrapper) => {
                this.dropoffReports = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInDropoffReports();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: DropoffReport) {
        return item.id;
    }
    registerChangeInDropoffReports() {
        this.eventSubscriber = this.eventManager.subscribe('dropoffReportListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
