import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DropoffReportComponent } from './dropoff-report.component';
import { DropoffReportDetailComponent } from './dropoff-report-detail.component';
import { DropoffReportPopupComponent } from './dropoff-report-dialog.component';
import { DropoffReportDeletePopupComponent } from './dropoff-report-delete-dialog.component';

export const dropoffReportRoute: Routes = [
    {
        path: 'dropoff-report',
        component: DropoffReportComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.dropoffReport.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'dropoff-report/:id',
        component: DropoffReportDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.dropoffReport.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dropoffReportPopupRoute: Routes = [
    {
        path: 'dropoff-report-new',
        component: DropoffReportPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.dropoffReport.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'dropoff-report/:id/edit',
        component: DropoffReportPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.dropoffReport.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'dropoff-report/:id/delete',
        component: DropoffReportDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.dropoffReport.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
