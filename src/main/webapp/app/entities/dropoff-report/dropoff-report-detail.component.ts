import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { DropoffReport } from './dropoff-report.model';
import { DropoffReportService } from './dropoff-report.service';

@Component({
    selector: 'jhi-dropoff-report-detail',
    templateUrl: './dropoff-report-detail.component.html'
})
export class DropoffReportDetailComponent implements OnInit, OnDestroy {

    dropoffReport: DropoffReport;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dropoffReportService: DropoffReportService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDropoffReports();
    }

    load(id) {
        this.dropoffReportService.find(id).subscribe((dropoffReport) => {
            this.dropoffReport = dropoffReport;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDropoffReports() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dropoffReportListModification',
            (response) => this.load(this.dropoffReport.id)
        );
    }
}
