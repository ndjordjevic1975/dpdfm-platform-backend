import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DropoffReport } from './dropoff-report.model';
import { DropoffReportPopupService } from './dropoff-report-popup.service';
import { DropoffReportService } from './dropoff-report.service';
import { BusRoute, BusRouteService } from '../bus-route';
import { Spot, SpotService } from '../spot';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-dropoff-report-dialog',
    templateUrl: './dropoff-report-dialog.component.html'
})
export class DropoffReportDialogComponent implements OnInit {

    dropoffReport: DropoffReport;
    isSaving: boolean;

    busroutes: BusRoute[];

    spots: Spot[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private dropoffReportService: DropoffReportService,
        private busRouteService: BusRouteService,
        private spotService: SpotService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.busRouteService.query()
            .subscribe((res: ResponseWrapper) => { this.busroutes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.spotService.query()
            .subscribe((res: ResponseWrapper) => { this.spots = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dropoffReport.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dropoffReportService.update(this.dropoffReport));
        } else {
            this.subscribeToSaveResponse(
                this.dropoffReportService.create(this.dropoffReport));
        }
    }

    private subscribeToSaveResponse(result: Observable<DropoffReport>) {
        result.subscribe((res: DropoffReport) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: DropoffReport) {
        this.eventManager.broadcast({ name: 'dropoffReportListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackBusRouteById(index: number, item: BusRoute) {
        return item.id;
    }

    trackSpotById(index: number, item: Spot) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-dropoff-report-popup',
    template: ''
})
export class DropoffReportPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dropoffReportPopupService: DropoffReportPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dropoffReportPopupService
                    .open(DropoffReportDialogComponent as Component, params['id']);
            } else {
                this.dropoffReportPopupService
                    .open(DropoffReportDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
