import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DropoffReport } from './dropoff-report.model';
import { DropoffReportPopupService } from './dropoff-report-popup.service';
import { DropoffReportService } from './dropoff-report.service';

@Component({
    selector: 'jhi-dropoff-report-delete-dialog',
    templateUrl: './dropoff-report-delete-dialog.component.html'
})
export class DropoffReportDeleteDialogComponent {

    dropoffReport: DropoffReport;

    constructor(
        private dropoffReportService: DropoffReportService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dropoffReportService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dropoffReportListModification',
                content: 'Deleted an dropoffReport'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-dropoff-report-delete-popup',
    template: ''
})
export class DropoffReportDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dropoffReportPopupService: DropoffReportPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dropoffReportPopupService
                .open(DropoffReportDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
