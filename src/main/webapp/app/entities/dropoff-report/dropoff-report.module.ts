import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BusRidePlatformBackendSharedModule } from '../../shared';
import {
    DropoffReportService,
    DropoffReportPopupService,
    DropoffReportComponent,
    DropoffReportDetailComponent,
    DropoffReportDialogComponent,
    DropoffReportPopupComponent,
    DropoffReportDeletePopupComponent,
    DropoffReportDeleteDialogComponent,
    dropoffReportRoute,
    dropoffReportPopupRoute,
} from './';

const ENTITY_STATES = [
    ...dropoffReportRoute,
    ...dropoffReportPopupRoute,
];

@NgModule({
    imports: [
        BusRidePlatformBackendSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DropoffReportComponent,
        DropoffReportDetailComponent,
        DropoffReportDialogComponent,
        DropoffReportDeleteDialogComponent,
        DropoffReportPopupComponent,
        DropoffReportDeletePopupComponent,
    ],
    entryComponents: [
        DropoffReportComponent,
        DropoffReportDialogComponent,
        DropoffReportPopupComponent,
        DropoffReportDeleteDialogComponent,
        DropoffReportDeletePopupComponent,
    ],
    providers: [
        DropoffReportService,
        DropoffReportPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusRidePlatformBackendDropoffReportModule {}
