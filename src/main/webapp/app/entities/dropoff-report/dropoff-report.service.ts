import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { DropoffReport } from './dropoff-report.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DropoffReportService {

    private resourceUrl = 'api/dropoff-reports';

    constructor(private http: Http) { }

    create(dropoffReport: DropoffReport): Observable<DropoffReport> {
        const copy = this.convert(dropoffReport);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(dropoffReport: DropoffReport): Observable<DropoffReport> {
        const copy = this.convert(dropoffReport);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<DropoffReport> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(dropoffReport: DropoffReport): DropoffReport {
        const copy: DropoffReport = Object.assign({}, dropoffReport);
        return copy;
    }
}
