import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SchoolAdmin } from './school-admin.model';
import { SchoolAdminPopupService } from './school-admin-popup.service';
import { SchoolAdminService } from './school-admin.service';

@Component({
    selector: 'jhi-school-admin-delete-dialog',
    templateUrl: './school-admin-delete-dialog.component.html'
})
export class SchoolAdminDeleteDialogComponent {

    schoolAdmin: SchoolAdmin;

    constructor(
        private schoolAdminService: SchoolAdminService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.schoolAdminService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'schoolAdminListModification',
                content: 'Deleted an schoolAdmin'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-school-admin-delete-popup',
    template: ''
})
export class SchoolAdminDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private schoolAdminPopupService: SchoolAdminPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.schoolAdminPopupService
                .open(SchoolAdminDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
