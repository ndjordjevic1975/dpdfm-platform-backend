import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { SchoolAdmin } from './school-admin.model';
import { SchoolAdminService } from './school-admin.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-school-admin',
    templateUrl: './school-admin.component.html'
})
export class SchoolAdminComponent implements OnInit, OnDestroy {
schoolAdmins: SchoolAdmin[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private schoolAdminService: SchoolAdminService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.schoolAdminService.query().subscribe(
            (res: ResponseWrapper) => {
                this.schoolAdmins = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSchoolAdmins();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SchoolAdmin) {
        return item.id;
    }
    registerChangeInSchoolAdmins() {
        this.eventSubscriber = this.eventManager.subscribe('schoolAdminListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
