import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SchoolAdminComponent } from './school-admin.component';
import { SchoolAdminDetailComponent } from './school-admin-detail.component';
import { SchoolAdminPopupComponent } from './school-admin-dialog.component';
import { SchoolAdminDeletePopupComponent } from './school-admin-delete-dialog.component';

export const schoolAdminRoute: Routes = [
    {
        path: 'school-admin',
        component: SchoolAdminComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.schoolAdmin.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'school-admin/:id',
        component: SchoolAdminDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.schoolAdmin.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const schoolAdminPopupRoute: Routes = [
    {
        path: 'school-admin-new',
        component: SchoolAdminPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.schoolAdmin.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'school-admin/:id/edit',
        component: SchoolAdminPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.schoolAdmin.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'school-admin/:id/delete',
        component: SchoolAdminDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.schoolAdmin.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
