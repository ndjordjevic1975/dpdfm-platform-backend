import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SchoolAdmin } from './school-admin.model';
import { SchoolAdminPopupService } from './school-admin-popup.service';
import { SchoolAdminService } from './school-admin.service';
import { User, UserService } from '../../shared';
import { School, SchoolService } from '../school';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-school-admin-dialog',
    templateUrl: './school-admin-dialog.component.html'
})
export class SchoolAdminDialogComponent implements OnInit {

    schoolAdmin: SchoolAdmin;
    isSaving: boolean;

    users: User[];

    schools: School[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private schoolAdminService: SchoolAdminService,
        private userService: UserService,
        private schoolService: SchoolService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.schoolService.query()
            .subscribe((res: ResponseWrapper) => { this.schools = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.schoolAdmin.id !== undefined) {
            this.subscribeToSaveResponse(
                this.schoolAdminService.update(this.schoolAdmin));
        } else {
            this.subscribeToSaveResponse(
                this.schoolAdminService.create(this.schoolAdmin));
        }
    }

    private subscribeToSaveResponse(result: Observable<SchoolAdmin>) {
        result.subscribe((res: SchoolAdmin) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: SchoolAdmin) {
        this.eventManager.broadcast({ name: 'schoolAdminListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackSchoolById(index: number, item: School) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-school-admin-popup',
    template: ''
})
export class SchoolAdminPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private schoolAdminPopupService: SchoolAdminPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.schoolAdminPopupService
                    .open(SchoolAdminDialogComponent as Component, params['id']);
            } else {
                this.schoolAdminPopupService
                    .open(SchoolAdminDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
