import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BusRidePlatformBackendSharedModule } from '../../shared';
import { BusRidePlatformBackendAdminModule } from '../../admin/admin.module';
import {
    SchoolAdminService,
    SchoolAdminPopupService,
    SchoolAdminComponent,
    SchoolAdminDetailComponent,
    SchoolAdminDialogComponent,
    SchoolAdminPopupComponent,
    SchoolAdminDeletePopupComponent,
    SchoolAdminDeleteDialogComponent,
    schoolAdminRoute,
    schoolAdminPopupRoute,
} from './';

const ENTITY_STATES = [
    ...schoolAdminRoute,
    ...schoolAdminPopupRoute,
];

@NgModule({
    imports: [
        BusRidePlatformBackendSharedModule,
        BusRidePlatformBackendAdminModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        SchoolAdminComponent,
        SchoolAdminDetailComponent,
        SchoolAdminDialogComponent,
        SchoolAdminDeleteDialogComponent,
        SchoolAdminPopupComponent,
        SchoolAdminDeletePopupComponent,
    ],
    entryComponents: [
        SchoolAdminComponent,
        SchoolAdminDialogComponent,
        SchoolAdminPopupComponent,
        SchoolAdminDeleteDialogComponent,
        SchoolAdminDeletePopupComponent,
    ],
    providers: [
        SchoolAdminService,
        SchoolAdminPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusRidePlatformBackendSchoolAdminModule {}
