import { BaseEntity } from './../../shared';

export class SchoolAdmin implements BaseEntity {
    constructor(
        public id?: number,
        public userId?: number,
        public schoolId?: number,
    ) {
    }
}
