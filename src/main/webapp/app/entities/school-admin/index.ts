export * from './school-admin.model';
export * from './school-admin-popup.service';
export * from './school-admin.service';
export * from './school-admin-dialog.component';
export * from './school-admin-delete-dialog.component';
export * from './school-admin-detail.component';
export * from './school-admin.component';
export * from './school-admin.route';
