import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { SchoolAdmin } from './school-admin.model';
import { SchoolAdminService } from './school-admin.service';

@Component({
    selector: 'jhi-school-admin-detail',
    templateUrl: './school-admin-detail.component.html'
})
export class SchoolAdminDetailComponent implements OnInit, OnDestroy {

    schoolAdmin: SchoolAdmin;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private schoolAdminService: SchoolAdminService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSchoolAdmins();
    }

    load(id) {
        this.schoolAdminService.find(id).subscribe((schoolAdmin) => {
            this.schoolAdmin = schoolAdmin;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSchoolAdmins() {
        this.eventSubscriber = this.eventManager.subscribe(
            'schoolAdminListModification',
            (response) => this.load(this.schoolAdmin.id)
        );
    }
}
