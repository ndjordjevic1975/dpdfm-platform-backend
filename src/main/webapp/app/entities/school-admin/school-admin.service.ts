import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { SchoolAdmin } from './school-admin.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class SchoolAdminService {

    private resourceUrl = 'api/school-admins';

    constructor(private http: Http) { }

    create(schoolAdmin: SchoolAdmin): Observable<SchoolAdmin> {
        const copy = this.convert(schoolAdmin);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(schoolAdmin: SchoolAdmin): Observable<SchoolAdmin> {
        const copy = this.convert(schoolAdmin);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<SchoolAdmin> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(schoolAdmin: SchoolAdmin): SchoolAdmin {
        const copy: SchoolAdmin = Object.assign({}, schoolAdmin);
        return copy;
    }
}
