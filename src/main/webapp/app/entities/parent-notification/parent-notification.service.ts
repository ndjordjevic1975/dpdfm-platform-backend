import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { ParentNotification } from './parent-notification.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ParentNotificationService {

    private resourceUrl = 'api/parent-notifications';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(parentNotification: ParentNotification): Observable<ParentNotification> {
        const copy = this.convert(parentNotification);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(parentNotification: ParentNotification): Observable<ParentNotification> {
        const copy = this.convert(parentNotification);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<ParentNotification> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.timestamp = this.dateUtils
            .convertDateTimeFromServer(entity.timestamp);
    }

    private convert(parentNotification: ParentNotification): ParentNotification {
        const copy: ParentNotification = Object.assign({}, parentNotification);

        copy.timestamp = this.dateUtils.toDate(parentNotification.timestamp);
        return copy;
    }
}
