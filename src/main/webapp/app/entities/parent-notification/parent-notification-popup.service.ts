import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { ParentNotification } from './parent-notification.model';
import { ParentNotificationService } from './parent-notification.service';

@Injectable()
export class ParentNotificationPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private parentNotificationService: ParentNotificationService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.parentNotificationService.find(id).subscribe((parentNotification) => {
                    parentNotification.timestamp = this.datePipe
                        .transform(parentNotification.timestamp, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.parentNotificationModalRef(component, parentNotification);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.parentNotificationModalRef(component, new ParentNotification());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    parentNotificationModalRef(component: Component, parentNotification: ParentNotification): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.parentNotification = parentNotification;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
