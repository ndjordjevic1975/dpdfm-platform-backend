import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ParentNotificationComponent } from './parent-notification.component';
import { ParentNotificationDetailComponent } from './parent-notification-detail.component';
import { ParentNotificationPopupComponent } from './parent-notification-dialog.component';
import { ParentNotificationDeletePopupComponent } from './parent-notification-delete-dialog.component';

export const parentNotificationRoute: Routes = [
    {
        path: 'parent-notification',
        component: ParentNotificationComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.parentNotification.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'parent-notification/:id',
        component: ParentNotificationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.parentNotification.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const parentNotificationPopupRoute: Routes = [
    {
        path: 'parent-notification-new',
        component: ParentNotificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.parentNotification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'parent-notification/:id/edit',
        component: ParentNotificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.parentNotification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'parent-notification/:id/delete',
        component: ParentNotificationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.parentNotification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
