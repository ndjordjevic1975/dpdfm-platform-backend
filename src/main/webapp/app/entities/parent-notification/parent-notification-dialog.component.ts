import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ParentNotification } from './parent-notification.model';
import { ParentNotificationPopupService } from './parent-notification-popup.service';
import { ParentNotificationService } from './parent-notification.service';
import { Parent, ParentService } from '../parent';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-parent-notification-dialog',
    templateUrl: './parent-notification-dialog.component.html'
})
export class ParentNotificationDialogComponent implements OnInit {

    parentNotification: ParentNotification;
    isSaving: boolean;

    parents: Parent[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private parentNotificationService: ParentNotificationService,
        private parentService: ParentService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.parentService.query()
            .subscribe((res: ResponseWrapper) => { this.parents = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.parentNotification.id !== undefined) {
            this.subscribeToSaveResponse(
                this.parentNotificationService.update(this.parentNotification));
        } else {
            this.subscribeToSaveResponse(
                this.parentNotificationService.create(this.parentNotification));
        }
    }

    private subscribeToSaveResponse(result: Observable<ParentNotification>) {
        result.subscribe((res: ParentNotification) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ParentNotification) {
        this.eventManager.broadcast({ name: 'parentNotificationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackParentById(index: number, item: Parent) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-parent-notification-popup',
    template: ''
})
export class ParentNotificationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private parentNotificationPopupService: ParentNotificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.parentNotificationPopupService
                    .open(ParentNotificationDialogComponent as Component, params['id']);
            } else {
                this.parentNotificationPopupService
                    .open(ParentNotificationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
