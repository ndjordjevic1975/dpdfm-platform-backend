import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ParentNotification } from './parent-notification.model';
import { ParentNotificationService } from './parent-notification.service';

@Component({
    selector: 'jhi-parent-notification-detail',
    templateUrl: './parent-notification-detail.component.html'
})
export class ParentNotificationDetailComponent implements OnInit, OnDestroy {

    parentNotification: ParentNotification;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private parentNotificationService: ParentNotificationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInParentNotifications();
    }

    load(id) {
        this.parentNotificationService.find(id).subscribe((parentNotification) => {
            this.parentNotification = parentNotification;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInParentNotifications() {
        this.eventSubscriber = this.eventManager.subscribe(
            'parentNotificationListModification',
            (response) => this.load(this.parentNotification.id)
        );
    }
}
