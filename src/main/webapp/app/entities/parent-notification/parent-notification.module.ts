import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BusRidePlatformBackendSharedModule } from '../../shared';
import {
    ParentNotificationService,
    ParentNotificationPopupService,
    ParentNotificationComponent,
    ParentNotificationDetailComponent,
    ParentNotificationDialogComponent,
    ParentNotificationPopupComponent,
    ParentNotificationDeletePopupComponent,
    ParentNotificationDeleteDialogComponent,
    parentNotificationRoute,
    parentNotificationPopupRoute,
} from './';

const ENTITY_STATES = [
    ...parentNotificationRoute,
    ...parentNotificationPopupRoute,
];

@NgModule({
    imports: [
        BusRidePlatformBackendSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ParentNotificationComponent,
        ParentNotificationDetailComponent,
        ParentNotificationDialogComponent,
        ParentNotificationDeleteDialogComponent,
        ParentNotificationPopupComponent,
        ParentNotificationDeletePopupComponent,
    ],
    entryComponents: [
        ParentNotificationComponent,
        ParentNotificationDialogComponent,
        ParentNotificationPopupComponent,
        ParentNotificationDeleteDialogComponent,
        ParentNotificationDeletePopupComponent,
    ],
    providers: [
        ParentNotificationService,
        ParentNotificationPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusRidePlatformBackendParentNotificationModule {}
