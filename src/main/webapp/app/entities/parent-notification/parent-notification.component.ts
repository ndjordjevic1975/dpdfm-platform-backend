import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { ParentNotification } from './parent-notification.model';
import { ParentNotificationService } from './parent-notification.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-parent-notification',
    templateUrl: './parent-notification.component.html'
})
export class ParentNotificationComponent implements OnInit, OnDestroy {
parentNotifications: ParentNotification[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private parentNotificationService: ParentNotificationService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.parentNotificationService.query().subscribe(
            (res: ResponseWrapper) => {
                this.parentNotifications = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInParentNotifications();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ParentNotification) {
        return item.id;
    }
    registerChangeInParentNotifications() {
        this.eventSubscriber = this.eventManager.subscribe('parentNotificationListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
