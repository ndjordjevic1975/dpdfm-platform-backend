import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ParentNotification } from './parent-notification.model';
import { ParentNotificationPopupService } from './parent-notification-popup.service';
import { ParentNotificationService } from './parent-notification.service';

@Component({
    selector: 'jhi-parent-notification-delete-dialog',
    templateUrl: './parent-notification-delete-dialog.component.html'
})
export class ParentNotificationDeleteDialogComponent {

    parentNotification: ParentNotification;

    constructor(
        private parentNotificationService: ParentNotificationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.parentNotificationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'parentNotificationListModification',
                content: 'Deleted an parentNotification'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-parent-notification-delete-popup',
    template: ''
})
export class ParentNotificationDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private parentNotificationPopupService: ParentNotificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.parentNotificationPopupService
                .open(ParentNotificationDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
