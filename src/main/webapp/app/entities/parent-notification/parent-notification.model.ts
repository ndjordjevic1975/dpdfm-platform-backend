import { BaseEntity } from './../../shared';

const enum ParentNotificationType {
    'ANNOUNCEMENT',
    'POLICY_CHANGE_ANNOUNCEMENT',
    'ACCOUNT_NOTIFICATION',
    'MEETING_REQUEST',
    'EMERGENCY',
    'DRIVER'
}

export class ParentNotification implements BaseEntity {
    constructor(
        public id?: number,
        public notification?: string,
        public timestamp?: any,
        public type?: ParentNotificationType,
        public parentId?: number,
    ) {
    }
}
