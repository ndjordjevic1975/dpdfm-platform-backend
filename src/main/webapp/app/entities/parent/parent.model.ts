import { BaseEntity } from './../../shared';

const enum DeviceType {
    'ANDROID',
    'IOS'
}

export class Parent implements BaseEntity {
    constructor(
        public id?: number,
        public address?: string,
        public houseNumber?: string,
        public city?: string,
        public zipCode?: string,
        public state?: string,
        public country?: string,
        public phone?: string,
        public landPhone?: string,
        public noOfStudents?: number,
        public deviceIdentifier?: string,
        public deviceType?: DeviceType,
        public gcmSessionId?: string,
        public endpointARN?: string,
        public nationalId?: string,
        public userId?: number,
        public spotId?: number,
        public linkToPrimaryId?: number,
        public schools?: BaseEntity[],
    ) {
    }
}
