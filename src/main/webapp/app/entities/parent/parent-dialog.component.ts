import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Parent } from './parent.model';
import { ParentPopupService } from './parent-popup.service';
import { ParentService } from './parent.service';
import { User, UserService } from '../../shared';
import { Spot, SpotService } from '../spot';
import { School, SchoolService } from '../school';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-parent-dialog',
    templateUrl: './parent-dialog.component.html'
})
export class ParentDialogComponent implements OnInit {

    parent: Parent;
    isSaving: boolean;

    users: User[];

    spots: Spot[];

    parents: Parent[];

    schools: School[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private parentService: ParentService,
        private userService: UserService,
        private spotService: SpotService,
        private schoolService: SchoolService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.spotService
            .query({filter: 'parent-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.parent.spotId) {
                    this.spots = res.json;
                } else {
                    this.spotService
                        .find(this.parent.spotId)
                        .subscribe((subRes: Spot) => {
                            this.spots = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.parentService.query()
            .subscribe((res: ResponseWrapper) => { this.parents = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.schoolService.query()
            .subscribe((res: ResponseWrapper) => { this.schools = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.parent.id !== undefined) {
            this.subscribeToSaveResponse(
                this.parentService.update(this.parent));
        } else {
            this.subscribeToSaveResponse(
                this.parentService.create(this.parent));
        }
    }

    private subscribeToSaveResponse(result: Observable<Parent>) {
        result.subscribe((res: Parent) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Parent) {
        this.eventManager.broadcast({ name: 'parentListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackSpotById(index: number, item: Spot) {
        return item.id;
    }

    trackParentById(index: number, item: Parent) {
        return item.id;
    }

    trackSchoolById(index: number, item: School) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-parent-popup',
    template: ''
})
export class ParentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private parentPopupService: ParentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.parentPopupService
                    .open(ParentDialogComponent as Component, params['id']);
            } else {
                this.parentPopupService
                    .open(ParentDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
