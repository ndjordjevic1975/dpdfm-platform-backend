import { BaseEntity } from './../../shared';

const enum StudentServiceType {
    'ROUND_TRIP',
    'PICKUP',
    'DROPOFF',
    'OFF'
}

export class Student implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public imageContentType?: string,
        public image?: any,
        public typeOfService?: StudentServiceType,
        public schoolId?: number,
        public parentId?: number,
    ) {
    }
}
