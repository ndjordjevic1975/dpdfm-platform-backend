import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { BusRidePlatformBackendSchoolModule } from './school/school.module';
import { BusRidePlatformBackendParentModule } from './parent/parent.module';
import { BusRidePlatformBackendDriverModule } from './driver/driver.module';
import { BusRidePlatformBackendParentNotificationModule } from './parent-notification/parent-notification.module';
import { BusRidePlatformBackendDriverNotificationModule } from './driver-notification/driver-notification.module';
import { BusRidePlatformBackendStudentModule } from './student/student.module';
import { BusRidePlatformBackendBusCategoryModule } from './bus-category/bus-category.module';
import { BusRidePlatformBackendBusRouteModule } from './bus-route/bus-route.module';
import { BusRidePlatformBackendPickupReportModule } from './pickup-report/pickup-report.module';
import { BusRidePlatformBackendDropoffReportModule } from './dropoff-report/dropoff-report.module';
import { BusRidePlatformBackendSpotModule } from './spot/spot.module';
import { BusRidePlatformBackendWhiteLabelModule } from './white-label/white-label.module';
import { BusRidePlatformBackendTempServiceCancellationModule } from './temp-service-cancellation/temp-service-cancellation.module';
import { BusRidePlatformBackendSchoolEmailModule } from './school-email/school-email.module';
import { BusRidePlatformBackendSchoolAdminModule } from './school-admin/school-admin.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        BusRidePlatformBackendSchoolModule,
        BusRidePlatformBackendParentModule,
        BusRidePlatformBackendDriverModule,
        BusRidePlatformBackendParentNotificationModule,
        BusRidePlatformBackendDriverNotificationModule,
        BusRidePlatformBackendStudentModule,
        BusRidePlatformBackendBusCategoryModule,
        BusRidePlatformBackendBusRouteModule,
        BusRidePlatformBackendPickupReportModule,
        BusRidePlatformBackendDropoffReportModule,
        BusRidePlatformBackendSpotModule,
        BusRidePlatformBackendWhiteLabelModule,
        BusRidePlatformBackendTempServiceCancellationModule,
        BusRidePlatformBackendSchoolEmailModule,
        BusRidePlatformBackendSchoolAdminModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusRidePlatformBackendEntityModule {}
