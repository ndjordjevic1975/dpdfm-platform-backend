import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TempServiceCancellation } from './temp-service-cancellation.model';
import { TempServiceCancellationPopupService } from './temp-service-cancellation-popup.service';
import { TempServiceCancellationService } from './temp-service-cancellation.service';
import { Student, StudentService } from '../student';
import { Spot, SpotService } from '../spot';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-temp-service-cancellation-dialog',
    templateUrl: './temp-service-cancellation-dialog.component.html'
})
export class TempServiceCancellationDialogComponent implements OnInit {

    tempServiceCancellation: TempServiceCancellation;
    isSaving: boolean;

    students: Student[];

    spots: Spot[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private tempServiceCancellationService: TempServiceCancellationService,
        private studentService: StudentService,
        private spotService: SpotService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.studentService
            .query({filter: 'tempservicecancellation-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.tempServiceCancellation.studentId) {
                    this.students = res.json;
                } else {
                    this.studentService
                        .find(this.tempServiceCancellation.studentId)
                        .subscribe((subRes: Student) => {
                            this.students = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.spotService.query()
            .subscribe((res: ResponseWrapper) => { this.spots = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tempServiceCancellation.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tempServiceCancellationService.update(this.tempServiceCancellation));
        } else {
            this.subscribeToSaveResponse(
                this.tempServiceCancellationService.create(this.tempServiceCancellation));
        }
    }

    private subscribeToSaveResponse(result: Observable<TempServiceCancellation>) {
        result.subscribe((res: TempServiceCancellation) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: TempServiceCancellation) {
        this.eventManager.broadcast({ name: 'tempServiceCancellationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackStudentById(index: number, item: Student) {
        return item.id;
    }

    trackSpotById(index: number, item: Spot) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-temp-service-cancellation-popup',
    template: ''
})
export class TempServiceCancellationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tempServiceCancellationPopupService: TempServiceCancellationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tempServiceCancellationPopupService
                    .open(TempServiceCancellationDialogComponent as Component, params['id']);
            } else {
                this.tempServiceCancellationPopupService
                    .open(TempServiceCancellationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
