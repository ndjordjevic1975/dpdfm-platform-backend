import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TempServiceCancellation } from './temp-service-cancellation.model';
import { TempServiceCancellationService } from './temp-service-cancellation.service';

@Component({
    selector: 'jhi-temp-service-cancellation-detail',
    templateUrl: './temp-service-cancellation-detail.component.html'
})
export class TempServiceCancellationDetailComponent implements OnInit, OnDestroy {

    tempServiceCancellation: TempServiceCancellation;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private tempServiceCancellationService: TempServiceCancellationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTempServiceCancellations();
    }

    load(id) {
        this.tempServiceCancellationService.find(id).subscribe((tempServiceCancellation) => {
            this.tempServiceCancellation = tempServiceCancellation;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTempServiceCancellations() {
        this.eventSubscriber = this.eventManager.subscribe(
            'tempServiceCancellationListModification',
            (response) => this.load(this.tempServiceCancellation.id)
        );
    }
}
