import { BaseEntity } from './../../shared';

const enum StudentServiceType {
    'ROUND_TRIP',
    'PICKUP',
    'DROPOFF',
    'OFF'
}

export class TempServiceCancellation implements BaseEntity {
    constructor(
        public id?: number,
        public cancellationType?: StudentServiceType,
        public reason?: string,
        public studentId?: number,
        public spotId?: number,
    ) {
    }
}
