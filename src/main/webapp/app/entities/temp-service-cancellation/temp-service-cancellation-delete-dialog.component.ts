import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TempServiceCancellation } from './temp-service-cancellation.model';
import { TempServiceCancellationPopupService } from './temp-service-cancellation-popup.service';
import { TempServiceCancellationService } from './temp-service-cancellation.service';

@Component({
    selector: 'jhi-temp-service-cancellation-delete-dialog',
    templateUrl: './temp-service-cancellation-delete-dialog.component.html'
})
export class TempServiceCancellationDeleteDialogComponent {

    tempServiceCancellation: TempServiceCancellation;

    constructor(
        private tempServiceCancellationService: TempServiceCancellationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tempServiceCancellationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tempServiceCancellationListModification',
                content: 'Deleted an tempServiceCancellation'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-temp-service-cancellation-delete-popup',
    template: ''
})
export class TempServiceCancellationDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tempServiceCancellationPopupService: TempServiceCancellationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tempServiceCancellationPopupService
                .open(TempServiceCancellationDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
