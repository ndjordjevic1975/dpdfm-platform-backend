import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { TempServiceCancellation } from './temp-service-cancellation.model';
import { TempServiceCancellationService } from './temp-service-cancellation.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-temp-service-cancellation',
    templateUrl: './temp-service-cancellation.component.html'
})
export class TempServiceCancellationComponent implements OnInit, OnDestroy {
tempServiceCancellations: TempServiceCancellation[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private tempServiceCancellationService: TempServiceCancellationService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.tempServiceCancellationService.query().subscribe(
            (res: ResponseWrapper) => {
                this.tempServiceCancellations = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTempServiceCancellations();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TempServiceCancellation) {
        return item.id;
    }
    registerChangeInTempServiceCancellations() {
        this.eventSubscriber = this.eventManager.subscribe('tempServiceCancellationListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
