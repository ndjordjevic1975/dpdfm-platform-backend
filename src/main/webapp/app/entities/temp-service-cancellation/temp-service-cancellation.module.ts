import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BusRidePlatformBackendSharedModule } from '../../shared';
import {
    TempServiceCancellationService,
    TempServiceCancellationPopupService,
    TempServiceCancellationComponent,
    TempServiceCancellationDetailComponent,
    TempServiceCancellationDialogComponent,
    TempServiceCancellationPopupComponent,
    TempServiceCancellationDeletePopupComponent,
    TempServiceCancellationDeleteDialogComponent,
    tempServiceCancellationRoute,
    tempServiceCancellationPopupRoute,
} from './';

const ENTITY_STATES = [
    ...tempServiceCancellationRoute,
    ...tempServiceCancellationPopupRoute,
];

@NgModule({
    imports: [
        BusRidePlatformBackendSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TempServiceCancellationComponent,
        TempServiceCancellationDetailComponent,
        TempServiceCancellationDialogComponent,
        TempServiceCancellationDeleteDialogComponent,
        TempServiceCancellationPopupComponent,
        TempServiceCancellationDeletePopupComponent,
    ],
    entryComponents: [
        TempServiceCancellationComponent,
        TempServiceCancellationDialogComponent,
        TempServiceCancellationPopupComponent,
        TempServiceCancellationDeleteDialogComponent,
        TempServiceCancellationDeletePopupComponent,
    ],
    providers: [
        TempServiceCancellationService,
        TempServiceCancellationPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusRidePlatformBackendTempServiceCancellationModule {}
