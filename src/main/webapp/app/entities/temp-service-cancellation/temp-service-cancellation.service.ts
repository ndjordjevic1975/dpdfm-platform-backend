import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { TempServiceCancellation } from './temp-service-cancellation.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TempServiceCancellationService {

    private resourceUrl = 'api/temp-service-cancellations';

    constructor(private http: Http) { }

    create(tempServiceCancellation: TempServiceCancellation): Observable<TempServiceCancellation> {
        const copy = this.convert(tempServiceCancellation);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(tempServiceCancellation: TempServiceCancellation): Observable<TempServiceCancellation> {
        const copy = this.convert(tempServiceCancellation);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<TempServiceCancellation> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(tempServiceCancellation: TempServiceCancellation): TempServiceCancellation {
        const copy: TempServiceCancellation = Object.assign({}, tempServiceCancellation);
        return copy;
    }
}
