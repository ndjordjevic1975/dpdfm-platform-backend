import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TempServiceCancellationComponent } from './temp-service-cancellation.component';
import { TempServiceCancellationDetailComponent } from './temp-service-cancellation-detail.component';
import { TempServiceCancellationPopupComponent } from './temp-service-cancellation-dialog.component';
import { TempServiceCancellationDeletePopupComponent } from './temp-service-cancellation-delete-dialog.component';

export const tempServiceCancellationRoute: Routes = [
    {
        path: 'temp-service-cancellation',
        component: TempServiceCancellationComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.tempServiceCancellation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'temp-service-cancellation/:id',
        component: TempServiceCancellationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.tempServiceCancellation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tempServiceCancellationPopupRoute: Routes = [
    {
        path: 'temp-service-cancellation-new',
        component: TempServiceCancellationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.tempServiceCancellation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'temp-service-cancellation/:id/edit',
        component: TempServiceCancellationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.tempServiceCancellation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'temp-service-cancellation/:id/delete',
        component: TempServiceCancellationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.tempServiceCancellation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
