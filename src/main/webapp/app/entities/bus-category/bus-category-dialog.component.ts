import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { BusCategory } from './bus-category.model';
import { BusCategoryPopupService } from './bus-category-popup.service';
import { BusCategoryService } from './bus-category.service';
import { School, SchoolService } from '../school';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-bus-category-dialog',
    templateUrl: './bus-category-dialog.component.html'
})
export class BusCategoryDialogComponent implements OnInit {

    busCategory: BusCategory;
    isSaving: boolean;

    schools: School[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private busCategoryService: BusCategoryService,
        private schoolService: SchoolService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.schoolService.query()
            .subscribe((res: ResponseWrapper) => { this.schools = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.busCategory.id !== undefined) {
            this.subscribeToSaveResponse(
                this.busCategoryService.update(this.busCategory));
        } else {
            this.subscribeToSaveResponse(
                this.busCategoryService.create(this.busCategory));
        }
    }

    private subscribeToSaveResponse(result: Observable<BusCategory>) {
        result.subscribe((res: BusCategory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: BusCategory) {
        this.eventManager.broadcast({ name: 'busCategoryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackSchoolById(index: number, item: School) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-bus-category-popup',
    template: ''
})
export class BusCategoryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private busCategoryPopupService: BusCategoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.busCategoryPopupService
                    .open(BusCategoryDialogComponent as Component, params['id']);
            } else {
                this.busCategoryPopupService
                    .open(BusCategoryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
