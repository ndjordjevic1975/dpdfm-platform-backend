import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BusRidePlatformBackendSharedModule } from '../../shared';
import {
    BusCategoryService,
    BusCategoryPopupService,
    BusCategoryComponent,
    BusCategoryDetailComponent,
    BusCategoryDialogComponent,
    BusCategoryPopupComponent,
    BusCategoryDeletePopupComponent,
    BusCategoryDeleteDialogComponent,
    busCategoryRoute,
    busCategoryPopupRoute,
} from './';

const ENTITY_STATES = [
    ...busCategoryRoute,
    ...busCategoryPopupRoute,
];

@NgModule({
    imports: [
        BusRidePlatformBackendSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        BusCategoryComponent,
        BusCategoryDetailComponent,
        BusCategoryDialogComponent,
        BusCategoryDeleteDialogComponent,
        BusCategoryPopupComponent,
        BusCategoryDeletePopupComponent,
    ],
    entryComponents: [
        BusCategoryComponent,
        BusCategoryDialogComponent,
        BusCategoryPopupComponent,
        BusCategoryDeleteDialogComponent,
        BusCategoryDeletePopupComponent,
    ],
    providers: [
        BusCategoryService,
        BusCategoryPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusRidePlatformBackendBusCategoryModule {}
