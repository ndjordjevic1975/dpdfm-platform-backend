import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { BusCategory } from './bus-category.model';
import { BusCategoryService } from './bus-category.service';

@Component({
    selector: 'jhi-bus-category-detail',
    templateUrl: './bus-category-detail.component.html'
})
export class BusCategoryDetailComponent implements OnInit, OnDestroy {

    busCategory: BusCategory;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private busCategoryService: BusCategoryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBusCategories();
    }

    load(id) {
        this.busCategoryService.find(id).subscribe((busCategory) => {
            this.busCategory = busCategory;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBusCategories() {
        this.eventSubscriber = this.eventManager.subscribe(
            'busCategoryListModification',
            (response) => this.load(this.busCategory.id)
        );
    }
}
