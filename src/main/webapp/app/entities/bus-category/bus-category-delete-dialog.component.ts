import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { BusCategory } from './bus-category.model';
import { BusCategoryPopupService } from './bus-category-popup.service';
import { BusCategoryService } from './bus-category.service';

@Component({
    selector: 'jhi-bus-category-delete-dialog',
    templateUrl: './bus-category-delete-dialog.component.html'
})
export class BusCategoryDeleteDialogComponent {

    busCategory: BusCategory;

    constructor(
        private busCategoryService: BusCategoryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.busCategoryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'busCategoryListModification',
                content: 'Deleted an busCategory'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-bus-category-delete-popup',
    template: ''
})
export class BusCategoryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private busCategoryPopupService: BusCategoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.busCategoryPopupService
                .open(BusCategoryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
