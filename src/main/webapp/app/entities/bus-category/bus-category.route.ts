import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { BusCategoryComponent } from './bus-category.component';
import { BusCategoryDetailComponent } from './bus-category-detail.component';
import { BusCategoryPopupComponent } from './bus-category-dialog.component';
import { BusCategoryDeletePopupComponent } from './bus-category-delete-dialog.component';

export const busCategoryRoute: Routes = [
    {
        path: 'bus-category',
        component: BusCategoryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.busCategory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'bus-category/:id',
        component: BusCategoryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.busCategory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const busCategoryPopupRoute: Routes = [
    {
        path: 'bus-category-new',
        component: BusCategoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.busCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bus-category/:id/edit',
        component: BusCategoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.busCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bus-category/:id/delete',
        component: BusCategoryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.busCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
