import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { BusCategory } from './bus-category.model';
import { BusCategoryService } from './bus-category.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-bus-category',
    templateUrl: './bus-category.component.html'
})
export class BusCategoryComponent implements OnInit, OnDestroy {
busCategories: BusCategory[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private busCategoryService: BusCategoryService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.busCategoryService.query().subscribe(
            (res: ResponseWrapper) => {
                this.busCategories = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInBusCategories();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: BusCategory) {
        return item.id;
    }
    registerChangeInBusCategories() {
        this.eventSubscriber = this.eventManager.subscribe('busCategoryListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
