import { BaseEntity } from './../../shared';

export class BusCategory implements BaseEntity {
    constructor(
        public id?: number,
        public noOfBuses?: number,
        public capacity?: number,
        public schoolId?: number,
    ) {
    }
}
