import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PickupReport } from './pickup-report.model';
import { PickupReportService } from './pickup-report.service';

@Component({
    selector: 'jhi-pickup-report-detail',
    templateUrl: './pickup-report-detail.component.html'
})
export class PickupReportDetailComponent implements OnInit, OnDestroy {

    pickupReport: PickupReport;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private pickupReportService: PickupReportService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPickupReports();
    }

    load(id) {
        this.pickupReportService.find(id).subscribe((pickupReport) => {
            this.pickupReport = pickupReport;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPickupReports() {
        this.eventSubscriber = this.eventManager.subscribe(
            'pickupReportListModification',
            (response) => this.load(this.pickupReport.id)
        );
    }
}
