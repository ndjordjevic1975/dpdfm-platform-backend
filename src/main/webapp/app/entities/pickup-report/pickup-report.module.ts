import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BusRidePlatformBackendSharedModule } from '../../shared';
import {
    PickupReportService,
    PickupReportPopupService,
    PickupReportComponent,
    PickupReportDetailComponent,
    PickupReportDialogComponent,
    PickupReportPopupComponent,
    PickupReportDeletePopupComponent,
    PickupReportDeleteDialogComponent,
    pickupReportRoute,
    pickupReportPopupRoute,
} from './';

const ENTITY_STATES = [
    ...pickupReportRoute,
    ...pickupReportPopupRoute,
];

@NgModule({
    imports: [
        BusRidePlatformBackendSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PickupReportComponent,
        PickupReportDetailComponent,
        PickupReportDialogComponent,
        PickupReportDeleteDialogComponent,
        PickupReportPopupComponent,
        PickupReportDeletePopupComponent,
    ],
    entryComponents: [
        PickupReportComponent,
        PickupReportDialogComponent,
        PickupReportPopupComponent,
        PickupReportDeleteDialogComponent,
        PickupReportDeletePopupComponent,
    ],
    providers: [
        PickupReportService,
        PickupReportPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusRidePlatformBackendPickupReportModule {}
