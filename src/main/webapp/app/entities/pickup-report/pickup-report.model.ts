import { BaseEntity } from './../../shared';

const enum PickupDropoffType {
    'SUCCESSFUL',
    'INCOMPLETE',
    'UNSUCCESSFUL'
}

export class PickupReport implements BaseEntity {
    constructor(
        public id?: number,
        public status?: PickupDropoffType,
        public delay?: number,
        public planned?: number,
        public accomplished?: number,
        public busRouteId?: number,
        public spotId?: number,
    ) {
    }
}
