import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PickupReport } from './pickup-report.model';
import { PickupReportPopupService } from './pickup-report-popup.service';
import { PickupReportService } from './pickup-report.service';

@Component({
    selector: 'jhi-pickup-report-delete-dialog',
    templateUrl: './pickup-report-delete-dialog.component.html'
})
export class PickupReportDeleteDialogComponent {

    pickupReport: PickupReport;

    constructor(
        private pickupReportService: PickupReportService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.pickupReportService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'pickupReportListModification',
                content: 'Deleted an pickupReport'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-pickup-report-delete-popup',
    template: ''
})
export class PickupReportDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pickupReportPopupService: PickupReportPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.pickupReportPopupService
                .open(PickupReportDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
