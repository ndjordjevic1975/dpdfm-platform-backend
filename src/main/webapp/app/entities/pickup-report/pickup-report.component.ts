import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { PickupReport } from './pickup-report.model';
import { PickupReportService } from './pickup-report.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-pickup-report',
    templateUrl: './pickup-report.component.html'
})
export class PickupReportComponent implements OnInit, OnDestroy {
pickupReports: PickupReport[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private pickupReportService: PickupReportService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.pickupReportService.query().subscribe(
            (res: ResponseWrapper) => {
                this.pickupReports = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPickupReports();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PickupReport) {
        return item.id;
    }
    registerChangeInPickupReports() {
        this.eventSubscriber = this.eventManager.subscribe('pickupReportListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
