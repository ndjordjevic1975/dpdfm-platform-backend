import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PickupReportComponent } from './pickup-report.component';
import { PickupReportDetailComponent } from './pickup-report-detail.component';
import { PickupReportPopupComponent } from './pickup-report-dialog.component';
import { PickupReportDeletePopupComponent } from './pickup-report-delete-dialog.component';

export const pickupReportRoute: Routes = [
    {
        path: 'pickup-report',
        component: PickupReportComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.pickupReport.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'pickup-report/:id',
        component: PickupReportDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.pickupReport.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pickupReportPopupRoute: Routes = [
    {
        path: 'pickup-report-new',
        component: PickupReportPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.pickupReport.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'pickup-report/:id/edit',
        component: PickupReportPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.pickupReport.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'pickup-report/:id/delete',
        component: PickupReportDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.pickupReport.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
