import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PickupReport } from './pickup-report.model';
import { PickupReportPopupService } from './pickup-report-popup.service';
import { PickupReportService } from './pickup-report.service';
import { BusRoute, BusRouteService } from '../bus-route';
import { Spot, SpotService } from '../spot';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-pickup-report-dialog',
    templateUrl: './pickup-report-dialog.component.html'
})
export class PickupReportDialogComponent implements OnInit {

    pickupReport: PickupReport;
    isSaving: boolean;

    busroutes: BusRoute[];

    spots: Spot[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private pickupReportService: PickupReportService,
        private busRouteService: BusRouteService,
        private spotService: SpotService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.busRouteService.query()
            .subscribe((res: ResponseWrapper) => { this.busroutes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.spotService.query()
            .subscribe((res: ResponseWrapper) => { this.spots = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.pickupReport.id !== undefined) {
            this.subscribeToSaveResponse(
                this.pickupReportService.update(this.pickupReport));
        } else {
            this.subscribeToSaveResponse(
                this.pickupReportService.create(this.pickupReport));
        }
    }

    private subscribeToSaveResponse(result: Observable<PickupReport>) {
        result.subscribe((res: PickupReport) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: PickupReport) {
        this.eventManager.broadcast({ name: 'pickupReportListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackBusRouteById(index: number, item: BusRoute) {
        return item.id;
    }

    trackSpotById(index: number, item: Spot) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-pickup-report-popup',
    template: ''
})
export class PickupReportPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pickupReportPopupService: PickupReportPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.pickupReportPopupService
                    .open(PickupReportDialogComponent as Component, params['id']);
            } else {
                this.pickupReportPopupService
                    .open(PickupReportDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
