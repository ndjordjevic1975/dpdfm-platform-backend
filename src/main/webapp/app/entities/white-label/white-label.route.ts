import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { WhiteLabelComponent } from './white-label.component';
import { WhiteLabelDetailComponent } from './white-label-detail.component';
import { WhiteLabelPopupComponent } from './white-label-dialog.component';
import { WhiteLabelDeletePopupComponent } from './white-label-delete-dialog.component';

export const whiteLabelRoute: Routes = [
    {
        path: 'white-label',
        component: WhiteLabelComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.whiteLabel.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'white-label/:id',
        component: WhiteLabelDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.whiteLabel.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const whiteLabelPopupRoute: Routes = [
    {
        path: 'white-label-new',
        component: WhiteLabelPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.whiteLabel.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'white-label/:id/edit',
        component: WhiteLabelPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.whiteLabel.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'white-label/:id/delete',
        component: WhiteLabelDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.whiteLabel.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
