import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BusRidePlatformBackendSharedModule } from '../../shared';
import {
    WhiteLabelService,
    WhiteLabelPopupService,
    WhiteLabelComponent,
    WhiteLabelDetailComponent,
    WhiteLabelDialogComponent,
    WhiteLabelPopupComponent,
    WhiteLabelDeletePopupComponent,
    WhiteLabelDeleteDialogComponent,
    whiteLabelRoute,
    whiteLabelPopupRoute,
} from './';

const ENTITY_STATES = [
    ...whiteLabelRoute,
    ...whiteLabelPopupRoute,
];

@NgModule({
    imports: [
        BusRidePlatformBackendSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        WhiteLabelComponent,
        WhiteLabelDetailComponent,
        WhiteLabelDialogComponent,
        WhiteLabelDeleteDialogComponent,
        WhiteLabelPopupComponent,
        WhiteLabelDeletePopupComponent,
    ],
    entryComponents: [
        WhiteLabelComponent,
        WhiteLabelDialogComponent,
        WhiteLabelPopupComponent,
        WhiteLabelDeleteDialogComponent,
        WhiteLabelDeletePopupComponent,
    ],
    providers: [
        WhiteLabelService,
        WhiteLabelPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusRidePlatformBackendWhiteLabelModule {}
