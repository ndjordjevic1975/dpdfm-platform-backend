import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { WhiteLabel } from './white-label.model';
import { WhiteLabelService } from './white-label.service';

@Component({
    selector: 'jhi-white-label-detail',
    templateUrl: './white-label-detail.component.html'
})
export class WhiteLabelDetailComponent implements OnInit, OnDestroy {

    whiteLabel: WhiteLabel;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private whiteLabelService: WhiteLabelService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInWhiteLabels();
    }

    load(id) {
        this.whiteLabelService.find(id).subscribe((whiteLabel) => {
            this.whiteLabel = whiteLabel;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInWhiteLabels() {
        this.eventSubscriber = this.eventManager.subscribe(
            'whiteLabelListModification',
            (response) => this.load(this.whiteLabel.id)
        );
    }
}
