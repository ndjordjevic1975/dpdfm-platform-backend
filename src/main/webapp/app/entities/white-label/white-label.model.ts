import { BaseEntity } from './../../shared';

export class WhiteLabel implements BaseEntity {
    constructor(
        public id?: number,
        public parentWhiteLabelContentType?: string,
        public parentWhiteLabel?: any,
        public driverWhiteLabelContentType?: string,
        public driverWhiteLabel?: any,
        public parentWhiteLabelVersion?: number,
        public driverWhiteLabelVersion?: number,
        public splashScreenContentType?: string,
        public splashScreen?: any,
        public icnBusContentType?: string,
        public icnBus?: any,
        public logoBgContentType?: string,
        public logoBg?: any,
        public logoTxtContentType?: string,
        public logoTxt?: any,
        public schoolId?: number,
    ) {
    }
}
