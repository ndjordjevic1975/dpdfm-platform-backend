import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { WhiteLabel } from './white-label.model';
import { WhiteLabelPopupService } from './white-label-popup.service';
import { WhiteLabelService } from './white-label.service';
import { School, SchoolService } from '../school';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-white-label-dialog',
    templateUrl: './white-label-dialog.component.html'
})
export class WhiteLabelDialogComponent implements OnInit {

    whiteLabel: WhiteLabel;
    isSaving: boolean;

    schools: School[];

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private whiteLabelService: WhiteLabelService,
        private schoolService: SchoolService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.schoolService
            .query({filter: 'whitelabel-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.whiteLabel.schoolId) {
                    this.schools = res.json;
                } else {
                    this.schoolService
                        .find(this.whiteLabel.schoolId)
                        .subscribe((subRes: School) => {
                            this.schools = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.whiteLabel.id !== undefined) {
            this.subscribeToSaveResponse(
                this.whiteLabelService.update(this.whiteLabel));
        } else {
            this.subscribeToSaveResponse(
                this.whiteLabelService.create(this.whiteLabel));
        }
    }

    private subscribeToSaveResponse(result: Observable<WhiteLabel>) {
        result.subscribe((res: WhiteLabel) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: WhiteLabel) {
        this.eventManager.broadcast({ name: 'whiteLabelListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackSchoolById(index: number, item: School) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-white-label-popup',
    template: ''
})
export class WhiteLabelPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private whiteLabelPopupService: WhiteLabelPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.whiteLabelPopupService
                    .open(WhiteLabelDialogComponent as Component, params['id']);
            } else {
                this.whiteLabelPopupService
                    .open(WhiteLabelDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
