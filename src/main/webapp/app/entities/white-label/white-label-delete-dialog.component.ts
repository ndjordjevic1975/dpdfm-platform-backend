import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { WhiteLabel } from './white-label.model';
import { WhiteLabelPopupService } from './white-label-popup.service';
import { WhiteLabelService } from './white-label.service';

@Component({
    selector: 'jhi-white-label-delete-dialog',
    templateUrl: './white-label-delete-dialog.component.html'
})
export class WhiteLabelDeleteDialogComponent {

    whiteLabel: WhiteLabel;

    constructor(
        private whiteLabelService: WhiteLabelService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.whiteLabelService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'whiteLabelListModification',
                content: 'Deleted an whiteLabel'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-white-label-delete-popup',
    template: ''
})
export class WhiteLabelDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private whiteLabelPopupService: WhiteLabelPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.whiteLabelPopupService
                .open(WhiteLabelDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
