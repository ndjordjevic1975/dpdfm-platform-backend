import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { BusRoute } from './bus-route.model';
import { BusRouteService } from './bus-route.service';

@Injectable()
export class BusRoutePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private busRouteService: BusRouteService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.busRouteService.find(id).subscribe((busRoute) => {
                    busRoute.createdOn = this.datePipe
                        .transform(busRoute.createdOn, 'yyyy-MM-ddTHH:mm:ss');
                    busRoute.inactiveOn = this.datePipe
                        .transform(busRoute.inactiveOn, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.busRouteModalRef(component, busRoute);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.busRouteModalRef(component, new BusRoute());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    busRouteModalRef(component: Component, busRoute: BusRoute): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.busRoute = busRoute;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
