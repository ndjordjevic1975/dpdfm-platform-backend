import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { BusRoute } from './bus-route.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class BusRouteService {

    private resourceUrl = 'api/bus-routes';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(busRoute: BusRoute): Observable<BusRoute> {
        const copy = this.convert(busRoute);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(busRoute: BusRoute): Observable<BusRoute> {
        const copy = this.convert(busRoute);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<BusRoute> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.createdOn = this.dateUtils
            .convertDateTimeFromServer(entity.createdOn);
        entity.inactiveOn = this.dateUtils
            .convertDateTimeFromServer(entity.inactiveOn);
    }

    private convert(busRoute: BusRoute): BusRoute {
        const copy: BusRoute = Object.assign({}, busRoute);

        copy.createdOn = this.dateUtils.toDate(busRoute.createdOn);

        copy.inactiveOn = this.dateUtils.toDate(busRoute.inactiveOn);
        return copy;
    }
}
