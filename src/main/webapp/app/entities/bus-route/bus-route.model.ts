import { BaseEntity } from './../../shared';

const enum BusRouteType {
    'PICKUP',
    'DROPOFF'
}

export class BusRoute implements BaseEntity {
    constructor(
        public id?: number,
        public details?: any,
        public type?: BusRouteType,
        public capacity?: number,
        public createdOn?: any,
        public inactiveOn?: any,
        public schoolId?: number,
        public driverId?: number,
        public spots?: BaseEntity[],
    ) {
    }
}
