import { BaseEntity } from './../../shared';

export class School implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public address?: string,
        public houseNumber?: string,
        public phone?: string,
        public city?: string,
        public zipcode?: string,
        public state?: string,
        public country?: string,
        public logoContentType?: string,
        public logo?: any,
        public spotId?: number,
        public parents?: BaseEntity[],
    ) {
    }
}
