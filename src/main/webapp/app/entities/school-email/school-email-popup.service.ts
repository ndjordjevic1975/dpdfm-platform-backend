import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { SchoolEmail } from './school-email.model';
import { SchoolEmailService } from './school-email.service';

@Injectable()
export class SchoolEmailPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private schoolEmailService: SchoolEmailService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.schoolEmailService.find(id).subscribe((schoolEmail) => {
                    schoolEmail.timestamp = this.datePipe
                        .transform(schoolEmail.timestamp, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.schoolEmailModalRef(component, schoolEmail);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.schoolEmailModalRef(component, new SchoolEmail());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    schoolEmailModalRef(component: Component, schoolEmail: SchoolEmail): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.schoolEmail = schoolEmail;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
