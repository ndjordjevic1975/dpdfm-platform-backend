import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SchoolEmail } from './school-email.model';
import { SchoolEmailPopupService } from './school-email-popup.service';
import { SchoolEmailService } from './school-email.service';

@Component({
    selector: 'jhi-school-email-delete-dialog',
    templateUrl: './school-email-delete-dialog.component.html'
})
export class SchoolEmailDeleteDialogComponent {

    schoolEmail: SchoolEmail;

    constructor(
        private schoolEmailService: SchoolEmailService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.schoolEmailService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'schoolEmailListModification',
                content: 'Deleted an schoolEmail'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-school-email-delete-popup',
    template: ''
})
export class SchoolEmailDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private schoolEmailPopupService: SchoolEmailPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.schoolEmailPopupService
                .open(SchoolEmailDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
