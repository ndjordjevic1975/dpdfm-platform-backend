import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BusRidePlatformBackendSharedModule } from '../../shared';
import {
    SchoolEmailService,
    SchoolEmailPopupService,
    SchoolEmailComponent,
    SchoolEmailDetailComponent,
    SchoolEmailDialogComponent,
    SchoolEmailPopupComponent,
    SchoolEmailDeletePopupComponent,
    SchoolEmailDeleteDialogComponent,
    schoolEmailRoute,
    schoolEmailPopupRoute,
} from './';

const ENTITY_STATES = [
    ...schoolEmailRoute,
    ...schoolEmailPopupRoute,
];

@NgModule({
    imports: [
        BusRidePlatformBackendSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        SchoolEmailComponent,
        SchoolEmailDetailComponent,
        SchoolEmailDialogComponent,
        SchoolEmailDeleteDialogComponent,
        SchoolEmailPopupComponent,
        SchoolEmailDeletePopupComponent,
    ],
    entryComponents: [
        SchoolEmailComponent,
        SchoolEmailDialogComponent,
        SchoolEmailPopupComponent,
        SchoolEmailDeleteDialogComponent,
        SchoolEmailDeletePopupComponent,
    ],
    providers: [
        SchoolEmailService,
        SchoolEmailPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusRidePlatformBackendSchoolEmailModule {}
