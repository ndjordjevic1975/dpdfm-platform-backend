import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SchoolEmailComponent } from './school-email.component';
import { SchoolEmailDetailComponent } from './school-email-detail.component';
import { SchoolEmailPopupComponent } from './school-email-dialog.component';
import { SchoolEmailDeletePopupComponent } from './school-email-delete-dialog.component';

export const schoolEmailRoute: Routes = [
    {
        path: 'school-email',
        component: SchoolEmailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.schoolEmail.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'school-email/:id',
        component: SchoolEmailDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.schoolEmail.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const schoolEmailPopupRoute: Routes = [
    {
        path: 'school-email-new',
        component: SchoolEmailPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.schoolEmail.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'school-email/:id/edit',
        component: SchoolEmailPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.schoolEmail.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'school-email/:id/delete',
        component: SchoolEmailDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.schoolEmail.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
