export * from './school-email.model';
export * from './school-email-popup.service';
export * from './school-email.service';
export * from './school-email-dialog.component';
export * from './school-email-delete-dialog.component';
export * from './school-email-detail.component';
export * from './school-email.component';
export * from './school-email.route';
