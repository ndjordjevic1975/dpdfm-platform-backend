import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { SchoolEmail } from './school-email.model';
import { SchoolEmailService } from './school-email.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-school-email',
    templateUrl: './school-email.component.html'
})
export class SchoolEmailComponent implements OnInit, OnDestroy {
schoolEmails: SchoolEmail[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private schoolEmailService: SchoolEmailService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.schoolEmailService.query().subscribe(
            (res: ResponseWrapper) => {
                this.schoolEmails = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSchoolEmails();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SchoolEmail) {
        return item.id;
    }
    registerChangeInSchoolEmails() {
        this.eventSubscriber = this.eventManager.subscribe('schoolEmailListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
