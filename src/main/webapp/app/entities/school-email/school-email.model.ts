import { BaseEntity } from './../../shared';

export class SchoolEmail implements BaseEntity {
    constructor(
        public id?: number,
        public subject?: string,
        public content?: string,
        public timestamp?: any,
        public schoolId?: number,
    ) {
    }
}
