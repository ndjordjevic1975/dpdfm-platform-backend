import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SchoolEmail } from './school-email.model';
import { SchoolEmailPopupService } from './school-email-popup.service';
import { SchoolEmailService } from './school-email.service';
import { School, SchoolService } from '../school';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-school-email-dialog',
    templateUrl: './school-email-dialog.component.html'
})
export class SchoolEmailDialogComponent implements OnInit {

    schoolEmail: SchoolEmail;
    isSaving: boolean;

    schools: School[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private schoolEmailService: SchoolEmailService,
        private schoolService: SchoolService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.schoolService.query()
            .subscribe((res: ResponseWrapper) => { this.schools = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.schoolEmail.id !== undefined) {
            this.subscribeToSaveResponse(
                this.schoolEmailService.update(this.schoolEmail));
        } else {
            this.subscribeToSaveResponse(
                this.schoolEmailService.create(this.schoolEmail));
        }
    }

    private subscribeToSaveResponse(result: Observable<SchoolEmail>) {
        result.subscribe((res: SchoolEmail) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: SchoolEmail) {
        this.eventManager.broadcast({ name: 'schoolEmailListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackSchoolById(index: number, item: School) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-school-email-popup',
    template: ''
})
export class SchoolEmailPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private schoolEmailPopupService: SchoolEmailPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.schoolEmailPopupService
                    .open(SchoolEmailDialogComponent as Component, params['id']);
            } else {
                this.schoolEmailPopupService
                    .open(SchoolEmailDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
