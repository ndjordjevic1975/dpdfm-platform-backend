import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { SchoolEmail } from './school-email.model';
import { SchoolEmailService } from './school-email.service';

@Component({
    selector: 'jhi-school-email-detail',
    templateUrl: './school-email-detail.component.html'
})
export class SchoolEmailDetailComponent implements OnInit, OnDestroy {

    schoolEmail: SchoolEmail;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private schoolEmailService: SchoolEmailService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSchoolEmails();
    }

    load(id) {
        this.schoolEmailService.find(id).subscribe((schoolEmail) => {
            this.schoolEmail = schoolEmail;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSchoolEmails() {
        this.eventSubscriber = this.eventManager.subscribe(
            'schoolEmailListModification',
            (response) => this.load(this.schoolEmail.id)
        );
    }
}
