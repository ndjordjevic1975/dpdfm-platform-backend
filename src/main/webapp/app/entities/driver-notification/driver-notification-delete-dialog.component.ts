import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DriverNotification } from './driver-notification.model';
import { DriverNotificationPopupService } from './driver-notification-popup.service';
import { DriverNotificationService } from './driver-notification.service';

@Component({
    selector: 'jhi-driver-notification-delete-dialog',
    templateUrl: './driver-notification-delete-dialog.component.html'
})
export class DriverNotificationDeleteDialogComponent {

    driverNotification: DriverNotification;

    constructor(
        private driverNotificationService: DriverNotificationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.driverNotificationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'driverNotificationListModification',
                content: 'Deleted an driverNotification'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-driver-notification-delete-popup',
    template: ''
})
export class DriverNotificationDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private driverNotificationPopupService: DriverNotificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.driverNotificationPopupService
                .open(DriverNotificationDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
