import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { DriverNotification } from './driver-notification.model';
import { DriverNotificationService } from './driver-notification.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-driver-notification',
    templateUrl: './driver-notification.component.html'
})
export class DriverNotificationComponent implements OnInit, OnDestroy {
driverNotifications: DriverNotification[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private driverNotificationService: DriverNotificationService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.driverNotificationService.query().subscribe(
            (res: ResponseWrapper) => {
                this.driverNotifications = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInDriverNotifications();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: DriverNotification) {
        return item.id;
    }
    registerChangeInDriverNotifications() {
        this.eventSubscriber = this.eventManager.subscribe('driverNotificationListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
