import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { DriverNotification } from './driver-notification.model';
import { DriverNotificationService } from './driver-notification.service';

@Component({
    selector: 'jhi-driver-notification-detail',
    templateUrl: './driver-notification-detail.component.html'
})
export class DriverNotificationDetailComponent implements OnInit, OnDestroy {

    driverNotification: DriverNotification;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private driverNotificationService: DriverNotificationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDriverNotifications();
    }

    load(id) {
        this.driverNotificationService.find(id).subscribe((driverNotification) => {
            this.driverNotification = driverNotification;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDriverNotifications() {
        this.eventSubscriber = this.eventManager.subscribe(
            'driverNotificationListModification',
            (response) => this.load(this.driverNotification.id)
        );
    }
}
