import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DriverNotification } from './driver-notification.model';
import { DriverNotificationPopupService } from './driver-notification-popup.service';
import { DriverNotificationService } from './driver-notification.service';
import { Driver, DriverService } from '../driver';
import { Spot, SpotService } from '../spot';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-driver-notification-dialog',
    templateUrl: './driver-notification-dialog.component.html'
})
export class DriverNotificationDialogComponent implements OnInit {

    driverNotification: DriverNotification;
    isSaving: boolean;

    drivers: Driver[];

    spots: Spot[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private driverNotificationService: DriverNotificationService,
        private driverService: DriverService,
        private spotService: SpotService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.driverService.query()
            .subscribe((res: ResponseWrapper) => { this.drivers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.spotService.query()
            .subscribe((res: ResponseWrapper) => { this.spots = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.driverNotification.id !== undefined) {
            this.subscribeToSaveResponse(
                this.driverNotificationService.update(this.driverNotification));
        } else {
            this.subscribeToSaveResponse(
                this.driverNotificationService.create(this.driverNotification));
        }
    }

    private subscribeToSaveResponse(result: Observable<DriverNotification>) {
        result.subscribe((res: DriverNotification) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: DriverNotification) {
        this.eventManager.broadcast({ name: 'driverNotificationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackDriverById(index: number, item: Driver) {
        return item.id;
    }

    trackSpotById(index: number, item: Spot) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-driver-notification-popup',
    template: ''
})
export class DriverNotificationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private driverNotificationPopupService: DriverNotificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.driverNotificationPopupService
                    .open(DriverNotificationDialogComponent as Component, params['id']);
            } else {
                this.driverNotificationPopupService
                    .open(DriverNotificationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
