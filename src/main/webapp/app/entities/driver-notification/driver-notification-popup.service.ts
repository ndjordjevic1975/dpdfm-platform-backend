import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DriverNotification } from './driver-notification.model';
import { DriverNotificationService } from './driver-notification.service';

@Injectable()
export class DriverNotificationPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private driverNotificationService: DriverNotificationService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.driverNotificationService.find(id).subscribe((driverNotification) => {
                    driverNotification.createdOnDevice = this.datePipe
                        .transform(driverNotification.createdOnDevice, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.driverNotificationModalRef(component, driverNotification);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.driverNotificationModalRef(component, new DriverNotification());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    driverNotificationModalRef(component: Component, driverNotification: DriverNotification): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.driverNotification = driverNotification;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
