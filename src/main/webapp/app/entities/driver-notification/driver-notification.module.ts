import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BusRidePlatformBackendSharedModule } from '../../shared';
import {
    DriverNotificationService,
    DriverNotificationPopupService,
    DriverNotificationComponent,
    DriverNotificationDetailComponent,
    DriverNotificationDialogComponent,
    DriverNotificationPopupComponent,
    DriverNotificationDeletePopupComponent,
    DriverNotificationDeleteDialogComponent,
    driverNotificationRoute,
    driverNotificationPopupRoute,
} from './';

const ENTITY_STATES = [
    ...driverNotificationRoute,
    ...driverNotificationPopupRoute,
];

@NgModule({
    imports: [
        BusRidePlatformBackendSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DriverNotificationComponent,
        DriverNotificationDetailComponent,
        DriverNotificationDialogComponent,
        DriverNotificationDeleteDialogComponent,
        DriverNotificationPopupComponent,
        DriverNotificationDeletePopupComponent,
    ],
    entryComponents: [
        DriverNotificationComponent,
        DriverNotificationDialogComponent,
        DriverNotificationPopupComponent,
        DriverNotificationDeleteDialogComponent,
        DriverNotificationDeletePopupComponent,
    ],
    providers: [
        DriverNotificationService,
        DriverNotificationPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BusRidePlatformBackendDriverNotificationModule {}
