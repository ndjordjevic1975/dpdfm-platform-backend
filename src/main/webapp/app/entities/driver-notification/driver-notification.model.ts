import { BaseEntity } from './../../shared';

const enum DriverNotificationType {
    'PROXIMITY',
    'ARRIVED',
    'SCHOOL_START',
    'SCHOOL_END'
}

export class DriverNotification implements BaseEntity {
    constructor(
        public id?: number,
        public notification?: string,
        public createdOnDevice?: any,
        public type?: DriverNotificationType,
        public driverId?: number,
        public spotId?: number,
    ) {
    }
}
