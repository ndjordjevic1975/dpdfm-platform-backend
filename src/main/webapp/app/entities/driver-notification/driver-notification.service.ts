import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { DriverNotification } from './driver-notification.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DriverNotificationService {

    private resourceUrl = 'api/driver-notifications';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(driverNotification: DriverNotification): Observable<DriverNotification> {
        const copy = this.convert(driverNotification);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(driverNotification: DriverNotification): Observable<DriverNotification> {
        const copy = this.convert(driverNotification);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<DriverNotification> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.createdOnDevice = this.dateUtils
            .convertDateTimeFromServer(entity.createdOnDevice);
    }

    private convert(driverNotification: DriverNotification): DriverNotification {
        const copy: DriverNotification = Object.assign({}, driverNotification);

        copy.createdOnDevice = this.dateUtils.toDate(driverNotification.createdOnDevice);
        return copy;
    }
}
