import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DriverNotificationComponent } from './driver-notification.component';
import { DriverNotificationDetailComponent } from './driver-notification-detail.component';
import { DriverNotificationPopupComponent } from './driver-notification-dialog.component';
import { DriverNotificationDeletePopupComponent } from './driver-notification-delete-dialog.component';

export const driverNotificationRoute: Routes = [
    {
        path: 'driver-notification',
        component: DriverNotificationComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.driverNotification.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'driver-notification/:id',
        component: DriverNotificationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.driverNotification.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const driverNotificationPopupRoute: Routes = [
    {
        path: 'driver-notification-new',
        component: DriverNotificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.driverNotification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'driver-notification/:id/edit',
        component: DriverNotificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.driverNotification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'driver-notification/:id/delete',
        component: DriverNotificationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'busRidePlatformBackendApp.driverNotification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
